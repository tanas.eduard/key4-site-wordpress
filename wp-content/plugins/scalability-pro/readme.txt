=== Scalability Pro ===

== Changelog ==
= 4.62 (30th September 2020) =
* Added new WPI Settings page where users can enter their license key to enable plugin updates

= 4.61 (22nd June 2020) =
* Added multisite compatibility

= 4.60 Beta (1st May 2020) =
* Prevent query alterations for cron and ajax where the order of results may actually matter for functionality
* Improved removal of GROUP BY when group by not needed - gives speed boost in some cases

= 4.59 Beta (16th April 2020) =
* Changed mechanism for the 'WHERE NOT EXISTS' option to make it work with all taxonomies and multiple taxonomies (previously was disabled and when enabled it only handled 1 taxonomy) - this optimisation helps in many cases but not all - you'll need to test it to check with your data config

= 4.58 (31st March 2020) =
* Added new index on wp_posts on just the guid column. Some themes grab posts directly using SQL against wp_posts and just use the guid. Typically this is when they're grabbing images. They shouldn't do it that way but some do.

= 4.57 (27th August 2019) =
* Fixed bug related to 'removing woocommerce ajax variations count' which improves performance of pages with many variations
* - bug was related to items which didn't have all variations created. Fix was to return at least 1 of the variations which seems to fix the Woo JS code so that it will correctly use Ajax to fetch prices.

= 4.56 (16th May 2019) =
* Updated code to prevent 'sort order suppression' and 'pagination suppression' from running on admin pages

= 4.55 (16th May 2019) =
* Re-added experimental EXISTS option for those users who have already set it and are successfully using it. Full fix coming soon to re-add it to options page.

= 4.54 (8th May 2019) =
* Further hotfix for sitemap index

= 4.53 (8th May 2019) =
* Fixed sitemap index in cases where somehow the keys were > 1000 characters

= 4.52 (8th May 2019) =
* Removed the experimental EXISTS feature as it was causing errors in many cases

= 4.51 (6th May 2019) =
* Altered the plugin update checker to use the latest version and avoid warning message.

= 4.50 (12th April 2019) =
* WHERE EXISTS experimental option now fixed - if all 3 wp_query options set, dogs category on foundthru is 0.0009s

= 4.49 (1st April 2019) =
* Swapped out use of dbDelta to use generic CREATE statement instead since it seems there is a bug inside WordPress core - https://wordpress.stackexchange.com/questions/141971/why-does-dbdelta-not-catch-mysqlerrors/141984
* Removed the .js file from front-end as it's not yet needed

= 4.48 (28th March 2019) =
* Bumped version number to help customer

= 4.47 (28th March 2019) =
* Removed row-level caching from WooCommerce->Orders (needs bugs fixed)
* Added caching table for better performance than using transients

= 4.46 (4th March 2019) =
* Added caching table for use by wp-admin views
* Added row-level caching to WooCommerce -> Orders
* Added row-level caching to wp-admin -> Products
* Removed warnings & notices

= 4.45 (11th February 2019) =
* V minor speed boost when checking for queries related to group BY (mostly around sitemaps)

= 4.44 (4th February 2019) =
* Added new index for XML sitemaps - optimises BWP Google XML Sitemaps and Yoast SEO XML sitemap functionality
* Added new filter to optimise paged query for BWP Google XML Sitemaps
* Added new filter to optimise main query for Yoast SEO XML sitemap functionality

= 4.43 (10th January 2019) =
* Removed warning caused by 'beta' check for updates for those who were not on beta program

= 4.42 (2nd December 2018) =
* Fixed issue with spotting indexes that were created using MySQL 8

= 4.41 (17th September 2018) =
* Fixed product's disappearing on some themes when 'no pagination' option set

= 4.37 (11th January 2018) =
* Improved product listing page speed

= 4.36 (11th January 2018) =
* Fixed 2 errors when object is not defined - was preventing correct operation of 'duplicate product' link (for example)

= 4.35 =
* Improved experimental left join
* Improved admin pages and documentation

= 4.34 =
* Fixed broken 'sort order' option
* Made experimental LEFT JOIN optimisation (to WHERE EXISTS) far more stable