<?php
/**
 * Plugin Name: Scalability Pro
 * Plugin URI: http://www.wpintense.com
 * Description: Fixes underlying database query performance for multiple plugins including WooCommerce, Custom Post Types, wp-admin, Datafeedr, WP All Import and much more.
 * Version: 4.62
 * Author: Dave Hilditch
 * Author URI: http://www.wpintense.com	
 * License: GPL v3
  Copyright (C) 2016, Matiogi Ltd (WP Intense)- support@wpintense.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
define('SP_PLUGIN_NAME', 'Scalability Pro');


define( 'SPROPLUGIN_DIR', dirname(__FILE__) );  

require 'plugin-updates/plugin-update-checker.php';
$SPROWidgetsUpdateChecker = PucFactory::buildUpdateChecker(
                'https://www.wpintense.com/assets/plugins/scalability-pro.json', __FILE__, 'scalability-pro'
);
require_once SPROPLUGIN_DIR . '/class-tgm-plugin-activation.php';
require_once(plugin_dir_path(__FILE__) . 'wpintense-settings.php');

require_once 'class-wp-list-table-cached.php';

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */

function awdff_dectivateplugin() {
//    wpi_dropindexes();
//   wpi_dropfulltextindex();
}

//register_activation_hook( __FILE__, 'awdff_activateplugin' );
//register_deactivation_hook(__FILE__, 'awdff_dectivateplugin');

function wpi_createindexes() {
    global $wpdb;
    $indexname = 'wpi_performance_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "postmeta(post_id, meta_key(50), meta_value(15));");
        }
    } catch (Exception $e) {
    }
    $indexname = 'wpi_postmeta_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "postmeta(meta_key(50), meta_value(15));");
        }
    } catch (Exception $e) {
        
    }

    $indexname = 'wpi_performance_boost2';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts(post_status, ID);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost8';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts(post_type, post_status, menu_order, post_title(15), ID);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost9';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts(post_type, post_status, post_date desc, ID);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost6';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_relationships' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "term_relationships(term_taxonomy_id, object_id);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost3';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "term_taxonomy (taxonomy, parent, term_taxonomy_id);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost4';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "term_taxonomy (taxonomy, term_id, term_taxonomy_id);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost5';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "term_taxonomy (term_taxonomy_id, taxonomy, term_id);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost7';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->terms' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "terms(term_id, name(50), slug(50));");
        }
    } catch (Exception $e) {
        
    }

    $indexname = 'wpi_options_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->options' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "options(autoload);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_latest_posts_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->options' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts(post_type, post_status, post_date desc);");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_perfboost_woopaypal';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->options' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts(post_type, post_mime_type);");
        }
    } catch (Exception $e) {        
    }
    $indexname = 'wpi_scalability_pro_sitemaps';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->options' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts (post_status(20), post_password(20), post_type(20), post_modified);");
        }
    } catch (Exception $e) {        
    }
    $indexname = 'wpi_guid';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->options' AND `INDEX_NAME` = '$indexname'");
        if (is_null($indexexists)) {
            $wpdb->query("create index $indexname on " . $wpdb->prefix . "posts (guid);");
        }
    } catch (Exception $e) {        
    }

}

function wpi_dropindexes() {
    global $wpdb;
    $indexname = 'wpi_performance_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "postmeta;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_postmeta_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "postmeta;");
        }
    } catch (Exception $e) {
        
    }

    $indexname = 'wpi_performance_boost2';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost8';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost9';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost3';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_taxonomy;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost4';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_taxonomy;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost5';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_taxonomy;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost6';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_relationships' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_relationships;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_performance_boost7';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->terms' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "terms;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "postmeta;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost2';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost8';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost9';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost3';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_taxonomy;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost4';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_taxonomy;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost5';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->term_taxonomy' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "term_taxonomy;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'awd_fast_filters_boost7';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->terms' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "terms;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_import_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "postmeta;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_options_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->options' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "options;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_latest_posts_boost';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
        
    }
    $indexname = 'wpi_perfboost_woopaypal';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
    }
    $indexname = 'wpi_scalability_pro_sitemaps';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
    }
    $indexname = 'wpi_guid';
    if (is_multisite()) {
        $indexname .= "_" . get_current_blog_id();
    }
    try {
        $indexexists = $wpdb->get_var("
		SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
		`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
		`TABLE_NAME` = '$wpdb->posts' AND `INDEX_NAME` = '$indexname'");
        if (!is_null($indexexists)) {
            $wpdb->query("drop index $indexname on " . $wpdb->prefix . "posts;");
        }
    } catch (Exception $e) {
    }
    
    

}

add_filter('admin_body_class', 'sp_admin_body_class');

function sp_admin_body_class( $classes ) {

    $screen = '';
    
    $screen = get_current_screen();

    if ( ! $screen || $screen->id != 'settings_page_wpi_performance' ) return $classes;        
    
    $classes .= ' wpsp';

    return $classes;
}

add_action('admin_menu', 'spro_add_admin_menu');

add_action('admin_init', 'spro_settings_init');


add_action('wp_head', function() {
    echo "<script> var awd_admin_ajax_url = '" . admin_url("admin-ajax.php") . "'; </script>";
});

function spro_add_admin_menu() {
    add_options_page(SP_PLUGIN_NAME . ' Plugin', SP_PLUGIN_NAME . ' Plugin', 'manage_options', 'scalabilitypro', 'spro_options_page');
}

function spro_settings_init() {
    global $wpdb;

    $exists = $wpdb->get_var("SELECT count(*)
FROM information_schema.TABLES
WHERE (TABLE_SCHEMA = '" . $wpdb->dbname . "') AND (TABLE_NAME = '" . $wpdb->prefix . "scalability_pro_cache')");
    if ($exists != 1) {
        $sql = "CREATE TABLE " . $wpdb->prefix . "scalability_pro_cache (
            postid int not null,
            userid int not null,
            cacheview varchar(50) not null,
            cachegroup varchar(50) not null,
            cachedata text CHARACTER SET utf8,
            primary key (postid, userid, cacheview, cachegroup)
        );";
        $wpdb->query($sql);
    }
    register_setting('wpi_perf_page', 'wpiperf_settings');

    add_settings_section(
            'wpiperf_wpi_perf_page_section', '', 'wpiperf_settings_section_callback', 'wpi_perf_page'
    );
    add_settings_section(
            'spro_archive_options', '', 'spro_archive_options', 'wpi_perf_page'
    );
    add_settings_section(
            'spro_pagedetail_options', '', 'spro_pagedetail_options', 'wpi_perf_page'
    );
    add_settings_section(
            'spro_import_options', '', 'spro_import_options', 'wpi_perf_page'
    );
    add_settings_section(
            'spro_woocommerce_options', '', 'spro_woocommerce_options', 'wpi_perf_page'
    );
    add_settings_section(
            'spro_wpadmin_options', '', 'spro_wpadmin_options', 'wpi_perf_page'
    );
    add_settings_section(
            'spro_wpedit_options', '', 'spro_wpedit_options', 'wpi_perf_page'
    );
	

    $options = get_option('wpiperf_settings');
    if (!isset($options['loadproductpageusingajax'])) {
        $options['loadproductpageusingajax'] = 'normalproductpage';
    }
    if (!isset($options['sidebarcssselector'])) {
        $options['sidebarcssselector'] = '.col-sm-3.col-sm-pull-9';
    }
    if (!isset($options['sortorder'])) {
        $options['sortorder'] = 'fullfunctionality';
    }
    if (!isset($options['calctotals'])) {
        $options['calctotals'] = 'keep';
    }
    if (!isset($options['defertermcounting'])) {
        $options['defertermcounting'] = 'keep';
    }
    if (!isset($options['removecustommeta'])) {
        $options['removecustommeta'] = 'keep';
    }
    if (!isset($options['changetoexists'])) {
        $options['changetoexists'] = 'keep';
    }
    if (!isset($options['optimisewoodeleteoptions'])) {
        $options['optimisewoodeleteoptions'] = 'keep';
    }
	
	
    if (!isset($options['removecast'])) {
        $options['removecast'] = 'keep';
    }
    if (!isset($options['optimisewoogroup'])) {
        $options['optimisewoogroup'] = 'no';
    }
    if (!isset($options['removewoosummary'])) {
        $options['removewoosummary'] = 'keep';
    }
    if (!isset($options['toplevelcatsonly'])) {
        $options['toplevelcatsonly'] = 'no';
    }
	
    if (!isset($options['removeajaxvariationscalc'])) {
        $options['removeajaxvariationscalc'] = 'keep';
    }
    update_option('wpiperf_settings', $options, false);

    add_settings_field(
            'sortorder', __('Remove sort options', 'wpi-performance'), 'wpiperf_sortorder_render', 'wpi_perf_page', 'spro_archive_options'
    );
    add_settings_field(
            'calctotals', __('Remove pagination', 'wpi-performance'), 'wpiperf_calctotals_render', 'wpi_perf_page', 'spro_archive_options'
    );
    add_settings_field(
            'changetoexists', __('Alter main query to use EXISTS rather than LEFT JOIN', 'wpi-performance'), 'wpiperf_changetoexists', 'wpi_perf_page', 'spro_archive_options'
    );
    add_settings_field(
            'optimisewoogroup', __('Optimise WooCommerce Group By', 'wpi-performance'), 'wpiperf_optimise_woo_groupby', 'wpi_perf_page', 'spro_archive_options'
    );
    add_settings_field(
            'removeajaxvariationscalc', __('Remove Woo Ajax variations calculations', 'wpi-performance'), 'wpiperf_remove_woo_ajax_variations', 'wpi_perf_page', 'spro_pagedetail_options'
    );
    add_settings_field(
            'defertermcounting', __('Defer term counting', 'wpi-performance'), 'wpiperf_defertermcounting', 'wpi_perf_page', 'spro_import_options'
    );
	
	
    add_settings_field(
            'optimisewoodeleteoptions', __('Optimise WooCommerce Updates', 'wpi-performance'), 'wpiperf_optimise_woo_delete_options', 'wpi_perf_page', 'spro_woocommerce_options'
    );
    add_settings_field(
            'removecustommeta', __('Remove custom-meta select box', 'wpi-performance'), 'wpiperf_removecustommeta', 'wpi_perf_page', 'spro_woocommerce_options'
    );

	
	
    add_settings_field(
            'removewoosummary', __('Remove Woo order summary', 'wpi-performance'), 'wpiperf_remove_woo_order_summary', 'wpi_perf_page', 'spro_wpadmin_options'
    );
    add_settings_field(
            'toplevelcatsonly', __('Optimise wp-admin WooCommerce products list', 'wpi-performance'), 'wpiperf_top_level_cats_woo_admin', 'wpi_perf_page', 'spro_wpadmin_options'
    );

    add_settings_field(
            'removecast', __('Remove cast on postmeta queries', 'wpi-performance'), 'wpiperf_removecast', 'wpi_perf_page', 'spro_wpedit_options'
    );
	

	
	
	
//        global $wp_settings_fields;
//        
//        print_r($wp_settings_fields);
}

function spro_archive_options() {
    ?>

    <div class="row">
        <div class="block float-l sp-welcome-wrapper">
            <section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0"><?php echo __('Archive Options - Optimising WP_Query', 'wordpress'); ?></h3>
                </div>
                <div class="box-container">				
					<p>These options optimise the 'Main Query' on your archive pages. Archive pages include your blog list, product list pages (e.g. /shop/) and any page which lists other pages, including category pages, attribute pages etc.</p>
					<p>To measure before/after performance, install Query Monitor, visit your archive pages, click Query Monitor then CTRL+F or CMD+F and search for 'Main Query'.</p>
                </div>
            </section>				
        </div>
    </div>
    <?php
}
function spro_pagedetail_options() {
    ?>

    <div class="row">
        <div class="block float-l sp-welcome-wrapper">
            <section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0"><?php echo __('Faster Product Detail or Single Post Pages', 'wordpress'); ?></h3>
                </div>
                <div class="box-container">				
					<p>If your product detail or single pages include widgets which run SQL which is optimised by the ‘archive’ optimisations above, then these pages will subequently be faster as a result. There is also this option, specifically for WooCommerce Product Variations:</p>
                </div>
            </section>				
        </div>
    </div>
    <?php
}
function spro_import_options() {
    ?>

    <div class="row">
        <div class="block float-l sp-welcome-wrapper">
            <section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0"><?php echo __('Faster Imports - Optimising wp_postmeta', 'wordpress'); ?></h3>
                </div>
                <div class="box-container">				
					<p>Adding the 13 indexes above optimises your imports. The option below optimises further by defering post/product counts until 2AM.</p>
                </div>
            </section>				
        </div>
    </div>
    <?php
}
function spro_woocommerce_options() {
    ?>

    <div class="row">
        <div class="block float-l sp-welcome-wrapper">
            <section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0"><?php echo __('Specific WooCommerce optimisations', 'wordpress'); ?></h3>
                </div>
                <div class="box-container">				
					<p>If you are running WooCommerce, and you have a large number of products or a large number of orders, these options will help you get speed back to your site.</p>
                </div>
            </section>				
        </div>
    </div>
    <?php
}
function spro_wpadmin_options() {
    ?>

    <div class="row">
        <div class="block float-l sp-welcome-wrapper">
            <section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0"><?php echo __('WP Admin Archive optimisations', 'wordpress'); ?></h3>
                </div>
                <div class="box-container">				
					<p>The WP_Query archive optimisations above will help, but there are more 'table scan' incurring widgets on wp-admin archives. Specifically 'post type' dropdown, 'category' dropdown, and the 'post status' counts. These options help you optimise those.</p>
                </div>
            </section>				
        </div>
    </div>
    <?php
}
function spro_wpedit_options() {
    ?>

    <div class="row">
        <div class="block float-l sp-welcome-wrapper">
            <section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0"><?php echo __('WP Admin EDIT optimisations', 'wordpress'); ?></h3>
                </div>
                <div class="box-container">				
					<p>There is a 'custom meta' dropdown box on the post/product/custom post editing pages. It's rarely used. It causes table scans. The option below lets you fix it.</p>
                </div>
            </section>				
        </div>
    </div>
    <?php
}


function wpiperf_sortorder_render() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[sortorder]'>
        <option value='natural' <?php selected($options['sortorder'], 'natural'); ?>>Remove sort options (fastest)</option>
        <option value='fullfunctionality' <?php selected($options['sortorder'], 'fullfunctionality'); ?>>Keep sort options (slowest)</option>
    </select>
    <p>This option lets you use the natural index sort order on wp_posts. When WP_Query (used by WooCommerce, Custom Post Types and Blog archives) to fetch data, it provides sorting options and these sorting options require a full sort of the retrieved data. On /shop/ or large top level categories that means a full table or index scan which can take a few seconds and thrash the disk and CPU.</p>
    <p><strong>Note: </strong> Using the natural sort order will disable any sort options you currently provide for your users and revert to the natural sort order (normally sort by Date DESC). If you choose this route, you should hide those sort options.</p>
    <?php
}

function wpiperf_calctotals_render() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[calctotals]'>
        <option value='remove' <?php selected($options['calctotals'], 'remove'); ?>>Remove pagination (fastest)</option>
        <option value='keep' <?php selected($options['calctotals'], 'keep'); ?>>Keep pagination (slowest)</option>
    </select>
    <p>When WP_Query fetches posts/products, it also calculates the total number of matching products. This is useful to display at the top of your shop/page, e.g. Showing 1 - 50 of 650,000. However, this count requires either an index scan or table scan. Removing it, can result in ultra-fast WP_Query speed. Depending on your setup, you may need to switch to the natural sort order above too for successful use of the indexes.</p>
    <p><strong>Note: </strong> Infinite scroll is required if you remove pagination.</p>
    <?php
}
function wpiperf_defertermcounting() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[defertermcounting]'>
        <option value='remove' <?php selected($options['defertermcounting'], 'remove'); ?>>Defer Term Counting to Nightly (fastest)</option>
        <option value='keep' <?php selected($options['defertermcounting'], 'keep'); ?>>Keep term counting (slowest)</option>
    </select>
    <p>Helps optimise imports and other bulk modifications by deferring term counts (recounting items per category) to a nightly job.</p>
    <?php
}
function wpiperf_removecustommeta() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[removecustommeta]'>
        <option value='remove' <?php selected($options['removecustommeta'], 'remove'); ?>>Remove custom-meta select box (fastest)</option>
        <option value='keep' <?php selected($options['removecustommeta'], 'keep'); ?>>Keep custom-meta select (slowest)</option>
    </select>
    <p>Page and post editing pages in wp-admin include a really badly written sql query to populate the 'custom meta' select box. This is redundant because if you want to edit meta, you can type in the meta name. Highly recommended you remove it.</p>
    <?php
}
function wpiperf_removecast() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[removecast]'>
        <option value='remove' <?php selected($options['removecast'], 'remove'); ?>>Remove cast on wp_postmeta queries (fastest)</option>
        <option value='keep' <?php selected($options['removecast'], 'keep'); ?>>Keep cast on wp_postmeta queries (slowest)</option>
    </select>
    <p>There is a CAST function applied to the 'value' column on many wp_postmeta queries. This cast is redundant since MySQL auto-casts where necessary, but worse, because a function is applied, mysql cannot use any indexes we create on these columns. Highly recommend you remove it.</p>
    <?php
}
function wpiperf_optimise_woo_groupby() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[optimisewoogroup]'>
        <option value='optimise' <?php selected($options['optimisewoogroup'], 'optimise'); ?>>Optimise Woo Group By (fastest)</option>
        <option value='no' <?php selected($options['optimisewoogroup'], 'no'); ?>>Don't optimise (slowest)</option>
    </select>
    <p>In some situations, WooCommerce runs a GROUP BY query even when there is no need. That causes either an index scan or a table scan, followed by a sort. If you see compatibility issues, this is one option you might try disabling, otherwise choose to optimise.</p>
    <?php
}
function wpiperf_remove_woo_order_summary() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[removewoosummary]'>
        <option value='remove' <?php selected($options['removewoosummary'], 'remove'); ?>>Remove Woo Dashboard Summary (fastest)</option>
        <option value='keep' <?php selected($options['removewoosummary'], 'keep'); ?>>Don't optimise (slowest)</option>
    </select>
    <p>If you have a lot of orders, you will notice your wp-admin pages slowing down. WooCommerce runs the Order Summary dashboard script which you probably never look at or use and it can add seconds to wp-admin page load. Order summaries are still available by going into WooCommerce -> Reports, this just removes the dashboard widget.</p>
    <?php
}
function wpiperf_top_level_cats_woo_admin() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[toplevelcatsonly]'>
        <option value='yes' <?php selected($options['toplevelcatsonly'], 'yes'); ?>>Optimise WooCommerce wp-admin products list (fastest)</option>
        <option value='no' <?php selected($options['toplevelcatsonly'], 'no'); ?>>Don't optimise (slowest)</option>
    </select>
    <p>If you have a lot of products, and a lot of categories, the WooCommerce back end products list will be slow.  This option lets you optimise that by only fetching the top-level categories for the category dropdown in the admin page. You will still be able to paginate through products and/or search for products.</p>
    <?php
}



function wpiperf_remove_woo_ajax_variations() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[removeajaxvariationscalc]'>
        <option value='remove' <?php selected($options['removeajaxvariationscalc'], 'remove'); ?>>Remove Ajax variations count (fastest)</option>
        <option value='keep' <?php selected($options['removeajaxvariationscalc'], 'keep'); ?>>Keep Ajax variations count (slowest)</option>
    </select>
    <p>If you have products with LOTS of variations, WooCommerce runs some slow code to count the variations on the product detail page. This is unnecessary so you should be able to safely remove this.</p>
    <?php
}
function wpiperf_changetoexists() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[changetoexists]'>
        <option value='change' <?php selected($options['changetoexists'], 'change'); ?>>Change to EXISTS (fastest)</option>
        <option value='keep' <?php selected($options['changetoexists'], 'keep'); ?>>Keep LEFT JOINS (slowest)</option>
    </select>
    <p><strong>WARNING - EXPERIMENTAL!</strong> This option will attempt to alter the main WP_QUERY SQL call to use WHERE EXISTS rather than a LEFT JOIN. This means that the SQL Query can avoid using a GROUP BY. This feature also removes SORTING of results. In many cases it can cause the indexes to be used properly and can avoid table scans. On our reference server, it turns a 4.6 second query (820,000 products) to a 0.05 second query. This option is EXPERIMENTAL. It definitely will not alter admin queries otherwise we might accidentally break your wp-admin pages. In future, once it's proven resilient, it may be used to optimise wp-admin calls too.</p>
	<p>You can learn more from this article: <a href="https://www.wpintense.com/2017/10/14/extra-performance-boosts-for-scalability-pro/">https://www.wpintense.com/2017/10/14/extra-performance-boosts-for-scalability-pro/</a></p>
    <?php
}
function wpiperf_optimise_woo_delete_options() {

    $options = get_option('wpiperf_settings');
    ?>
    <select name='wpiperf_settings[optimisewoodeleteoptions]'>
        <option value='optimise' <?php selected($options['optimisewoodeleteoptions'], 'optimise'); ?>>Optimise DELETE wp_options operations (fastest)</option>
        <option value='keep' <?php selected($options['optimisewoodeleteoptions'], 'keep'); ?>>Keep slow deletes for wp_options (slowest)</option>
    </select>
    <p>WooCommerce has code on multiple pages which forces deletes against wp_options. These delete operations are written in such a way that they cannot use indexes on wp_options. This means, if you have a lot of options (e.g. a lot of transients) that your site will intermittently be locked - on our reference site (820,000+ products) this intermittent slowdown can last for up to 3 minutes. Enabling this option makes Scalability Pro rewrite these delete operations to be able to use the indexes and makes the delete operation virtually instant.</p>
	<p>You can learn more from this article: <a href="https://www.wpintense.com/2017/10/14/extra-performance-boosts-for-scalability-pro/">https://www.wpintense.com/2017/10/14/extra-performance-boosts-for-scalability-pro/</a></p>
    <?php
}

function wpiperf_settings_section_callback() {
    //todo: make multilingual friendly
    global $wpdb;
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	$options = get_option('wpiperf_settings');

	$wpiurls = array();
	
	$url = get_home_url();
	if ( is_ssl() ) { $url = set_url_scheme( $url, 'https' ); }
	$wpiurls[] = $url;

	$url = get_admin_url();
	if ( is_ssl() ) { $url = set_url_scheme( $url, 'https' ); }
	$wpiurls[] = $url;

	try {
		if (class_exists('WooCommerce')) {
			$url = get_permalink( wc_get_page_id( 'shop' ));
			if ( is_ssl() ) { $url = set_url_scheme( $url, 'https' ); }
			$wpiurls[] = $url;
		}
	}catch (Exception $e) {		
	}

	try {
		$url = get_home_url() . "?s=test+search+performance";
		if ( is_ssl() ) { $url = set_url_scheme( $url, 'https' ); }
		$wpiurls[] = $url;
	}catch (Exception $e) {		
	}
	if ( 'page' === get_option( 'show_on_front' ) ) {
		$url = get_permalink( get_option( 'page_for_posts' ) );
		if ( is_ssl() ) { $url = set_url_scheme( $url, 'https' ); }
		$wpiurls[] = $url;
	}
	$args = array( 'numberposts' => '1');
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		$url = get_permalink($recent["ID"]);
		if ( is_ssl() ) { $url = set_url_scheme( $url, 'https' ); }
		$wpiurls[] = $url;
	}
	echo "<script>var wpisiteurls = " . json_encode($wpiurls) . ";</script>";
	
	//set up profiling table
	$sql = "CREATE TABLE " . $wpdb->prefix . "scalability_pro_profiling (
        profileid varchar(50) not null,
        url varchar(2000) not null,
        profiledt datetime not null,
        results TEXT NOT NULL
    );";
    dbDelta($sql);	

		?>
		<script>var profileresults = [];
		</script>
		<div id="wpisp_perf_profile" style="display:none"><h2>Performance Profile</h2>
			<div id="perfprofile"></div>
		</div>
		<div id="wpisp-first-run">
			<div class="row">
				<div class="">                		
					<section class="dev-box wp-sp-container" id="wp-sp-welcome-box">
						<div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
							<h3 tabindex="0">SCALABILITY PRO</h3>
						</div>
						<div> 
							<p>Welcome to Scalability Pro by <a href="https://www.wpintense.com/">www.wpintense.com</a>. This plugin helps you with <em>scalability</em> by optimising your database and giving you options to eliminate some unneccessary and slow functionality from WordPress and WooCommerce. This allows you to have a fast enough site and server to populate your cache using your favourite caching plugin.</p>
							<p>1 million + products is achievable on a $40pcm server. Follow the steps below to get started.</p>
						<ol><li>Create indexes by clicking the create index button below</li>
							<li>Set as many options as you can below, depending on your compatibility level and feature requirements (see notes for each option below)</li>
							<li>Your site will be far faster and more scalable - now you can add a caching plugin - any page caching plugin will do.</li>
							</ol>
							<p>Note: To measure page performance you measure TTFB (Time to First Byte) - to do this you can use the Query Monitor plugin. Load your slow pages and you'll see the TTFB displayed in your WP admin bar.</p>
							<p>If you are still having performance/scalability issues after the above steps:</p>
							<ol><li>Check your hosting is good quality</li>
							<li>Run Query Monitor on your slow pages - if you have > 100 queries on your slow pages, use the 'group by component' feature of Query Monitor to find out which plugins to deactivate</li>
							</ol>
							<p>For more help, please visit <a href="https://www.wpintense.com/">www.wpintense.com</a>. We're making WordPress the fastest CMS on the planet.</p>
						</div>
						<!-- Content -->
					</section>
				</div>
			</div>
		</div>
		<?php
	$optionscount = $wpdb->get_var("SELECT count(*) from $wpdb->options where autoload = 'yes'");
	if ($optionscount > 1000) {
		?>
		<div class="row" style="margin-bottom:20px"> 
			<div class="block float-l sp-welcome-wrapper">
				<section class="dev-box bulk-smush-wrapper wp-sp-container mb-0"> 
					<div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
						<h3 tabindex="0">PERFORMANCE WARNING</h3>
						<div class="sp-container-subheading roboto-medium">

						</div>			
					</div>
					<div class="box-container">				
						<p>You have <?php echo $optionscount; ?> options set to autoload. This is too many and will be slowing down your site significantly. <a target="_blank" href="https://www.wpintense.com/knowledgebase/how-do-i-fix-the-performance-warning-of-too-many-options-set-to-autoload/">Read our autoload options guide</a> to learn how to fix this quickly and easily.</p>
					</div>
				</section>				
			</div>
		</div>	
		<?php
	}
	?>

    <div class="row">
        <div class="wp-spshit-container">
            <section class="dev-box bulk-smush-wrapper wp-sp-container" id="wp-sp-bulk-wrap-box">
                <div class="wp-sp-container-header box-title" xmlns="http://www.w3.org/1999/html">
                    <h3 tabindex="0">Index Status</h3>
                    <div class="sp-container-subheading roboto-medium">

                    </div>			
                </div>
                <div class="box-container">				
                    <p>Below you can see the indexes created and maintained by this plugin in order to help avoid table scans.</p>
                    <p>If you suspect table scans are still occurring, follow our guide to identify them: <a target="_blank" href="https://www.wpintense.com/2016/07/26/enable-slow-query-log-and-identify-slow-queries-percona-db/">Identify slow queries and table scans</a></p>
                        <?php
                        $indexstats = $wpdb->get_results("
                                        SELECT DISTINCT s.TABLE_NAME, s.INDEX_NAME
                                FROM INFORMATION_SCHEMA.STATISTICS s
                                LEFT OUTER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS t ON t.TABLE_SCHEMA=s.TABLE_SCHEMA AND t.TABLE_NAME=s.TABLE_NAME
                                AND s.INDEX_NAME=t.CONSTRAINT_NAME 
                                WHERE 0=0
                                AND t.CONSTRAINT_NAME IS NULL
                                and s.INDEX_NAME != 'wpi_fulltext'
                                AND s.INDEX_NAME like 'wpi%';");
//	echo "<pre>" . print_r($indexstats, true) . "</pre>";	

                        if (count($indexstats) == 0) {
                            echo "<p class='tc roboto-regular'>No indexes created yet.</p>";
                            echo '<p><span class="wp-sp-upload-images tc"><a class="button button-cta" href="javascript:void(0);" id="wpicreateindexes" title="'.__("click here to create a full text index/Settings will not be configured until you do this", "wpi-performance").'">Create b-tree indexes now</a></span></p>';
                            echo '<p class="tc roboto-regular"><strong>Note: </strong>Creating the indexes may time out - refresh this page to view progress and click to create them again if you have to - it will continue from where it left off until all are created</p>';
                        } elseif (count($indexstats) < 15) {
                            echo "<p>" . count($indexstats) . " of 15 indexes created.</p>";
                            echo '<p><a href="javascript:void(0);" id="wpicreateindexes">Try creating remaining b-tree indexes now</a></p>';
                            echo '<p><strong>Note: </strong>Creating the indexes may time out - refresh this page to view progress and click to create them again if you have to - it will continue from where it left off until all are created</p>';
                            echo '<h2>B-tree indexes</h2>';
                        } else {
                            echo "<p>All b-tree indexes successfully created.</p>";
                            echo '<h3>WPI Indexes</h3>';
                        }
                        foreach ($indexstats as $index) {
                            echo $index->TABLE_NAME . '.' . $index->INDEX_NAME . '<br>';
                        }

                        if (count($indexstats) > 0) {
                            echo '<p><span class="wp-sp-upload-images tc"><a href="javascript:void(0);" class="button button-cta" id="wpidropindexes" title="'.__("Delete indexes", "wpi-performance").'">Delete indexes</a></span></p>';
                        }
                    ?>		
                </div>
            </section>				
        </div>
    </div>

	<script>
	function wpisp_saveprofile() {
	//console.log(response);
	qm = jQuery('#sp_monitoring iframe').contents();
	qm = jQuery('#qm', qm).wrap('p').html();

	profile = jQuery('#sp_monitoring iframe').data('profile');
	tindex = jQuery('#sp_monitoring iframe').data('index');
	url = jQuery('#sp_monitoring iframe').attr('src');
	if (jQuery('#qm-overview table tr td:first-child', qm).length > 0) {
		profiletime = jQuery('#qm-overview table tr td:first-child', qm).html().split('<br>')[0];
		jQuery('#p' + tindex + ' .' + profile).html(parseFloat(profiletime).toFixed(2) + 's');
		profileresults[profile, url] = qm;
	} else {
//		profileresults[profile, url] = -1; // could not grab profile results for some reason (timeout?)
	}
	
	
	jQuery.post(ajaxurl, 
		{
			action: 'wpisp_saveperfresults',
			profileid: profile,
			url: url,
			profileresults: qm
		},
		function(response) {
			wpiindex = jQuery('#sp_monitoring iframe').data('index');
			wpiindex++;
			jQuery('#sp_monitoring iframe').remove();
			if (wpiindex < wpisiteurls.length) {
				url = window.wpisiteurls[wpiindex];
				if (profile == 'before') {
					jQuery('#sp_results').append('<div class="perfrow" id="p' + wpiindex + '"><div class="url">' + url + '</div><div class="before"></div></div>');					
					jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + profile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');
				} else {
					jQuery.get(url, function() {
						jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + profile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');						
					});					
				}
			} else {
				scalabilityprostage++;
				if (scalabilityprostage >= 1) {
					jQuery('#sp_progress').append('\nProfiling complete.');
					jQuery('#startoptimisation').remove();
					jQuery('#sp_submitreport').append('<p>Now click Submit Report to WPI to submit your performance report and plugin list to us to help us optimise Scalability Pro further:</p>');
					jQuery('#sp_submitreport').append('<p><a class="button" href="javascript:void(0);" id="submitreport">Submit Report to WPI</a></p>');
				}
				
			}

			
		}
	);
}
var scalabilityprostage = 1;
jQuery(document).on('click', '#startoptimisation', function() {
	jQuery('#sp_progress').append('<br>Started profiling - please wait...');
	jQuery('#startoptimisation').html('Processing...');
	jQuery('#startoptimisation').attr('disabled', true);
	var wpiindex = 0; 
	var wpiprofile = 'before'; //todo: add profile name here
	
	url = wpisiteurls[wpiindex];			
	jQuery('#sp_results').append('<div class="perfrow"><div class="url">URL</div><div class="before">Speed</div></div>');
	jQuery('#sp_results').append('<div class="perfrow" id="p' + wpiindex + '"><div class="url">' + url + '</div><div class="before"></div></div>');
	jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + wpiprofile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');
});

function scalability_pro_create_normalindex (){
	jQuery.ajax({
		url: ajaxurl,
		type: "POST",
		dataType: "json",
		data: {action: 'wpi_createindexes'},
		timeout: 15000,
		success: function(response) { 
			jQuery('#sp_progress').append('<br>B-tree indexes created');
			jQuery('#wpicreateindexes').text('Complete');
		},
		error: function(x, t, m) {
			if(t==="timeout") {
				jQuery('#sp_progress').append('<br>Working on creating b-tree indexes - please wait...');
				window.setTimeout(function() {scalability_pro_create_normalindex();}, 25000);
			} else {
				jQuery('#sp_progress').append('<4>Error</h4>');
				jQuery('#sp_progress').append('<pre>' + t + '</pre>');
			}
		}
	});
}
jQuery(document).on('click', '#wpidropindexes', function () {
	jQuery('#wpidropindexes').text('Working...');
	jQuery.post(
			ajaxurl,
			{action: 'wpi_dropindexes'},
			function (response) {
				alert('Indexes dropped');
				jQuery('#wpidropindexes').text('Complete');
				console.log(response);
			}
	);

});
jQuery(document).on('click', '#wpicreateindexes', function () {
	jQuery('#wpicreateindexes').text('Working...');
	scalability_pro_create_normalindex();

});
jQuery(document).on('click', '#submitreport', function() {
	//todo: submit profile to WPI
	console.log (profileresults.length);
	console.log (profileresults.size);
	console.log (profileresults);
	jQuery.ajax({
		type: 'POST',
		url: 'https://profiling.wpintense.com/wp-admin/admin-ajax.php',
		crossDomain: true,
		data: {
				"action": 'wpi_submitreport',
				"websiteurl": window.location.href,
				"profile": JSON.stringify(profileresults)
		},
		dataType: 'jsonp',
		success: function(responseData, textStatus, jqXHR) {
			alert(responseData);
		},
		error: function (responseData, textStatus, errorThrown) {
			alert('Submit failed.');
		}
	});
	
});
jQuery(document).on('change', '#sponoffstatus', function() {
	console.log ('changing status');
	jQuery.post(ajaxurl, 
		{
			action: 'wpisp_switchonoff',
			newstatus: jQuery('#sponoffswitch').val()
		},
		function(response) {
			console.log(response);
		}
	);	
});

jQuery(document).on('click', '#closereport', function() {
	jQuery('#wpisp-first-run').css('display', 'none');
	wpisp_showprofile();
});
function wpisp_showprofile() {
	jQuery('#wpisp_perf_profile').css('display', 'block');
	jQuery.each(profileresults , function( index, obj ) {
		var perfout = '';
		perfout += '<div class="perfhrow"><div class="perfurl"></div>';
		jQuery.each(obj, function( key, value ) {
			perfout += '<div class="perfurlresult">' + key + '</div>';
			return false;
		});
		perfout += '</div>';
		jQuery('#perfprofile').append(perfout);		
		return false;
	});
	jQuery.each(profileresults , function( index, obj ) {
		var perfout = '';
		perfout += '<div class="perfhrow"><div class="perfurl">' + index + '</div>';
		jQuery.each(obj, function( key, value ) {
			perfout += '<div class="perfurlresult">' + value + '</div>';
		});
		perfout += '</div>';
		jQuery('#perfprofile').append(perfout);		
	});
}
</script>
	<style>
	.perfrow {
	clear:both;
	background-color:#eee;
	padding:2px 5px;
	font-size:16px;
	line-height:20px;
	overflow:hidden;
	border:1px solid #ebebeb;
	padding:5px;
}
.perfrow:nth-child(odd) {
	background-color:#f4f4f4;
}
.perfrow .url {
	display:block;
	float:left;
}
.perfrow .before {
	display:block;
	float:right;
	width:100px;
	overflow:hidden;
}
.perfrow .after1 {
	display:block;
	float:right;
	width:100px;
	overflow:hidden;
	height:20px;
}
.perfrow .after2 {
	display:block;
	float:right;
	width:50px;
	overflow:hidden;
	height:20px;
}
.settings_page_scalabilitypro form h1 {
	font-size:36px;
	text-align:left;
}


.settings_page_scalabilitypro form{
    margin: 10px 20px 0 2px;
}

.settings_page_scalabilitypro .row, .settings_page_scalabilitypro .row-sep {
    clear: both;
    margin: 0 0 0 0;
    position: relative;
    display: table;
    width: 100%;
    table-layout: fixed;
}

.hidden{
    display: none;
}

.settings_page_scalabilitypro .form-table{
    clear: both;
    margin: 0 0 0 0;
    position: relative;
    display: table;
    width: 100%;
    table-layout: fixed;
    padding-bottom: 0;
    border-radius: 5px;
    background-color: #FFF;
    box-shadow: 0 2px 0 #EAEAEA;
    padding: 30px;
    margin: 0 0 30px;
}

.settings_page_scalabilitypro .form-table th, .settings_page_scalabilitypro .form-table td{
    padding: 30px;
    border-bottom: 1px solid #ddd;
}

.settings_page_scalabilitypro form h1, .settings_page_scalabilitypro form h2, .settings_page_scalabilitypro form h3 {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    max-width: 900px;
    text-transform: uppercase;
}

.settings_page_scalabilitypro .float-l {
    float: left;
}

.settings_page_scalabilitypro .float-r {
    float: right;
}

.sp-dismiss-welcome {
    margin: 1.4em 0;
}

.settings_page_scalabilitypro form a {
    text-decoration: none;
    color: #19B4CF;
}

.settings_page_scalabilitypro .wp-sp-welcome.wp-sp-container .box-content {
    min-height: 149px;
}

.settings_page_scalabilitypro .sp-dismiss-welcome a {
    color: #9b9b9b;
}

.settings_page_scalabilitypro .block {
    display: block;
    width: 100%;
    float: none;
}

.settings_page_scalabilitypro .wp-sp-welcome.wp-sp-container {
    background-image: url(../../images/imgpsh_fullsize.png);
    background-repeat: no-repeat;
    background-position: 30px 100px;
    padding-bottom: 0;
    background-size: 150px
}

.settings_page_scalabilitypro .dev-box {
    border-radius: 5px;
    background-color: #FFF;
    box-shadow: 0 2px 0 #EAEAEA;
    padding: 30px;
    margin: 0 0 30px;
}

.settings_page_scalabilitypro .dev-box .box-title {
    height: 60px;
    padding: 0 30px;
    border-bottom: 1px solid #EAEAEA;
    margin: -30px -30px 30px -30px;
}

.settings_page_scalabilitypro .dev-box .box-title h3 {
    font-family: 'Roboto', sans-serif;
    font-size: 18px;
    font-weight: 700;
    color: #777771;
    line-height: 60px;
    margin: 0;
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

.settings_page_scalabilitypro .dev-box .box-title h3 {
    display: inline-block;
}
#sponoffstatus {
	float: right;
    padding: 10px;
    font-weight: bold;
font-size: 18px;
}

.sp-page-heading{
    text-align: left;
    text-transform: uppercase;
    font-size: 32px;
    font-weight: bold;
    max-width: none;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 40px;
    font-family: 'Roboto', sans-serif;
    display: block;
    margin: 0.5em auto;
    padding: 0;
    color: #777771;
}

.settings_page_scalabilitypro form h4 {
    text-transform: uppercase;
}

.settings_page_scalabilitypro form h4, .settings_page_scalabilitypro form h5, .settings_page_scalabilitypro form h6 {
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
}


.settings_page_scalabilitypro .wp-sp-welcome-content {
    padding-bottom: 20px;
    display: inline-block;
    overflow: hidden;
    max-width: 68%;
    margin: 0 30px 0px 200px;
}

.settings_page_scalabilitypro .wp-sp-welcome-content h4 {
    font-size: 30px;
    line-height: 1.4;
    text-align: left;
}

.settings_page_scalabilitypro .roboto-condensed-regular {
    font-family: 'Roboto', sans-serif;
}

.settings_page_scalabilitypro form p:last-child {
    margin-bottom: 0;
}

.settings_page_scalabilitypro .wp-sp-welcome-message {
    line-height: 1.7;
    margin-top: 20px;
    margin-bottom: 30px;
}

.settings_page_scalabilitypro .roboto-medium {
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
}



.settings_page_scalabilitypro .wp-sp-welcome-message {
    line-height: 1.7;
    margin-top: 20px;
    margin-bottom: 30px;
}

.settings_page_scalabilitypro .row .col-half.wp-spushit-container-left {
    padding-left: 0;
}

.settings_page_scalabilitypro .row .col-half {
    width: 47%;
}

.settings_page_scalabilitypro .row .col-third, .settings_page_scalabilitypro .row .col-two-third, .settings_page_scalabilitypro .row .col-half {
    display: table-cell;
    padding: 0 15px 0 15px;
    position: relative;
    vertical-align: top;
}

.settings_page_scalabilitypro .bulk-sp-wrapper.wp-sp-container:not(.wp-sp-pro-install) {
    padding-bottom: 0;
}

.settings_page_scalabilitypro .dev-box {
    border-radius: 5px;
    background-color: #FFF;
    box-shadow: 0 2px 0 #EAEAEA;
    padding: 30px;
    margin: 0 0 30px;
}

.settings_page_scalabilitypro .dev-box .box-title {
    height: 60px;
    padding: 0 30px;
    border-bottom: 1px solid #EAEAEA;
    margin: -30px -30px 30px -30px;
}

.settings_page_scalabilitypro .dev-box .box-title h3 {
    font-family: 'Roboto Condensed', 'Roboto', sans-serif;
    font-size: 18px;
    font-weight: 700;
    color: #777771;
    line-height: 60px;
    margin: 0;
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

.settings_page_scalabilitypro .dev-box .box-title h3 {
    display: inline-block;
}

.settings_page_scalabilitypro-container-subheading {
    display: inline-block;
    float: right;
    margin: 1.5em auto;
    max-width: 60%;
}


.wp-sp-no-image {
    display: block;
    overflow: hidden;
}

.settings_page_scalabilitypro .wp-sp-no-images-content {
    color: #514a30;
    margin-top: 20px;
    padding: 20px 10px;
}

.settings_page_scalabilitypro .roboto-regular {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
}

.wp-sp-upload-images {
    display: block;
    overflow: hidden;
    padding: 10px 0 40px;
}
.settings_page_scalabilitypro .wp-core-ui .button-secondary:focus, .settings_page_scalabilitypro .wp-core-ui .button-secondary:hover, .settings_page_scalabilitypro .wp-core-ui .button.focus, .settings_page_scalabilitypro .wp-core-ui .button.hover, .settings_page_scalabilitypro .wp-core-ui .button:focus, .settings_page_scalabilitypro .wp-core-ui .button:hover {
    background: #fafafa;
    border-color: #999;
    color: #23282d;
}

.settings_page_scalabilitypro button:hover:not(:focus):not(:active), .settings_page_scalabilitypro .button:hover:not(:focus):not(:active) {
    background-color: #0093B1;
    color: #FFF;
    box-shadow: none;
}

.settings_page_scalabilitypro .button.button-cta:hover:not(:focus):not(:active), .settings_page_scalabilitypro .button.button-cta:active, .settings_page_scalabilitypro .button.button-cta:focus {
    box-shadow: 0px 3px 0px 0px #007391;
}

.settings_page_scalabilitypro form button, .settings_page_scalabilitypro form .button {
    font: 500 15px/20px 'Roboto';
    background: #00ACCA;
    color: #FFF;
    text-transform: uppercase;
    padding: 11px 20px 9px;
    height: auto;
    display: inline-block;
    margin: 0;
    border: 0;
    border-radius: 3px;
    -moz-border-radius: 3px;
    cursor: pointer;
    box-shadow: none;
    -moz-transition: color 0.3s, opacity 0.3s;
    transition: color 0.3s, opacity 0.3s;
    text-shadow: none;
}

.settings_page_scalabilitypro #submit{
    box-shadow: 0px 3px 0px 0px #007391;
}

.settings_page_scalabilitypro .button.button-cta {
    box-shadow: 0px 3px 0px 0px #0093B1;
}

.settings_page_scalabilitypro .row .col-half {
    width: 47%;
}

.settings_page_scalabilitypro .row .col-third, .settings_page_scalabilitypro .row .col-two-third, .settings_page_scalabilitypro .row .col-half {
    display: table-cell;
    padding: 0 15px 0 15px;
    position: relative;
    vertical-align: top;
}

.s140{
    width: 140px;
    height: auto;
}

.mb-0{
    margin-bottom: 0px!important;
}
#sp_progress {
	line-height:1em;
}
</style>
	
    <?php
}

add_action('wp_ajax_wpi_createindexes', 'wpi_createindexes');
add_action('wp_ajax_wpi_dropindexes', 'wpi_dropindexes');




function spro_options_page() {
    global $wpdb;
    $options = get_option('wpiperf_settings');
    ?>
    <form action='options.php' method='post'>		
    <?php
    settings_fields('wpi_perf_page');
    do_settings_sections('wpi_perf_page');
    submit_button();
    ?>

    </form>
    <?php
}


add_filter('posts_groupby', 'awd_improvewoocommerce_performance_groupby', PHP_INT_MAX);

function awd_improvewoocommerce_performance_groupby($groupby) {
    global $wpdb, $wp_query;
	$options = get_option('wpiperf_settings');
    if (isset($options['optimisewoogroup']) && $options['optimisewoogroup'] == 'optimise') {
        //if only one term set, we can remove the group by query safely and allow natural index to be used for sorting - much faster and happens often
        if (isset($wp_query->tax_query) && isset($wp_query->tax_query->queries) and count($wp_query->tax_query->queries) == 1) {
            $oneq = $wp_query->tax_query->queries[0];
            if (isset($oneq['terms']) && count($oneq['terms']) == 1) {
                if (is_object(get_queried_object()) && isset(get_queried_object()->term_id)) {
                    $currenttermid = get_queried_object()->term_id;
                } else {
                    $currenttermid = "";
                }
                $checkchildexistssql = "select count(*) c
                from " . $wpdb->prefix . "term_taxonomy tt inner join " . $wpdb->prefix . "terms t ON tt.term_id = t.term_id
                where tt.taxonomy='product_cat' and tt.parent=%d";
                $childrenexist = $wpdb->get_var($wpdb->prepare($checkchildexistssql, $currenttermid));
                if (empty($childrenexist) || $childrenexist == 0) {
                    // can safely remove the group by statement for the p.ID field which will speed up the query because it allows the indexes to be used
                    $groupby = '';
                }
            }
        }
    }
    return $groupby;
}


/* Removing the CAST on the query allows mysql to use the indexes on postmeta table - helps a LOT of places - HOT PICKS, shop, search, any meta_value based queries */
add_filter('posts_where', 'awdff_remove_cast_on_meta_query', PHP_INT_MAX);
function awdff_remove_cast_on_meta_query($where) {
    //gets the global query var object
    global $wp_query, $wpdb;

	$options = get_option('wpiperf_settings');
    if (isset($options['removecast']) && $options['removecast'] == 'remove') {
        // There is a new index added on postmeta.meta_value - limited to 15 characters - but importantly this index cannot be used if the meta_value column is unneccessarily cast to CHAR value as wp does in core
        // this statement reverts this back to remove the cast allowing mysql to use the index resulting in major speed gains
        $where = str_replace("CAST(" . $wpdb->prefix . "postmeta.meta_value AS CHAR)", $wpdb->prefix . "postmeta.meta_value ", $where);
    }
	return $where;
}

add_action('wp', 'wpi_fake_pagination_alttechnique', PHP_INT_MAX);

function wpi_fake_pagination_alttechnique() {
    global $wp_query;
    if (is_admin()) {
        return;
    }
    $options = get_option('wpiperf_settings');


    if ($wp_query->is_main_query()) {
        if (isset($options['calctotals']) && $options['calctotals'] == 'remove') {
            $wp_query->max_num_pages = 100;
            if (isset($GLOBALS['woocommerce_loop'])) {
                $GLOBALS['woocommerce_loop']['total'] = 9999;
            }
			$wp_query->found_posts = PHP_INT_MAX;
        }
    }
}

function wpi_fake_pagination($found_posts) {
    global $wp_query;
    $wp_query->max_num_pages = 100;
    return PHP_INT_MAX; // fakes pagination if CALC_TOTALS (SQL_CALC_FOUND_ROWS) is disabled
}

function custom_wc_ajax_variation_threshold( $qty, $product ) {
	$options = get_option('wpiperf_settings');
    if (isset($options['removeajaxvariationscalc']) && $options['removeajaxvariationscalc'] == 'remove') {
        $qty = 1;
    }
	return $qty;
}
add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 10, 2 );

$options = get_option('wpiperf_settings');
if (isset($options['removecustommeta']) && $options['removecustommeta'] == 'remove') {
    add_filter('query', 'remove_awful_meta_box_select_query');
}
function remove_awful_meta_box_select_query($sql){
	global $wpdb;
	if (strpos($sql, "WHERE meta_key NOT BETWEEN '_' AND '_z'")) {
		$sql = "select meta_key from $wpdb->postmeta limit 1";
	}
    return $sql;
}

//defer term counting - improves import speed
//do daily term counting instead
function avoid_term_recounts( $data , $postarr ) {
  // do something with the post data
	$options = get_option('wpiperf_settings');
    if (isset($options['defertermcounting']) && $options['defertermcounting'] == 'remove') {
        wp_defer_term_counting( true );
        wp_defer_comment_counting( true );		
    }

  return $data;
}

add_filter( 'wp_insert_post_data', 'avoid_term_recounts', '99', 2 );

//wp_schedule_event( strtotime('16:20:00'), 'daily', 'import_into_db' );
register_activation_hook(__FILE__, 'wpisp_activation');
function wpisp_activation() {
    if (! wp_next_scheduled ( 'wpisp_performance_daily' )) {
	wp_schedule_event(strtotime('02:00'), 'daily', 'wpisp_performance_daily');
    }
}

add_action('wpisp_performance_daily', 'cron_wpisp_performance_daily');
function cron_wpisp_performance_daily() {
	// run daily maintenance
	wp_defer_term_counting( false );
	wp_defer_comment_counting( false );
	
}

function wpisp_add_perf_stats_var( $vars ){
  $vars[] = "wpisp_measure_performance";
  return $vars;
}
add_filter( 'query_vars', 'wpisp_add_perf_stats_var' );

	
 
add_action( 'wp_ajax_wpisp_saveperfresults', 'wpisp_saveperfresults' );
function wpisp_saveperfresults() {
	global $wpdb; 

	$sql = "INSERT INTO {$wpdb->prefix}scalability_pro_profiling (profileid,url,profiledt, results) VALUES (%s,%s,%s,%s)";
	$sql = $wpdb->prepare($sql,$_POST['profileid'],$_POST['url'],date('Y-m-d H:i:s'), $_POST['profileresults']);
	
	$wpdb->query($sql);
}

//add_filter('postmeta_form_keys', array());


function wpisp_remove_woo_meta_boxes() {
	$options = get_option('wpiperf_settings');
    if (isset($options['removewoosummary']) && $options['removewoosummary'] == 'remove') {
        remove_meta_box( 'woocommerce_dashboard_status', 'dashboard', 'normal');
    }
}
add_action( 'admin_menu', 'wpisp_remove_woo_meta_boxes', PHP_INT_MAX );
add_action('wp_user_dashboard_setup', 'wpisp_remove_woo_meta_boxes', PHP_INT_MAX);
add_action('wp_dashboard_setup', 'wpisp_remove_woo_meta_boxes', PHP_INT_MAX);


function wpisp_changeTaxToExists($tax) {
    global $wpdb;
    //this is complicated! Need to do recursive call to unnest the tax queries as they can be nested and convert them to WHERE EXISTS
    $sql ='';
    $originalrelation = $tax['relation']; //there's always a relation
    $relation = '';
    foreach($tax as $key=>$taxarray) {
        if ($key === 'relation') {
            continue;
        }
        if (array_key_exists('relation', $taxarray)) {
            $subtax = wpisp_changeTaxToExists($taxarray);
            $sql .= $relation . "(" . $subtax['sql']. ")";
            $tax[$key] = $subtax['tax']; //override $tax if sub changed it
            $relation = $originalrelation;
        } else {
            if ($taxarray['field'] == 'term_taxonomy_id') {
                //convert tax, field, terms, all params to WHERE EXISTS
                $op = " EXISTS ";
                if ($taxarray['operator'] === "NOT IN") {
                    $op = " NOT EXISTS ";
                }
                $sql .= $relation . $op . "
                (select * from " .$wpdb->term_relationships . " tr
                inner join " .$wpdb->term_taxonomy . " tt on tr.term_taxonomy_id = tt.term_taxonomy_id 
                where tt.taxonomy = '" . $taxarray['taxonomy'] . "' 
                and " . $wpdb->posts .".ID = tr.object_id 
                AND tr.term_taxonomy_id IN (" . implode(',', $taxarray['terms']) . "))"; // is the check for term_taxonomy_id redundant?

                $relation = $originalrelation;
                unset($tax[$key]);
            }
        }
    }
    if (!empty($sql)) {
        $sql = ' AND (' . $sql . ") "; //the first AND is because the tax query is required - it's only multiple tax queries that can be OR'd
    }
    if (count($tax) == 1) $tax = array(); // all that's left is the relation key, no actual taxonomies
    $output = array('tax'=> $tax, 'sql' => $sql);
    return $output;
}
add_filter('posts_where', 'wpintense_changetaxtoexists', 500, 2);
function wpintense_changetaxtoexists($where, $query) {
    if (!empty($query->get('wpintense_taxexists'))) {
        $where .= $query->get('wpintense_taxexists');
    }
    return $where;
}

function wpisp_improvewoocommerce_performance($query) {
    global $wpdb;
    if (is_admin()) {
        return;
    }
    if ( defined( 'DOING_CRON' ) ) {
        return;
    }
    if (wp_doing_ajax()) {
        return;
    }
    if (!$query->is_main_query()) {
        return;
    }
    $options = get_option('wpiperf_settings');
    if (isset($options['changetoexists']) && $options['changetoexists'] == 'change') {
        
        $taxqueries = $query->get('tax_query');
        if (is_array($taxqueries)) {
            $alteredtax = wpisp_changeTaxToExists($taxqueries);
            $query->set('tax_query', $alteredtax['tax']);
            $query->set('wpintense_taxexists', $alteredtax['sql']);

        }
    }
    


    if (isset($options['sortorder']) && $options['sortorder'] == 'natural') {
        $orderbycolumn = trim($query->get('orderby'));
        if (strpos($orderbycolumn, "meta_value") !== false) {
            $query->set('meta_key', '');			
        }
        
        $query->set('orderby', '');
        $query->set('order', '');
    }
    if (isset($options['calctotals']) && $options['calctotals'] == 'remove') {
        $query->set('no_found_rows', true);
        add_filter('found_posts', 'wpi_fake_pagination', 1, 2);
    }
}
add_action('pre_get_posts', 'wpisp_improvewoocommerce_performance', 9999999);

add_action('woocommerce_before_shop_loop', 'scalability_pro_fix_loop_total', PHP_INT_MAX);
function scalability_pro_fix_loop_total() {
    $options = get_option('wpiperf_settings');
    if (isset($options['calctotals']) && $options['calctotals'] == 'remove') {
        if (isset($GLOBALS['woocommerce_loop'])) {
            $GLOBALS['woocommerce_loop']['total'] = 9999;
        }    
    }
}
add_filter('posts_orderby', 'wpi_orderby_pages_callback', 10, 2);

// The posts_orderby filter
function wpi_orderby_pages_callback($orderby_statement, $wp_query) {
    $options = get_option('wpiperf_settings');
	if ($wp_query->is_main_query()) {
		if (isset($options['sortorder']) && $options['sortorder'] == 'natural') {
			return '';
		}
	}
	return $orderby_statement;
}

function fww_remove_categories_from_products_page( $args, $taxonomies ) {
	if (!is_admin()) {
		return $args;
	}
	require_once(ABSPATH . 'wp-admin/includes/screen.php');
	$screen = get_current_screen();
	if (!isset($screen->id)) {
		return $args;
	}
	if ($screen->id != 'edit-product') {
		return $args;
	}
	$options = get_option('wpiperf_settings');
	if (isset($options['toplevelcatsonly']) && $options['toplevelcatsonly'] == 'yes') {
		if (isset($args['taxonomy']) && count($args['taxonomy']) == 1) {
			if ($args['taxonomy'] == 'product_type') {
				if (!isset($args['object_ids'])) {
					$args['term_taxonomy_id'] = -1;
				} else {
					if (!is_array($args['object_ids'])) {
						$args['term_taxonomy_id'] = -1;
					}
				}
			}
			if ($args['taxonomy'] == 'product_cat') {
				if (!isset($args['object_ids'])) {
					$args['term_taxonomy_id'] = -1;
					$args['parent'] = 0;
				} else {
					if (!is_array($args['object_ids'])) {
						$args['term_taxonomy_id'] = -1;
						$args['parent'] = 0;
					}
				}
			}
		}
	}
	return $args;
}
add_filter( 'get_terms_args', 'fww_remove_categories_from_products_page', 10, 2 );
/*
function fww_remove_product_type_from_products_admin($tq) {
	if (isset($tq->query_vars) && isset($tq->query_vars['taxonomy']) && isset($tq->query_vars['taxonomy'][0]) && count($tq->query_vars['taxonomy']) == 0 && $tq->query_vars['taxonomy'][0] == 'product_type') {
		error_log("TQ:
		" . print_r($tq, true));
	}
}
add_action('pre_get_terms', 'fww_remove_product_type_from_products_admin');
*/


function scalabilitypro_changewpquerytoexists($sql){
	global $wp_query, $wpdb;
	if (is_admin()) {
		return $sql;
	}
    if (isset($wp_query) && is_main_query()) {
		if (strpos($sql, "WHEREISMAINQUERY") !== false) {
			$sql = str_replace("WHEREISMAINQUERY", "", $sql);
			$meta_query = $wp_query->meta_query;
			if ($meta_query !== false) {
				$mq = $meta_query->get_sql(
					'post',
					$wpdb->posts,
					'ID',
					null
                );
				$tmpsql = $sql;
				//if meta_query order by is being used, then abort replacing this as left join IS required
				$orderbycolumn = trim($wp_query->get('orderby'));
				if (isset($mq['join']) && !empty($mq['join']) && strpos($orderbycolumn, "meta_value") === false) {
					$metaexists = str_replace("LEFT JOIN ", "AND EXISTS( SELECT * FROM ", $mq["join"]);
					$metaexists = str_replace("INNER JOIN ", "AND EXISTS( SELECT * FROM ", $metaexists);
					$metaexists = str_replace(" ON ", " WHERE ", $metaexists);
					$metaexists = str_replace("postmeta.post_id", "postmeta.post_id " . $mq["where"] . ") ", $metaexists);
					$tmpsql = str_replace($mq["where"], "", $tmpsql);
					$tmpsql = str_replace($mq["join"], "", $tmpsql);
					$tmpsql = str_replace("WHERE 1=1", "WHERE 1=1 " . $metaexists . " ", $tmpsql);
					
					if (strpos($tmpsql, "INNER JOIN") === false && strpos($tmpsql, "LEFT JOIN") === false) {
						$tmpsql = str_replace("GROUP BY {$wpdb->posts}.ID", "", $tmpsql);
					}
					
					$tmpsql = str_replace("ORDER BY {$wpdb->posts}.post_date DESC", "", $tmpsql);
					$sql = $tmpsql;
				}		
            }
            $tax_query = '';
            if (isset($wp_query->tax_query) && isset($wp_query->tax_query->queries)) {
                $tax_query = new WP_Tax_Query($wp_query->tax_query->queries);
            }
//			if (!empty($tax_query) && $tax_query !== false && count($tax_query->queries) == 1) { // only works when ONE tax query is selected
            if (!empty($tax_query) && $tax_query !== false) { // only works when ONE tax query is selected
                $tq = $tax_query->get_sql($wpdb->posts,'ID');
                if (isset($tq['join']) && !empty($tq['join'])) {
                    $sql = str_replace($tq['join'], '', $sql);
                    $sql = str_replace($tq['where'], '', $sql);
                    /*
                    $tq['join'] = str_replace('LEFT JOIN', 'INNER JOIN', $tq['join']);
                    $joinreplace = '/'.preg_quote('INNER JOIN', '/').'/';
                    $onreplace = '/'.preg_quote('ON (', '/').'/';
                    $joinchangedtoexists = preg_replace($joinreplace, "AND EXISTS (SELECT 1 FROM", $tq["join"], 1);
                    $joinchangedtoexists = preg_replace($onreplace, "WHERE (", $joinchangedtoexists, 1);
                    $joinchangedtoexists .= " " . $tq['where'] . ')';
                    $sql = str_replace("WHERE 1=1", "WHERE 1=1 ". $joinchangedtoexists, $sql);
                    */
                    $existswhere = '';
                    foreach($wp_query->tax_query->queries as $singletaxquery) {
                        $sq = new WP_Tax_Query(array($singletaxquery));
                        $newsql = $sq->get_sql($wpdb->posts,'ID');
                        if (isset($newsql['join']) && !empty($newsql['join'])) {
                            $newsql['join'] = str_replace('LEFT JOIN', 'INNER JOIN', $newsql['join']);
                            $joinreplace = '/'.preg_quote('INNER JOIN', '/').'/';
                            $onreplace = '/'.preg_quote('ON (', '/').'/';
                            $joinchangedtoexists = preg_replace($joinreplace, "AND EXISTS (SELECT 1 FROM", $newsql["join"], 1);
                            $joinchangedtoexists = preg_replace($onreplace, "WHERE (", $joinchangedtoexists, 1);
                            $joinchangedtoexists .= " " . $newsql['where'] . ')';
                            $existswhere .= $joinchangedtoexists;
                        }
                    }
                    $sql = str_replace("WHERE 1=1", "WHERE 1=1 ". $existswhere, $sql);

				}							
			}
		}
	}
	return $sql;
}


function wpi_str_replace_first($from, $to, $subject)
{
    $from = '/'.preg_quote($from, '/').'/';

    return preg_replace($from, $to, $subject, 1);
}
function wpi_substr($str, $startertext, $endtext, $toend = false) {
	$startpos = strpos($str, $startertext);
	$endpos = strpos($str, $endtext);
	if ($startpos === false) {
		if (!$toend) {
			return "";
		} else {
			$startpos = 0;
		}
	}
	if ($endpos === false) {
		if (!$toend) {
			return "";
		} else {
			$endpos = strlen($str);
		}
	}	
	$length = $endpos - $startpos;
	return substr($str, $startpos, $length);	
//LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)  LEFT JOIN wp_term_relationships AS tt1 ON (wp_posts.ID = tt1.object_id)

}
function morescalable_posts_join( $join, $query ) {    
    global $wpdb;
	if (is_admin()) {
		return $join;
	}
    if ($query->is_main_query()) {
		$join = "WHEREISMAINQUERY" . $join; // using this to identify that this is the main query - stupid wordpress doesn't pass $query into the 'query' filter...
    }
    return $join;
    
}
$options = get_option('wpiperf_settings');
if (isset($options['changetoexists']) && $options['changetoexists'] == 'change') {
    //todo: remove these completely if new way works
//	add_filter('query', 'scalabilitypro_changewpquerytoexists', 99999999);
//	add_filter( 'posts_join', 'morescalable_posts_join', 99999999, 2 );
}


function scalabilitypro_fixwootransientcleanup ($sql) {
	global $wpdb;
	if (strpos($sql, "DELETE FROM {$wpdb->options} WHERE option_name LIKE ") !== false) {
		if (strpos($sql, " OR ") !== false) {
			return $sql;
		}
		$likestatement = wpi_substr($sql, "option_name LIKE ", "ORDER BY");
		if (!empty($likestatement)) {
			$newlike = str_replace("%", "product%", $likestatement) . " OR " . str_replace("%", "orders%", $likestatement) . " OR " . str_replace("%", "shipping%", $likestatement);
			$sql = str_replace($likestatement, $newlike, $sql);
		}
		
	}
	return $sql;
}

if (isset($options['optimisewoodeleteoptions']) && $options['optimisewoodeleteoptions'] == 'optimise') {
	add_filter('query', 'scalabilitypro_fixwootransientcleanup', 999999);
}

add_filter( 'query' , 'spro_fix_redundant_datecheck', PHP_INT_MAX);

function spro_fix_redundant_datecheck( $query ) {
	global $wpdb;


    /* some plugins do a weird thing where they check both post_status = 'published' AND they check 'post_date != '0000-00-00 00:00:00'
    The fact is that if a post is published, it will have a post date.
    The second fact is that this != check ruins the use of indexes and caused table scans.
    On one reference site (a client of WP Intense), they have about 2 million post entries and the perf improvement is:
    4 minutes 45 seconds per 100 posts in a sitemp -> 0.89 seconds

    Here's an example from the popular Yoast SEO plugin:

    SELECT wp_posts.ID
    FROM wp_posts
    WHERE wp_posts.post_status = 'publish'
    AND wp_posts.post_type = 'product'
    AND wp_posts.post_password = ''
    AND wp_posts.post_date != '0000-00-00 00:00:00'
    ORDER BY wp_posts.post_modified ASC LIMIT 100 OFFSET 483300
    */

    $checkpoststatus = "{$wpdb->posts}.post_status = 'publish'";
    $checkpostdate = "AND {$wpdb->posts}.post_date != '0000-00-00 00:00:00'";
    if (stripos($query, $checkpoststatus) !== false && stripos($query, $checkpostdate) !== false) {
        $query = str_replace($checkpostdate, "", $query); // remove the redundant check of post_date
    }
    $query = str_replace("OR {$wpdb->posts}.post_status = 'private'", "", $query);

    if (!strpos($query, "JOIN")) {
        $query = str_replace("GROUP BY {$wpdb->posts}.ID", "", $query);
    }
	return $query;
}

add_filter('query', 'spro_fix_offset_queries', PHP_INT_MAX);
function spro_fix_offset_queries($query) {
	global $wpdb;
    /* some queries perform OFFSET type queries - e.g. to grab rows 50,000 to 55,000
    There is an issue with the mysql query optimiser where it stupidly checks every row from 1 to 49,999 when it doesn't need to.
    this function fixes that.
    For example: BWP Google XML Sitemaps does this:

    SELECT p.*
    FROM wp_posts p
    WHERE p.post_status = 'publish'
    AND p.post_password = ''
    AND p.post_type = 'product'
    ORDER BY p.post_modified DESC LIMIT 600000,5000

    Rewriting it as this allows the index to be used:

    select p.*
    from 
    (
    SELECT p.id
    FROM wp_posts p
    WHERE p.post_status = 'publish'
    AND p.post_password = ''
    AND p.post_type = 'product'
    ORDER BY p.post_modified DESC LIMIT 600000,5000
    ) smallset
    join wp_posts p
    on smallset.id = p.id

    currently this is only optimised for wp_posts, and only when p.* is selected
    if others come to light in future, they could be optimised too
    */

    if (stripos($query, "SELECT p.*") !== false && stripos($query, " LIMIT ") !== false) {
        $count = 0;
        $newquery = str_ireplace("p.*", "p.ID", $query, $count);
        if ($count == 1) { // if we replaced more than 1, something is terribly wrong
            $newquery = "select p.* from (" . $newquery . ") smallset join {$wpdb->posts} p on smallset.id = p.id";
            $query = $newquery;
        }
    }

    return $query;
}


function sppro_get_cache($postid, $userid, $view, $group) {
    global $wpdb;
    $sql = $wpdb->prepare("select cachedata from " . $wpdb->prefix . "scalability_pro_cache where postid = %d and userid = %d and cacheview = %s and cachegroup = %s",
        $postid,
        $userid, 
        $view, 
        $group);

    $data = $wpdb->get_var($sql);

    return $data;
}
function sppro_set_cache($postid, $userid, $view, $group, $data) {
    global $wpdb;


    $sql = $wpdb->insert($wpdb->prefix . "scalability_pro_cache",
        array('postid' => $postid,
            'userid' => $userid, 
            'cacheview' => $view, 
            'cachegroup' => $group,
            'cachedata' => $data),
        array('%d', '%d', '%s', '%s', '%s'));
    return true;
}
function sppro_delete_cache($postid) {
    global $wpdb;
    $sql = $wpdb->prepare("delete from " . $wpdb->prefix . "scalability_pro_cache where postid = %d", $postid);

    $wpdb->query($sql);
}
function scalabilitypro_scripts() {
    wp_enqueue_script('scalabilitypro-script', plugins_url('/assets/js/scalability-pro.js', __FILE__), array(), '1.00');
}
//add_action('wp_enqueue_scripts', 'scalabilitypro_scripts');
add_filter('puc_manual_check_message-' . $SPROWidgetsUpdateChecker->slug, function($message) {
	global $SPROWidgetsUpdateChecker;
	$licensemessage = wpi_check_license($SPROWidgetsUpdateChecker->slug);
	if ($licensemessage['error'] = 'valid') {
		return $message;
	}
	return $licensemessage;
}, 10);
add_filter('puc_request_info_result-' . $SPROWidgetsUpdateChecker->slug, function($pluginInfo, $wpiresponse) {
	global $SPROWidgetsUpdateChecker;
	$licensemessage = wpi_check_license($SPROWidgetsUpdateChecker->slug);
	if ($licensemessage['error'] != 'valid') {
		$pluginInfo->version = 1.0; // setting version to 1.0 means the update checker thinks there is no update available
	}
	return $pluginInfo;
}, 10, 2);
//todo: if WPI license settins don't exist, create settings page
//todo: add link on plugin links to WPI settings page
add_filter('plugin_row_meta', function($pluginMeta, $pluginFile) {
	global $SPROWidgetsUpdateChecker;

	$isRelevant = ($pluginFile == $SPROWidgetsUpdateChecker->pluginFile)
	|| (!empty($SPROWidgetsUpdateChecker->muPluginFile) && $pluginFile == $SPROWidgetsUpdateChecker->muPluginFile);
	if ( $isRelevant && current_user_can('update_plugins') ) {
		$linkUrl = get_admin_url(null, 'admin.php?page=wpintense');
		$linkText = 'License key';
		$pluginMeta[] = sprintf('<a href="%s">%s</a>', esc_attr($linkUrl), $linkText);

		$pluginMeta[] = sprintf('<a href="%s">%s</a>', esc_attr(get_admin_url(null, 'admin.php?page=scalabilitypro')), "Settings");
	}
	return $pluginMeta;
}, 20, 2);
