<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */
 
/**
 * custom option and settings
 */

if (!function_exists('wpi_check_license')) {
    function wpintense_settings_init() {
        // Register a new setting for "wpintense" page.
        register_setting( 'wpintense', 'wpintense_options' );
    
        // Register a new section in the "wpintense" page.
        add_settings_section(
            'wpintense_section_license',
            __( 'Settings', 'wpintense' ), 'wpintense_section_license_callback',
            'wpintense'
        );
    
        // Register a new field in the "wpintense_section_license" section, inside the "wpintense" page.
        add_settings_field(
            'wpintense_field_licensekey', // As of WP 4.6 this value is used only internally.
                                    // Use $args' label_for to populate the id inside the callback.
                __( 'License Key', 'wpintense' ),
            'wpintense_field_licensekey_cb',
            'wpintense',
            'wpintense_section_license',
            array(
                'label_for'         => 'wpintense_field_licensekey',
                'class'             => 'wpintense_row',
                'wpintense_custom_data' => 'custom',
            )
        );
    }
    
    /**
     * Register our wpintense_settings_init to the admin_init action hook.
     */
    add_action( 'admin_init', 'wpintense_settings_init' );
    
    
    /**
     * Custom option and settings:
     *  - callback functions
     */
    
    
    /**
     * Developers section callback function.
     *
     * @param array $args  The settings array, defining title, id, callback.
     */
    function wpintense_section_license_callback( $args ) {
        ?>
        <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'WP Intense plugins transform the speed of your site. More speed means more money.', 'wpintense' ); ?></p>
        <?php 
        $wpilicenses = array();
        $wpilicenses[] = array('name' => 'Scalability Pro', 'license' => wpi_check_license('scalability-pro'));
        $wpilicenses[] = array('name' => 'Faster Woo Widgets', 'license' => wpi_check_license('faster-woo-widgets'));
        $wpilicenses[] = array('name' => 'Super Speedy Search', 'license' => wpi_check_license('super-speedy-search'));
        $wpilicenses[] = array('name' => 'External Images', 'license' => wpi_check_license('external-images'));
        $wpilicenses[] = array('name' => 'Auto Infinite Scroll', 'license' => wpi_check_license('auto-infinite-scroll'));
        $wpilicenses[] = array('name' => 'Price Comparison Pro', 'license' => wpi_check_license('price-comparison-pro'));

//        echo "<pre>URL: " . get_home_url() . "</pre>";
//        echo "<pre>" . print_r($wpilicenses, true) . "</pre>";
        foreach($wpilicenses as $wpilicense) {
            echo '<div class="wpipluginname">' . $wpilicense['name'] . '</div>';
            if ($wpilicense['license']['error'] == 'not installed') {
                echo '<div class="wpiinstalledstatus">Not installed</div>';
                echo '<div class="wpilicensestatus"></div>';
            } else {
                echo '<div class="wpiinstalledstatus">Installed</div>';
                echo '<div class="wpilicensestatus">' . $wpilicense['license']['error'] . '</div>';
            }
        }
        ?>
        <style>
            .wpipluginname {display:block; width:200px; float:left; clear:both; font-weight:bold}
            .wpiinstalledstatus{display:block; width:100px; float:left;}
            .wpilicensestatus {display:block; float:left;}
            #wpintense_field_licensekey {width:300px;}
        </style>
        <?php
    }

    /**
     * Pill field callbakc function.
     *
     * WordPress has magic interaction with the following keys: label_for, class.
     * - the "label_for" key value is used for the "for" attribute of the <label>.
     * - the "class" key value is used for the "class" attribute of the <tr> containing the field.
     * Note: you can add custom key value pairs to be used inside your callbacks.
     *
     * @param array $args
     */
    function wpintense_field_licensekey_cb( $args ) {
        // Get the value of the setting we've registered with register_setting()
        $options = get_option( 'wpintense_options' );
        ?>
        <input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>"
                data-custom="<?php echo esc_attr( $args['wpintense_custom_data'] ); ?>"
                name="wpintense_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
                value="<?php esc_html_e($options[ $args['label_for'] ]);?>">
        <p class="description">
            Enter your <a href="https://www.wpintense.com/my-account/">WP Intense license key</a> to enable updates.
        </p>

        <?php
    }
    
    /**
     * Add the top level menu page.
     */
    function wpintense_options_page() {
        add_menu_page(
            'WP Intense',
            'WP Intense',
            'manage_options',
            'wpintense',
            'wpintense_options_page_html'
        );
    }
    
    
    /**
     * Register our wpintense_options_page to the admin_menu action hook.
     */
    add_action( 'admin_menu', 'wpintense_options_page' );
    
    
    /**
     * Top level menu callback function
     */
    function wpintense_options_page_html() {
        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
    
        // add error/update messages
    
        // check if the user have submitted the settings
        // WordPress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            add_settings_error( 'wpintense_messages', 'wpintense_message', __( 'Settings Saved', 'wpintense' ), 'updated' );
        }
    
        // show error/update messages
        settings_errors( 'wpintense_messages' );
        ?>
        <div class="wrap">
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <form action="options.php" method="post">
                <?php
                // output security fields for the registered setting "wpintense"
                settings_fields( 'wpintense' );
                // output setting sections and their fields
                // (sections are registered for "wpintense", each field is registered to a specific section)
                do_settings_sections( 'wpintense' );
                // output save settings button
                submit_button( 'Save Settings' );
                ?>
            </form>
        </div>
        <?php
    }
    function wpi_check_license($slug) {
        $options = get_option('wpintense_options');
        $installed = false;
        if (is_dir(WP_PLUGIN_DIR . '/' . $slug)) {
            $installed = true;
        }
        if (!$installed) {
            return array('error' => 'not installed', 'message' => 'This plugin is not installed');
        }
        if (!isset($options['wpintense_field_licensekey'])) {
            return array('error' => 'invalid', 'message' =>'Enter a valid license key to enable updates for WP Intense plugins.');
        }
        $homeurl = get_home_url();
        $parsedurl = parse_url($homeurl);

        $keycheckurl = 'https://www.wpintense.com/wpiapi/check_product_key/' . $options['wpintense_field_licensekey'] . '/' . $slug . '/' . $parsedurl['host'] . '/?v=1';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_URL, $keycheckurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($curl, CURLOPT_TIMEOUT,60);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Accept: application/json", 
        "Cache-Control: no-cache"
        ));
        $keycheckdata = curl_exec($curl);
        curl_close($curl);

        //echo "<pre>" . print_r($keycheckdata, true) . "</pre>";

        $api_resp = json_decode($keycheckdata);
//        echo "<pre>" . print_r($api_resp, true) . "</pre>";

//        $api_resp = array('activated' => true, 'error' => 'No message');
        if ((bool) $api_resp->activated == true) {
            return array('error' => 'valid', 'message' => 'Plugin installed with valid key');
        }
        if ($api_resp->error == 'invalid') {
            return array('error' => 'invalid', 'message' => 'Enter a valid license key to enable updates for WP Intense plugins.');
        }
        if ($api_resp->error == 'expired') {
            return array('error' => 'expired', 'message' => 'License expired - please purchase a license extension to enable updates.');
        }
        if ($api_resp->error == 'exceeded') {
            return array('error' => 'exceeded', 'message' => 'Website limit exceeded - please purchase additional licenses or deactivate a previous website license to enable updates.');
        }
        

//        return array('error' => $api_resp->error, 'message' => 'Enter a valid license key to enable updates for WP Intense plugins.');
        return array('error' => $api_resp->error, 'message' => $api_resp->error);
    }
}