function wpisp_saveprofile() {
	//console.log(response);
	qm = jQuery('#sp_monitoring iframe').contents();
	qm = jQuery('#qm', qm).wrap('p').html();

	profile = jQuery('#sp_monitoring iframe').data('profile');
	tindex = jQuery('#sp_monitoring iframe').data('index');
	url = jQuery('#sp_monitoring iframe').attr('src');
	if (jQuery('#qm-overview table tr td:first-child', qm).length > 0) {
		profileresults[profile, url] = jQuery('#qm-overview table tr td:first-child', qm).html().split('<br>')[0];
	} else {
		profileresults[profile, url] = -1; // could not grab profile results for some reason (timeout?)
	}
	
	jQuery('#p' + tindex + ' .' + profile).html(parseFloat(profileresults[url]).toFixed(2) + 's');
	
	jQuery.post(ajaxurl, 
		{
			action: 'wpisp_saveperfresults',
			profileid: profile,
			url: url,
			profileresults: qm
		},
		function(response) {
			wpiindex = jQuery('#sp_monitoring iframe').data('index');
			wpiindex++;
			jQuery('#sp_monitoring iframe').remove();
			if (wpiindex < wpisiteurls.length) {
				url = window.wpisiteurls[wpiindex];
				if (profile == 'before') {
					jQuery('#sp_results').append('<div class="perfrow" id="p' + wpiindex + '"><div class="url">' + url + '</div><div class="after2"></div><div class="after1"></div><div class="before"></div></div>');					
					jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + profile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');
				} else {
					jQuery.get(url, function() {
						jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + profile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');						
					});					
				}
			} else {
				scalabilityprostage++;
				if (scalabilityprostage == 2) {
					jQuery('#sp_progress').append('<h5>Adding indexes</h5>');
					jQuery('#sp_progress').append('<p>Optimising free-text-search indexes</p>');
					scalability_pro_create_ftindex();
				}
				console.log ('stage ' + scalabilityprostage);
				if (scalabilityprostage >= 5) {
					console.log(profileresults);
					//todo: POP UP ASKING IF THEY WANT TO SEND RESULTS TO US
					jQuery('#sp_monitoring').append('<p>Now click Submit Report to WPI to submit your performance report and plugin list to us to help us optimise Scalability Pro further:</p>');
					jQuery('#sp_monitoring').append('<p><a class="button" href="javascript:void(0);" id="submitreport">Submit Report to WPI</a></p>');
					jQuery('#sp_monitoring').append('<p>or <a href="javascript:void(0);" id="closereport">cancel</a></p>');
					jQuery.post(ajaxurl, 
						{
							action: 'wpisp_saveprofile',
							profileresults: profileresults,
							profilename: profile
						},
						function(response) {console.log ('saved results locally'); console.log(response); console.log(profileresults);}
					);
					wpisp_showprofile();
				}
				
			}

			
		}
	);
}
var scalabilityprostage = 1;
jQuery(document).on('click', '#startoptimisation', function() {
	jQuery('#startoptimisation').html('Processing...');
	jQuery('#startoptimisation').attr('disabled', true);
	jQuery('#sp_progress').append('<h5>Measuring performance without indexes</h5>');
	var wpiindex = 0; 
	var wpiprofile = 'before';
	
	jQuery.post(
		ajaxurl,
		{action: 'wpisp_startmeasurements'},
		function (response) {
			url = wpisiteurls[wpiindex];			
			jQuery('#sp_results').append('<div class="perfrow"><div class="url">URL</div><div class="after2"></div><div class="after1">Speed After</div><div class="before">Speed Before</div></div>');
			jQuery('#sp_results').append('<div class="perfrow" id="p' + wpiindex + '"><div class="url">' + url + '</div><div class="after2"></div><div class="after1"></div><div class="before"></div></div>');
			jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + wpiprofile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');
		}
	);
});

jQuery(document).on('click', '#wpidropfulltextindex', function () {
	jQuery('#wpidropfulltextindex').text('Working...');
	jQuery.post(
			ajaxurl,
			{action: 'wpi_dropfulltextindex'},
			function (response) {
				alert('Full text index dropped');
				jQuery('#wpidropfulltextindex').text('Complete');
				console.log(response);
			}
	);
});
function scalability_pro_create_ftindex (){
	jQuery.ajax({
		url: ajaxurl,
		type: "POST",
		dataType: "json",
		data: {action: 'wpi_createfulltextindex'},
		timeout: 15000,
		success: function(response) { 
			if (scalabilityprostage == 2) {
				scalabilityprostage++;
				jQuery('#sp_progress').append('Full text indexes created\n');
				jQuery('#sp_progress').append('Optimising b-tree indexes\n');
				scalability_pro_create_normalindex();
				
			} else {
				alert('Full text index created');
			}
			jQuery('#wpicreatefulltextindex').text('Complete');
		},
		error: function(x, t, m) {
			if(t==="timeout") {
				jQuery('#sp_progress').append('Working on creating fulltext indexes - please wait...\n');
				scalability_pro_create_ftindex();
			} else {
				jQuery('#sp_progress').append('Error\n');
				jQuery('#sp_progress').append(t + '\n\n');
			}
		}
	});
}
function scalability_pro_create_normalindex (){
	jQuery.ajax({
		url: ajaxurl,
		type: "POST",
		dataType: "json",
		data: {action: 'wpi_createindexes'},
		timeout: 15000,
		success: function(response) { 
			if (scalabilityprostage == 3) {
				scalabilityprostage++;
				
				jQuery.post(
						ajaxurl,
						{action: 'wpisp_startoptimisation'},
						function (response) {
							scalabilityprostage++;
							wpiindex = 0;
							url = wpisiteurls[wpiindex];
							wpiprofile = 'after1';
							jQuery.get(url, function() {
								jQuery('#sp_monitoring').append('<iframe data-index="' + wpiindex + '" data-profile="' + wpiprofile + '" src="' + url + '" onload="wpisp_saveprofile();"></iframe>');
							});
						}
				);
				
				
			} else {
				console.log(response);
				alert('B-tree indexes created');
			}
			jQuery('#wpicreateindexes').text('Complete');
		},
		error: function(x, t, m) {
			if(t==="timeout") {
				jQuery('#sp_progress').append('<br>Working on creating b-tree indexes - please wait...');
				scalability_pro_create_normalindex();
			} else {
				jQuery('#sp_progress').append('<4>Error</h4>');
				jQuery('#sp_progress').append('<pre>' + t + '</pre>');
			}
		}
	});
}
jQuery(document).on('click', '#wpicreatefulltextindex', function () {
	jQuery('#wpicreatefulltextindex').text('Working...');
	scalability_pro_create_ftindex();

});
jQuery(document).on('click', '#wpidropindexes', function () {
	jQuery('#wpidropindexes').text('Working...');
	jQuery.post(
			ajaxurl,
			{action: 'wpi_dropindexes'},
			function (response) {
				alert('Indexes dropped');
				jQuery('#wpidropindexes').text('Complete');
				console.log(response);
			}
	);

});
jQuery(document).on('click', '#wpicreateindexes', function () {
	jQuery('#wpicreateindexes').text('Working...');
	scalability_pro_create_normalindex();

});
jQuery(document).on('click', '#submitreport', function() {
	//todo: submit profile to WPI
	
	jQuery('#wpisp-first-run').css('display', 'none');
	wpisp_showprofile();
	
});
jQuery(document).on('click', '#closereport', function() {
	jQuery('#wpisp-first-run').css('display', 'none');
	wpisp_showprofile();
});
function wpisp_showprofile() {
	jQuery('#wpisp_perf_profile').css('display', 'block');
	jQuery.each(profileresults , function( index, obj ) {
		var perfout = '';
		perfout += '<div class="perfhrow"><div class="perfurl"></div>';
		jQuery.each(obj, function( key, value ) {
			perfout += '<div class="perfurlresult">' + key + '</div>';
			return false;
		});
		perfout += '</div>';
		jQuery('#perfprofile').append(perfout);		
		return false;
	});
	jQuery.each(profileresults , function( index, obj ) {
		var perfout = '';
		perfout += '<div class="perfhrow"><div class="perfurl">' + index + '</div>';
		jQuery.each(obj, function( key, value ) {
			perfout += '<div class="perfurlresult">' + value + '</div>';
		});
		perfout += '</div>';
		jQuery('#perfprofile').append(perfout);		
	});
}