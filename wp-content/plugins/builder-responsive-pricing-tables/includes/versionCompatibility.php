<?php
if(!defined('ABSPATH')) die('Direct access of plugin file not allowed');

function wbrpt_resolve_version_compatibility( &$value )
{
    $pluginVersion = wbrpt_get_plugin_data( 'Version' );
    $valueVersion = '1.5.1';
    foreach( $value as $instance ) {
        if( isset( $instance[ 'PluginVersion' ] ) ) {
            $valueVersion = $instance[ 'PluginVersion' ];
            break;
        }
    }
    if( $pluginVersion == $valueVersion ) {
        return;
    }
    if( version_compare( $valueVersion, '1.5.1', '<=' ) ) {
        /* Backward compatibility with older versions without versions */
        if( isset( $value[ 'PlanName' ] ) || isset( $value[ 'FeatureName' ] ) ) {
            $value = array( 1 => $value );
        }
        /* Backward compatibility with older versions with separate fields for tooltips */
        foreach( $value as $instanceIndex => $instance ) {
            if( isset( $instance[ 'FeatureTooltipType' ] ) ) {
                foreach( $instance[ 'FeatureTooltipType' ] as $featureID => $featureTooltipType ) {
                    if( $featureTooltipType == 3 ) {
                        $value[ $instanceIndex ][ 'FeatureTooltipType' ][ $featureID ] = '2';
                        $value[ $instanceIndex ][ 'FeatureTooltipColumns' ][ $featureID ] = true;
                    }
                }
            }
            if( isset( $instance[ 'FeatureValueTooltipType' ] ) ) {
                foreach( $instance[ 'FeatureValueTooltipType' ] as $planID => $featureList ) {
                    foreach( $featureList as $featureID => $featureValueTooltipType ) {
                        if( $featureValueTooltipType == 3 ) {
                            $value[ $instanceIndex ][ 'FeatureValueTooltipType' ][ $planID ][ $featureID ] = '2';
                            $value[ $instanceIndex ][ 'FeatureValueTooltipColumns' ][ $planID ][ $featureID ] = true;
                        }
                    }
                }
            }
            if( isset( $instance[ 'FeatureTooltipText' ] ) || isset( $instance[ 'FeatureTooltipList' ] ) ) {
                $value[ $instanceIndex ][ 'FeatureTooltipContent' ] = array_replace_recursive( ( isset( $instance[ 'FeatureTooltipText' ] ) ? $instance[ 'FeatureTooltipText' ] : array() ), ( isset( $instance[ 'FeatureTooltipList' ] ) ? $instance[ 'FeatureTooltipList' ] : array() ) );
            }
            unset( $value[ $instanceIndex ][ 'FeatureTooltipText' ] );
            unset( $value[ $instanceIndex ][ 'FeatureTooltipList' ] );
            if( isset( $instance[ 'FeatureValueTooltipText' ] ) || isset( $instance[ 'FeatureValueTooltipList' ] ) ) {
                $value[ $instanceIndex ][ 'FeatureValueTooltipContent' ] = array_replace_recursive( ( isset( $instance[ 'FeatureValueTooltipText' ] ) ? $instance[ 'FeatureValueTooltipText' ] : array() ), ( isset( $instance[ 'FeatureValueTooltipList' ] ) ? $instance[ 'FeatureValueTooltipList' ] : array() ) );
            }
            unset( $value[ $instanceIndex ][ 'FeatureValueTooltipText' ] );
            unset( $value[ $instanceIndex ][ 'FeatureValueTooltipList' ] );
        }
    }
}
