<?php
if(!defined('ABSPATH')) die('Direct access of plugin file not allowed');

class wbrptColorFunctions
{
    static function lighten( $color, $amount )
    {
        return self::adjust( $color, 'lightness', $amount );
    }

    static function darken( $color, $amount )
    {
        return self::adjust( $color, 'lightness', -1 * $amount );
    }

    static function highlight( $color, $perc )
    {
        return ( self::lightness( $color ) < 30 ) ? self::lighten( $color, $perc ) : self::darken( $color, $perc );
    }

    static function downlight( $color, $perc )
    {
        return ( self::lightness( $color ) < 70 ) ? self::lighten( $color, $perc ) : self::darken( $color, $perc );
    }

    static function lightness( $color, $value = null )
    {
        $hsl = self::rgbToHsl( self::hexToRgb( $color ) );
        if( $value ) {
          $hsl[ 'l' ] = $value;
          return self::rgbToHex( self::hslToRgb( $hsl ) );
        }
        return $hsl[ 'l' ];
    }

    static function adjust( $color, $prop, $amount )
    {
        $hsl = self::rgbToHsl( self::hexToRgb( $color ) );
        $propList = array( 'hue' => 'h', 'saturation' => 's', 'lightness' => 'l' );
        $prop = $propList[ $prop ];
        $val = str_replace( '%', '', $amount );
        if( '%' == substr( $amount, -1 ) ) {
          $val = 'l' == $prop && $val > 0
            ? (100 - $hsl[$prop]) * $val
            : $hsl[$prop] * $val;
        }
        $hsl[$prop] += $val;
        if( $prop == 's' || $prop == 'l' ) {
            if( $hsl[$prop] > 100 ) {
                $hsl[$prop] = 100;
            } else if( $hsl[$prop] < 0 ) {
                $hsl[$prop] = 0;
            }
        } else if( $prop == 'h' ) {
            if( $hsl[$prop] > 360 ) {
                $hsl[$prop] = 360;
            } else if( $hsl[$prop] < 0 ) {
                $hsl[$prop] = 0;
            }
        }
        return self::rgbToHex( self::hslToRgb( $hsl ) );
    }

    static function rgba( $color, $alpha = null )
    {
        if( $alpha === null ) {
            return $color;
        }
        $rgb = self::hexToRgb( $color );
        return 'rgba(' . $rgb[ 'r' ] . ',' . $rgb[ 'g' ] . ',' . $rgb[ 'b' ] . ',' . round( $alpha, 3 ) . ')';
    }

    static function rgbToHex( $rgb )
    {
        return sprintf( '#%02x%02x%02x', $rgb[ 'r' ], $rgb[ 'g' ], $rgb[ 'b' ] );
    }

    static function hexToRgb( $hex )
    {
       $hex = str_replace('#', '', $hex);
       if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
       } else {
          $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
       }
       $rgb = array( 'r' => $r, 'g' => $g, 'b' => $b);
       return $rgb;
    }

    static function rgbToHsl( $color )
    {
        $r = $color[ 'r' ];
        $g = $color[ 'g' ];
        $b = $color[ 'b' ];
        $r /= 255;
        $g /= 255;
        $b /= 255;
        $max = max($r, $g, $b);
        $min = min($r, $g, $b);
        $l = ($max + $min) / 2;
        if ($max == $min) {
            $h = $s = 0;
        } else {
            $d = $max - $min;
            $s = $l > 0.5 ? $d / (2 - $max - $min) : $d / ($max + $min);
            switch ($max) {
                case $r:
                    $h = ($g - $b) / $d + ($g < $b ? 6 : 0);
                    break;
                case $g:
                    $h = ($b - $r) / $d + 2;
                    break;
                case $b:
                    $h = ($r - $g) / $d + 4;
                    break;
            }
            $h /= 6;
        }
        $h = $h * 360;
        $s = $s * 100;
        $l = $l * 100;
        return array( 'h' => $h, 's' => $s, 'l' => $l );
    }

    static function hslToRgb( $color )
    {
        $h = $color[ 'h' ];
        $s = $color[ 's' ];
        $l = $color[ 'l' ];
        $h /= 60;
        if ($h < 0) $h = 6 - fmod(-$h, 6);
        $h = fmod($h, 6);
        $s = max(0, min(1, $s / 100));
        $l = max(0, min(1, $l / 100));
        $c = (1 - abs((2 * $l) - 1)) * $s;
        $x = $c * (1 - abs(fmod($h, 2) - 1));
        if ($h < 1) {
            $r = $c;
            $g = $x;
            $b = 0;
        } elseif ($h < 2) {
            $r = $x;
            $g = $c;
            $b = 0;
        } elseif ($h < 3) {
            $r = 0;
            $g = $c;
            $b = $x;
        } elseif ($h < 4) {
            $r = 0;
            $g = $x;
            $b = $c;
        } elseif ($h < 5) {
            $r = $x;
            $g = 0;
            $b = $c;
        } else {
            $r = $c;
            $g = 0;
            $b = $x;
        }
        $m = $l - $c / 2;
        $r = intval( round(($r + $m) * 255) );
        $g = intval( round(($g + $m) * 255) );
        $b = intval( round(($b + $m) * 255) );
        return array( 'r' => $r, 'g' => $g, 'b' => $b );
    }
}