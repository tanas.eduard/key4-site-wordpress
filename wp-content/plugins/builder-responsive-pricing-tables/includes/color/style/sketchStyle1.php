<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#fff';
  $default['$block-border-color']='#d1d1d1';
  $default['$title-color']='#fff';
  $default['$title-background-color']='$theme-color';
  $default['$price-color']='$title-background-color';
  $default['$list-color']='#696969';
  $default['$button-color']='$title-background-color';
  $default['$button-background-color']='$block-background-color';
  $default['$button-border-color']='$title-background-color';
  $default['$hover-button-color']='highlight-color($button-color, 7)';
  $default['$hover-button-background-color']='$button-background-color';
  $default['$hover-button-border-color']='highlight-color($button-border-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-block-border-color']='lighten($theme-color, 15)';
  $default['$active-title-color']='$title-color';
  $default['$active-title-background-color']='$title-background-color';
  $default['$active-price-color']='$active-block-border-color';
  $default['$active-list-color']='$active-title-background-color';
  $default['$active-button-color']='$active-title-color';
  $default['$active-button-background-color']='$active-title-background-color';
  $default['$active-button-border-color']='$active-button-background-color';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
  $default['$active-hover-button-border-color']='highlight-color($active-button-border-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#fff';
  if(!isset($variables['$block-border-color'])) $variables['$block-border-color']='#d1d1d1';
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$title-background-color'])) $variables['$title-background-color']=$variables['$theme-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$title-background-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']='#696969';
  if(!isset($variables['$button-color'])) $variables['$button-color']=$variables['$title-background-color'];
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$button-border-color'])) $variables['$button-border-color']=$variables['$title-background-color'];
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=wbrptColorFunctions::highlight($variables['$button-color'], 7);
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$hover-button-border-color'])) $variables['$hover-button-border-color']=wbrptColorFunctions::highlight($variables['$button-border-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-block-border-color'])) $variables['$active-block-border-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 15);
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-title-background-color'])) $variables['$active-title-background-color']=$variables['$title-background-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$active-block-border-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$active-title-background-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$active-title-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$active-title-background-color'];
  if(!isset($variables['$active-button-border-color'])) $variables['$active-button-border-color']=$variables['$active-button-background-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
  if(!isset($variables['$active-hover-button-border-color'])) $variables['$active-hover-button-border-color']=wbrptColorFunctions::highlight($variables['$active-button-border-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.sketch-style1-<?php echo $colorClass; ?> .pt-block > .pt-back,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-block > .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-title,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-title .pt-back:before,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-title .pt-back:before {
  background-color: <?php echo $variables['$title-background-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-price,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-btn,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-btn:hover,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-btn:hover,
.sketch-style1-<?php echo $colorClass; ?> .pt-btn:focus,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-list,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) > .pt-back,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) > .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected > .pt-back,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected > .pt-back,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover > .pt-back,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover > .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  border-color: <?php echo $variables['$active-block-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-title,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-title,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-title .pt-back:before,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-title .pt-back:before,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title .pt-back:before,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title .pt-back:before {
  background-color: <?php echo $variables['$active-title-background-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-price,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-price,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-list,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-list,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  border-color: <?php echo $variables['$active-button-border-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.sketch-style1[class*='pt-animation-'] .sketch-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$active-hover-button-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) > .pt-back,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) > .pt-back,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected > .pt-back,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected > .pt-back {
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title .pt-back:before,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title .pt-back:before,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title .pt-back:before,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-title .pt-back:before {
  background-color: <?php echo $variables['$title-background-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.sketch-style1 .pt-cols .sketch-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected > .pt-back,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected > .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.sketch-style1[class*='pt-animation-']:hover .sketch-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.sketch-style1 .sketch-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
