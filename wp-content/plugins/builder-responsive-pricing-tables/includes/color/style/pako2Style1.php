<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#363636';
  $default['$block-box-shadow-color']='#666';
  $default['$ribbon-color']='#e5d202';
  $default['$ribbon-background-color']='$theme-color';
  $default['$title-color']='#fff';
  $default['$content-background-color']='#fff';
  $default['$price-color']='#fff';
  $default['$price-background-color']='$theme-color';
  $default['$price-border-color']='lighten($price-background-color, 15)';
  $default['$list-color']='#6f6f6f';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$theme-color';
  $default['$button-border-color']='lighten($button-background-color, 15)';
  $default['$hover-button-color']='$button-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$hover-button-border-color']='lighten($hover-button-background-color, 15)';
  $default['$tooltip-background-color']='lightness($theme-color, 18)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-block-box-shadow-color']='$block-box-shadow-color';
  $default['$active-ribbon-color']='$ribbon-color';
  $default['$active-ribbon-background-color']='$ribbon-background-color';
  $default['$active-title-color']='$title-color';
  $default['$active-content-background-color']='$active-block-background-color';
  $default['$active-price-color']='$price-color';
  $default['$active-price-background-color']='$price-background-color';
  $default['$active-price-border-color']='lighten($active-price-background-color, 15)';
  $default['$active-list-color']='#fff';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-button-border-color']='lighten($active-button-background-color, 15)';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
  $default['$active-hover-button-border-color']='lighten($active-hover-button-background-color, 15)';
  $default['$inactive-button-color']='$button-color';
  $default['$inactive-button-background-color']='#b4b3b3';
  $default['$inactive-button-border-color']='#c7c7c7';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#363636';
  if(!isset($variables['$block-box-shadow-color'])) $variables['$block-box-shadow-color']='#666';
  if(!isset($variables['$ribbon-color'])) $variables['$ribbon-color']='#e5d202';
  if(!isset($variables['$ribbon-background-color'])) $variables['$ribbon-background-color']=$variables['$theme-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$content-background-color'])) $variables['$content-background-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$price-background-color'])) $variables['$price-background-color']=$variables['$theme-color'];
  if(!isset($variables['$price-border-color'])) $variables['$price-border-color']=wbrptColorFunctions::lighten($variables['$price-background-color'], 15);
  if(!isset($variables['$list-color'])) $variables['$list-color']='#6f6f6f';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$theme-color'];
  if(!isset($variables['$button-border-color'])) $variables['$button-border-color']=wbrptColorFunctions::lighten($variables['$button-background-color'], 15);
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=$variables['$button-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$hover-button-border-color'])) $variables['$hover-button-border-color']=wbrptColorFunctions::lighten($variables['$hover-button-background-color'], 15);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 18);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']=$variables['$block-box-shadow-color'];
  if(!isset($variables['$active-ribbon-color'])) $variables['$active-ribbon-color']=$variables['$ribbon-color'];
  if(!isset($variables['$active-ribbon-background-color'])) $variables['$active-ribbon-background-color']=$variables['$ribbon-background-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-content-background-color'])) $variables['$active-content-background-color']=$variables['$active-block-background-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-price-background-color'])) $variables['$active-price-background-color']=$variables['$price-background-color'];
  if(!isset($variables['$active-price-border-color'])) $variables['$active-price-border-color']=wbrptColorFunctions::lighten($variables['$active-price-background-color'], 15);
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-button-border-color'])) $variables['$active-button-border-color']=wbrptColorFunctions::lighten($variables['$active-button-background-color'], 15);
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
  if(!isset($variables['$active-hover-button-border-color'])) $variables['$active-hover-button-border-color']=wbrptColorFunctions::lighten($variables['$active-hover-button-background-color'], 15);
  if(!isset($variables['$inactive-button-color'])) $variables['$inactive-button-color']=$variables['$button-color'];
  if(!isset($variables['$inactive-button-background-color'])) $variables['$inactive-button-background-color']='#b4b3b3';
  if(!isset($variables['$inactive-button-border-color'])) $variables['$inactive-button-border-color']='#c7c7c7';
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.pako2-style1-<?php echo $colorClass; ?> .pt-back,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  -webkit-box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-ribbon,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-ribbon {
  color: <?php echo $variables['$ribbon-color']; ?>;
  background-color: <?php echo $variables['$ribbon-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-ribbon .pt-arrow:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-ribbon .pt-arrow:before,
.pako2-style1-<?php echo $colorClass; ?> .pt-ribbon .pt-arrow:after,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-ribbon .pt-arrow:after {
  background-color: <?php echo $variables['$ribbon-background-color']; ?>;
  border-color: <?php echo $variables['$ribbon-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-title,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-btn-container:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-btn-container:before {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-btn,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-btn:hover,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-btn:hover,
.pako2-style1-<?php echo $colorClass; ?> .pt-btn:focus,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-list-container:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-list-container:before {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-list,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-price-container:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-price-container:before {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-price-container:after,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-price-container:after {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-price-wrapper:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-price-wrapper:before {
  background-color: <?php echo $variables['$price-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-price-block,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-price-block {
  color: <?php echo $variables['$price-color']; ?>;
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:hover,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:hover,
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:focus,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:focus {
  color: <?php echo $variables['$inactive-button-color']; ?>;
  background-color: <?php echo $variables['$inactive-button-background-color']; ?>;
  border-color: <?php echo $variables['$inactive-button-border-color']; ?>;
  cursor: default;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  -webkit-box-shadow: 0 2px 5px 2px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 2px 5px 2px <?php echo $variables['$active-block-box-shadow-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-ribbon,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-ribbon {
  color: <?php echo $variables['$active-ribbon-color']; ?>;
  background-color: <?php echo $variables['$active-ribbon-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon .pt-arrow:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon .pt-arrow:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-ribbon .pt-arrow:before,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-ribbon .pt-arrow:before,
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon .pt-arrow:after,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon .pt-arrow:after,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-ribbon .pt-arrow:after,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-ribbon .pt-arrow:after {
  background-color: <?php echo $variables['$active-ribbon-background-color']; ?>;
  border-color: <?php echo $variables['$active-ribbon-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-container:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-container:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-container:before,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-container:before {
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-container:after,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-container:after,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-container:after,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-container:after {
  background-color: <?php echo $variables['$active-content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-wrapper:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-wrapper:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-wrapper:before,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-wrapper:before {
  background-color: <?php echo $variables['$active-price-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-block,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-block {
  color: <?php echo $variables['$active-price-color']; ?>;
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-list-container:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-list-container:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list-container:before,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list-container:before {
  background-color: <?php echo $variables['$active-content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn-container:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn-container:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn-container:before,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn-container:before {
  background-color: <?php echo $variables['$active-content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  color: <?php echo $variables['$active-button-color']; ?>;
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  border-color: <?php echo $variables['$active-button-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.pako2-style1[class*='pt-animation-'] .pako2-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$active-hover-button-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  -webkit-box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-ribbon,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-ribbon,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-ribbon,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon {
  color: <?php echo $variables['$ribbon-color']; ?>;
  background-color: <?php echo $variables['$ribbon-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-ribbon .pt-arrow:before,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-ribbon .pt-arrow:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-ribbon .pt-arrow:before,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon .pt-arrow:before,
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-ribbon .pt-arrow:after,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-ribbon .pt-arrow:after,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-ribbon .pt-arrow:after,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-ribbon .pt-arrow:after {
  background-color: <?php echo $variables['$ribbon-background-color']; ?>;
  border-color: <?php echo $variables['$ribbon-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-container:before,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-container:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-container:before,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-container:before {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-container:after,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-container:after,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-container:after,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-container:after {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-wrapper:before,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-wrapper:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-wrapper:before,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-wrapper:before {
  background-color: <?php echo $variables['$price-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-block,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-block,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-block,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-price-block {
  color: <?php echo $variables['$price-color']; ?>;
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list-container:before,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list-container:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list-container:before,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-list-container:before {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn-container:before,
.pako2-style1 .pt-cols .pako2-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn-container:before,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn-container:before,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn-container:before {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako2-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.pako2-style1[class*='pt-animation-']:hover .pako2-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.pako2-style1 .pako2-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
