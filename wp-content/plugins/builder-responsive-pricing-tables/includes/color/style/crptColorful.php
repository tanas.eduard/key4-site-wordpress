<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-colorful-<?php echo $colorClass; ?> .pt-tabs .pt-tab {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-tabs .pt-tab:before {
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-btn,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-btn {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-btn:hover .pt-select,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-btn:hover .pt-select,
.crpt-colorful-<?php echo $colorClass; ?> .pt-btn:focus .pt-select,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-btn:focus .pt-select {
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-1 .pt-col:nth-child(1) .pt-block .pt-content,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(2) .pt-block .pt-content,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(3) .pt-block .pt-content,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(4) .pt-block .pt-content,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(5) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 0); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-1 .pt-col:nth-child(1) .pt-block .pt-footer,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(1) .pt-block .pt-footer,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(1) .pt-block .pt-footer,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(1) .pt-block .pt-footer,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(1) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 22); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-1 .pt-col:nth-child(1) .pt-block .pt-btn,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(1) .pt-block .pt-btn,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(1) .pt-block .pt-btn,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(1) .pt-block .pt-btn,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(1) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 14); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(1) .pt-block .pt-content,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(2) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 7); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(2) .pt-block .pt-footer,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(3) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 15); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(2) .pt-block .pt-btn,
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(3) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 7); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(1) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 9.333333333333334); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(2) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 4.666666666666667); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(2) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 17.333333333333336); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(2) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 9.333333333333334); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(3) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 12.666666666666668); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(3) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 4.666666666666667); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(1) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10.5); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(2) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18.5); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(2) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10.5); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(3) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 3.5); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(4) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 11.5); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(4) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 3.5); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(1) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 11.2); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(2) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 8.399999999999999); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(2) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 19.2); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(2) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 11.2); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(3) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5.6); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(3) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 16.4); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(3) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 8.399999999999999); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(4) .pt-block .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 2.8); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(4) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 13.6); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(4) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5.6); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(5) .pt-block .pt-footer {
  border-top: 1px solid <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10.8); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(5) .pt-block .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 2.8); ?>;
}
.crpt-colorful .pt-cols .pt-cols-main .pt-col.crpt-colorful-<?php echo $colorClass; ?> .pt-block .pt-content {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-colorful .pt-cols .pt-cols-main .pt-col.crpt-colorful-<?php echo $colorClass; ?> .pt-block .pt-btn {
  border-top-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 9); ?>;
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 1); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-list .pt-list-item + .pt-list-item,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-list .pt-list-item + .pt-list-item {
  border-top-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-side {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-side .pt-side-text {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols .pt-cols-side .pt-tabs .pt-tab + .pt-tab {
  border-left-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-side .pt-list-item + .pt-list-item .pt-border {
  border-top: 1px dashed <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-side .pt-selection .pt-arrow {
  border-color: transparent <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?> transparent transparent;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-cols-side .pt-selection .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 18); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-colorful .crpt-colorful-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
