<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.rmsbpt-extra-<?php echo $colorClass; ?>,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-block,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-block {
  background: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-price-container,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-price-container {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-selected .pt-text,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-selected .pt-text,
.rmsbpt-extra-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-text,
.rmsbpt-extra.pt-animation-default .rmsbpt-extra-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-text,
.rmsbpt-extra-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-text,
.rmsbpt-extra.pt-animation-1 .rmsbpt-extra-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-text {
  text-shadow: 0 0 6px <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 20); ?>;
}
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.rmsbpt-extra .rmsbpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
