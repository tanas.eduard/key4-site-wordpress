<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style2-<?php echo $colorClass; ?> .pt-head:after,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-head:after {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style2-<?php echo $colorClass; ?> .pt-title,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style2-<?php echo $colorClass; ?> .pt-btn,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style2-<?php echo $colorClass; ?> .pt-list .pt-list-sub-text,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-list .pt-list-sub-text {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style2 .smoozy-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
