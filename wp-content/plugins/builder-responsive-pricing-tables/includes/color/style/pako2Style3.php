<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#363636';
  $default['$block-box-shadow-color']='#666';
  $default['$title-color']='#fff';
  $default['$content-background-color']='$theme-color';
  $default['$price-color']='#fff';
  $default['$list-color']='#6f6f6f';
  $default['$list-background-color']='#fff';
  $default['$list-border-color']='lighten($content-background-color, 15)';
  $default['$button-color']='#fff';
  $default['$hover-button-color']='$button-color';
  $default['$tooltip-background-color']='lightness($theme-color, 18)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-block-box-shadow-color']='$block-box-shadow-color';
  $default['$active-title-color']='$title-color';
  $default['$active-content-background-color']='$content-background-color';
  $default['$active-price-color']='$price-color';
  $default['$active-list-color']='#fff';
  $default['$active-list-border-color']='lighten($active-content-background-color, 15)';
  $default['$active-button-color']='$button-color';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$inactive-button-color']='#ccc';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#363636';
  if(!isset($variables['$block-box-shadow-color'])) $variables['$block-box-shadow-color']='#666';
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$content-background-color'])) $variables['$content-background-color']=$variables['$theme-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']='#6f6f6f';
  if(!isset($variables['$list-background-color'])) $variables['$list-background-color']='#fff';
  if(!isset($variables['$list-border-color'])) $variables['$list-border-color']=wbrptColorFunctions::lighten($variables['$content-background-color'], 15);
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=$variables['$button-color'];
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 18);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']=$variables['$block-box-shadow-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-content-background-color'])) $variables['$active-content-background-color']=$variables['$content-background-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
  if(!isset($variables['$active-list-border-color'])) $variables['$active-list-border-color']=wbrptColorFunctions::lighten($variables['$active-content-background-color'], 15);
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$inactive-button-color'])) $variables['$inactive-button-color']='#ccc';
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.pako2-style3-<?php echo $colorClass; ?> .pt-back,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  -webkit-box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-title,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-content > .pt-slope-container .pt-slope:before {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-list-container .pt-slope:before,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-list-container .pt-slope:before {
  background-color: <?php echo $variables['$list-background-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-price,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-btn,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.pako2-style3-<?php echo $colorClass; ?> .pt-btn:focus,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-list,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:hover,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:hover,
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:focus,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:focus {
  color: <?php echo $variables['$inactive-button-color']; ?>;
  cursor: default;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  -webkit-box-shadow: 0 2px 5px 2px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 2px 5px 2px <?php echo $variables['$active-block-box-shadow-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content > .pt-slope-container .pt-slope:before {
  background-color: <?php echo $variables['$active-content-background-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-price,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-price,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-list-container .pt-slope:before,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-list-container .pt-slope:before,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list-container .pt-slope:before,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list-container .pt-slope:before {
  border-color: <?php echo $variables['$active-list-border-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  color: <?php echo $variables['$active-button-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.pako2-style3[class*='pt-animation-'] .pako2-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  -webkit-box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 2px 5px 2px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-content > .pt-slope-container .pt-slope:before,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-content > .pt-slope-container .pt-slope:before {
  background-color: <?php echo $variables['$content-background-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list-container .pt-slope:before,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list-container .pt-slope:before,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list-container .pt-slope:before,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-list-container .pt-slope:before {
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako2-style3 .pt-cols .pako2-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako2-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.pako2-style3[class*='pt-animation-']:hover .pako2-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.pako2-style3 .pako2-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
