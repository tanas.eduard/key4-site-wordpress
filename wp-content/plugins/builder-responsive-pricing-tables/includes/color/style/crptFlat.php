<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$side-list-color']='#233039';
  $default['$side-list-border-color']='#ddd';
  $default['$block-background-color']='#233039';
  $default['$block-box-shadow-color']='$block-background-color';
  $default['$title-color']='highlight-color($block-background-color, 45)';
  $default['$price-color']='#fff';
  $default['$button-icon-color']='#fff';
  $default['$button-icon-background-color']='darken($theme-color, 10)';
  $default['$button-color']='$button-icon-color';
  $default['$button-background-color']='lighten($block-background-color, 10)';
  $default['$list-color']='#dcdcdc';
  $default['$list-border-color']='$button-background-color';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$theme-color';
  $default['$active-title-color']='#fff';
  $default['$active-price-color']='$price-color';
  $default['$active-button-icon-color']='$button-icon-color';
  $default['$active-button-icon-background-color']='$button-icon-background-color';
  $default['$active-button-color']='$active-button-icon-color';
  $default['$active-button-background-color']='lighten($active-button-icon-background-color, 5)';
  $default['$active-list-color']='#fff';
  $default['$active-list-border-color']='lighten($active-block-background-color, 15)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$side-list-color'])) $variables['$side-list-color']='#233039';
  if(!isset($variables['$side-list-border-color'])) $variables['$side-list-border-color']='#ddd';
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$block-box-shadow-color'])) $variables['$block-box-shadow-color']=$variables['$block-background-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']=wbrptColorFunctions::highlight($variables['$block-background-color'], 45);
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$button-icon-color'])) $variables['$button-icon-color']='#fff';
  if(!isset($variables['$button-icon-background-color'])) $variables['$button-icon-background-color']=wbrptColorFunctions::darken($variables['$theme-color'], 10);
  if(!isset($variables['$button-color'])) $variables['$button-color']=$variables['$button-icon-color'];
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 10);
  if(!isset($variables['$list-color'])) $variables['$list-color']='#dcdcdc';
  if(!isset($variables['$list-border-color'])) $variables['$list-border-color']=$variables['$button-background-color'];
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']='#fff';
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-button-icon-color'])) $variables['$active-button-icon-color']=$variables['$button-icon-color'];
  if(!isset($variables['$active-button-icon-background-color'])) $variables['$active-button-icon-background-color']=$variables['$button-icon-background-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$active-button-icon-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=wbrptColorFunctions::lighten($variables['$active-button-icon-background-color'], 5);
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
  if(!isset($variables['$active-list-border-color'])) $variables['$active-list-border-color']=wbrptColorFunctions::lighten($variables['$active-block-background-color'], 15);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-flat-<?php echo $colorClass; ?> .pt-back,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-back:before,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-back:before {
  outline-color: <?php echo $variables['$list-border-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-title,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-btn,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-btn .pt-icon,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-btn .pt-icon {
  color: <?php echo $variables['$button-icon-color']; ?>;
  background-color: <?php echo $variables['$button-icon-background-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-list,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-price-block,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-price-block {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-list-block {
  color: <?php echo $variables['$side-list-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-list-block .pt-list-item + .pt-list-item {
  border-top: 1px solid <?php echo $variables['$side-list-border-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back:after,
.crpt-flat[class*='pt-animation-'] .crpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back:after {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  -webkit-box-shadow: 0 0 10px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 0 10px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.crpt-flat[class*='pt-animation-'] .crpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-block,
.crpt-flat[class*='pt-animation-'] .crpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-block {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.crpt-flat[class*='pt-animation-'] .crpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
  border-color: <?php echo $variables['$active-list-border-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.crpt-flat[class*='pt-animation-'] .crpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn .pt-icon,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn .pt-icon,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn .pt-icon,
.crpt-flat[class*='pt-animation-'] .crpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn .pt-icon {
  background-color: <?php echo $variables['$active-button-icon-background-color']; ?>;
  color: <?php echo $variables['$active-button-icon-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-flat .pt-cols .crpt-flat-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-block,
.crpt-flat .pt-cols .crpt-flat-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-block,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-block,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-price-block {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-flat .pt-cols .crpt-flat-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-flat .pt-cols .crpt-flat-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn .pt-icon,
.crpt-flat .pt-cols .crpt-flat-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn .pt-icon,
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn .pt-icon,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn .pt-icon {
  color: <?php echo $variables['$button-icon-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn .pt-icon,
.crpt-flat[class*='pt-animation-']:hover .crpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-btn .pt-icon {
  background-color: <?php echo $variables['$button-icon-background-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-flat .crpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
