<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='lightness($theme-color, 90)';
  $default['$title-color']='lightness($theme-color, 30)';
  $default['$list-color']='$theme-color';
  $default['$price-color']='$theme-color';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$theme-color';
  $default['$hover-button-color']='$button-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='lighten($theme-color, 15)';
  $default['$active-block-box-shadow-color']='#666';
  $default['$active-title-color']='#fff';
  $default['$active-list-color']='#fff';
  $default['$active-price-color']='#fff';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 90);
  if(!isset($variables['$title-color'])) $variables['$title-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 30);
  if(!isset($variables['$list-color'])) $variables['$list-color']=$variables['$theme-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$theme-color'];
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$theme-color'];
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=$variables['$button-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 15);
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']='#666';
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']='#fff';
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']='#fff';
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.rings-style3-<?php echo $colorClass; ?> .pt-back,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-title,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-price,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-btn,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.rings-style3-<?php echo $colorClass; ?> .pt-btn:focus,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-list,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-back,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-back,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  -webkit-box-shadow: 0 0 10px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 0 10px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-title,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-title,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-price,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-price,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.rings-style3[class*='pt-animation-'] .rings-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.rings-style3 .pt-cols .rings-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.rings-style3 .pt-cols .rings-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.rings-style3 .pt-cols .rings-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.rings-style3 .pt-cols .rings-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.rings-style3 .pt-cols .rings-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rings-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.rings-style3[class*='pt-animation-']:hover .rings-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.rings-style3 .rings-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
