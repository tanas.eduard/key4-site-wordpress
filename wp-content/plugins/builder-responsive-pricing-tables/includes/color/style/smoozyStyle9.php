<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style9-<?php echo $colorClass; ?> .pt-back:after,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-back:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> .pt-more-link,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-more-link {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> .pt-btn,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> div .pt-block.pt-not-available .pt-back:after,
.smoozy-style9 div.smoozy-style9-<?php echo $colorClass; ?> .pt-block.pt-not-available .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?> div .pt-block.pt-not-available:hover .pt-back:after,
.smoozy-style9 div.smoozy-style9-<?php echo $colorClass; ?> .pt-block.pt-not-available:hover .pt-back:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-back:after,
.smoozy-style9.pt-animation-default .smoozy-style9-<?php echo $colorClass; ?> .pt-block:hover .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-back:after,
.smoozy-style9.pt-animation-default .smoozy-style9-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-back:after,
.smoozy-style9.pt-animation-2 .smoozy-style9-<?php echo $colorClass; ?> .pt-block:hover .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-back:after,
.smoozy-style9.pt-animation-2 .smoozy-style9-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-back:after {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-back:after,
.smoozy-style9.pt-animation-default:hover .smoozy-style9-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-back:after,
.smoozy-style9.pt-animation-2:hover .smoozy-style9-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style9-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-back:after,
.smoozy-style9.pt-animation-1 .smoozy-style9-<?php echo $colorClass; ?> .pt-selected .pt-back:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style9 .smoozy-style9-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
