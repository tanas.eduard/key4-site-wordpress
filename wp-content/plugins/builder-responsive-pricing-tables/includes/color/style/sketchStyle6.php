<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#fff';
  $default['$block-border-color']='#d1d1d1';
  $default['$button-border-color']='$theme-color';
  $default['$button-background-color']='$block-background-color';
  $default['$button-color']='$button-border-color';
  $default['$hover-button-border-color']='highlight-color($button-border-color, 7)';
  $default['$hover-button-background-color']='$button-background-color';
  $default['$hover-button-color']='highlight-color($button-color, 7)';
  $default['$title-color']='$button-color';
  $default['$title-background-color']='$button-background-color';
  $default['$title-border-color']='$button-border-color';
  $default['$price-color']='$button-border-color';
  $default['$list-color']='$button-color';
  $default['$list-background-color']='$button-background-color';
  $default['$list-border-color']='$button-border-color';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-block-border-color']='lighten($theme-color, 15)';
  $default['$active-button-border-color']='$button-border-color';
  $default['$active-button-background-color']='$active-button-border-color';
  $default['$active-button-color']='#fff';
  $default['$active-hover-button-border-color']='highlight-color($active-button-border-color, 7)';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-title-color']='$active-button-color';
  $default['$active-title-background-color']='$active-button-background-color';
  $default['$active-title-border-color']='$active-button-border-color';
  $default['$active-price-color']='$active-block-border-color';
  $default['$active-list-color']='$active-button-color';
  $default['$active-list-background-color']='$active-button-background-color';
  $default['$active-list-border-color']='$active-button-border-color';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#fff';
  if(!isset($variables['$block-border-color'])) $variables['$block-border-color']='#d1d1d1';
  if(!isset($variables['$button-border-color'])) $variables['$button-border-color']=$variables['$theme-color'];
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$button-color'])) $variables['$button-color']=$variables['$button-border-color'];
  if(!isset($variables['$hover-button-border-color'])) $variables['$hover-button-border-color']=wbrptColorFunctions::highlight($variables['$button-border-color'], 7);
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=wbrptColorFunctions::highlight($variables['$button-color'], 7);
  if(!isset($variables['$title-color'])) $variables['$title-color']=$variables['$button-color'];
  if(!isset($variables['$title-background-color'])) $variables['$title-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$title-border-color'])) $variables['$title-border-color']=$variables['$button-border-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$button-border-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']=$variables['$button-color'];
  if(!isset($variables['$list-background-color'])) $variables['$list-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$list-border-color'])) $variables['$list-border-color']=$variables['$button-border-color'];
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-block-border-color'])) $variables['$active-block-border-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 15);
  if(!isset($variables['$active-button-border-color'])) $variables['$active-button-border-color']=$variables['$button-border-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$active-button-border-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']='#fff';
  if(!isset($variables['$active-hover-button-border-color'])) $variables['$active-hover-button-border-color']=wbrptColorFunctions::highlight($variables['$active-button-border-color'], 7);
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-title-background-color'])) $variables['$active-title-background-color']=$variables['$active-button-background-color'];
  if(!isset($variables['$active-title-border-color'])) $variables['$active-title-border-color']=$variables['$active-button-border-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$active-block-border-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-list-background-color'])) $variables['$active-list-background-color']=$variables['$active-button-background-color'];
  if(!isset($variables['$active-list-border-color'])) $variables['$active-list-border-color']=$variables['$active-button-border-color'];
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.sketch-style6-<?php echo $colorClass; ?> .pt-back,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-title,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
  background-color: <?php echo $variables['$title-background-color']; ?>;
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-title:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-title:before {
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-title:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-title:after {
  border-color: <?php echo $variables['$title-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-price,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-btn,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:before {
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:after {
  border-color: <?php echo $variables['$button-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:hover,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:hover,
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:focus,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:hover:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:hover:before,
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:focus:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:focus:before {
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:hover:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:hover:after,
.sketch-style6-<?php echo $colorClass; ?> .pt-btn:focus:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-btn:focus:after {
  border-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-list,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  background-color: <?php echo $variables['$list-background-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-list:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-list:before {
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-list:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-list:after {
  border-color: <?php echo $variables['$list-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-back,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-back,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  border-color: <?php echo $variables['$active-block-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
  background-color: <?php echo $variables['$active-title-background-color']; ?>;
  border-color: <?php echo $variables['$active-title-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title:before,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-title-border-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title:after,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title:after {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-title-background-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-price,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-price,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
  background-color: <?php echo $variables['$active-list-background-color']; ?>;
  border-color: <?php echo $variables['$active-list-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list:before,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-list-border-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list:after,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list:after {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-list-background-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  color: <?php echo $variables['$active-button-color']; ?>;
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  border-color: <?php echo $variables['$active-button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:before,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-button-border-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:after,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:after {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-button-background-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$active-hover-button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover:before,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover:before,
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus:before,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-hover-button-border-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover:after,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover:after,
.sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus:after,
.sketch-style6[class*='pt-animation-'] .sketch-style6-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus:after {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-hover-button-background-color'], 20); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-back {
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
  background-color: <?php echo $variables['$title-background-color']; ?>;
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:before,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title:before,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title:before {
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:after,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title:after,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-title:after {
  border-color: <?php echo $variables['$title-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  background-color: <?php echo $variables['$list-background-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list:before,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list:before,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list:before {
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list:after,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list:after,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-list:after {
  border-color: <?php echo $variables['$list-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:after,
.sketch-style6 .pt-cols .sketch-style6-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:after,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:after {
  border-color: <?php echo $variables['$button-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:before,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:before {
  border-color: <?php echo $variables['$button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover:before,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover:before,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus:before,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus:before {
  border-color: <?php echo $variables['$hover-button-border-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover:after,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover:after,
.sketch-style6-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus:after,
.sketch-style6[class*='pt-animation-']:hover .sketch-style6-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus:after {
  border-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.sketch-style6 .sketch-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
