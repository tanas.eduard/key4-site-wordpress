<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.rmsbpt-flat-<?php echo $colorClass; ?>,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-block,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-block {
  background: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-head,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-head {
  background: <?php echo $variables['$theme-color']; ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-list-container:before,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-list-container:before {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-price-container,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-price-container {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-text,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-text,
.rmsbpt-flat-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-text,
.rmsbpt-flat.pt-animation-default .rmsbpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-text,
.rmsbpt-flat-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-text,
.rmsbpt-flat.pt-animation-1 .rmsbpt-flat-<?php echo $colorClass; ?> .pt-selected .pt-text,
.rmsbpt-flat-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-text,
.rmsbpt-flat.pt-animation-1 .rmsbpt-flat-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-text,
.rmsbpt-flat-<?php echo $colorClass; ?>.pt-animation-2 .pt-text,
.rmsbpt-flat.pt-animation-2 .rmsbpt-flat-<?php echo $colorClass; ?> .pt-text {
  text-shadow: 0 0 6px <?php echo wbrptColorFunctions::highlight(wbrptColorFunctions::lighten($variables['$theme-color'], 10), 30); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.rmsbpt-flat .rmsbpt-flat-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
