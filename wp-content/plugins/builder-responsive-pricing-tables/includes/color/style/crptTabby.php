<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-tabby-<?php echo $colorClass; ?> .pt-btn,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-btn {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-tabby-<?php echo $colorClass; ?> .pt-btn:focus,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-btn:focus {
  -webkit-box-shadow: 200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  box-shadow: 200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-tabs .pt-tab:before,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-tabs .pt-tab:before {
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-tabby-<?php echo $colorClass; ?>.pt-animation-default .pt-block .pt-list-item:hover,
.crpt-tabby.pt-animation-default .crpt-tabby-<?php echo $colorClass; ?> .pt-block .pt-list-item:hover,
.crpt-tabby-<?php echo $colorClass; ?>.pt-animation-2 .pt-block .pt-list-item:hover,
.crpt-tabby.pt-animation-2 .crpt-tabby-<?php echo $colorClass; ?> .pt-block .pt-list-item:hover {
  background: <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lighten($variables['$theme-color'], 10), 5); ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-tabby-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-title,
.crpt-tabby.pt-animation-default .crpt-tabby-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title,
.crpt-tabby-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-title,
.crpt-tabby.pt-animation-1 .crpt-tabby-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-tabby-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-content,
.crpt-tabby.pt-animation-default .crpt-tabby-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content,
.crpt-tabby-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-content,
.crpt-tabby.pt-animation-2 .crpt-tabby-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-tabby .crpt-tabby-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
