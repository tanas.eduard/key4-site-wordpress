<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style6-<?php echo $colorClass; ?> .pt-head:after,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-head:after {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-title,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-title,
.smoozy-style6-<?php echo $colorClass; ?> .pt-btn,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-btn-alt,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-btn-alt {
  background-color: transparent;
  border-color: <?php echo $variables['$theme-color']; ?>;
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-btn-alt:hover,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-btn-alt:hover,
.smoozy-style6-<?php echo $colorClass; ?> .pt-btn-alt:focus,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-btn-alt:focus {
  background-color: <?php echo $variables['$theme-color']; ?>;
  color: #fff;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-list .pt-list-sub-text,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-list .pt-list-sub-text {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style6 .smoozy-style6-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
