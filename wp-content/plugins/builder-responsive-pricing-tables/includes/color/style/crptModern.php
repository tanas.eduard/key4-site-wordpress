<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-modern-<?php echo $colorClass; ?> .pt-price-container,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-price-container,
.crpt-modern-<?php echo $colorClass; ?> .pt-title:before,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-title:before {
  -webkit-box-shadow: 100px 0 100px -100px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -100px 0 100px -100px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset;
  box-shadow: 100px 0 100px -100px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -100px 0 100px -100px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset;
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-modern-<?php echo $colorClass; ?> .pt-btn,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-btn {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-modern .crpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
