<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$side-list-color']='#5b5b5b';
  $default['$side-list-item-background-color']='#e1e1e1';
  $default['$block-background-color']='$side-list-item-background-color';
  $default['$title-color']='$side-list-color';
  $default['$price-color']='$side-list-color';
  $default['$list-color']='$side-list-color';
  $default['$list-fa-color']='$theme-color';
  $default['$list-item-background-color']='lighten($side-list-item-background-color, 7)';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$theme-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$theme-color';
  $default['$active-title-color']='#fff';
  $default['$active-price-color']='#fff';
  $default['$active-list-color']='$list-color';
  $default['$active-list-fa-color']='$list-fa-color';
  $default['$active-list-item-background-color']='$list-item-background-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$side-list-color'])) $variables['$side-list-color']='#5b5b5b';
  if(!isset($variables['$side-list-item-background-color'])) $variables['$side-list-item-background-color']='#e1e1e1';
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']=$variables['$side-list-item-background-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']=$variables['$side-list-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$side-list-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']=$variables['$side-list-color'];
  if(!isset($variables['$list-fa-color'])) $variables['$list-fa-color']=$variables['$theme-color'];
  if(!isset($variables['$list-item-background-color'])) $variables['$list-item-background-color']=wbrptColorFunctions::lighten($variables['$side-list-item-background-color'], 7);
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$theme-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']='#fff';
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']='#fff';
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-list-fa-color'])) $variables['$active-list-fa-color']=$variables['$list-fa-color'];
  if(!isset($variables['$active-list-item-background-color'])) $variables['$active-list-item-background-color']=$variables['$list-item-background-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-plaid-<?php echo $colorClass; ?> .pt-back,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-title,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-price,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-btn,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-plaid-<?php echo $colorClass; ?> .pt-btn:focus,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-list,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-list .fa,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-list .fa {
  color: <?php echo $variables['$list-fa-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-list .pt-list-item,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-list .pt-list-item {
  background-color: <?php echo $variables['$list-item-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-list-block {
  color: <?php echo $variables['$side-list-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-list-block .pt-list-item {
  background-color: <?php echo $variables['$side-list-item-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-back,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-back,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-price,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-price,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list .fa,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list .fa,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list .fa,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list .fa {
  color: <?php echo $variables['$active-list-fa-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-list-item,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-list-item,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list .pt-list-item,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list .pt-list-item {
  background-color: <?php echo $variables['$active-list-item-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.crpt-plaid[class*='pt-animation-'] .crpt-plaid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list .fa,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list .fa,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list .fa,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list .fa {
  color: <?php echo $variables['$list-fa-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list .pt-list-item,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list .pt-list-item,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list .pt-list-item,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-list-item {
  background-color: <?php echo $variables['$list-item-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-plaid .pt-cols .crpt-plaid-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-plaid-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.crpt-plaid[class*='pt-animation-']:hover .crpt-plaid-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-plaid .crpt-plaid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
