<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style11-<?php echo $colorClass; ?> .pt-head:after,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-head:after {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-title,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-price,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-more-link,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-more-link {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-btn,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style11 .smoozy-style11-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
