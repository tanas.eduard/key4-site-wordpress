<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#233039';
  $default['$title-color']='#fff';
  $default['$price-color']='#fff';
  $default['$price-background-color']='darken($theme-color, 10)';
  $default['$list-color']='#dcdcdc';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$price-background-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$theme-color';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$price-color';
  $default['$active-price-background-color']='$price-background-color';
  $default['$active-list-color']='#fff';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$price-background-color'])) $variables['$price-background-color']=wbrptColorFunctions::darken($variables['$theme-color'], 10);
  if(!isset($variables['$list-color'])) $variables['$list-color']='#dcdcdc';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$price-background-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-price-background-color'])) $variables['$active-price-background-color']=$variables['$price-background-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.ribbon-style3-<?php echo $colorClass; ?> .pt-back,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-title,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-price,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-price {
  background-color: <?php echo $variables['$price-background-color']; ?>;
  color: <?php echo $variables['$price-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-price:before,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-price:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$price-background-color'], 20); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-btn,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-btn:before,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$button-background-color'], 20); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.ribbon-style3-<?php echo $colorClass; ?> .pt-btn:focus,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-list,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.ribbon-style3 .pt-cols .ribbon-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-back,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-back,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-title,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-title,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-price,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-price,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
  color: <?php echo $variables['$active-price-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-price:before,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-price:before,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price:before,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-price-background-color'], 20); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-list,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-list,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:before,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:before,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:before,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-button-background-color'], 20); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.ribbon-style3[class*='pt-animation-'] .ribbon-style3-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.ribbon-style3 .pt-cols .ribbon-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.ribbon-style3 .pt-cols .ribbon-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-price {
  background-color: <?php echo $variables['$price-background-color']; ?>;
  color: <?php echo $variables['$price-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:before,
.ribbon-style3 .pt-cols .ribbon-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:before,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price:before,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-price:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$price-background-color'], 20); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.ribbon-style3 .pt-cols .ribbon-style3-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:before,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$button-background-color'], 20); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.ribbon-style3-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.ribbon-style3[class*='pt-animation-']:hover .ribbon-style3-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.ribbon-style3 .ribbon-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
