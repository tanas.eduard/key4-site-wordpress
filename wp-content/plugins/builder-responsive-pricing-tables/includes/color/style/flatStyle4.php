<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#233039';
  $default['$block-box-shadow-color']='#666';
  $default['$title-color']='#fff';
  $default['$price-background-color']='lighten($block-background-color, 10)';
  $default['$price-color']='#fff';
  $default['$list-color']='#fff';
  $default['$button-color']='#fff';
  $default['$button-background-color']='darken($theme-color, 25)';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$theme-color';
  $default['$active-title-color']='$title-color';
  $default['$active-price-background-color']='lightness($active-block-background-color, 92)';
  $default['$active-price-color']='$button-background-color';
  $default['$active-list-color']='$list-color';
  $default['$active-button-color']='$button-background-color';
  $default['$active-button-background-color']='$active-price-background-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$block-box-shadow-color'])) $variables['$block-box-shadow-color']='#666';
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-background-color'])) $variables['$price-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 10);
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=wbrptColorFunctions::darken($variables['$theme-color'], 25);
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-background-color'])) $variables['$active-price-background-color']=wbrptColorFunctions::lightness($variables['$active-block-background-color'], 92);
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$active-price-background-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.flat-style4-<?php echo $colorClass; ?> .pt-back,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-title,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-price,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-btn,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-btn:hover,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-btn:hover,
.flat-style4-<?php echo $colorClass; ?> .pt-btn:focus,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-list,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-price,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-price,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-price,
.flat-style4-<?php echo $colorClass; ?> .pt-price:before,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-price:before {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.flat-style4 .pt-cols .flat-style4-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 8); ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-back:before,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-back:before,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back:before,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back:before {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  -webkit-box-shadow: 0 0 10px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 0 10px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-title,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-title,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price,
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price:before,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price:before,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price:before,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price:before {
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-list,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-list,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.flat-style4[class*='pt-animation-'] .flat-style4-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.flat-style4 .pt-cols .flat-style4-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.flat-style4 .pt-cols .flat-style4-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.flat-style4 .pt-cols .flat-style4-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price,
.flat-style4-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:before,
.flat-style4 .pt-cols .flat-style4-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:before,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price:before,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-price:before {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.flat-style4 .pt-cols .flat-style4-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.flat-style4-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.flat-style4[class*='pt-animation-']:hover .flat-style4-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.flat-style4 .flat-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
