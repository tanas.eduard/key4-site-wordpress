<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$side-block-background-color']='lighten($theme-color, 15)';
  $default['$side-header-color']='#fff';
  $default['$side-header-background-color']='lighten($side-block-background-color, 5)';
  $default['$side-tab-color']='#fff';
  $default['$side-tab-background-color']='$side-block-background-color';
  $default['$active-side-tab-color']='$side-tab-color';
  $default['$active-side-tab-background-color']='lighten($theme-color, 5)';
  $default['$side-list-color']='#fff';
  $default['$side-list-background-color']='lighten($theme-color, 5)';
  $default['$side-list-even-item-background-color']='lighten($side-list-background-color, 8)';
  $default['$side-list-item-border-color']='lighten($side-list-background-color, 8)';
  $default['$block-background-color']='$theme-color';
  $default['$title-color']='#fff';
  $default['$price-color']='#fff';
  $default['$list-color']='#fff';
  $default['$list-background-color']='lighten($block-background-color, 5)';
  $default['$button-color']='#fff';
  $default['$button-background-color']='lighten($block-background-color, 20)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-box-shadow-color']='#333';
  $default['$active-button-border-color']='#fff';
  $default['$active-hover-button-border-color']='lighten($button-background-color, 15)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$side-block-background-color'])) $variables['$side-block-background-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 15);
  if(!isset($variables['$side-header-color'])) $variables['$side-header-color']='#fff';
  if(!isset($variables['$side-header-background-color'])) $variables['$side-header-background-color']=wbrptColorFunctions::lighten($variables['$side-block-background-color'], 5);
  if(!isset($variables['$side-tab-color'])) $variables['$side-tab-color']='#fff';
  if(!isset($variables['$side-tab-background-color'])) $variables['$side-tab-background-color']=$variables['$side-block-background-color'];
  if(!isset($variables['$active-side-tab-color'])) $variables['$active-side-tab-color']=$variables['$side-tab-color'];
  if(!isset($variables['$active-side-tab-background-color'])) $variables['$active-side-tab-background-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 5);
  if(!isset($variables['$side-list-color'])) $variables['$side-list-color']='#fff';
  if(!isset($variables['$side-list-background-color'])) $variables['$side-list-background-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 5);
  if(!isset($variables['$side-list-even-item-background-color'])) $variables['$side-list-even-item-background-color']=wbrptColorFunctions::lighten($variables['$side-list-background-color'], 8);
  if(!isset($variables['$side-list-item-border-color'])) $variables['$side-list-item-border-color']=wbrptColorFunctions::lighten($variables['$side-list-background-color'], 8);
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$list-background-color'])) $variables['$list-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 5);
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 20);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']='#333';
  if(!isset($variables['$active-button-border-color'])) $variables['$active-button-border-color']='#fff';
  if(!isset($variables['$active-hover-button-border-color'])) $variables['$active-hover-button-border-color']=wbrptColorFunctions::lighten($variables['$button-background-color'], 15);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side .pt-tabs .pt-tab:hover,
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side .pt-tabs .pt-tab-active {
  color: <?php echo $variables['$active-side-tab-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-block,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-block {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-col:nth-child(2n + 1) .pt-block,
.crpt-extra .pt-col.crpt-extra-<?php echo $colorClass; ?>:nth-child(2n + 1) .pt-block {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 3); ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-col:nth-child(2n + 1) .pt-list,
.crpt-extra .pt-col.crpt-extra-<?php echo $colorClass; ?>:nth-child(2n + 1) .pt-list {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$list-background-color'], 3); ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-col:nth-child(2n + 1) .pt-btn,
.crpt-extra .pt-col.crpt-extra-<?php echo $colorClass; ?>:nth-child(2n + 1) .pt-btn {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$button-background-color'], 3); ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-title,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-price,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-btn,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-btn .pt-select,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-btn .pt-select {
  border-color: <?php echo $variables['$active-button-border-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-btn:hover .pt-select,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-btn:hover .pt-select,
.crpt-extra-<?php echo $colorClass; ?> .pt-btn:focus .pt-select,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-btn:focus .pt-select {
  border-color: <?php echo $variables['$active-hover-button-border-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-list,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  background-color: <?php echo $variables['$list-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-list .pt-list-item + .pt-list-item:after,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-list .pt-list-item + .pt-list-item:after {
  border-top-color: <?php echo $variables['$side-list-item-border-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-list-item:nth-child(2n),
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-list-item:nth-child(2n) {
  background-color: <?php echo $variables['$side-list-even-item-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-tabs .pt-tab {
  background: <?php echo $variables['$side-tab-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-tabs .pt-tab:before {
  background: <?php echo $variables['$active-side-tab-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side {
  color: <?php echo $variables['$side-tab-color']; ?>;
  background-color: <?php echo $variables['$side-block-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side .pt-side-text {
  color: <?php echo $variables['$side-header-color']; ?>;
  background-color: <?php echo $variables['$side-header-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-cols .pt-cols-side .pt-tabs .pt-tab + .pt-tab {
  border-left-color: <?php echo $variables['$side-tab-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side .pt-list-block {
  color: <?php echo $variables['$side-list-color']; ?>;
  background-color: <?php echo $variables['$side-list-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side .pt-list-item + .pt-list-item .pt-border {
  border-top-color: <?php echo $variables['$side-list-item-border-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-cols-side .pt-list-item:nth-child(2n) {
  background-color: <?php echo $variables['$side-list-even-item-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-extra-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-content,
.crpt-extra.pt-animation-default .crpt-extra-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content,
.crpt-extra-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-content,
.crpt-extra.pt-animation-2 .crpt-extra-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content {
  -webkit-box-shadow: 0 0 30px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 0 30px <?php echo $variables['$active-block-box-shadow-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-extra .crpt-extra-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
