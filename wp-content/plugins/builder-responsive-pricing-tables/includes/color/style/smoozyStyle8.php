<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style8-<?php echo $colorClass; ?> .pt-block,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-block {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-title,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-price,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-more-link,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-more-link {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-more-link:hover,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-more-link:hover {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> div .pt-block.pt-not-available,
.smoozy-style8 div.smoozy-style8-<?php echo $colorClass; ?> .pt-block.pt-not-available,
.smoozy-style8-<?php echo $colorClass; ?> div .pt-block.pt-not-available:hover,
.smoozy-style8 div.smoozy-style8-<?php echo $colorClass; ?> .pt-block.pt-not-available:hover {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-selected,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-selected,
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover,
.smoozy-style8.pt-animation-default .smoozy-style8-<?php echo $colorClass; ?> .pt-block:hover,
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover,
.smoozy-style8.pt-animation-default .smoozy-style8-<?php echo $colorClass; ?> .pt-block.pt-selected:hover,
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-1 .pt-block:hover,
.smoozy-style8.pt-animation-1 .smoozy-style8-<?php echo $colorClass; ?> .pt-block:hover,
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-1 .pt-block.pt-selected:hover,
.smoozy-style8.pt-animation-1 .smoozy-style8-<?php echo $colorClass; ?> .pt-block.pt-selected:hover {
  background-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected,
.smoozy-style8.pt-animation-default:hover .smoozy-style8-<?php echo $colorClass; ?> .pt-selected,
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected,
.smoozy-style8.pt-animation-1:hover .smoozy-style8-<?php echo $colorClass; ?> .pt-selected,
.smoozy-style8-<?php echo $colorClass; ?>.pt-animation-2 .pt-selected,
.smoozy-style8.pt-animation-2 .smoozy-style8-<?php echo $colorClass; ?> .pt-selected {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style8 .smoozy-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
