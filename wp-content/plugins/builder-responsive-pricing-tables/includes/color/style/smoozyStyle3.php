<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style3-<?php echo $colorClass; ?> .pt-back:before,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-back:before {
  -webkit-box-shadow: 0 0 20px <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lightness($variables['$theme-color'], 30), 20); ?>;
  box-shadow: 0 0 20px <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lightness($variables['$theme-color'], 30), 20); ?>;
}
.smoozy-style3-<?php echo $colorClass; ?> .pt-back:after,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-back:after {
  background-color: <?php echo $variables['$theme-color']; ?>;
  -webkit-box-shadow: 0 0 5px <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 30); ?>;
  box-shadow: 0 0 5px <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 30); ?>;
}
.smoozy-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-btn:hover,
.smoozy-style3-<?php echo $colorClass; ?> .pt-btn:focus,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style3 .smoozy-style3-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
