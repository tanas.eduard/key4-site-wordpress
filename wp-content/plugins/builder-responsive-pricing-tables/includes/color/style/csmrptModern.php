<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.csmrpt-modern-<?php echo $colorClass; ?> .pt-btn:hover,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-btn:hover,
.csmrpt-modern-<?php echo $colorClass; ?> .btn:hover,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .btn:hover,
.csmrpt-modern-<?php echo $colorClass; ?> .pt-btn:focus,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-btn:focus,
.csmrpt-modern-<?php echo $colorClass; ?> .btn:focus,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .btn:focus {
  background-color: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.csmrpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-head,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-head,
.csmrpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-head,
.csmrpt-modern[class*='pt-animation-'] .csmrpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-head {
  background-color: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.csmrpt-modern .csmrpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
