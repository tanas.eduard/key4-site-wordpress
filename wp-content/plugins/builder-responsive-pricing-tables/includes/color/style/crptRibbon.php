<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$side-list-color']='#233039';
  $default['$side-list-border-color']='#ddd';
  $default['$block-background-color']='#233039';
  $default['$button-color']='#fff';
  $default['$button-background-color']='darken($theme-color, 10)';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$title-color']='#fff';
  $default['$price-color']='$button-background-color';
  $default['$list-color']='#dcdcdc';
  $default['$list-border-color']='lighten($block-background-color, 20)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$theme-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$title-color';
  $default['$active-list-color']='#fff';
  $default['$active-list-border-color']='lighten($active-block-background-color, 15)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$side-list-color'])) $variables['$side-list-color']='#233039';
  if(!isset($variables['$side-list-border-color'])) $variables['$side-list-border-color']='#ddd';
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=wbrptColorFunctions::darken($variables['$theme-color'], 10);
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$button-background-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']='#dcdcdc';
  if(!isset($variables['$list-border-color'])) $variables['$list-border-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 20);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$title-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
  if(!isset($variables['$active-list-border-color'])) $variables['$active-list-border-color']=wbrptColorFunctions::lighten($variables['$active-block-background-color'], 15);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-1 .pt-col:nth-child(1) .pt-back,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(2) .pt-back,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(3) .pt-back,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(4) .pt-back,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(5) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 0); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(1) .pt-back,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(2) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 7); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(1) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 9.333333333333334); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(2) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 4.666666666666667); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(1) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10.5); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(3) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 3.5); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(1) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 11.2); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(2) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 8.399999999999999); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(3) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 5.6); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(4) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 2.8); ?>;
}
.crpt-ribbon .pt-cols .pt-cols-main .pt-col.crpt-ribbon-<?php echo $colorClass; ?> .pt-block .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-title,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-price,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-btn,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-btn:before,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$button-background-color'], 20); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-btn:focus,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-list,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-list-block {
  color: <?php echo $variables['$side-list-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-list-block .pt-list-item + .pt-list-item {
  border-top: 1px solid <?php echo $variables['$side-list-border-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-back:before,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-back:before,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back:before,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back:before {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-price,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-price,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
  border-color: <?php echo $variables['$active-list-border-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:before,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:before,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:before,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-button-background-color'], 20); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.crpt-ribbon[class*='pt-animation-'] .crpt-ribbon-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-ribbon .pt-cols .crpt-ribbon-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.crpt-ribbon .pt-cols .crpt-ribbon-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-ribbon .pt-cols .crpt-ribbon-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-ribbon .pt-cols .crpt-ribbon-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:before,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$button-background-color'], 20); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-ribbon-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.crpt-ribbon[class*='pt-animation-']:hover .crpt-ribbon-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-ribbon .crpt-ribbon-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
