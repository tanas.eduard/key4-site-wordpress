<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-hipo-<?php echo $colorClass; ?> .pt-tabs .pt-tab:before {
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-1 .pt-col:nth-child(1) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(2) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(3) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(4) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(5) .pt-block {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-2 .pt-col:nth-child(1) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(2) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(3) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(4) .pt-block {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-3 .pt-col:nth-child(1) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(2) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(3) .pt-block {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 15); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-4 .pt-col:nth-child(1) .pt-block,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(2) .pt-block {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols-5 .pt-col:nth-child(1) .pt-block {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 25); ?>;
}
.crpt-hipo .pt-cols:nth-child(n) .crpt-hipo-<?php echo $colorClass; ?> .pt-block:nth-child(n) {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-cols .pt-col .pt-block:hover .pt-top,
.crpt-hipo[class*='pt-animation-'] .pt-cols .crpt-hipo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-top,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols .pt-selected .pt-top,
.crpt-hipo .pt-cols .crpt-hipo-<?php echo $colorClass; ?> .pt-selected .pt-top {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 20); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 20); ?> inset;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 20); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 20); ?> inset;
  background-color: <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 10); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-cols .pt-col .pt-block:hover .pt-footer,
.crpt-hipo[class*='pt-animation-'] .pt-cols .crpt-hipo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-footer,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols .pt-selected .pt-footer,
.crpt-hipo .pt-cols .crpt-hipo-<?php echo $colorClass; ?> .pt-selected .pt-footer {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 8); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-cols .pt-col .pt-block:hover .pt-btn:hover,
.crpt-hipo[class*='pt-animation-'] .pt-cols .crpt-hipo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols .pt-selected .pt-btn:hover,
.crpt-hipo .pt-cols .crpt-hipo-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover {
  background-color: <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 8); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-cols .pt-col .pt-block:hover .pt-list,
.crpt-hipo[class*='pt-animation-'] .pt-cols .crpt-hipo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list,
.crpt-hipo-<?php echo $colorClass; ?> .pt-cols .pt-selected .pt-list,
.crpt-hipo .pt-cols .crpt-hipo-<?php echo $colorClass; ?> .pt-selected .pt-list {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 8); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?>.pt-animation-default .pt-block .pt-list-item:hover,
.crpt-hipo.pt-animation-default .crpt-hipo-<?php echo $colorClass; ?> .pt-block .pt-list-item:hover,
.crpt-hipo-<?php echo $colorClass; ?>.pt-animation-2 .pt-block .pt-list-item:hover,
.crpt-hipo.pt-animation-2 .crpt-hipo-<?php echo $colorClass; ?> .pt-block .pt-list-item:hover {
  background-color: <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lighten($variables['$theme-color'], 8), 5); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-hipo .crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-hipo .crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-hipo .crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-hipo .crpt-hipo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
