<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.csmrpt-classic-<?php echo $colorClass; ?> .pt-btn,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?> .btn,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 5); ?>;
}
.csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .pt-footer,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .pt-footer,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-footer,
.csmrpt-classic.pt-animation-default .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-footer,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-footer,
.csmrpt-classic.pt-animation-2 .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-footer {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-btn,
.csmrpt-classic.pt-animation-default .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-btn,
.csmrpt-classic.pt-animation-2 .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .btn,
.csmrpt-classic.pt-animation-default .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .btn,
.csmrpt-classic.pt-animation-2 .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn {
  background-color: #fff;
  color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn,
.csmrpt-classic.pt-animation-default:hover .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-btn,
.csmrpt-classic.pt-animation-2:hover .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-btn,
.csmrpt-classic.pt-animation-1 .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block .pt-btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .btn,
.csmrpt-classic.pt-animation-default:hover .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .btn,
.csmrpt-classic.pt-animation-2:hover .csmrpt-classic-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-classic-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .btn,
.csmrpt-classic.pt-animation-1 .csmrpt-classic-<?php echo $colorClass; ?>.pt-col .pt-block .btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 5); ?>;
}
.csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.csmrpt-classic .csmrpt-classic-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
