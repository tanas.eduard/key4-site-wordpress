<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style4-<?php echo $colorClass; ?> .pt-title,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-backline:before,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-backline:before {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-sub-text-fixed,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-sub-text-fixed {
  min-height: 65px;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-btn,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$theme-color']; ?>;
  border-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-btn:hover,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-btn:hover,
.smoozy-style4-<?php echo $colorClass; ?> .pt-btn:focus,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$theme-color']; ?>;
  color: #fff;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-list .pt-list-sub-text,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-list .pt-list-sub-text {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style4 .smoozy-style4-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
