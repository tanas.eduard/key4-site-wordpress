<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#000';
  $default['$title-color']='#fff';
  $default['$list-color']='#fff';
  $default['$price-color']='$theme-color';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$theme-color';
  $default['$hover-button-color']='$button-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-block-background-semitransparent-color']='$theme-color';
  $default['$active-block-box-shadow-color']='#666';
  $default['$active-title-color']='$title-color';
  $default['$active-list-color']='$list-color';
  $default['$active-price-color']='$price-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#000';
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$theme-color'];
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$theme-color'];
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=$variables['$button-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-block-background-semitransparent-color'])) $variables['$active-block-background-semitransparent-color']=$variables['$theme-color'];
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']='#666';
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.rings-style2-<?php echo $colorClass; ?> .pt-back:before,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-back:before {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-title,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-price,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-btn,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-btn:hover,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-btn:hover,
.rings-style2-<?php echo $colorClass; ?> .pt-btn:focus,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-list,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  -webkit-box-shadow: 0 0 10px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 0 10px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back:before,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back:before,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back:before,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back:before {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back:after,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back:after {
  background-color: <?php echo wbrptColorFunctions::rgba($variables['$active-block-background-semitransparent-color'], 0.5); ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-title,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-title,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-price,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-price,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.rings-style2[class*='pt-animation-'] .rings-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back:before,
.rings-style2 .pt-cols .rings-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back:before,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back:before,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-back:before {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.rings-style2 .pt-cols .rings-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.rings-style2 .pt-cols .rings-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.rings-style2 .pt-cols .rings-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.rings-style2 .pt-cols .rings-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rings-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.rings-style2[class*='pt-animation-']:hover .rings-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.rings-style2 .rings-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
