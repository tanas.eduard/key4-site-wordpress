<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-btn,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?> .btn,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .btn {
  color: #fff;
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-head,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-head,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-details-title-wrap,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-details-title-wrap {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-list,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-list:before,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-list:before {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-list li:nth-child(even),
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-list li:nth-child(even) {
  background: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 93); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-list-not-available:before,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-list-not-available:before {
  background: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-price-container,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-price-container {
  background: <?php echo $variables['$theme-color']; ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-head,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-head,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-head,
.rmsbpt-modern.pt-animation-default .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-head,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-head,
.rmsbpt-modern.pt-animation-2 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-head {
  background-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  -webkit-box-shadow: 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
  box-shadow: 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-content,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-content,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-content,
.rmsbpt-modern.pt-animation-default .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-content,
.rmsbpt-modern.pt-animation-2 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-list,
.rmsbpt-modern.pt-animation-default .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-list,
.rmsbpt-modern.pt-animation-2 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: #fff;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list li:nth-child(even),
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list li:nth-child(even),
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-list li:nth-child(even),
.rmsbpt-modern.pt-animation-default .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list li:nth-child(even),
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block:hover .pt-list li:nth-child(even),
.rmsbpt-modern.pt-animation-2 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list li:nth-child(even) {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.rmsbpt-modern[class*='pt-animation-'] .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn,
.rmsbpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .btn,
.rmsbpt-modern[class*='pt-animation-'] .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn {
  color: #fff;
  background-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rmsbpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.rmsbpt-modern[class*='pt-animation-'] .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn:hover,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn:hover,
.rmsbpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .btn:hover,
.rmsbpt-modern[class*='pt-animation-'] .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn:hover,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rmsbpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.rmsbpt-modern[class*='pt-animation-'] .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn:focus,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn:focus,
.rmsbpt-modern-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .btn:focus,
.rmsbpt-modern[class*='pt-animation-'] .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn:focus {
  color: #fff;
  background-color: <?php echo wbrptColorFunctions::highlight(wbrptColorFunctions::darken($variables['$theme-color'], 10), 7); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-head,
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-head,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-head,
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-head,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-head,
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .pt-head,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .pt-head,
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .pt-head {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
  -webkit-box-shadow: 0 0 2px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
  box-shadow: 0 0 2px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-content,
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-content,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-content,
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-content,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-content,
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .pt-content,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .pt-content,
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .pt-content {
  background-color: #fff;
}
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-list,
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-list,
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-list,
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .pt-list,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .pt-list,
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .pt-list {
  color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-list li:nth-child(even),
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list li:nth-child(even),
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-list li:nth-child(even),
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list li:nth-child(even),
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-list li:nth-child(even),
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .pt-list li:nth-child(even),
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .pt-list li:nth-child(even),
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .pt-list li:nth-child(even) {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 93); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-list-not-available:before,
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list-not-available:before,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-list-not-available:before,
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-list-not-available:before,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-list-not-available:before,
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .pt-list-not-available:before,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .pt-list-not-available:before,
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .pt-list-not-available:before {
  background-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn,
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-btn,
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .pt-btn,
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .pt-btn,
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .pt-btn,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .btn,
.rmsbpt-modern.pt-animation-default:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .btn,
.rmsbpt-modern.pt-animation-2:hover .rmsbpt-modern-<?php echo $colorClass; ?> .pt-selected .btn,
.rmsbpt-modern-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block .btn,
.rmsbpt-modern.pt-animation-1 .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block .btn,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-cols .pt-col .pt-block.pt-not-available .btn,
.rmsbpt-modern .pt-cols .rmsbpt-modern-<?php echo $colorClass; ?>.pt-col .pt-block.pt-not-available .btn {
  color: #fff;
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.rmsbpt-modern .rmsbpt-modern-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
