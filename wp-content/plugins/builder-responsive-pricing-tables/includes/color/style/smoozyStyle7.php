<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.smoozy-style7-<?php echo $colorClass; ?> .pt-title,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-price,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-btn,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-btn:hover,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-btn:hover,
.smoozy-style7-<?php echo $colorClass; ?> .pt-btn:focus,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-btn:focus {
  -webkit-box-shadow: 0 0 15px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?>;
  box-shadow: 0 0 15px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-btn-alt,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-btn-alt {
  background-color: transparent;
  border-color: <?php echo $variables['$theme-color']; ?>;
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-btn-alt:hover,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?> .pt-btn-alt:focus,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-btn-alt:focus {
  color: #fff;
  -webkit-box-shadow: none;
  box-shadow: none;
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> div .pt-block.pt-not-available .pt-title,
.smoozy-style7 div.smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-not-available .pt-title,
.smoozy-style7-<?php echo $colorClass; ?> div .pt-block.pt-not-available:hover .pt-title,
.smoozy-style7 div.smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-not-available:hover .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> div .pt-block.pt-not-available .pt-price,
.smoozy-style7 div.smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-not-available .pt-price,
.smoozy-style7-<?php echo $colorClass; ?> div .pt-block.pt-not-available:hover .pt-price,
.smoozy-style7 div.smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-not-available:hover .pt-price {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> div .pt-block.pt-not-available .pt-back:after,
.smoozy-style7 div.smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-not-available .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?> div .pt-block.pt-not-available:hover .pt-back:after,
.smoozy-style7 div.smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-not-available:hover .pt-back:after {
  background-color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-title,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-title,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-title,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-title,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-title,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-title,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-title,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-title,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-title,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-title {
  color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-back:after,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-back:after,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-back:after,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-back:after,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-back:after {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-price,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-price,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-price,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-price,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-price,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-price,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-price,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-price,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-price,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-price {
  color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-btn,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-btn,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-btn,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-btn,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn {
  color: <?php echo $variables['$theme-color']; ?>;
  background-color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-btn-alt,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn-alt,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-btn-alt,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn-alt,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-btn-alt,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn-alt,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-btn-alt,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn-alt {
  background-color: transparent;
  color: #fff;
  border-color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:hover,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-btn-alt:hover,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-btn-alt:hover,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-btn-alt:hover,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-btn-alt:hover,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:focus,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:focus,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block:hover .pt-btn-alt:focus,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn-alt:focus,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default .pt-block.pt-selected:hover .pt-btn-alt:focus,
.smoozy-style7.pt-animation-default .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn-alt:focus,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block:hover .pt-btn-alt:focus,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block:hover .pt-btn-alt:focus,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2 .pt-block.pt-selected:hover .pt-btn-alt:focus,
.smoozy-style7.pt-animation-2 .smoozy-style7-<?php echo $colorClass; ?> .pt-block.pt-selected:hover .pt-btn-alt:focus {
  background-color: #fff;
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-title,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-title,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-title,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-title,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-title,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-back:after,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-back:after,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-back:after,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-back:after,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-back:after {
  background-color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-price,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-price,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-price,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-price,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-price,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-btn,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-btn,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: #fff;
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn-alt,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-btn-alt,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-btn-alt,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt {
  background-color: transparent;
  border-color: <?php echo $variables['$theme-color']; ?>;
  color: <?php echo $variables['$theme-color']; ?>;
}
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn-alt:hover,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-btn-alt:hover,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-btn-alt:hover,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:hover,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn-alt:focus,
.smoozy-style7.pt-animation-default:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:focus,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-btn-alt:focus,
.smoozy-style7.pt-animation-1 .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:focus,
.smoozy-style7-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-btn-alt:focus,
.smoozy-style7.pt-animation-2:hover .smoozy-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn-alt:focus {
  background-color: <?php echo $variables['$theme-color']; ?>;
  color: #fff;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.smoozy-style7 .smoozy-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
