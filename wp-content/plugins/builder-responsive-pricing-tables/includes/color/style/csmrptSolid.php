<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.csmrpt-solid-<?php echo $colorClass; ?>,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?>,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-block,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-block {
  color: <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 10); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-back,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-back {
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-title,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-sub-title,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-sub-title {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 15); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-sub-text,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-sub-text {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-btn,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?> .btn,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
  color: #fff;
  border-color: <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 5); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-price-block,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-price-block {
  color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-title,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-title,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-title,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: #fff;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-title,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-sub-title,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-sub-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-sub-title,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-sub-title,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-text,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-text,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-sub-text,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-sub-text,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-sub-text,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-sub-text {
  color: #eee;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-price-block,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-block,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-price-block,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-block {
  color: #fff;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-btn,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-btn,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .btn,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .btn,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn {
  background-color: #fff;
  color: <?php echo $variables['$theme-color']; ?>;
  -webkit-box-shadow: 0 3px 10px -3px #000;
  box-shadow: 0 3px 10px -3px #000;
  border-color: <?php echo wbrptColorFunctions::highlight('#fff', 5); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-btn:hover,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-btn:hover,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn:hover,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn:hover,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .btn:hover,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn:hover,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .btn:hover,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn:hover,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-btn:focus,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-btn:focus,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn:focus,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn:focus,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .btn:focus,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn:focus,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .btn:focus,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .btn:focus {
  -webkit-box-shadow: 0 3px 10px -3px <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 15); ?>;
  box-shadow: 0 3px 10px -3px <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 15); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-back,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-back,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-back,
.csmrpt-solid.pt-animation-default .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-back,
.csmrpt-solid.pt-animation-1 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block {
  color: <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 10); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-title,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .pt-title,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .pt-title,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-sub-title,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .pt-sub-title,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-title,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .pt-sub-title,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .pt-sub-title {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 15); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-sub-text,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-text,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .pt-sub-text,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-sub-text,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .pt-sub-text,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .pt-sub-text {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 5); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-back,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-back,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .pt-back,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-back,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .pt-back,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .pt-back {
  background-color: #fff;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-price-block,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .pt-price-block,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .pt-price-block,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .pt-price-block {
  color: <?php echo $variables['$theme-color']; ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-btn,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .pt-btn,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .pt-btn,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .pt-btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .btn,
.csmrpt-solid.pt-animation-default:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-1:hover .pt-selected .btn,
.csmrpt-solid.pt-animation-1:hover .csmrpt-solid-<?php echo $colorClass; ?> .pt-selected .btn,
.csmrpt-solid-<?php echo $colorClass; ?>.pt-animation-2 .pt-col .pt-block .btn,
.csmrpt-solid.pt-animation-2 .csmrpt-solid-<?php echo $colorClass; ?>.pt-col .pt-block .btn {
  background-color: <?php echo $variables['$theme-color']; ?>;
  color: #fff;
  border-color: <?php echo wbrptColorFunctions::highlight($variables['$theme-color'], 5); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.csmrpt-solid .csmrpt-solid-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
