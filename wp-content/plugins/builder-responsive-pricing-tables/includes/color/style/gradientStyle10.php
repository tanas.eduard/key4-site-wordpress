<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#233039';
  $default['$block-background-gradient-start-color']='$theme-color';
  $default['$block-background-gradient-end-color']='lighten($block-background-gradient-start-color, 25)';
  $default['$title-color']='#fff';
  $default['$price-color']='#fff';
  $default['$list-color']='#fff';
  $default['$button-color']='#fff';
  $default['$hover-button-color']='highlight-color($button-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-gradient-start-color']='$block-background-gradient-start-color';
  $default['$active-block-background-gradient-end-color']='$block-background-gradient-end-color';
  $default['$active-block-background-semitransparent-color']='#fff';
  $default['$active-block-box-shadow-color']='#666';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$price-color';
  $default['$active-list-color']='$list-color';
  $default['$active-button-color']='$button-color';
  $default['$active-hover-button-color']='highlight-color($active-button-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$block-background-gradient-start-color'])) $variables['$block-background-gradient-start-color']=$variables['$theme-color'];
  if(!isset($variables['$block-background-gradient-end-color'])) $variables['$block-background-gradient-end-color']=wbrptColorFunctions::lighten($variables['$block-background-gradient-start-color'], 25);
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=wbrptColorFunctions::highlight($variables['$button-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-gradient-start-color'])) $variables['$active-block-background-gradient-start-color']=$variables['$block-background-gradient-start-color'];
  if(!isset($variables['$active-block-background-gradient-end-color'])) $variables['$active-block-background-gradient-end-color']=$variables['$block-background-gradient-end-color'];
  if(!isset($variables['$active-block-background-semitransparent-color'])) $variables['$active-block-background-semitransparent-color']='#fff';
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']='#666';
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=wbrptColorFunctions::highlight($variables['$active-button-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.gradient-style10-<?php echo $colorClass; ?> .pt-back:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-back:before {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-back:after,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-back:after {
  background: <?php echo $variables['$active-block-background-gradient-end-color']; ?>;
  background: -webkit-linear-gradient(top, <?php echo $variables['$active-block-background-gradient-start-color']; ?>, <?php echo $variables['$active-block-background-gradient-end-color']; ?>);
  background: -moz-linear-gradient(top, <?php echo $variables['$active-block-background-gradient-start-color']; ?>, <?php echo $variables['$active-block-background-gradient-end-color']; ?>);
  background: linear-gradient(to bottom, <?php echo $variables['$active-block-background-gradient-start-color']; ?>, <?php echo $variables['$active-block-background-gradient-end-color']; ?>);
  -webkit-box-shadow: 0 0 10px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 0 10px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-title,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-title:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-title:before,
.gradient-style10-<?php echo $colorClass; ?> .pt-content:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-content:before {
  background: <?php echo $variables['$block-background-gradient-end-color']; ?>;
  background: -webkit-linear-gradient(top, <?php echo $variables['$block-background-gradient-start-color']; ?>, <?php echo $variables['$block-background-gradient-end-color']; ?>);
  background: -moz-linear-gradient(top, <?php echo $variables['$block-background-gradient-start-color']; ?>, <?php echo $variables['$block-background-gradient-end-color']; ?>);
  background: linear-gradient(to bottom, <?php echo $variables['$block-background-gradient-start-color']; ?>, <?php echo $variables['$block-background-gradient-end-color']; ?>);
}
.gradient-style10-<?php echo $colorClass; ?> .pt-title .pt-wrapper:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-title .pt-wrapper:before,
.gradient-style10-<?php echo $colorClass; ?> .pt-content .pt-wrapper:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-content .pt-wrapper:before {
  background-color: <?php echo $variables['$active-block-background-semitransparent-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-title .pt-slope:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-title .pt-slope:before,
.gradient-style10-<?php echo $colorClass; ?> .pt-content .pt-slope:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-content .pt-slope:before {
  border-color: <?php echo $variables['$block-background-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-title .pt-slope:after,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-title .pt-slope:after,
.gradient-style10-<?php echo $colorClass; ?> .pt-content .pt-slope:after,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-content .pt-slope:after {
  border-color: <?php echo $variables['$active-block-background-semitransparent-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-price,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-btn,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-btn:hover,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-btn:hover,
.gradient-style10-<?php echo $colorClass; ?> .pt-btn:focus,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-list,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-list:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-list:before {
  background: <?php echo $variables['$block-background-gradient-end-color']; ?>;
  background: -webkit-linear-gradient(top, <?php echo $variables['$block-background-gradient-start-color']; ?>, <?php echo $variables['$block-background-gradient-end-color']; ?>);
  background: -moz-linear-gradient(top, <?php echo $variables['$block-background-gradient-start-color']; ?>, <?php echo $variables['$block-background-gradient-end-color']; ?>);
  background: linear-gradient(to bottom, <?php echo $variables['$block-background-gradient-start-color']; ?>, <?php echo $variables['$block-background-gradient-end-color']; ?>);
}
.gradient-style10-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back:before,
.gradient-style10 .pt-cols .gradient-style10-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back:before {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title .pt-slope:before,
.gradient-style10 .pt-cols .gradient-style10-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title .pt-slope:before,
.gradient-style10-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-content .pt-slope:before,
.gradient-style10 .pt-cols .gradient-style10-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-content .pt-slope:before {
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-title,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-title,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.gradient-style10[class*='pt-animation-'] .gradient-style10-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-price,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-price,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.gradient-style10[class*='pt-animation-'] .gradient-style10-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-list,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-list,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.gradient-style10[class*='pt-animation-'] .gradient-style10-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.gradient-style10[class*='pt-animation-'] .gradient-style10-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  color: <?php echo $variables['$active-button-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.gradient-style10[class*='pt-animation-'] .gradient-style10-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.gradient-style10[class*='pt-animation-'] .gradient-style10-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.gradient-style10 .pt-cols .gradient-style10-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.gradient-style10[class*='pt-animation-']:hover .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.gradient-style10 .pt-cols .gradient-style10-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.gradient-style10[class*='pt-animation-']:hover .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.gradient-style10 .pt-cols .gradient-style10-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.gradient-style10[class*='pt-animation-']:hover .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.gradient-style10[class*='pt-animation-']:hover .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.gradient-style10[class*='pt-animation-']:hover .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.gradient-style10-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.gradient-style10[class*='pt-animation-']:hover .gradient-style10-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.gradient-style10 .gradient-style10-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
