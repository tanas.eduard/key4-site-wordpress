<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='lightness($theme-color, 12)';
  $default['$block-border-color']='$theme-color';
  $default['$title-color']='$theme-color';
  $default['$price-color']='#fff';
  $default['$price-background-color']='$theme-color';
  $default['$list-color']='#fff';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$theme-color';
  $default['$hover-button-color']='$button-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 18)';
  $default['$active-block-background-color']='lightness($block-background-color, 7)';
  $default['$active-block-border-color']='$block-border-color';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$price-color';
  $default['$active-price-background-color']='$price-background-color';
  $default['$active-price-border-color']='$active-price-background-color';
  $default['$active-price-box-shadow-color']='#000';
  $default['$active-list-color']='$list-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 12);
  if(!isset($variables['$block-border-color'])) $variables['$block-border-color']=$variables['$theme-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']=$variables['$theme-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$price-background-color'])) $variables['$price-background-color']=$variables['$theme-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$theme-color'];
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=$variables['$button-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 18);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=wbrptColorFunctions::lightness($variables['$block-background-color'], 7);
  if(!isset($variables['$active-block-border-color'])) $variables['$active-block-border-color']=$variables['$block-border-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-price-background-color'])) $variables['$active-price-background-color']=$variables['$price-background-color'];
  if(!isset($variables['$active-price-border-color'])) $variables['$active-price-border-color']=$variables['$active-price-background-color'];
  if(!isset($variables['$active-price-box-shadow-color'])) $variables['$active-price-box-shadow-color']='#000';
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.pako-style1-<?php echo $colorClass; ?> .pt-back,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-title,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-price,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-price:before,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-price:before {
  border-color: <?php echo $variables['$active-price-border-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-price:after,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-price:after {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-btn,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-btn:hover,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-btn:hover,
.pako-style1-<?php echo $colorClass; ?> .pt-btn:focus,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-list,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  border-color: <?php echo $variables['$active-block-border-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-price,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-price,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-price:after,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-price:after,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price:after,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price:after {
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
  -webkit-box-shadow: 0 3px 4px 1px <?php echo wbrptColorFunctions::rgba($variables['$active-price-box-shadow-color'], 0.5); ?>;
  box-shadow: 0 3px 4px 1px <?php echo wbrptColorFunctions::rgba($variables['$active-price-box-shadow-color'], 0.5); ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.pako-style1[class*='pt-animation-'] .pako-style1-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako-style1 .pt-cols .pako-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako-style1 .pt-cols .pako-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.pako-style1 .pt-cols .pako-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:after,
.pako-style1 .pt-cols .pako-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:after,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price:after,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-price:after {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako-style1 .pt-cols .pako-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako-style1 .pt-cols .pako-style1-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako-style1-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.pako-style1[class*='pt-animation-']:hover .pako-style1-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.pako-style1 .pako-style1-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
