<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-exo-<?php echo $colorClass; ?> .pt-sub,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-sub {
  color: <?php echo wbrptColorFunctions::lighten('#666', 10); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-header .pt-back,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-header .pt-back {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-btn:before,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-btn:before {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  background-color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-default .pt-cols .pt-block:hover .pt-content,
.crpt-exo.pt-animation-default .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-content {
  -webkit-box-shadow: 0 0 5px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 5); ?>;
  box-shadow: 0 0 5px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 5); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-default .pt-cols .pt-block:hover .pt-sub,
.crpt-exo.pt-animation-default .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-sub,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-block:hover .pt-sub,
.crpt-exo.pt-animation-2 .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-sub {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 35); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header .pt-back,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header .pt-back,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-default .pt-cols .pt-block:hover .pt-header .pt-back,
.crpt-exo.pt-animation-default .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-header .pt-back,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-block:hover .pt-header .pt-back,
.crpt-exo.pt-animation-2 .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-header .pt-back {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header:hover .pt-back,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header:hover .pt-back,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-default .pt-cols .pt-block:hover .pt-header:hover .pt-back,
.crpt-exo.pt-animation-default .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-header:hover .pt-back,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-block:hover .pt-header:hover .pt-back,
.crpt-exo.pt-animation-2 .pt-cols .crpt-exo-<?php echo $colorClass; ?> .pt-block:hover .pt-header:hover .pt-back {
  -webkit-box-shadow: 25px 0 25px -25px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -25px 0 25px -25px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 25px 0 25px -25px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -25px 0 25px -25px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-title,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-cols .pt-col .pt-block:hover .pt-title,
.crpt-exo.pt-animation-1 .pt-cols .crpt-exo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-price-container,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-price-container,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-cols .pt-col .pt-block:hover .pt-price-container,
.crpt-exo.pt-animation-1 .pt-cols .crpt-exo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-container,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-btn-text,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-btn-text,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-cols .pt-col .pt-block:hover .pt-btn-text,
.crpt-exo.pt-animation-1 .pt-cols .crpt-exo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn-text,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-list,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-cols .pt-col .pt-block:hover .pt-list,
.crpt-exo.pt-animation-1 .pt-cols .crpt-exo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-footer,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-footer,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-cols .pt-col .pt-block:hover .pt-footer,
.crpt-exo.pt-animation-1 .pt-cols .crpt-exo-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-footer {
  -webkit-box-shadow: 0 0 5px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 5); ?>;
  box-shadow: 0 0 5px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 5); ?>;
}
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-sub,
.crpt-exo.pt-animation-default:hover .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-sub,
.crpt-exo.pt-animation-2:hover .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-sub,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-sub {
  color: <?php echo wbrptColorFunctions::lighten('#666', 10); ?>;
}
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-selected .pt-header .pt-back,
.crpt-exo.pt-animation-default:hover .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header .pt-back,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-selected .pt-header .pt-back,
.crpt-exo.pt-animation-2:hover .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header .pt-back,
.crpt-exo-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-header .pt-back,
.crpt-exo.pt-animation-1 .crpt-exo-<?php echo $colorClass; ?> .pt-selected .pt-header .pt-back {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-exo .crpt-exo-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
