<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='lighten($theme-color, 9)';
  $default['$block-folded-corner-color']='lighten($block-background-color, 20)';
  $default['$block-folded-corner-shadow-color']='#000';
  $default['$title-color']='#fff';
  $default['$price-color']='#fff';
  $default['$price-background-color']='$theme-color';
  $default['$price-border-color']='lighten($block-background-color, 9)';
  $default['$list-color']='#fff';
  $default['$list-border-color']='lighten($block-background-color, 9)';
  $default['$button-color']='#fff';
  $default['$button-background-color']='lighten($block-background-color, 9)';
  $default['$button-box-shadow-color']='darken($button-background-color, 18)';
  $default['$hover-button-color']='$button-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$hover-button-box-shadow-color']='darken($hover-button-background-color, 18)';
  $default['$tooltip-background-color']='lightness($theme-color, 18)';
  $default['$active-block-background-color']='darken($block-background-color, 9)';
  $default['$active-block-folded-corner-color']='lighten($active-block-background-color, 20)';
  $default['$active-block-folded-corner-shadow-color']='$block-folded-corner-shadow-color';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$price-color';
  $default['$active-price-background-color']='darken($price-background-color, 12)';
  $default['$active-price-border-color']='lighten($active-block-background-color, 9)';
  $default['$active-list-color']='$list-color';
  $default['$active-list-border-color']='lighten($active-block-background-color, 12)';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='lighten($active-block-background-color, 9)';
  $default['$active-button-box-shadow-color']='darken($active-button-background-color, 18)';
  $default['$active-hover-button-color']='$active-button-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
  $default['$active-hover-button-box-shadow-color']='darken($active-hover-button-background-color, 18)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 9);
  if(!isset($variables['$block-folded-corner-color'])) $variables['$block-folded-corner-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 20);
  if(!isset($variables['$block-folded-corner-shadow-color'])) $variables['$block-folded-corner-shadow-color']='#000';
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$price-background-color'])) $variables['$price-background-color']=$variables['$theme-color'];
  if(!isset($variables['$price-border-color'])) $variables['$price-border-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 9);
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$list-border-color'])) $variables['$list-border-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 9);
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 9);
  if(!isset($variables['$button-box-shadow-color'])) $variables['$button-box-shadow-color']=wbrptColorFunctions::darken($variables['$button-background-color'], 18);
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=$variables['$button-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$hover-button-box-shadow-color'])) $variables['$hover-button-box-shadow-color']=wbrptColorFunctions::darken($variables['$hover-button-background-color'], 18);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 18);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=wbrptColorFunctions::darken($variables['$block-background-color'], 9);
  if(!isset($variables['$active-block-folded-corner-color'])) $variables['$active-block-folded-corner-color']=wbrptColorFunctions::lighten($variables['$active-block-background-color'], 20);
  if(!isset($variables['$active-block-folded-corner-shadow-color'])) $variables['$active-block-folded-corner-shadow-color']=$variables['$block-folded-corner-shadow-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-price-background-color'])) $variables['$active-price-background-color']=wbrptColorFunctions::darken($variables['$price-background-color'], 12);
  if(!isset($variables['$active-price-border-color'])) $variables['$active-price-border-color']=wbrptColorFunctions::lighten($variables['$active-block-background-color'], 9);
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-list-border-color'])) $variables['$active-list-border-color']=wbrptColorFunctions::lighten($variables['$active-block-background-color'], 12);
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=wbrptColorFunctions::lighten($variables['$active-block-background-color'], 9);
  if(!isset($variables['$active-button-box-shadow-color'])) $variables['$active-button-box-shadow-color']=wbrptColorFunctions::darken($variables['$active-button-background-color'], 18);
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=$variables['$active-button-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
  if(!isset($variables['$active-hover-button-box-shadow-color'])) $variables['$active-hover-button-box-shadow-color']=wbrptColorFunctions::darken($variables['$active-hover-button-background-color'], 18);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.pako-style2-<?php echo $colorClass; ?> .pt-back,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-folded-corner:before,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-folded-corner:before {
  border-color: <?php echo $variables['$block-folded-corner-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-folded-corner:after,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-folded-corner:after {
  border-color: <?php echo $variables['$block-folded-corner-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-title,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-btn,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
  -webkit-box-shadow: 0px 7px 0px 0px <?php echo $variables['$button-box-shadow-color']; ?>;
  box-shadow: 0px 7px 0px 0px <?php echo $variables['$button-box-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-btn:hover,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-btn:hover,
.pako-style2-<?php echo $colorClass; ?> .pt-btn:focus,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  -webkit-box-shadow: 0px 7px 0px 0px <?php echo $variables['$hover-button-box-shadow-color']; ?>;
  box-shadow: 0px 7px 0px 0px <?php echo $variables['$hover-button-box-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-list,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-list li,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-list li {
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-price-container:before,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-price-container:before,
.pako-style2-<?php echo $colorClass; ?> .pt-price-wrapper,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-price-wrapper {
  background-color: <?php echo $variables['$price-border-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-price-block,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-price-block {
  color: <?php echo $variables['$price-color']; ?>;
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-back,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-folded-corner:before,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-folded-corner:before,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-folded-corner:before,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-folded-corner:before {
  border-color: <?php echo $variables['$active-block-folded-corner-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-folded-corner:after,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-folded-corner:after,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-folded-corner:after,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-folded-corner:after {
  border-color: <?php echo $variables['$active-block-folded-corner-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-title,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-container:before,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-container:before,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-container:before,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-container:before,
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-wrapper,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-wrapper,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-wrapper,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-wrapper {
  background-color: <?php echo $variables['$active-price-border-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-block,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price-block,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-block {
  color: <?php echo $variables['$active-price-color']; ?>;
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-list,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-list li,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-list li,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list li,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list li {
  border-color: <?php echo $variables['$active-list-border-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  color: <?php echo $variables['$active-button-color']; ?>;
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  -webkit-box-shadow: 0px 7px 0px 0px <?php echo $variables['$active-button-box-shadow-color']; ?>;
  box-shadow: 0px 7px 0px 0px <?php echo $variables['$active-button-box-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.pako-style2[class*='pt-animation-'] .pako-style2-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
  -webkit-box-shadow: 0px 7px 0px 0px <?php echo $variables['$active-hover-button-box-shadow-color']; ?>;
  box-shadow: 0px 7px 0px 0px <?php echo $variables['$active-hover-button-box-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-folded-corner:before,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-folded-corner:before,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-folded-corner:before,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-folded-corner:before {
  border-color: <?php echo $variables['$block-folded-corner-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-folded-corner:after,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-folded-corner:after,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-folded-corner:after,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-folded-corner:after {
  border-color: <?php echo $variables['$block-folded-corner-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-container:before,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-container:before,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-container:before,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-container:before,
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-wrapper,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-wrapper,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-wrapper,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-wrapper {
  background-color: <?php echo $variables['$price-border-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-block,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price-block,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price-block,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-price-block {
  color: <?php echo $variables['$price-color']; ?>;
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list li,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list li,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list li,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-list li {
  border-color: <?php echo $variables['$list-border-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako-style2 .pt-cols .pako-style2-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  -webkit-box-shadow: 0px 7px 0px 0px <?php echo $variables['$button-box-shadow-color']; ?>;
  box-shadow: 0px 7px 0px 0px <?php echo $variables['$button-box-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.pako-style2-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.pako-style2[class*='pt-animation-']:hover .pako-style2-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
  -webkit-box-shadow: 0px 7px 0px 0px <?php echo $variables['$hover-button-box-shadow-color']; ?>;
  box-shadow: 0px 7px 0px 0px <?php echo $variables['$hover-button-box-shadow-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.pako-style2 .pako-style2-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
