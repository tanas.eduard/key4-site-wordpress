<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#233039';
  $default['$button-color']='#fff';
  $default['$button-background-color']='darken($theme-color, 10)';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$title-color']='#fff';
  $default['$title-background-color']='$button-background-color';
  $default['$price-color']='$button-background-color';
  $default['$list-color']='#dcdcdc';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$theme-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
  $default['$active-title-color']='$title-color';
  $default['$active-title-background-color']='$title-background-color';
  $default['$active-price-color']='#fff';
  $default['$active-list-color']='#fff';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=wbrptColorFunctions::darken($variables['$theme-color'], 10);
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$title-background-color'])) $variables['$title-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$button-background-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']='#dcdcdc';
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$theme-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-title-background-color'])) $variables['$active-title-background-color']=$variables['$title-background-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']='#fff';
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']='#fff';
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.radius-style5-<?php echo $colorClass; ?> .pt-back,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-title,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
  background-color: <?php echo $variables['$title-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-price,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-btn,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-btn:hover,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-btn:hover,
.radius-style5-<?php echo $colorClass; ?> .pt-btn:focus,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-list,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.radius-style5 .pt-cols .radius-style5-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-back,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-back,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-title,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-title,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
  background-color: <?php echo $variables['$active-title-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-price,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-price,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-list,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-list,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.radius-style5[class*='pt-animation-'] .radius-style5-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.radius-style5 .pt-cols .radius-style5-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
  background-color: <?php echo $variables['$title-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.radius-style5 .pt-cols .radius-style5-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.radius-style5 .pt-cols .radius-style5-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.radius-style5 .pt-cols .radius-style5-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.radius-style5-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.radius-style5[class*='pt-animation-']:hover .radius-style5-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.radius-style5 .radius-style5-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
