<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#fff';
  $default['$block-border-color']='#d1d1d1';
  $default['$title-border-color']='$theme-color';
  $default['$title-background-color']='$block-background-color';
  $default['$title-color']='$title-border-color';
  $default['$price-color']='$title-color';
  $default['$price-background-color']='$title-background-color';
  $default['$price-border-color']='$title-border-color';
  $default['$list-color']='#696969';
  $default['$button-color']='$title-border-color';
  $default['$hover-button-color']='highlight-color($button-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-block-border-color']='lighten($theme-color, 15)';
  $default['$active-title-border-color']='$title-border-color';
  $default['$active-title-background-color']='$active-title-border-color';
  $default['$active-title-color']='#fff';
  $default['$active-price-color']='$active-title-color';
  $default['$active-price-background-color']='$active-title-background-color';
  $default['$active-price-border-color']='$active-title-border-color';
  $default['$active-list-color']='$active-title-background-color';
  $default['$active-button-color']='$button-color';
  $default['$active-hover-button-color']='highlight-color($active-button-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#fff';
  if(!isset($variables['$block-border-color'])) $variables['$block-border-color']='#d1d1d1';
  if(!isset($variables['$title-border-color'])) $variables['$title-border-color']=$variables['$theme-color'];
  if(!isset($variables['$title-background-color'])) $variables['$title-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']=$variables['$title-border-color'];
  if(!isset($variables['$price-color'])) $variables['$price-color']=$variables['$title-color'];
  if(!isset($variables['$price-background-color'])) $variables['$price-background-color']=$variables['$title-background-color'];
  if(!isset($variables['$price-border-color'])) $variables['$price-border-color']=$variables['$title-border-color'];
  if(!isset($variables['$list-color'])) $variables['$list-color']='#696969';
  if(!isset($variables['$button-color'])) $variables['$button-color']=$variables['$title-border-color'];
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=wbrptColorFunctions::highlight($variables['$button-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-block-border-color'])) $variables['$active-block-border-color']=wbrptColorFunctions::lighten($variables['$theme-color'], 15);
  if(!isset($variables['$active-title-border-color'])) $variables['$active-title-border-color']=$variables['$title-border-color'];
  if(!isset($variables['$active-title-background-color'])) $variables['$active-title-background-color']=$variables['$active-title-border-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']='#fff';
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$active-title-color'];
  if(!isset($variables['$active-price-background-color'])) $variables['$active-price-background-color']=$variables['$active-title-background-color'];
  if(!isset($variables['$active-price-border-color'])) $variables['$active-price-border-color']=$variables['$active-title-border-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$active-title-background-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=wbrptColorFunctions::highlight($variables['$active-button-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.sketch-style7-<?php echo $colorClass; ?> .pt-back,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-title,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
  background-color: <?php echo $variables['$title-background-color']; ?>;
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-title:before,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-title:before {
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-title:after,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-title:after {
  border-color: <?php echo $variables['$title-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-price,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
  background-color: <?php echo $variables['$price-background-color']; ?>;
  border-color: <?php echo $variables['$price-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-price:before,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-price:before {
  border-color: <?php echo $variables['$price-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-price:after,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-price:after {
  border-color: <?php echo $variables['$price-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-btn,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-btn:hover,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-btn:hover,
.sketch-style7-<?php echo $colorClass; ?> .pt-btn:focus,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-list,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$block-background-color'], 10); ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-back,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-back,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
  border-color: <?php echo $variables['$active-block-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
  background-color: <?php echo $variables['$active-title-background-color']; ?>;
  border-color: <?php echo $variables['$active-title-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title:before,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title:before,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title:before,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-title-border-color'], 20); ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title:after,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title:after,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title:after,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title:after {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-title-background-color'], 20); ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
  background-color: <?php echo $variables['$active-price-background-color']; ?>;
  border-color: <?php echo $variables['$active-price-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price:before,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price:before,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price:before,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price:before {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-price-border-color'], 20); ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price:after,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price:after,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price:after,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price:after {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$active-price-background-color'], 20); ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-list,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-list,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  color: <?php echo $variables['$active-button-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.sketch-style7[class*='pt-animation-'] .sketch-style7-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-back {
  border-color: <?php echo $variables['$block-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
  background-color: <?php echo $variables['$title-background-color']; ?>;
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:before,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:before,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title:before,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title:before {
  border-color: <?php echo $variables['$title-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:after,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title:after,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title:after,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-title:after {
  border-color: <?php echo $variables['$title-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price {
  background-color: <?php echo $variables['$price-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:after,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price:after,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price:after,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price:after {
  border-color: <?php echo $variables['$price-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.sketch-style7 .pt-cols .sketch-style7-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-back {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
  border-color: <?php echo $variables['$price-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price:before,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-price:before {
  border-color: <?php echo $variables['$price-border-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.sketch-style7-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.sketch-style7[class*='pt-animation-']:hover .sketch-style7-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.sketch-style7 .sketch-style7-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
