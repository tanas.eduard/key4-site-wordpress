<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-advanced-<?php echo $colorClass; ?> .pt-btn,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-btn {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-advanced-<?php echo $colorClass; ?> .pt-btn:focus,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-btn:focus {
  -webkit-box-shadow: 200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
  box-shadow: 200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, -200px 0 200px -200px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 20); ?> inset, 0 0 2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 15); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-content,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-content {
  border-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 3); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-content:before,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-content:before {
  background: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 3); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-list,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-list {
  color: #fff;
  border-color: <?php echo wbrptColorFunctions::highlight(wbrptColorFunctions::darken(wbrptColorFunctions::lighten($variables['$theme-color'], 10), 10), 3); ?>;
  background-color: <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::lighten($variables['$theme-color'], 10), 10); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-list .pt-list-item + .pt-list-item,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-list .pt-list-item + .pt-list-item {
  border-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-list .pt-list-item:hover,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-list .pt-list-item:hover {
  background: <?php echo wbrptColorFunctions::darken(wbrptColorFunctions::darken($variables['$theme-color'], 3), 2); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-list-block {
  color: #fff;
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::highlight(wbrptColorFunctions::lighten($variables['$theme-color'], 10), 5); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-list-block:before {
  border-bottom-color: <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 3); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-advanced-<?php echo $colorClass; ?>.pt-animation-default .pt-col .pt-block:hover .pt-title,
.crpt-advanced.pt-animation-default .crpt-advanced-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title,
.crpt-advanced-<?php echo $colorClass; ?>.pt-animation-1 .pt-col .pt-block:hover .pt-title,
.crpt-advanced.pt-animation-1 .crpt-advanced-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-advanced .crpt-advanced-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
