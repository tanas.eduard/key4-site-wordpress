<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-gradient-middle-color']='$theme-color';
  $default['$block-background-gradient-start-color']='lighten($block-background-gradient-middle-color, 20)';
  $default['$block-background-gradient-end-color']='darken($block-background-gradient-middle-color, 10)';
  $default['$block-background-pattern-color']='#fff';
  $default['$block-box-shadow-color']='$block-background-gradient-middle-color';
  $default['$title-color']='#fff';
  $default['$price-color']='#fff';
  $default['$list-color']='#fff';
  $default['$button-color']='$block-background-gradient-middle-color';
  $default['$button-background-color']='#fff';
  $default['$hover-button-color']='highlight-color($button-color, 15)';
  $default['$hover-button-background-color']='$button-background-color';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-gradient-middle-color']='$block-background-gradient-start-color';
  $default['$active-block-background-gradient-start-color']='$block-background-gradient-start-color';
  $default['$active-block-background-gradient-end-color']='$block-background-gradient-middle-color';
  $default['$active-block-background-pattern-color']='$block-background-pattern-color';
  $default['$active-block-box-shadow-color']='$active-block-background-gradient-middle-color';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$price-color';
  $default['$active-list-color']='$list-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-color']='highlight-color($active-button-color, 15)';
  $default['$active-hover-button-background-color']='$active-button-background-color';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-gradient-middle-color'])) $variables['$block-background-gradient-middle-color']=$variables['$theme-color'];
  if(!isset($variables['$block-background-gradient-start-color'])) $variables['$block-background-gradient-start-color']=wbrptColorFunctions::lighten($variables['$block-background-gradient-middle-color'], 20);
  if(!isset($variables['$block-background-gradient-end-color'])) $variables['$block-background-gradient-end-color']=wbrptColorFunctions::darken($variables['$block-background-gradient-middle-color'], 10);
  if(!isset($variables['$block-background-pattern-color'])) $variables['$block-background-pattern-color']='#fff';
  if(!isset($variables['$block-box-shadow-color'])) $variables['$block-box-shadow-color']=$variables['$block-background-gradient-middle-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']='#fff';
  if(!isset($variables['$button-color'])) $variables['$button-color']=$variables['$block-background-gradient-middle-color'];
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']='#fff';
  if(!isset($variables['$hover-button-color'])) $variables['$hover-button-color']=wbrptColorFunctions::highlight($variables['$button-color'], 15);
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-gradient-middle-color'])) $variables['$active-block-background-gradient-middle-color']=$variables['$block-background-gradient-start-color'];
  if(!isset($variables['$active-block-background-gradient-start-color'])) $variables['$active-block-background-gradient-start-color']=$variables['$block-background-gradient-start-color'];
  if(!isset($variables['$active-block-background-gradient-end-color'])) $variables['$active-block-background-gradient-end-color']=$variables['$block-background-gradient-middle-color'];
  if(!isset($variables['$active-block-background-pattern-color'])) $variables['$active-block-background-pattern-color']=$variables['$block-background-pattern-color'];
  if(!isset($variables['$active-block-box-shadow-color'])) $variables['$active-block-box-shadow-color']=$variables['$active-block-background-gradient-middle-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-color'])) $variables['$active-hover-button-color']=wbrptColorFunctions::highlight($variables['$active-button-color'], 15);
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=$variables['$active-button-background-color'];
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.rainbow-style8-<?php echo $colorClass; ?> .pt-back,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-back {
  -webkit-box-shadow: 0 7px 16px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 7px 16px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-back:before,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-back:before {
  background: <?php echo $variables['$block-background-gradient-middle-color']; ?>;
  background: -webkit-linear-gradient(135deg, <?php echo $variables['$block-background-gradient-end-color']; ?> 15%, <?php echo $variables['$block-background-gradient-middle-color']; ?> 45%, <?php echo $variables['$block-background-gradient-middle-color']; ?> 55%, <?php echo $variables['$block-background-gradient-start-color']; ?> 85%);
  background: -moz-linear-gradient(135deg, <?php echo $variables['$block-background-gradient-end-color']; ?> 15%, <?php echo $variables['$block-background-gradient-middle-color']; ?> 45%, <?php echo $variables['$block-background-gradient-middle-color']; ?> 55%, <?php echo $variables['$block-background-gradient-start-color']; ?> 85%);
  background: linear-gradient(-45deg, <?php echo $variables['$block-background-gradient-end-color']; ?> 15%, <?php echo $variables['$block-background-gradient-middle-color']; ?> 45%, <?php echo $variables['$block-background-gradient-middle-color']; ?> 55%, <?php echo $variables['$block-background-gradient-start-color']; ?> 85%);
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-back:after,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-back:after {
  background: <?php echo $variables['$active-block-background-gradient-middle-color']; ?>;
  background: -webkit-linear-gradient(135deg, <?php echo $variables['$active-block-background-gradient-end-color']; ?> 15%, <?php echo $variables['$active-block-background-gradient-middle-color']; ?> 45%, <?php echo $variables['$active-block-background-gradient-middle-color']; ?> 55%, <?php echo $variables['$active-block-background-gradient-start-color']; ?> 85%);
  background: -moz-linear-gradient(135deg, <?php echo $variables['$active-block-background-gradient-end-color']; ?> 15%, <?php echo $variables['$active-block-background-gradient-middle-color']; ?> 45%, <?php echo $variables['$active-block-background-gradient-middle-color']; ?> 55%, <?php echo $variables['$active-block-background-gradient-start-color']; ?> 85%);
  background: linear-gradient(-45deg, <?php echo $variables['$active-block-background-gradient-end-color']; ?> 15%, <?php echo $variables['$active-block-background-gradient-middle-color']; ?> 45%, <?php echo $variables['$active-block-background-gradient-middle-color']; ?> 55%, <?php echo $variables['$active-block-background-gradient-start-color']; ?> 85%);
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-title,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-pattern,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-pattern {
  color: <?php echo $variables['$block-background-pattern-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-price,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-btn,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-btn:hover,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-btn:hover,
.rainbow-style8-<?php echo $colorClass; ?> .pt-btn:focus,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-list,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-back,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-back,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-back,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-back {
  -webkit-box-shadow: 0 7px 16px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
  box-shadow: 0 7px 16px 1px <?php echo $variables['$active-block-box-shadow-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-title,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-title,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-pattern,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-pattern,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-pattern,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-pattern {
  color: <?php echo $variables['$active-block-background-pattern-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-price,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-price,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-list,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.rainbow-style8[class*='pt-animation-'] .rainbow-style8-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  color: <?php echo $variables['$active-hover-button-color']; ?>;
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.rainbow-style8 .pt-cols .rainbow-style8-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-back,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-back,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-back {
  -webkit-box-shadow: 0 7px 16px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
  box-shadow: 0 7px 16px 1px <?php echo $variables['$block-box-shadow-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.rainbow-style8 .pt-cols .rainbow-style8-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-pattern,
.rainbow-style8 .pt-cols .rainbow-style8-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-pattern,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-pattern,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-pattern {
  color: <?php echo $variables['$block-background-pattern-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.rainbow-style8 .pt-cols .rainbow-style8-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.rainbow-style8 .pt-cols .rainbow-style8-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.rainbow-style8 .pt-cols .rainbow-style8-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.rainbow-style8-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.rainbow-style8[class*='pt-animation-']:hover .rainbow-style8-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  color: <?php echo $variables['$hover-button-color']; ?>;
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.rainbow-style8 .rainbow-style8-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
