<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $getDefault ) ) {
  $default['$block-background-color']='#233039';
  $default['$main-background-color']='lighten($block-background-color, 12)';
  $default['$side-header-color']='#dcdcdc';
  $default['$side-list-color']='$side-header-color';
  $default['$side-odd-list-item-background-color']='lighten($block-background-color, 7)';
  $default['$block-border-color']='$theme-color';
  $default['$title-color']='#fff';
  $default['$price-color']='#fff';
  $default['$list-color']='$side-list-color';
  $default['$button-color']='#fff';
  $default['$button-background-color']='$theme-color';
  $default['$hover-button-background-color']='highlight-color($button-background-color, 7)';
  $default['$tooltip-background-color']='lightness($theme-color, 10)';
  $default['$active-block-background-color']='$block-background-color';
  $default['$active-title-color']='$title-color';
  $default['$active-price-color']='$price-color';
  $default['$active-list-color']='$list-color';
  $default['$active-odd-list-item-background-color']='$side-odd-list-item-background-color';
  $default['$active-button-color']='$button-color';
  $default['$active-button-background-color']='$button-background-color';
  $default['$active-hover-button-background-color']='highlight-color($active-button-background-color, 7)';
} ?>
<?php if( !empty( $calculateColors ) ) {
  if(!isset($variables['$block-background-color'])) $variables['$block-background-color']='#233039';
  if(!isset($variables['$main-background-color'])) $variables['$main-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 12);
  if(!isset($variables['$side-header-color'])) $variables['$side-header-color']='#dcdcdc';
  if(!isset($variables['$side-list-color'])) $variables['$side-list-color']=$variables['$side-header-color'];
  if(!isset($variables['$side-odd-list-item-background-color'])) $variables['$side-odd-list-item-background-color']=wbrptColorFunctions::lighten($variables['$block-background-color'], 7);
  if(!isset($variables['$block-border-color'])) $variables['$block-border-color']=$variables['$theme-color'];
  if(!isset($variables['$title-color'])) $variables['$title-color']='#fff';
  if(!isset($variables['$price-color'])) $variables['$price-color']='#fff';
  if(!isset($variables['$list-color'])) $variables['$list-color']=$variables['$side-list-color'];
  if(!isset($variables['$button-color'])) $variables['$button-color']='#fff';
  if(!isset($variables['$button-background-color'])) $variables['$button-background-color']=$variables['$theme-color'];
  if(!isset($variables['$hover-button-background-color'])) $variables['$hover-button-background-color']=wbrptColorFunctions::highlight($variables['$button-background-color'], 7);
  if(!isset($variables['$tooltip-background-color'])) $variables['$tooltip-background-color']=wbrptColorFunctions::lightness($variables['$theme-color'], 10);
  if(!isset($variables['$active-block-background-color'])) $variables['$active-block-background-color']=$variables['$block-background-color'];
  if(!isset($variables['$active-title-color'])) $variables['$active-title-color']=$variables['$title-color'];
  if(!isset($variables['$active-price-color'])) $variables['$active-price-color']=$variables['$price-color'];
  if(!isset($variables['$active-list-color'])) $variables['$active-list-color']=$variables['$list-color'];
  if(!isset($variables['$active-odd-list-item-background-color'])) $variables['$active-odd-list-item-background-color']=$variables['$side-odd-list-item-background-color'];
  if(!isset($variables['$active-button-color'])) $variables['$active-button-color']=$variables['$button-color'];
  if(!isset($variables['$active-button-background-color'])) $variables['$active-button-background-color']=$variables['$button-background-color'];
  if(!isset($variables['$active-hover-button-background-color'])) $variables['$active-hover-button-background-color']=wbrptColorFunctions::highlight($variables['$active-button-background-color'], 7);
} ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-frame-<?php echo $colorClass; ?> .pt-back:before,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-back:before {
  background-color: <?php echo $variables['$block-border-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-header:before,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-header:before {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-title,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-content,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-content {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-price,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-btn,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
  color: <?php echo $variables['$button-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-btn:hover,
.crpt-frame-<?php echo $colorClass; ?> .pt-btn:focus,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-list,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-list-item:nth-child(odd),
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-list-item:nth-child(odd) {
  background-color: <?php echo $variables['$side-odd-list-item-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-side-text {
  color: <?php echo $variables['$side-header-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-list-block {
  color: <?php echo $variables['$side-list-color']; ?>;
}
.crpt-frame.crpt-frame-<?php echo $colorClass; ?> .pt-cols,
.crpt-frame.crpt-frame-<?php echo $colorClass; ?> .pt-col:before {
  background-color: <?php echo $variables['$main-background-color']; ?>;
}
.crpt-frame.crpt-frame-<?php echo $colorClass; ?> .pt-header {
  background-color: <?php echo $variables['$main-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-header:before,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-header:before,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-header:before,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-header:before {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-title,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$active-title-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-content,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-content,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-content {
  background-color: <?php echo $variables['$active-block-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-price,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-price,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-price,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price {
  color: <?php echo $variables['$active-price-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-list,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list {
  color: <?php echo $variables['$active-list-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-list-item:nth-child(odd),
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-list-item:nth-child(odd),
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-list .pt-list-item:nth-child(odd),
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list .pt-list-item:nth-child(odd) {
  background-color: <?php echo $variables['$active-odd-list-item-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn {
  background-color: <?php echo $variables['$active-button-background-color']; ?>;
  color: <?php echo $variables['$active-button-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:hover,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:hover,
.crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-col .pt-block:hover .pt-btn:focus,
.crpt-frame[class*='pt-animation-'] .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-btn:focus {
  background-color: <?php echo $variables['$active-hover-button-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-header:before,
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-header:before,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-header:before,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-header:before {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-title,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-title,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-content,
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-content,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-content,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-content {
  background-color: <?php echo $variables['$block-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-price,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-price,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-price {
  color: <?php echo $variables['$price-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-list {
  color: <?php echo $variables['$list-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list .pt-list-item:nth-child(odd),
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-list .pt-list-item:nth-child(odd),
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-list .pt-list-item:nth-child(odd),
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-list-item:nth-child(odd) {
  background-color: <?php echo $variables['$side-odd-list-item-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-cols .pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-frame .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col:nth-child(n) .pt-block.pt-not-available:nth-child(n) .pt-btn,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  color: <?php echo $variables['$button-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn {
  background-color: <?php echo $variables['$button-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:hover,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn:hover,
.crpt-frame-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-selected .pt-btn:focus,
.crpt-frame[class*='pt-animation-']:hover .crpt-frame-<?php echo $colorClass; ?> .pt-selected .pt-btn:focus {
  background-color: <?php echo $variables['$hover-button-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-col .pt-block .pt-title,
.crpt-frame.pt-animation-2 .pt-cols .crpt-frame-<?php echo $colorClass; ?>.pt-col .pt-block .pt-title {
  color: <?php echo $variables['$title-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo $variables['$tooltip-background-color']; ?>;
  border-color: <?php echo $variables['$tooltip-background-color']; ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
.crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-frame .crpt-frame-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$tooltip-background-color'], 8); ?>;
}
<?php } ?>
