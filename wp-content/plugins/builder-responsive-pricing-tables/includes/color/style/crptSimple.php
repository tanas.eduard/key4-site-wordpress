<?php if(!defined('ABSPATH')) die('Direct access of plugin file not allowed'); ?>
<?php if( !empty( $generateStyles ) ) { ?>
.crpt-simple-<?php echo $colorClass; ?> .pt-price-container:after,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-price-container:after {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-btn,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-btn {
  -webkit-box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 20); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 20); ?>;
  box-shadow: 50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, -50px 0 50px -50px <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 30); ?> inset, 0 2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 20); ?>, 0 -2px 2px -2px <?php echo wbrptColorFunctions::darken($variables['$theme-color'], 20); ?>;
  background: <?php echo $variables['$theme-color']; ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-list .pt-back:after,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-list .pt-back:after {
  background: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-title,
.crpt-simple-<?php echo $colorClass; ?>[class*='pt-animation-'] .pt-cols .pt-col .pt-block:hover .pt-title,
.crpt-simple[class*='pt-animation-'] .pt-cols .crpt-simple-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-title {
  color: <?php echo $variables['$theme-color']; ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-default .pt-cols .pt-col .pt-block:hover .pt-sub,
.crpt-simple.pt-animation-default .pt-cols .crpt-simple-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-sub,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-col .pt-block:hover .pt-sub,
.crpt-simple.pt-animation-2 .pt-cols .crpt-simple-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-sub {
  color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 35); ?>;
}
.crpt-simple-<?php echo $colorClass; ?>[class*='pt-animation-']:hover .pt-cols .pt-selected .pt-title,
.crpt-simple[class*='pt-animation-']:hover .pt-cols .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-title {
  color: #666;
}
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-default:hover .pt-cols .pt-selected .pt-sub,
.crpt-simple.pt-animation-default:hover .pt-cols .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-sub,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-2:hover .pt-cols .pt-selected .pt-sub,
.crpt-simple.pt-animation-2:hover .pt-cols .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-sub {
  color: <?php echo wbrptColorFunctions::lighten('#666', 10); ?>;
}
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-2 .pt-selected .pt-list .pt-back:after,
.crpt-simple.pt-animation-2 .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-back:after,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-col .pt-block:hover .pt-list .pt-back:after,
.crpt-simple.pt-animation-2 .pt-cols .crpt-simple-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list .pt-back:after,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-1 .pt-selected .pt-list .pt-back:after,
.crpt-simple.pt-animation-1 .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-list .pt-back:after,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-1 .pt-cols .pt-col .pt-block:hover .pt-list .pt-back:after,
.crpt-simple.pt-animation-1 .pt-cols .crpt-simple-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-list .pt-back:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-2 .pt-selected .pt-price-container:after,
.crpt-simple.pt-animation-2 .crpt-simple-<?php echo $colorClass; ?> .pt-selected .pt-price-container:after,
.crpt-simple-<?php echo $colorClass; ?>.pt-animation-2 .pt-cols .pt-col .pt-block:hover .pt-price-container:after,
.crpt-simple.pt-animation-2 .pt-cols .crpt-simple-<?php echo $colorClass; ?>.pt-col .pt-block:hover .pt-price-container:after {
  background-color: <?php echo wbrptColorFunctions::lighten($variables['$theme-color'], 10); ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content {
  background-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
  border-color: <?php echo wbrptColorFunctions::lightness($variables['$theme-color'], 10); ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-heading {
  border-bottom-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
.crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:before,
.crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after,
.crpt-simple .crpt-simple-<?php echo $colorClass; ?> .pt-tooltip .pt-tooltip-content .pt-tooltip-center-line span:after {
  background-color: <?php echo wbrptColorFunctions::lighten(wbrptColorFunctions::lightness($variables['$theme-color'], 10), 8); ?>;
}
<?php } ?>
