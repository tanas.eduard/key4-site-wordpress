<?php
if(!defined('ABSPATH')) die('Direct access of plugin file not allowed');

class wbrptGeneralSettings {
    static $data;
    static function init()
    {
        $rmpptGeneralSettings = array();

        $rmpptGeneralSettings[ 'controlList' ] = array(
            'base' => array(
                'DetailText'          => array( 'text', 'Detail text', '', array( true ) ),
                'FeaturesColumnWidth' => array( 'select', 'Features column width', '', array(
                    ''   => 'Default',
                    'xs' => 'Extra small (180px)',
                    'sm' => 'Small (220px)',
                    'md' => 'Medium (240px)',
                    'lg' => 'Large (280px)'
                ))
            ),
            'column' => array(
                'PlanMakeInactive'         => array( 'checkbox', 'Make this plan inactive', false, array( 'fa fa-lock', 'plan-state', null ) ),
                'PlanHighlight'            => array( 'checkbox', 'Highlight this plan', false, array( 'fa fa-rocket', 'plan-state', null ) ),
                'PlanShowCaption'          => array( 'checkbox', 'Show plan caption', false, array( 'fa fa-header', null, array( 'PlanCaptionHeader', 'PlanCaptionText' ) ) ),
                'PlanShowPriceCaption'     => array( 'checkbox', 'Show price caption', false, array( 'fa fa-cart-arrow-down', null, array( 'PlanPriceCaptionText' ) ) ),
                'PlanShowMostPopularSign'  => array( 'checkbox', "Show 'most popular' sign", false, array( 'fa fa-bolt', 'plan-sign', array( 'PlanMostPopularSignText' ) ) ),
                'PlanShowDiscountSign'     => array( 'checkbox', "Show 'discount' sign", false, array( 'fa fa-money', 'plan-sign', array( 'PlanDiscountSignText' ) ) ),
                'PlanShowNotAvailableSign' => array( 'checkbox', "Show 'not available' sign", false, array( 'fa fa-ban', 'plan-sign', array( 'PlanNotAvailableSignText' ) ) ),
                'PlanShowDiscountRibbon'   => array( 'checkbox', "Show 'discount' ribbon", false, array( 'fa fa-bookmark-o', 'plan-ribbon', array( 'PlanDiscountRibbonText' ) ) ),
                'PlanShowNewRibbon'        => array( 'checkbox', "Show 'new' ribbon", false, array( 'fa fa-bookmark', 'plan-ribbon', array( 'PlanNewRibbonText' ) ) ),
                'PlanShowRatingIcons'      => array( 'checkbox', 'Show rating icons', false, array( 'fa fa-star', null, array( 'PlanRatingIconsText' ) ) ),
                'PlanName'                 => array( 'text', 'Pricing plan name', '', array( true ) ),
                'PlanImageURL'             => array( 'image', 'Pricing plan image', '' ),
                'PlanIframeURL'            => array( 'text', 'Plan iframe URL', '', array( false ) ),
                'PlanPurchaseButtonName'   => array( 'text', 'Purchase button name', '', array( true ) ),
                'PlanPurchaseButtonLink'   => array( 'text', 'Purchase button link', '', array( false ) ),
                'PlanPurchaseButtonCode'   => array( 'text', 'Purchase button code', '', array( false ) ),
                'PlanPrice'                => array( 'text', 'Plan price', '', array( true ) ),
                'PlanCurrencySymbol'       => array( 'text', 'Currency symbol', '', array( true ) ),
                'PlanCaptionHeader'        => array( 'text', 'Plan caption header', '', array( true ) ),
                'PlanCaptionText'          => array( 'text', 'Plan caption text', '', array( true ) ),
                'PlanPriceCaptionText'     => array( 'text', 'Price caption text', '', array( true ) ),
                'PlanDiscountSignText'     => array( 'text', "Text for 'discount' sign", '', array( true ) ),
                'PlanMostPopularSignText'  => array( 'text', "Text for 'most popular' sign", '', array( true ) ),
                'PlanNotAvailableSignText' => array( 'text', "Text for 'not available' sign", '', array( true ) ),
                'PlanNewRibbonText'        => array( 'text', "Text for 'new' ribbon", '', array( true ) ),
                'PlanDiscountRibbonText'   => array( 'text', "Text for 'discount' ribbon", '', array( true ) ),
                'PlanRatingIconsText'      => array( 'text', 'Text for rating icons', '', array( true ) ),
                'PlanDetailsButtonName'    => array( 'text', 'Details button name', '', array( true ) ),
                'PlanDetailsButtonLink'    => array( 'text', 'Details button link', '', array( false ) )
            ),
            'row' => array(
                'FeatureTooltipType'     => array( 'radio', null, 0, array(
                    0 => array( 'fa fa-times-circle', 'Hide tooltip', false, null ),
                    1 => array( 'fa fa-comment', 'Show tooltip as icon', null, array( 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureTooltipColumns' ) ),
                    2 => array( 'fa fa-commenting', 'Show tooltip as text', null, array( 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureTooltipColumns' ) )
                )),
                'FeatureTooltipColumns'  => array( 'checkbox', 'Tooltip with 2 columns', false, array( 'fa fa-columns', null, null ) ),
                'FeatureName'            => array( 'text', 'Feature name', '', array( true ) ),
                'FeatureTooltipHeader'   => array( 'text', 'Tooltip header', '', array( true ) ),
                'FeatureTooltipContent'  => array( 'textarea', 'Tooltip content', '', array( true ) ),
                'FeatureTooltipPosition' => array( 'select', 'Tooltip position', 't', array(
                    't'  => 'Top',
                    'tl' => 'Top-left',
                    'tr' => 'Top-right',
                    'b'  => 'Bottom',
                    'bl' => 'Bottom-left',
                    'br' => 'Bottom-right',
                    'lt' => 'Left-top',
                    'l'  => 'Left-middle',
                    'lb' => 'Left-bottom',
                    'rt' => 'Right-top',
                    'r'  => 'Right-middle',
                    'rb' => 'Right-bottom'
                )),
                'FeatureRowHeight'       => array( 'select', 'Feature row height', 1, array(
                    1 => 'Single',
                    2 => 'Double',
                    3 => 'Triple'
                ))
            ),
            'cell' => array(
                'FeatureValueTooltipType'     => array( 'radio', null, 0, array(
                    0 => array( 'fa fa-times-circle', 'Hide tooltip', false, null ),
                    1 => array( 'fa fa-comment', 'Show tooltip as icon', null, array( 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValueTooltipColumns' ) ),
                    2 => array( 'fa fa-commenting', 'Show tooltip as text', null, array( 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValueTooltipColumns' ) )
                )),
                'FeatureValueTooltipColumns'  => array( 'checkbox', 'Tooltip with 2 columns', false, array( 'fa fa-columns', null, null ) ),
                'FeatureValue'                   => array( 'text', 'Feature value', '', array( true ) ),
                'FeatureValueTooltipHeader'   => array( 'text', 'Tooltip header', '', array( true ) ),
                'FeatureValueTooltipContent'     => array( 'textarea', 'Tooltip content', '', array( true ) ),
                'FeatureValueTooltipPosition' => array( 'select', 'Tooltip position', 't', array(
                    't'  => 'Top',
                    'tl' => 'Top-left',
                    'tr' => 'Top-right',
                    'b'  => 'Bottom',
                    'bl' => 'Bottom-left',
                    'br' => 'Bottom-right',
                    'lt' => 'Left-top',
                    'l'  => 'Left-middle',
                    'lb' => 'Left-bottom',
                    'rt' => 'Right-top',
                    'r'  => 'Right-middle',
                    'rb' => 'Right-bottom'
                )),
                'FeatureValuePrice'                => array( 'text', 'Feature value price', '', array( true ) ),
                'FeatureValueCurrencySymbol'       => array( 'text', 'Currency symbol', '', array( true ) ),
                'FeatureValuePurchaseButtonName'   => array( 'text', 'Purchase button name', '', array( true ) ),
                'FeatureValuePurchaseButtonLink'   => array( 'text', 'Purchase button link', '', array( false ) ),
                'FeatureValuePurchaseButtonCode'   => array( 'text', 'Purchase button code', '', array( false ) )
            )
        );

        $rmpptGeneralSettings[ 'designList' ] = array(
            'crpt-modern' => array(
                'name'         => 'Modern',
                'controlList'  => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanShowDiscountRibbon', 'PlanShowNewRibbon', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'PlanNewRibbonText', 'PlanDiscountRibbonText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptModern.php',
                'importFile'   => 'crptCommon.txt',
                'defaultColor' => '4682b4',
                'colorFile'    => 'crptModern.php'
            ),
            'crpt-colorful' => array(
                'name'                 => 'Colorful',
                'navigationItemNumber' => 2,
                'controlList'          => array( 'DetailText', 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanShowRatingIcons', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanRatingIconsText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValue' ),
                'template'             => 'crptColorful.php',
                'importFile'           => 'crpt2Versions.txt',
                'defaultColor'         => 'b22222',
                'colorFile'            => 'crptColorful.php'
            ),
            'crpt-extra' => array(
                'name'                 => 'Extra',
                'navigationItemNumber' => 2,
                'controlList'          => array( 'DetailText', 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'             => 'crptExtra.php',
                'importFile'           => 'crpt2Versions.txt',
                'defaultColor'         => '708090',
                'colorFile'            => 'crptExtra.php'
            ),
            'crpt-exo' => array(
                'name'         => 'Exo',
                'controlList'  => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptExo.php',
                'importFile'   => 'crptCommon.txt',
                'defaultColor' => 'ff8000',
                'colorFile'    => 'crptExo.php',
                'styleList'    => array(
                    'google-font-exo2-400-500'
                )
            ),
            'crpt-hipo' => array(
                'name'                 => 'Hipo',
                'navigationItemNumber' => 2,
                'controlList'          => array( 'DetailText', 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'             => 'crptHipo.php',
                'cssIdentifier'        => 'hipo',
                'importFile'           => 'crpt2Versions.txt',
                'defaultColor'         => '355e74',
                'colorFile'            => 'crptHipo.php'
            ),
            'crpt-advanced' => array(
                'name'                 => 'Advanced',
                'navigationItemNumber' => 3,
                'controlList'          => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'             => 'crptAdvanced.php',
                'importFile'           => 'crpt3Versions.txt',
                'defaultColor'         => '333333',
                'colorFile'            => 'crptAdvanced.php',
                'styleList'            => array(
                    'google-font-abeezee'
                )
            ),
            'crpt-simple' => array(
                'name'         => 'Simple',
                'controlList'  => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValue' ),
                'template'     => 'crptSimple.php',
                'importFile'   => 'crptCommon.txt',
                'defaultColor' => '7ba428',
                'colorFile'    => 'crptSimple.php',
                'styleList'    => array(
                    'google-font-abeezee'
                )
            ),
            'crpt-tabby' => array(
                'name'                 => 'Tabby',
                'navigationItemNumber' => 3,
                'controlList'          => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'             => 'crptTabby.php',
                'importFile'           => 'crpt3Versions.txt',
                'defaultColor'         => 'ed4d13',
                'colorFile'            => 'crptTabby.php'
            ),
            'crpt-flat' => array(
                'name'         => 'Flat',
                'controlList'  => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptFlat.php',
                'importFile'   => 'crptFlatRibbon.txt',
                'defaultColor' => 'fecd0f',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'crptFlat.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-900'
                )
            ),
            'crpt-ribbon' => array(
                'name'         => 'Ribbon',
                'controlList'  => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptRibbon.php',
                'importFile'   => 'crptFlatRibbon.txt',
                'defaultColor' => 'fecd0f',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'crptRibbon.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'crpt-plain' => array(
                'name'         => 'Plain',
                'controlList'  => array( 'DetailText', 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptPlain.php',
                'importFile'   => 'crptPlainFrame.txt',
                'defaultColor' => '2397f2',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'crptPlain.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-500'
                )
            ),
            'crpt-frame' => array(
                'name'         => 'Frame',
                'controlList'  => array( 'DetailText', 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptFrame.php',
                'importFile'   => 'crptPlainFrame.txt',
                'defaultColor' => '90d400',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'crptFrame.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-500'
                )
            ),
            'crpt-plaid' => array(
                'name'         => 'Plaid',
                'controlList'  => array( 'FeaturesColumnWidth', 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureRowHeight', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'crptPlaid.php',
                'importFile'   => 'crptPlaid.txt',
                'defaultColor' => '2397f2',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'crptPlaid.php',
                'styleList'    => array(
                    'google-font-roboto-400-700'
                )
            ),
            'rmsbpt-modern' => array(
                'name'         => 'Modern',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rmsbptModern.php',
                'importFile'   => 'rmsbptModern.txt',
                'defaultColor' => '228b22',
                'colorFile'    => 'rmsbptModern.php'
            ),
            'rmsbpt-flat' => array(
                'name'         => 'Flat',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanShowPriceCaption', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanCaptionText', 'PlanPriceCaptionText', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rmsbptFlat.php',
                'importFile'   => 'rmsbptExtraFlat.txt',
                'defaultColor' => '3499fe',
                'colorFile'    => 'rmsbptFlat.php',
                'styleList'    => array(
                    'google-font-abeezee'
                )
            ),
            'rmsbpt-extra' => array(
                'name'         => 'Extra',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rmsbptExtra.php',
                'importFile'   => 'rmsbptExtraFlat.txt',
                'defaultColor' => 'dc143c',
                'colorFile'    => 'rmsbptExtra.php',
                'styleList'    => array(
                    'google-font-abeezee'
                )
            ),
            'csmrpt-solid' => array(
                'name'         => 'Solid',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanShowPriceCaption', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanCaptionText', 'PlanPriceCaptionText', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'csmrptSolid.php',
                'importFile'   => 'csmrptSolid.txt',
                'defaultColor' => '4682b4',
                'colorFile'    => 'csmrptSolid.php'
            ),
            'csmrpt-modern' => array(
                'name'         => 'Modern',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanCaptionText', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureTooltipType', 'FeatureTooltipColumns', 'FeatureName', 'FeatureTooltipHeader', 'FeatureTooltipContent', 'FeatureTooltipPosition', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'csmrptModern.php',
                'importFile'   => 'csmrptModern.txt',
                'defaultColor' => 'b22222',
                'colorFile'    => 'csmrptModern.php'
            ),
            'csmrpt-classic' => array(
                'name'         => 'Classic',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanShowPriceCaption', 'PlanShowMostPopularSign', 'PlanShowDiscountSign', 'PlanShowNotAvailableSign', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanCaptionText', 'PlanPriceCaptionText', 'PlanDiscountSignText', 'PlanMostPopularSignText', 'PlanNotAvailableSignText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'csmrptClassic.php',
                'importFile'   => 'csmrptClassic.txt',
                'defaultColor' => '39b54a',
                'colorFile'    => 'csmrptClassic.php'
            ),
            'smoozy-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol' ),
                'template'     => 'smoozyStyle1Style10.php',
                'importFile'   => 'smoozyStyle1Style2.txt',
                'defaultColor' => 'dc143c',
                'colorFile'    => 'smoozyStyle1.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol' ),
                'template'     => 'smoozyStyle2Style3.php',
                'importFile'   => 'smoozyStyle1Style2.txt',
                'defaultColor' => 'ed4d13',
                'colorFile'    => 'smoozyStyle2.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol' ),
                'template'     => 'smoozyStyle2Style3.php',
                'importFile'   => 'smoozyStyle3Style5.txt',
                'defaultColor' => '9400d3',
                'colorFile'    => 'smoozyStyle3.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionHeader', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol' ),
                'template'     => 'smoozyStyle4Style5.php',
                'importFile'   => 'smoozyStyle4.txt',
                'defaultColor' => 'c44767',
                'colorFile'    => 'smoozyStyle4.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionHeader', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol' ),
                'template'     => 'smoozyStyle4Style5.php',
                'importFile'   => 'smoozyStyle3Style5.txt',
                'defaultColor' => '00a594',
                'colorFile'    => 'smoozyStyle5.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style6' => array(
                'name'         => 'Style 6',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol', 'FeatureValuePurchaseButtonName', 'FeatureValuePurchaseButtonLink', 'FeatureValuePurchaseButtonCode' ),
                'template'     => 'smoozyStyle6.php',
                'importFile'   => 'smoozyStyle6.txt',
                'defaultColor' => 'ff8000',
                'colorFile'    => 'smoozyStyle6.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style7' => array(
                'name'         => 'Style 7',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanDetailsButtonName', 'PlanDetailsButtonLink', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'smoozyStyle7.php',
                'importFile'   => 'smoozyStyle7Style9.txt',
                'defaultColor' => '3499fe',
                'colorFile'    => 'smoozyStyle7.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style8' => array(
                'name'         => 'Style 8',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanIframeURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'PlanDetailsButtonName', 'PlanDetailsButtonLink', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'smoozyStyle8.php',
                'importFile'   => 'smoozyStyle8.txt',
                'defaultColor' => '7ba428',
                'colorFile'    => 'smoozyStyle8.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style9' => array(
                'name'         => 'Style 9',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanDetailsButtonName', 'PlanDetailsButtonLink', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'smoozyStyle9.php',
                'importFile'   => 'smoozyStyle7Style9.txt',
                'defaultColor' => '708090',
                'colorFile'    => 'smoozyStyle9.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style10' => array(
                'name'         => 'Style 10',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanCaptionText', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition', 'FeatureValuePrice', 'FeatureValueCurrencySymbol' ),
                'template'     => 'smoozyStyle1Style10.php',
                'importFile'   => 'smoozyStyle10.txt',
                'defaultColor' => 'c44767',
                'colorFile'    => 'smoozyStyle10.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'smoozy-style11' => array(
                'name'         => 'Style 11',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowPriceCaption', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanPriceCaptionText', 'PlanDetailsButtonName', 'PlanDetailsButtonLink', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'smoozyStyle11.php',
                'importFile'   => 'smoozyStyle11.txt',
                'defaultColor' => 'ff8000',
                'colorFile'    => 'smoozyStyle11.php',
                'styleList'    => array(
                    'google-font-amaranth-400-italic',
                    'google-font-alegreya-sans-500',
                    'google-font-allura'
                )
            ),
            'flat-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle1Style2.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => '1a2215',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle1Style2.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => 'ff9700',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle3.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => '78b100',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle3.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle4.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => '099dbb',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle4.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle5Style6.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => '78b100',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle5.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style6' => array(
                'name'         => 'Style 6',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle5Style6.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => 'b21325',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle6.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style7' => array(
                'name'         => 'Style 7',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle7Style8.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => 'a9bec5',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle7.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'flat-style8' => array(
                'name'         => 'Style 8',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'flatStyle7Style8.php',
                'importFile'   => 'flatCommon.txt',
                'defaultColor' => 'b21325',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'flatStyle8.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'ribbon-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle1.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '2397f2',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'ribbonStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'ribbon-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle2Style3Style4.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '2397f2',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'ribbonStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'ribbon-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle2Style3Style4.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '1a2215',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'ribbonStyle3.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'ribbon-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle2Style3Style4.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => 'ff9700',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'ribbonStyle4.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'ribbon-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle5.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '78b100',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'ribbonStyle5.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'radius-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusStyle1Style2.php',
                'importFile'   => 'radiusStyle1.txt',
                'defaultColor' => 'b1d8e5',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'radius-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusStyle1Style2.php',
                'importFile'   => 'radiusStyle2.txt',
                'defaultColor' => 'a9bec5',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'radius-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusStyle3.php',
                'importFile'   => 'radiusStyle3.txt',
                'defaultColor' => '2cb2c9',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle3.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'radius-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusCommon.php',
                'importFile'   => 'radiusCommon.txt',
                'defaultColor' => 'b1d8e5',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle4.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-900'
                )
            ),
            'radius-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusCommon.php',
                'importFile'   => 'radiusCommon.txt',
                'defaultColor' => '4d5873',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle5.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-900'
                )
            ),
            'radius-style6' => array(
                'name'         => 'Style 6',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusCommon.php',
                'importFile'   => 'radiusCommon.txt',
                'defaultColor' => '4d5873',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle6.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-900'
                )
            ),
            'radius-style7' => array(
                'name'         => 'Style 7',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusCommon.php',
                'importFile'   => 'radiusCommon.txt',
                'defaultColor' => '009946',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle7.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-900'
                )
            ),
            'radius-style8' => array(
                'name'         => 'Style 8',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusCommon.php',
                'importFile'   => 'radiusCommon.txt',
                'defaultColor' => '54ab3e',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'radiusStyle8.php',
                'styleList'    => array(
                    'google-font-roboto-300-400-900'
                )
            ),
            'sketch-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusStyle1Style2.php',
                'importFile'   => 'radiusStyle1.txt',
                'defaultColor' => '00b69c',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'sketch-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'radiusStyle1Style2.php',
                'importFile'   => 'radiusStyle2.txt',
                'defaultColor' => '597489',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'sketch-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle1.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '87b600',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle3.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'sketch-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle2Style3Style4.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '00b69c',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle4.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'sketch-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle2Style3Style4.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => 'b01639',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle5.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'sketch-style6' => array(
                'name'         => 'Style 6',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle2Style3Style4.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '597489',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle6.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'sketch-style7' => array(
                'name'         => 'Style 7',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ribbonStyle5.php',
                'importFile'   => 'ribbonCommon.txt',
                'defaultColor' => '1c80a3',
                'iconPattern'  => array( '$theme-color', '$active-block-border-color' ),
                'colorFile'    => 'sketchStyle7.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle1.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => 'f19e38',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle2.php',
                'importFile'   => 'gradient4Features.txt',
                'defaultColor' => 'f45099',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle3.php',
                'importFile'   => 'gradient4Features.txt',
                'defaultColor' => '7dc6b8',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle3.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle4.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => '97c956',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle4.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle5.php',
                'importFile'   => 'gradient4Features.txt',
                'defaultColor' => '513cf0',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle5.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style6' => array(
                'name'         => 'Style 6',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle6.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => '7e0c0d',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle6.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style7' => array(
                'name'         => 'Style 7',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle7.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => 'db6b55',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle7.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style8' => array(
                'name'         => 'Style 8',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle8.php',
                'importFile'   => 'gradient4Features.txt',
                'defaultColor' => '0ec173',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle8.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style9' => array(
                'name'         => 'Style 9',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle9.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => 'e22282',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle9.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style10' => array(
                'name'         => 'Style 10',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle10.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => '7dc6b8',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle10.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style11' => array(
                'name'         => 'Style 11',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle11.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => '8bcce2',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle11.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style12' => array(
                'name'         => 'Style 12',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle12.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => '0ec173',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle12.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'gradient-style13' => array(
                'name'         => 'Style 13',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'gradientStyle13.php',
                'importFile'   => 'gradient3Features.txt',
                'defaultColor' => 'e22282',
                'iconPattern'  => array( '$theme-color', '$block-background-gradient-end-color' ),
                'colorFile'    => 'gradientStyle13.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle1.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => '1569d2',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle2.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => 'ce0f86',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle3.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => '2397db',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle3.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle4.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => '51d6af',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle4.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style5' => array(
                'name'         => 'Style 5',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle5.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => '802eaa',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle5.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style6' => array(
                'name'         => 'Style 6',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle6.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => '1c67ff',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle6.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style7' => array(
                'name'         => 'Style 7',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle7.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => '47d225',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle7.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rainbow-style8' => array(
                'name'         => 'Style 8',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'rainbowStyle8.php',
                'importFile'   => 'rainbowCommon.txt',
                'defaultColor' => 'bffff9',
                'iconPattern'  => array( '$theme-color', '$active-block-background-gradient-middle-color' ),
                'colorFile'    => 'rainbowStyle8.php',
                'styleList'    => array(
                    'google-font-roboto-300-400'
                )
            ),
            'rings-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ringsStyle1.php',
                'importFile'   => 'ringsStyle1.txt',
                'defaultColor' => '186b0d',
                'iconPattern'  => array( '$theme-color' ),
                'colorFile'    => 'ringsStyle1.php',
                'styleList'    => array(
                    'google-font-allura',
                    'google-font-amaranth-400-italic',
                    'google-font-roboto-400'
                )
            ),
            'rings-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ringsStyle2Style3.php',
                'importFile'   => 'ringsStyle2.txt',
                'defaultColor' => 'f6840b',
                'iconPattern'  => array( '$theme-color' ),
                'colorFile'    => 'ringsStyle2.php',
                'styleList'    => array(
                    'google-font-allura',
                    'google-font-amaranth-400-italic',
                    'google-font-roboto-400'
                )
            ),
            'rings-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ringsStyle2Style3.php',
                'importFile'   => 'ringsStyle3Style4.txt',
                'defaultColor' => 'b5ba22',
                'iconPattern'  => array( '$theme-color' ),
                'colorFile'    => 'ringsStyle3.php',
                'styleList'    => array(
                    'google-font-allura',
                    'google-font-amaranth-400-italic',
                    'google-font-roboto-400'
                )
            ),
            'rings-style4' => array(
                'name'         => 'Style 4',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanImageURL', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'ringsStyle4.php',
                'importFile'   => 'ringsStyle3Style4.txt',
                'defaultColor' => '7c1962',
                'iconPattern'  => array( '$theme-color' ),
                'colorFile'    => 'ringsStyle4.php',
                'styleList'    => array(
                    'google-font-allura',
                    'google-font-amaranth-400-italic',
                    'google-font-roboto-400'
                )
            ),
            'pako-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'pakoStyle1.php',
                'importFile'   => 'pakoCommon.txt',
                'defaultColor' => '00aeef',
                'iconPattern'  => array( '$theme-color', '$block-background-color', '$active-block-background-color' ),
                'colorFile'    => 'pakoStyle1.php',
                'styleList'    => array(
                    'google-font-roboto-300-500'
                )
            ),
            'pako-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'pakoStyle2.php',
                'importFile'   => 'pakoCommon.txt',
                'defaultColor' => '334654',
                'iconPattern'  => array( '$theme-color', '$block-background-color', '$active-block-background-color' ),
                'colorFile'    => 'pakoStyle2.php',
                'styleList'    => array(
                    'google-font-roboto-300-500'
                )
            ),
            'pako-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'pakoStyle3.php',
                'importFile'   => 'pakoCommon.txt',
                'defaultColor' => '00a99d',
                'iconPattern'  => array( '$theme-color', '$block-background-color', '$active-block-background-color' ),
                'colorFile'    => 'pakoStyle3.php',
                'styleList'    => array(
                    'google-font-pt-sans-400-700'
                )
            ),
            'pako2-style1' => array(
                'name'         => 'Style 1',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowRatingIcons', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanRatingIconsText', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'pako2Style1.php',
                'importFile'   => 'pakoCommon.txt',
                'defaultColor' => '5fa886',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'pako2Style1.php',
                'styleList'    => array(
                    'google-font-roboto-300-500'
                )
            ),
            'pako2-style2' => array(
                'name'         => 'Style 2',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanShowRatingIcons', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'PlanRatingIconsText', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'pako2Style2.php',
                'importFile'   => 'pakoCommon.txt',
                'defaultColor' => '5674b9',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'pako2Style2.php',
                'styleList'    => array(
                    'google-font-roboto-300-500'
                )
            ),
            'pako2-style3' => array(
                'name'         => 'Style 3',
                'controlList'  => array( 'PlanMakeInactive', 'PlanHighlight', 'PlanDesignColor', 'PlanName', 'PlanPurchaseButtonName', 'PlanPurchaseButtonLink', 'PlanPurchaseButtonCode', 'PlanPrice', 'PlanCurrencySymbol', 'FeatureName', 'FeatureValueTooltipType', 'FeatureValueTooltipColumns', 'FeatureValue', 'FeatureValueTooltipHeader', 'FeatureValueTooltipContent', 'FeatureValueTooltipPosition' ),
                'template'     => 'pako2Style3.php',
                'importFile'   => 'pakoCommon.txt',
                'defaultColor' => 'd18730',
                'iconPattern'  => array( '$theme-color', '$block-background-color' ),
                'colorFile'    => 'pako2Style3.php',
                'styleList'    => array(
                    'google-font-roboto-300-500'
                )
            )
        );

        $rmpptGeneralSettings[ 'designGroupList' ] = array(
            'crpt' => array(
                'name'       => 'Comparative Responsive Pricing Tables',
                'designList' => array( 'crpt-modern', 'crpt-colorful', 'crpt-extra', 'crpt-exo', 'crpt-hipo', 'crpt-advanced', 'crpt-simple', 'crpt-tabby', 'crpt-flat', 'crpt-ribbon', 'crpt-plain', 'crpt-frame', 'crpt-plaid' )
            ),
            'rmsbpt' => array(
                'name'       => 'Responsive Multi Style Bootstrap Pricing Tables',
                'designList' => array( 'rmsbpt-modern', 'rmsbpt-flat', 'rmsbpt-extra' )
            ),
            'csmrpt' => array(
                'name'       => 'Classic Solid Modern - Responsive Pricing Tables',
                'designList' => array( 'csmrpt-solid', 'csmrpt-modern', 'csmrpt-classic' )
            ),
            'smoozy' => array(
                'name'       => 'Smoozy - Responsive Pricing Tables',
                'designList' => array( 'smoozy-style1', 'smoozy-style2', 'smoozy-style3', 'smoozy-style4', 'smoozy-style5', 'smoozy-style6', 'smoozy-style7', 'smoozy-style8', 'smoozy-style9', 'smoozy-style10', 'smoozy-style11' )
            ),
            'flat' => array(
                'name'       => 'Flat - Responsive Pricing Tables',
                'designList' => array( 'flat-style1', 'flat-style2', 'flat-style3', 'flat-style4', 'flat-style5', 'flat-style6', 'flat-style7', 'flat-style8' )
            ),
            'ribbon' => array(
                'name'       => 'Ribbon - Responsive Pricing Tables',
                'designList' => array( 'ribbon-style1', 'ribbon-style2', 'ribbon-style3', 'ribbon-style4', 'ribbon-style5' )
            ),
            'radius' => array(
                'name'       => 'Radius - Responsive Pricing Tables',
                'designList' => array( 'radius-style1', 'radius-style2', 'radius-style3', 'radius-style4', 'radius-style5', 'radius-style6', 'radius-style7', 'radius-style8' )
            ),
            'sketch' => array(
                'name'       => 'Sketch - Responsive Pricing Tables',
                'designList' => array( 'sketch-style1', 'sketch-style2', 'sketch-style3', 'sketch-style4', 'sketch-style5', 'sketch-style6', 'sketch-style7' )
            ),
            'gradient' => array(
                'name'       => 'Gradient - Responsive Pricing Tables',
                'designList' => array( 'gradient-style1', 'gradient-style2', 'gradient-style3', 'gradient-style4', 'gradient-style5', 'gradient-style6', 'gradient-style7', 'gradient-style8', 'gradient-style9', 'gradient-style10', 'gradient-style11', 'gradient-style12', 'gradient-style13' )
            ),
            'rainbow' => array(
                'name'       => 'Rainbow - Responsive Pricing Tables',
                'designList' => array( 'rainbow-style1', 'rainbow-style2', 'rainbow-style3', 'rainbow-style4', 'rainbow-style5', 'rainbow-style6', 'rainbow-style7', 'rainbow-style8' )
            ),
            'rings' => array(
                'name'       => 'Rings - Responsive Pricing Tables',
                'designList' => array( 'rings-style1', 'rings-style2', 'rings-style3', 'rings-style4' )
            ),
            'pako' => array(
                'name'       => 'Pako - Responsive pricing tables',
                'designList' => array( 'pako-style1', 'pako-style2', 'pako-style3' )
            ),
            'pako2' => array(
                'name'       => 'Pako 2 - Responsive pricing tables',
                'designList' => array( 'pako2-style1', 'pako2-style2', 'pako2-style3' )
            )
        );

        $rmpptGeneralSettings[ 'additionalStyleList' ] = array(
            'google-font-amaranth-400-italic' => 'https://fonts.googleapis.com/css?family=Amaranth:400i',
            'google-font-alegreya-sans-500'   => 'https://fonts.googleapis.com/css?family=Alegreya+Sans:500&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-pt-sans-400-700'     => 'https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext',
            'google-font-allura'              => 'https://fonts.googleapis.com/css?family=Allura&subset=latin-ext',
            'google-font-exo2-400-500'        => 'https://fonts.googleapis.com/css?family=Exo+2:400,500&subset=cyrillic,latin-ext',
            'google-font-abeezee'             => 'https://fonts.googleapis.com/css?family=ABeeZee',
            'google-font-roboto-300-400'      => 'https://fonts.googleapis.com/css?family=Roboto:300,400&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-roboto-400-700'      => 'https://fonts.googleapis.com/css?family=Roboto:400,700&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-roboto-300-400-500'  => 'https://fonts.googleapis.com/css?family=Roboto:300,400,500&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-roboto-300-400-700'  => 'https://fonts.googleapis.com/css?family=Roboto:300,400,700&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-roboto-300-400-900'  => 'https://fonts.googleapis.com/css?family=Roboto:300,400,900&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-roboto-300-500'      => 'https://fonts.googleapis.com/css?family=Roboto:300,500&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese',
            'google-font-roboto-400'          => 'https://fonts.googleapis.com/css?family=Roboto:400&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese'
        );

        self::$data = $rmpptGeneralSettings;
    }
}
