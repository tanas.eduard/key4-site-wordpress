jQuery( document ).ready( function() {
    var blockTypeSelectorList = {
        'title'      : '.pt-title',
        'price'      : '.pt-price',
        'list'       : '.pt-list li',
        'list-block' : '.pt-list-item [class*="pt-lines-"], .pt-list-item:not(:has(.pt-line-middle))',
        'button'     : '.pt-btn'
    };
    var smartResizeList = {};

    jQuery.each( jQuery( '.pt-smart-resize' ), function( instanceIndex, instance ) {
        var instanceEl = jQuery( instance );
        smartResizeList[ instanceIndex ] = { el: instanceEl, blockList: {} };
        jQuery.each( instanceEl.find( '.pt-col, .pt-cols-side' ), function( columnIndex, column ) {
            jQuery.each( blockTypeSelectorList, function( blockID, blockSelector ) {
                if( !( blockID in smartResizeList[ instanceIndex ].blockList ) ) {
                    smartResizeList[ instanceIndex ].blockList[ blockID ] = {};
                }
                jQuery.each( jQuery( column ).find( blockSelector ), function( blockIndex, block ) {
                    if( !( blockIndex in smartResizeList[ instanceIndex ].blockList[ blockID ] ) ) {
                        smartResizeList[ instanceIndex ].blockList[ blockID ][ blockIndex ] = jQuery( block );
                    } else {
                        smartResizeList[ instanceIndex ].blockList[ blockID ][ blockIndex ] = smartResizeList[ instanceIndex ].blockList[ blockID ][ blockIndex ].add( jQuery( block ) );
                    }
                });
            });
        });
    });

    function runSmartResize( instanceEl ) {
        jQuery.each( smartResizeList, function( instanceIndex, instance ) {
            if( instance.el.is( ':hidden' ) || ( typeof instanceEl !== 'undefined' && instanceEl[ 0 ] !== instance.el[ 0 ] ) ) {
                return true;
            }
            jQuery.each( instance.blockList, function( blockTypeID, blockList ) {
                jQuery.each( blockList, function( blockIndex, blockEls ) {
                    var heightList = [];
                    blockEls.css( 'height', '' );
                    jQuery.each( blockEls, function( blockElIndex, blockEl ) {
                        heightList.push( jQuery( blockEl ).outerHeight() );
                    });
                    blockEls.css( 'height', Math.max.apply( Math,heightList ) );
                });
            });
        });
    }

    jQuery( window ).load( function() {
        runSmartResize();
    });
    jQuery( window ).resize( function() {
        runSmartResize();
    });
    jQuery( '.pt-instance-switch' ).change( function() {
        var instanceEl = jQuery( this ).next( '.pt-instance' ).children( '.pt-smart-resize' );
        if( instanceEl.length ) {
            runSmartResize( instanceEl );
        }
    });

    runSmartResize();
});
