<?php if(!defined('ABSPATH')) die(); ?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
            <div class="pt-col <?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                    <div class="pt-top-block">
                        <div class="pt-back"></div>
                        <div class="pt-relative">
                            <?php if( !empty( $ptPlansFeaturesMeta['PlanImageURL'][$planID] ) ) { ?>
                                <div class="pt-image">
                                    <img src="<?php echo esc_url($ptPlansFeaturesMeta['PlanImageURL'][$planID]); ?>" alt="">
                                </div>
                            <?php } ?>
                            <div class="pt-content">
                                <?php if( $planName != '' ) { ?>
                                    <div class="pt-title"><?php echo $planName; ?></div>
                                <?php } ?>
                                <ul class="pt-list">
                                    <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                                        <li>
                                            <?php include(dirname(__FILE__).'/parts/tooltipValue.php'); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <?php if( $ptPlansFeaturesMeta['PlanDetailsButtonName'][$planID] != '' ) { ?>
                                    <a <?php if( $ptPlansFeaturesMeta['PlanDetailsButtonLink'][$planID] != '' ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanDetailsButtonLink'][$planID]); ?>"<?php } ?> class="pt-more-link"><?php echo $ptPlansFeaturesMeta['PlanDetailsButtonName'][$planID]; ?></a>
                                <?php } ?>
                            </div>
                            <?php if( $ptPlansFeaturesMeta['PlanIframeURL'][$planID] != '' ) { ?>
                                <div class="pt-map">
                                    <iframe src="<?php echo esc_url($ptPlansFeaturesMeta['PlanIframeURL'][$planID]); ?>"></iframe>
                                </div>
                            <?php } ?>
                            <?php if( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' ) { ?>
                                <?php if( $ptPlansFeaturesMeta['PlanShowPriceCaption'][$planID] && $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID] != '' ) { ?>
                                    <div class="pt-old-price"><?php echo $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID]; ?></div>
                                <?php } ?>
                                <div class="pt-price"><?php echo $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID]; ?><?php echo $ptPlansFeaturesMeta['PlanPrice'][$planID]; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                        <a <?php if( !$ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-link"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>