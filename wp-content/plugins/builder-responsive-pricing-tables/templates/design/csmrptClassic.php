<?php if(!defined('ABSPATH')) die(); ?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <?php
            $showPlanCaptionBlock = count( array_intersect_key( array_filter( $ptPlansFeaturesMeta['PlanShowCaption'], 'strlen' ), array_filter( $ptPlansFeaturesMeta['PlanCaptionText'], 'strlen' ) ) );
            $showPriceCaptionBlock = count( array_intersect_key( array_filter( $ptPlansFeaturesMeta['PlanShowPriceCaption'], 'strlen' ), array_filter( $ptPlansFeaturesMeta['PlanPriceCaptionText'], 'strlen' ) ) );
        ?>
        <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
            <div class="pt-col<?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                    <?php if( $planName != '' ) { ?>
                        <div class="pt-head">
                            <?php if( $ptPlansFeaturesMeta['PlanShowDiscountSign'][$planID] || $ptPlansFeaturesMeta['PlanShowMostPopularSign'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) { ?>
                                <div class="pt-badge<?php echo ( $ptPlansFeaturesMeta['PlanShowDiscountSign'][$planID] ) ? ' pt-discount' : ( $ptPlansFeaturesMeta['PlanShowMostPopularSign'][$planID] ? ' pt-popular' : '' ); ?>">
                                    <?php echo ( $ptPlansFeaturesMeta['PlanShowDiscountSign'][$planID] ) ? $ptPlansFeaturesMeta['PlanDiscountSignText'][$planID] : ( $ptPlansFeaturesMeta['PlanShowMostPopularSign'][$planID] ? $ptPlansFeaturesMeta['PlanMostPopularSignText'][$planID] : ( $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ? $ptPlansFeaturesMeta['PlanNotAvailableSignText'][$planID] : '' ) ); ?>
                                </div>
                            <?php } ?>
                            <div class="pt-title"><?php echo $planName; ?></div>
                            <?php if( $showPlanCaptionBlock ) { ?>
                                <div class="pt-sub-title"><?php echo ( $ptPlansFeaturesMeta['PlanShowCaption'][$planID] && $ptPlansFeaturesMeta['PlanCaptionText'][$planID] != '' ) ? $ptPlansFeaturesMeta['PlanCaptionText'][$planID] : '&nbsp;'; ?></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <ul class="pt-list">
                        <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                            <li>
                                <?php include(dirname(__FILE__).'/parts/tooltipValue.php'); ?>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php if( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' || $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                        <div class="pt-footer">
                            <?php if( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' ) { ?>
                                <div class="pt-price-block">
                                    <?php include(dirname(__FILE__).'/parts/complexPrice.php'); ?>
                                </div>
                                <?php if( $showPriceCaptionBlock ) { ?>
                                    <div class="pt-sub-text"><?php echo ( $ptPlansFeaturesMeta['PlanShowPriceCaption'][$planID] && $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID] != '' ) ? $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID] : '&nbsp;'; ?></div>
                                <?php } else { ?>
                                    <div></div>
                                <?php } ?>
                            <?php } ?>
                            <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                                <a <?php if( !( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>