<?php if(!defined('ABSPATH')) die(); ?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
            <div class="pt-col <?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                    <div class="pt-back"></div>
                    <?php if( $ptPlansFeaturesMeta['PlanShowRatingIcons'][$planID] && $ptPlansFeaturesMeta['PlanRatingIconsText'][$planID] != '' ) { ?>
                        <div class="pt-ribbon">
                            <?php echo $ptPlansFeaturesMeta['PlanRatingIconsText'][$planID]; ?>
                            <div class="pt-arrow"></div>
                        </div>
                    <?php } ?>
                    <?php if( $planName != '' ) { ?>
                        <div class="pt-title"><?php echo $planName; ?></div>
                    <?php } ?>
                    <?php if( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' ) { ?>
                        <div class="pt-price-container">
                            <div class="pt-price-wrapper">
                                <div class="pt-price-block">
                                    <span class="pt-currency"><?php echo $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID]; ?></span>
                                    <span class="pt-price-main"><?php echo $ptPlansFeaturesMeta['PlanPrice'][$planID]; ?></span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="pt-list-container">
                        <ul class="pt-list">
                            <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                                <li>
                                    <?php echo $featureName; ?> <?php include(dirname(__FILE__).'/parts/tooltipValue.php'); ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                        <div class="pt-btn-container">
                            <a <?php if( !$ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
