<?php if(!defined('ABSPATH')) die(); ?>
<?php
    if( count( array_intersect_key( array_filter( $ptPlansFeaturesMeta['PlanShowPriceCaption'] ), array_filter( $ptPlansFeaturesMeta['PlanPriceCaptionText'], 'strlen' ) ) ) == 0 && count( array_filter( $ptPlansFeaturesMeta['PlanName'], 'strlen' ) ) == 0 && count( array_filter( $ptPlansFeaturesMeta['PlanPrice'], 'strlen' ) ) == 0 && count( array_filter( $ptPlansFeaturesMeta['PlanCurrencySymbol'], 'strlen' ) ) == 0 ) {
        $baseCSSClassList[] = 'pt-no-header';
    }
    if( count( array_filter( $ptPlansFeaturesMeta['PlanPurchaseButtonName'], 'strlen' ) ) == 0 ) {
        $baseCSSClassList[] = 'pt-no-footer';
    }
?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <div class="pt-cols-side">
            <div class="pt-list-block">
                <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                    <div class="pt-list-item">
                        <span class="pt-text">
                            <?php
                                include(dirname(__FILE__).'/parts/rowHeight.php');
                                echo $rowHeightPrependHTML;
                                include(dirname(__FILE__).'/parts/tooltipFeature.php');
                                echo $rowHeightAppendHTML;
                            ?>
                        </span>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="pt-cols-main">
            <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
                <div class="pt-col<?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                    <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                        <div class="pt-content">
                            <?php
                                $showPriceCaptionBlock = ( $ptPlansFeaturesMeta['PlanShowPriceCaption'][$planID] && $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID] != '' );
                                $showPriceBlock = ( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' || $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID] != '' );
                            ?>
                            <?php if( $planName != '' || $showPriceBlock || $showPriceCaptionBlock ) { ?>
                                <div class="pt-head">
                                    <?php if( $planName != '' ) { ?>
                                        <div class="pt-title"><span><?php echo $planName; ?></span></div>
                                    <?php } ?>
                                    <?php if( $showPriceBlock || $showPriceCaptionBlock ) { ?>
                                        <div class="pt-price-container<?php echo $showPriceCaptionBlock ? ' pt-has-sub' : ''; ?>">
                                            <?php if( $showPriceBlock ) { ?>
                                                <div class="pt-price-block">
                                                    <span class="pt-currency"><?php echo $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID]; ?></span>
                                                    <span class="pt-price-main"><?php echo $ptPlansFeaturesMeta['PlanPrice'][$planID]; ?></span>
                                                </div>
                                            <?php } ?>
                                            <?php if( $showPriceCaptionBlock ) { ?>
                                                <span class="pt-sub"><?php echo $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID]; ?></span>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if( $ptPlansFeaturesMeta['PlanShowNewRibbon'][$planID] ) { ?>
                                        <div class="pt-badge pt-new">
                                            <span class="pt-text"><?php echo $ptPlansFeaturesMeta['PlanNewRibbonText'][$planID]; ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php if( $ptPlansFeaturesMeta['PlanShowDiscountRibbon'][$planID] ) { ?>
                                        <div class="pt-badge pt-discount">
                                            <span class="pt-text"><?php echo $ptPlansFeaturesMeta['PlanDiscountRibbonText'][$planID]; ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="pt-list">
                                <div class="pt-back"></div>
                                <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                                    <div class="pt-list-item">
                                        <div class="pt-text">
                                            <?php include(dirname(__FILE__).'/parts/tooltipFeature.php'); ?>
                                        </div>
                                        <div class="pt-value">
                                            <?php
                                                include(dirname(__FILE__).'/parts/rowHeight.php');
                                                echo $rowHeightPrependHTML;
                                                include(dirname(__FILE__).'/parts/tooltipValue.php');
                                                echo $rowHeightAppendHTML;
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="pt-footer">
                                <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                                    <a <?php if( !$ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>