<?php if(!defined('ABSPATH')) die(); ?>
<?php
    if( count( array_filter( $ptPlansFeaturesMeta['PlanPurchaseButtonName'], 'strlen' ) ) == 0 ) {
        $baseCSSClassList[] = 'pt-no-footer';
    }
?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <div class="pt-cols-side">
            <div class="pt-list-block">
                <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                    <div class="pt-list-item">
                        <?php
                            include(dirname(__FILE__).'/parts/rowHeight.php');
                            echo $rowHeightPrependHTML;
                            include(dirname(__FILE__).'/parts/tooltipFeature.php');
                            echo $rowHeightAppendHTML;
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="pt-cols-main">
            <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
                <div class="pt-col<?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                    <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                        <div class="pt-content">
                            <?php
                                $showPriceCaptionBlock = ( $ptPlansFeaturesMeta['PlanShowPriceCaption'][$planID] && $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID] != '' );
                                $showPriceBlock = ( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' || $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID] != '' );
                            ?>
                            <?php if( $planName != '' || $showPriceBlock || $showPriceCaptionBlock || $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                                <div class="pt-top">
                                    <?php if( $planName != '' ) { ?>
                                        <div class="pt-title"><?php echo $planName; ?></div>
                                    <?php } ?>
                                    <?php if( $ptPlansFeaturesMeta['PlanShowDiscountSign'][$planID] || $ptPlansFeaturesMeta['PlanShowMostPopularSign'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) { ?>
                                        <div class="pt-badge<?php echo ( $ptPlansFeaturesMeta['PlanShowDiscountSign'][$planID] ) ? ' pt-discount' : ( $ptPlansFeaturesMeta['PlanShowMostPopularSign'][$planID] ? ' pt-popular' : '' ); ?>">
                                            <span><?php echo ( $ptPlansFeaturesMeta['PlanShowDiscountSign'][$planID] ) ? $ptPlansFeaturesMeta['PlanDiscountSignText'][$planID] : ( $ptPlansFeaturesMeta['PlanShowMostPopularSign'][$planID] ? $ptPlansFeaturesMeta['PlanMostPopularSignText'][$planID] : ( $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ? $ptPlansFeaturesMeta['PlanNotAvailableSignText'][$planID] : '' ) ); ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php if( $showPriceBlock || $showPriceCaptionBlock ) { ?>
                                        <div class="pt-price-container<?php echo $showPriceCaptionBlock ? ' pt-has-sub' : ''; ?>">
                                            <?php if( $showPriceCaptionBlock ) { ?>
                                                <span class="pt-sub"><?php echo $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID]; ?></span>
                                            <?php } ?>
                                            <?php if( $showPriceBlock ) { ?>
                                                <div class="pt-price-block">
                                                    <span class="pt-currency"><?php echo $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID]; ?></span>
                                                    <span class="pt-price-main"><?php echo $ptPlansFeaturesMeta['PlanPrice'][$planID]; ?></span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                                        <a <?php if( !( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="pt-list">
                                <div class="pt-back"></div>
                                <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                                    <div class="pt-list-item">
                                        <div class="pt-text">
                                            <?php include(dirname(__FILE__).'/parts/tooltipFeature.php'); ?>
                                        </div>
                                        <div class="pt-value">
                                            <?php
                                                include(dirname(__FILE__).'/parts/rowHeight.php');
                                                echo $rowHeightPrependHTML;
                                                echo $ptPlansFeaturesMeta['FeatureValue'][$planID][$featureID];
                                                echo $rowHeightAppendHTML;
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="pt-footer">
                                <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                                    <a <?php if( !( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || $ptPlansFeaturesMeta['PlanShowNotAvailableSign'][$planID] ) ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>