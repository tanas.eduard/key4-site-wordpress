<?php if(!defined('ABSPATH')) die(); ?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <?php
            $showPlanCaptionBlock = count( array_intersect_key( array_filter( $ptPlansFeaturesMeta['PlanShowCaption'], 'strlen' ), array_filter( $ptPlansFeaturesMeta['PlanCaptionText'], 'strlen' ) ) );
        ?>
        <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
            <div class="pt-col <?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                    <div class="pt-back"></div>
                    <div class="pt-head">
                        <?php if( !empty( $ptPlansFeaturesMeta['PlanImageURL'][$planID] ) ) { ?>
                            <div class="pt-image">
                                <img src="<?php echo esc_url($ptPlansFeaturesMeta['PlanImageURL'][$planID]); ?>" alt="">
                            </div>
                        <?php } ?>
                        <?php if( $planName != '' ) { ?>
                            <div class="pt-title"><?php echo $planName; ?></div>
                        <?php } ?>
                    </div>
                    <div class="pt-content">
                        <div class="pt-top">
                            <?php if( $showPlanCaptionBlock ) { ?>
                                <div class="pt-sub-text-fixed">
                                    <div class="pt-sub-text"><?php echo ( $ptPlansFeaturesMeta['PlanShowCaption'][$planID] && $ptPlansFeaturesMeta['PlanCaptionText'][$planID] != '' ) ? $ptPlansFeaturesMeta['PlanCaptionText'][$planID] : '&nbsp;'; ?></div>
                                </div>
                            <?php } ?>
                            <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                                <a <?php if( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || $ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID] == '' ) { ?>class="pt-btn pt-btn-alt pt-btn-disabled"<?php } else { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" class="pt-btn pt-btn-alt"<?php } ?>><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                            <?php } ?>
                        </div>
                        <ul class="pt-list">
                            <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                                <li>
                                    <span class="pt-list-text"><?php include(dirname(__FILE__).'/parts/tooltipValue.php'); ?></span>
                                    <span class="pt-list-sub-text"><?php echo $ptPlansFeaturesMeta['FeatureValueCurrencySymbol'][$planID][$featureID]; ?><?php echo $ptPlansFeaturesMeta['FeatureValuePrice'][$planID][$featureID]; ?></span>
                                    <?php if( !empty( $ptPlansFeaturesMeta['FeatureValuePurchaseButtonName'][$planID][$featureID] ) ) { ?>
                                        <a <?php if( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] || empty( $ptPlansFeaturesMeta['FeatureValuePurchaseButtonLink'][$planID][$featureID] ) ) { ?>class="pt-btn pt-btn-disabled"<?php } else { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['FeatureValuePurchaseButtonLink'][$planID][$featureID]); ?>" class="pt-btn"<?php } ?>><?php echo $ptPlansFeaturesMeta['FeatureValuePurchaseButtonName'][$planID][$featureID]; ?></a>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>