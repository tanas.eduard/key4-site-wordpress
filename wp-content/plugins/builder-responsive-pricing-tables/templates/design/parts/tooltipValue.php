<?php if(!defined('ABSPATH')) die(); ?>
<?php /* the spaces must be removed */ ?>
<?php $tooltipPositionCSSClasses = array( 't' => 'pt-tooltip-top', 'b' => 'pt-tooltip-bottom', 'tl' => 'pt-tooltip-top-left', 'tr' => 'pt-tooltip-top-right', 'bl' => 'pt-tooltip-bottom-left', 'br' => 'pt-tooltip-bottom-right', 'lt' => 'pt-tooltip-left-top', 'l' => 'pt-tooltip-left', 'lb' => 'pt-tooltip-left-bottom', 'rt' => 'pt-tooltip-right-top', 'r' => 'pt-tooltip-right', 'rb' => 'pt-tooltip-right-bottom' ); ?>
<?php if( $ptPlansFeaturesMeta['FeatureValueTooltipType'][$planID][$featureID] != 0 ) { ?>
<?php if( $ptPlansFeaturesMeta['FeatureValueTooltipType'][$planID][$featureID] == 1 ) { ?>
<?php echo $ptPlansFeaturesMeta['FeatureValue'][$planID][$featureID]; ?>
<span class="pt-tooltip-show-icon">
<span class="pt-tooltip-show"><i class="fa fa-info-circle"></i></span>
<span class="pt-tooltip <?php echo $tooltipPositionCSSClasses[$ptPlansFeaturesMeta['FeatureValueTooltipPosition'][$planID][$featureID]]; ?>">
<span class="pt-tooltip-content">
<?php } elseif( $ptPlansFeaturesMeta['FeatureValueTooltipType'][$planID][$featureID] == 2 ) { ?>
<span class="pt-tooltip-show-text">
<?php if( $ptDesignColorMeta[ 'design' ] == 'csmrpt-classic' ) { ?>
<?php
    $textPart = preg_replace( '/^<i class="[^"]*fa[^"]*">[^\<]*<\/i>\s*/', '', $ptPlansFeaturesMeta['FeatureValue'][$planID][$featureID] );
    $iconPart = mb_substr( $ptPlansFeaturesMeta['FeatureValue'][$planID][$featureID], 0 , mb_strlen( $textPart ) * -1 );
?>
<?php echo $iconPart; ?>
<span class="pt-tooltip-show"><?php echo $textPart; ?></span>
<?php } else { ?>
<span class="pt-tooltip-show">
<?php echo $ptPlansFeaturesMeta['FeatureValue'][$planID][$featureID]; ?>
</span>
<?php } ?>
<span class="pt-tooltip <?php echo $tooltipPositionCSSClasses[$ptPlansFeaturesMeta['FeatureValueTooltipPosition'][$planID][$featureID]]; ?>">
<span class="pt-tooltip-content">
<?php } ?>
<?php if( $ptPlansFeaturesMeta['FeatureValueTooltipHeader'][$planID][$featureID] != '' ) { ?>
<span class="pt-tooltip-heading"><?php echo $ptPlansFeaturesMeta['FeatureValueTooltipHeader'][$planID][$featureID]; ?></span>
<?php } ?>
<span class="pt-tooltip-list<?php echo $ptPlansFeaturesMeta['FeatureValueTooltipColumns'][$planID][$featureID] ? ' pt-tooltip-2col' : ''; ?>">
<?php foreach( explode( "\n", $ptPlansFeaturesMeta['FeatureValueTooltipContent'][$planID][$featureID] ) as $featureTooltipListItem ) { ?>
<span><?php echo $featureTooltipListItem; ?></span>
<?php } ?>
</span>
</span>
</span>
</span>
<?php } else { ?>
<?php echo $ptPlansFeaturesMeta['FeatureValue'][$planID][$featureID]; ?>
<?php } ?>
