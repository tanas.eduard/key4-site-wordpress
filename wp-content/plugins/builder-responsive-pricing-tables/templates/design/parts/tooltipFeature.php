<?php if(!defined('ABSPATH')) die(); ?>
<?php /* the spaces must be removed */ ?>
<?php $tooltipPositionCSSClasses = array( 't' => 'pt-tooltip-top', 'b' => 'pt-tooltip-bottom', 'tl' => 'pt-tooltip-top-left', 'tr' => 'pt-tooltip-top-right', 'bl' => 'pt-tooltip-bottom-left', 'br' => 'pt-tooltip-bottom-right', 'lt' => 'pt-tooltip-left-top', 'l' => 'pt-tooltip-left', 'lb' => 'pt-tooltip-left-bottom', 'rt' => 'pt-tooltip-right-top', 'r' => 'pt-tooltip-right', 'rb' => 'pt-tooltip-right-bottom' ); ?>
<?php if( $ptPlansFeaturesMeta['FeatureTooltipType'][$featureID] != 0 ) { ?>
<?php if( $ptPlansFeaturesMeta['FeatureTooltipType'][$featureID] == 1 ) { ?>
<?php echo $featureName; ?>
<span class="pt-tooltip-show-icon">
<span class="pt-tooltip-show"><i class="fa fa-info-circle"></i></span>
<span class="pt-tooltip <?php echo $tooltipPositionCSSClasses[$ptPlansFeaturesMeta['FeatureTooltipPosition'][$featureID]]; ?>">
<span class="pt-tooltip-content">
<?php } elseif( $ptPlansFeaturesMeta['FeatureTooltipType'][$featureID] == 2 ) { ?>
<span class="pt-tooltip-show-text">
<span class="pt-tooltip-show"><?php echo $featureName; ?></span>
<span class="pt-tooltip <?php echo $tooltipPositionCSSClasses[$ptPlansFeaturesMeta['FeatureTooltipPosition'][$featureID]]; ?>">
<span class="pt-tooltip-content">
<?php } ?>
<?php if( $ptPlansFeaturesMeta['FeatureTooltipHeader'][$featureID] != '' ) { ?>
<span class="pt-tooltip-heading"><?php echo $ptPlansFeaturesMeta['FeatureTooltipHeader'][$featureID]; ?></span>
<?php } ?>
<span class="pt-tooltip-list<?php echo $ptPlansFeaturesMeta['FeatureTooltipColumns'][$featureID] ? ' pt-tooltip-2col' : ''; ?>">
<?php foreach( explode( "\n", $ptPlansFeaturesMeta['FeatureTooltipContent'][$featureID] ) as $featureTooltipListItem ) { ?>
<span><?php echo $featureTooltipListItem; ?></span>
<?php } ?>
</span>
</span>
</span>
</span>
<?php } else { ?>
<?php echo $featureName; ?>
<?php } ?>
