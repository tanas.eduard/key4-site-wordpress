<?php if(!defined('ABSPATH')) die(); ?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
            <div class="pt-col <?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                    <div class="pt-back">
                        <div class="pt-folded-corner"></div>
                    </div>
                    <?php if( $planName != '' ) { ?>
                        <div class="pt-title"><?php echo $planName; ?></div>
                    <?php } ?>
                    <?php if( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' ) { ?>
                        <div class="pt-price-container">
                            <div class="pt-price-wrapper">
                                <div class="pt-price-block">
                                    <?php if( $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID] != '' ) { ?>
                                        <span class="pt-currency"><?php echo $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID]; ?></span>
                                    <?php } ?>
                                    <span class="pt-price-main"><?php echo $ptPlansFeaturesMeta['PlanPrice'][$planID]; ?></span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <ul class="pt-list">
                        <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                            <li>
                                <?php echo $featureName; ?> <?php include(dirname(__FILE__).'/parts/tooltipValue.php'); ?>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                        <a <?php if( !$ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?></a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>