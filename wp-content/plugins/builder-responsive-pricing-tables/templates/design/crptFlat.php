<?php if(!defined('ABSPATH')) die(); ?>
<div class="<?php echo implode( ' ', $baseCSSClassList ); ?>">
    <div class="pt-cols pt-cols-<?php echo count($ptPlansFeaturesMeta['PlanName']); ?>">
        <div class="pt-cols-side">
            <div class="pt-list-block">
                <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                    <div class="pt-list-item">
                        <?php
                            include(dirname(__FILE__).'/parts/rowHeight.php');
                            echo $rowHeightPrependHTML;
                            include(dirname(__FILE__).'/parts/tooltipFeature.php');
                            echo $rowHeightAppendHTML;
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="pt-cols-main">
            <?php foreach( $ptPlansFeaturesMeta['PlanName'] as $planID => $planName ) { ?>
                <div class="pt-col<?php echo ( $ptPlansFeaturesMeta['PlanDesignColor'][$planID] != '' ) ? ' ' . $ptDesign . '-c' .  $ptPlansFeaturesMeta['PlanDesignColor'][$planID] : ''; ?>">
                    <div class="pt-block<?php echo ( $ptPlansFeaturesMeta['PlanHighlight'][$planID] ) ? ' pt-selected' : ( ( $ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) ? ' pt-not-available' : '' ); ?>">
                        <div class="pt-back"></div>
                        <?php if( $planName != '' ) { ?>
                            <div class="pt-title"><?php echo $planName; ?></div>
                        <?php } ?>
                        <?php
                            $showPriceBlock = ( $ptPlansFeaturesMeta['PlanPrice'][$planID] != '' );
                            $showCurrencySymbolBlock = ( $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID] != '' );
                            $showPriceCaptionBlock = ( $ptPlansFeaturesMeta['PlanShowPriceCaption'][$planID] && $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID] != '' );
                        ?>
                        <?php if( $showPriceBlock || $showCurrencySymbolBlock || $showPriceCaptionBlock ) { ?>
                            <div class="pt-price-block">
                                <?php if( $showPriceBlock ) { ?>
                                    <span class="pt-price-main"><?php echo $ptPlansFeaturesMeta['PlanPrice'][$planID]; ?></span>
                                <?php } ?>
                                <?php if( $showCurrencySymbolBlock || $showPriceCaptionBlock ) { ?>
                                    <span class="pt-price-rest">
                                        <?php if( $showCurrencySymbolBlock ) { ?>
                                            <span class="pt-currency"><?php echo $ptPlansFeaturesMeta['PlanCurrencySymbol'][$planID]; ?></span>
                                        <?php } ?>
                                        <?php if( $showCurrencySymbolBlock && $showPriceCaptionBlock ) { ?>
                                            <br>
                                        <?php } ?>
                                        <?php if( $showPriceCaptionBlock ) { ?>
                                            <span class="pt-sub"><?php echo $ptPlansFeaturesMeta['PlanPriceCaptionText'][$planID]; ?></span>
                                        <?php } ?>
                                    </span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if( $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID] != '' ) { ?>
                            <a <?php if( !$ptPlansFeaturesMeta['PlanMakeInactive'][$planID] ) { ?>href="<?php echo esc_url($ptPlansFeaturesMeta['PlanPurchaseButtonLink'][$planID]); ?>" <?php } ?>class="pt-btn"><?php echo $ptPlansFeaturesMeta['PlanPurchaseButtonName'][$planID]; ?>
                                <span class="pt-icon">
                                    <span><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                </span>
                            </a>
                        <?php } ?>
                        <div class="pt-list">
                            <?php foreach( $ptPlansFeaturesMeta['FeatureName'] as $featureID => $featureName ) { ?>
                                <div class="pt-list-item">
                                    <div class="pt-text">
                                        <?php include(dirname(__FILE__).'/parts/tooltipFeature.php'); ?>
                                    </div>
                                    <div class="pt-value">
                                        <?php
                                            include(dirname(__FILE__).'/parts/rowHeight.php');
                                            echo $rowHeightPrependHTML;
                                            include(dirname(__FILE__).'/parts/tooltipValue.php');
                                            echo $rowHeightAppendHTML;
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>