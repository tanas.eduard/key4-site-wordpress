<?php if(!defined('ABSPATH')) die(); ?>
<div class="wrap">
    <h2><?php _e('Pricing Tables Options', 'wbrpt'); ?></h2>
    <form method="post" action="options.php">
        <?php settings_fields('wbrpt_plugin_options'); ?>
        <?php $options = get_option('wbrpt_plugin_options'); ?>
        <table class="form-table">
            <tr valign="top"><th scope="row"><?php _e('Experimental functionality', 'wbrpt'); ?></th>
                <td>
                    <label for="enable-smart-resize"><input type="checkbox" <?php echo (isset($options['EnableSmartResize']) && $options['EnableSmartResize']) ? 'checked' : ''; ?> value="1" id="enable-smart-resize" name="wbrpt_plugin_options[EnableSmartResize]"> <?php _e('Smart resize', 'wbrpt'); ?></label>
                    <p class="description"><?php _e("(It's still in development and it can cause an unexpected result in certain cases.)", 'wbrpt'); ?></p>
                </td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" class="button button-primary" value="<?php _e('Save Changes', 'wbrpt') ?>" />
        </p>
    </form>
</div>
