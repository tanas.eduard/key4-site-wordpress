<?php if(!defined('ABSPATH')) die(); ?>
<div class="wbrpt-design-attribute wbrpt-attr-bl">
    <?php if( $post->post_status == 'draft' || $post->post_status == 'auto-draft' ) { ?>
        <label for="wbrpt-design-selector" class="wbrpt-attr-lbl"><?php _e('Design', 'wbrpt'); ?></label><br>
        <select id="wbrpt-design-selector" name="wbrptDesign" autocomplete="off">
            <?php foreach( $wbrptGeneralSettings[ 'designGroupList' ] as $designGroup ) { ?>
                <optgroup label="<?php echo $designGroup['name']; ?>">
                    <?php foreach( $designGroup[ 'designList' ] as $designID ) { ?>
                        <option <?php if( $value[ 'design' ] == $designID ) echo 'selected '; ?>value="<?php echo $designID; ?>"><?php echo $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'name' ]; ?></option>
                    <?php } ?>
                </optgroup>
            <?php } ?>
        </select>
        <a id="wbrpt-load-demo-data" class="button"><?php _e('Load Demo Data', 'wbrpt'); ?></a>
        <div class="wbrpt-design-preview"><?php foreach( $wbrptGeneralSettings[ 'designGroupList' ] as $designGroup ) { ?><?php foreach( $designGroup[ 'designList' ] as $designID ) { ?><input type="radio" id="<?php echo "wbrpt-design-preview-$designID"; ?>" name="wbrptDesignPreview" value="<?php echo $designID; ?>"<?php if( $value[ 'design' ] == $designID ) echo ' checked'; ?> autocomplete="off"><label class="item" for="<?php echo "wbrpt-design-preview-$designID"; ?>" title="<?php echo esc_attr( $designGroup['name'] . ': ' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'name' ] ); ?>"><img src="<?php echo( plugins_url( "/assets/admin/images/design-preview/$designID.png", $pluginFile )); ?>"></label><?php } ?><?php } ?></div>
    <?php } else { ?>
        <label><?php printf(__('Design can only be changed in %sdraft mode%s.', 'wbrpt'), '<a id="wbrpt-post-status" href="#">', '</a>'); ?></label>
        <input id="wbrpt-design-selector" type="hidden" name="wbrptDesign" value="<?php echo $value[ 'design' ]; ?>">
    <?php } ?>
    <a id="wbrpt-preview-changes" class="button hidden"><?php _e('Preview Changes', 'wbrpt'); ?></a>
</div>
<div class="wbrpt-animation-attribute wbrpt-attr-bl">
    <label for="wbrpt-animation-selector" class="wbrpt-attr-lbl"><?php _e('Animation type', 'wbrpt'); ?></label><br>
    <select id="wbrpt-animation-selector" name="wbrptAnimation">
        <option <?php if( $value[ 'animation' ] == 0 ) echo 'selected '; ?>value="0"><?php _ex('Default', 'Animation type', 'wbrpt'); ?></option>
        <option <?php if( $value[ 'animation' ] == 1 ) echo 'selected '; ?>value="1"><?php _ex('Style 1', 'Animation type', 'wbrpt'); ?></option>
        <option <?php if( $value[ 'animation' ] == 2 ) echo 'selected '; ?>value="2"><?php _ex('Style 2', 'Animation type', 'wbrpt'); ?></option>
        <option <?php if( $value[ 'animation' ] == -1 ) echo 'selected '; ?>value="-1"><?php _ex('None', 'Animation type', 'wbrpt'); ?></option>
    </select>
</div>
<?php if( isset( $pluginOptions[ 'EnableSmartResize' ] ) && $pluginOptions[ 'EnableSmartResize' ] ) { ?>
    <div class="wbrpt-smart-resize-attribute wbrpt-attr-bl">
        <label class="wbrpt-attr-lbl"><?php _e('Smart resize', 'wbrpt'); ?></label><br>
        <label title="<?php _e('It requires JavaScript', 'wbrpt'); ?>"><input value="1" name="wbrptSmartResize[LineUpContent]" <?php if( $value[ 'smartResize' ][ 'LineUpContent' ] ) echo 'checked '; ?>type="checkbox"><?php _e('Line up content', 'wbrpt'); ?></label>
    </div>
<?php } ?>
<div class="wbrpt-color-attribute wbrpt-attr-bl">
    <label class="wbrpt-attr-lbl"><?php _e('Color theme', 'wbrpt'); ?></label><br>
    <div class="wbrpt-color-picker" id="wbrpt-color-selector">
        <?php include( 'parts/colorList.php' ); ?>
    </div>
</div>
<div class="wbrpt-custom-color-attribute wbrpt-attr-bl hidden">
    <div id="wbrpt-custom-color-variable-list" class="wbrpt-color-variable-list">
        <?php include( 'parts/colorVariableList.php' ); ?>
    </div>
    <div>
        <a id="wbrpt-custom-color-generate" class="button" title="<?php _e('Generate New Color', 'wbrpt'); ?>"><?php _ex('Generate New', 'Generate New Color', 'wbrpt'); ?></a>
        <a id="wbrpt-custom-color-remove" class="button" title="<?php _e('Remove Selected Color', 'wbrpt'); ?>"><?php _ex('Remove Selected', 'Remove Selected Color', 'wbrpt'); ?></a>
        <a id="wbrpt-custom-color-preview" class="button" title="<?php _e('Preview Color', 'wbrpt'); ?>"><?php _ex('Preview', 'Preview Color', 'wbrpt'); ?></a>
        <a id="wbrpt-custom-color-load-all" class="button" title="<?php _e('Load All Default Colors', 'wbrpt'); ?>"><?php _ex('Load All Colors', 'Load All Default Colors', 'wbrpt'); ?></a>
        <a id="wbrpt-custom-color-cancel" class="button" title="<?php _e('Hide Color Settings', 'wbrpt'); ?>"><?php _ex('Hide Settings', 'Hide Color Settings', 'wbrpt'); ?></a>
    </div>
    <input id="wbrpt-custom-color-variable-data" type="hidden" name="wbrptColorVariableList" value="">
    <hr>
</div>
<div class="wbrpt-custom-styles-attribute wbrpt-attr-bl">
    <div id="wbrpt-custom-styles-block" class="hidden">
        <label for="wbrpt-custom-styles-content" class="wbrpt-attr-lbl"><?php _e('Custom Styles', 'wbrpt'); ?></label>
        <textarea class="full-width" id="wbrpt-custom-styles-content" rows="10" name="wbrptStyles" placeholder=".pt-id-<?php echo $post->ID; ?> { /* <?php _e('your styles', 'wbrpt'); ?> */ }"><?php echo esc_textarea( $value[ 'styles' ] ); ?></textarea>
    </div>
    <a id="wbrpt-custom-styles-show" class="button"><?php _e('Custom Styles', 'wbrpt'); ?></a>
    <a id="wbrpt-custom-styles-hide" class="button hidden"><?php _e('Hide Custom Styles', 'wbrpt'); ?></a>
</div>
