<?php
/*
Plugin Name: wpPricing Builder - WordPress Builder for Responsive Pricing Tables
Plugin URI: https://codecanyon.net/item/wppricing-builder-wordpress-responsive-pricing-tables/13471310
Description: Build your Pricing Tables in few minutes
Author: Swebdeveloper
Version: 1.6.0
Author URI: http://codecanyon.net/user/swebdeveloper
*/

if(!defined('ABSPATH'))
    die('Direct access of plugin file not allowed');

include( plugin_dir_path( __FILE__ ) . 'includes/wbrptColorFunctions.php' );
include( plugin_dir_path( __FILE__ ) . 'includes/wbrptSettings.php' );
include( plugin_dir_path( __FILE__ ) . 'includes/versionCompatibility.php' );
wbrptGeneralSettings::init();

add_action( 'init', 'wbrpt_pricing_table_init' );

function wbrpt_pricing_table_init()
{
    $labels = array(
        'name'               => _x( 'Pricing Tables', 'post type general name', 'wbrpt' ),
        'singular_name'      => _x( 'Pricing Table', 'post type singular name', 'wbrpt' ),
        'add_new'            => _x( 'Add New', 'pricing-table', 'wbrpt' ),
        'add_new_item'       => __( 'Add New Pricing Table', 'wbrpt' ),
        'edit_item'          => __( 'Edit Pricing Table', 'wbrpt' ),
        'new_item'           => __( 'New Pricing Table', 'wbrpt' ),
        'all_items'          => __( 'All Pricing Tables', 'wbrpt' ),
        'view_item'          => __( 'View Pricing Table', 'wbrpt' ),
        'search_items'       => __( 'Search Pricing Tables', 'wbrpt' ),
        'not_found'          => __( 'No Pricing Table found', 'wbrpt' ),
        'not_found_in_trash' => __( 'No Pricing Tables found in Trash', 'wbrpt' ),
        'parent_item_colon'  => '',
        'menu_name'          => __( 'Pricing Tables', 'wbrpt' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => plugin_dir_url( __FILE__ ) . 'assets/admin/images/menu-icon.gif'
    );

    register_post_type( 'wbr-pricing-table' , $args );
}

function wbrpt_add_meta_boxes()
{
    add_meta_box(
        'wbr-pricing-table-design-color-meta-box',
        __( 'Design and color theme', 'wbrpt' ),
        'wbrpt_meta_box_desing_color_callback',
        'wbr-pricing-table',
        'advanced',
        'low'
    );

    add_meta_box(
        'wbr-pricing-table-plans-features-meta-box',
        __( 'Plans and features', 'wbrpt' ),
        'wbrpt_meta_box_plans_features_callback',
        'wbr-pricing-table',
        'advanced',
        'low'
    );
}

add_action( 'add_meta_boxes', 'wbrpt_add_meta_boxes' );

function wbrpt_meta_box_desing_color_callback( $post )
{
    $pluginFile = __FILE__;
    $pluginOptions = get_option( 'wbrpt_plugin_options' );
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $value = wbrpt_get_design_color_meta( $post->ID );
    wbrpt_get_available_design_color_list( $value[ 'design' ] );
    wbrpt_get_design_color_variable_list( $value[ 'design' ] );
    if( empty( $value[ 'color' ] ) || !isset( $wbrptGeneralSettings[ 'designList' ][ $value[ 'design' ] ][ 'colorList' ][ $value[ 'color' ] ] ) ) {
        $value[ 'color' ] = wbrpt_get_default_color( $value[ 'design' ] );
    }
    if( !isset( $value[ 'animation' ] ) ) {
        $value[ 'animation' ] = 0;
    }
    if( !isset( $value[ 'smartResize' ][ 'LineUpContent' ] ) ) {
        $value[ 'smartResize' ][ 'LineUpContent' ] = false;
    }
    if( !isset( $value[ 'styles' ] ) ) {
        $value[ 'styles' ] = '';
    }
    include( 'templates/admin/metaBoxDesingColor.php' );
}

function wbrpt_meta_box_plans_features_callback( $post )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $designID = wbrpt_get_design_color_meta( $post->ID, 'design' );
    wbrpt_get_available_design_color_list( $designID );
    $value = get_post_meta( $post->ID, 'wbrpt_plans_features', true );
    if( empty( $value ) ) {
        $value = array( 1 => array(
            'PlanName'    => array( 1 => '', 2 => '' ),
            'FeatureName' => array( 1 => '', 2 => '' )
        ));
    }
    wbrpt_resolve_version_compatibility( $value );
    wbrpt_plans_features_default_value( $value, $designID );
    include( 'templates/admin/metaBoxPlansFeatures.php' );
}

function wbrpt_get_design_color_meta( $postID, $attributeID = null )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $value = get_post_meta( $postID, 'wbrpt_design_color', true );
    if( !is_array( $value ) ) {
        $value = array();
    }
    if( empty( $value[ 'design' ] ) || !isset( $wbrptGeneralSettings[ 'designList' ][ $value[ 'design' ] ] ) ) {
        reset( $wbrptGeneralSettings[ 'designList' ] );
        $value[ 'design' ] = key( $wbrptGeneralSettings[ 'designList' ] );
    }
    if( !empty( $attributeID ) && isset( $value[ $attributeID ] ) ) {
        return $value[ $attributeID ];
    }
    return $value;
}

function wbrpt_get_plugin_data( $headerName )
{
    static $pluginData = null;
    if( $pluginData === null ) {
        $pluginData = get_plugin_data( __FILE__ );
    }
    return $pluginData[ $headerName ];
}

function wbrpt_get_default_color( $designID )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    if( isset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ][ $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ] ] ) ) {
        return $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ];
    }
    reset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ] );
    return key( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ] );
}

function wbrpt_get_available_design_color_list( $designID, $return = false, $raw = false )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    if( !$return ) {
        $raw = false;
    }
    if( !$raw ) {
        $default = array( '$theme-color' => '#' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ] );
        $getDefault = true;
        include( 'includes/color/style/' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorFile' ] );
        $colorVariableNameList = array_keys( $default );
    }
    $colorList = array();
    foreach( glob( plugin_dir_path( __FILE__ ) . 'assets/css/pt-color/' . $designID . '/*.css' ) as $fileName ) {
        $f = fopen( $fileName, 'r' );
        $line = fgets( $f );
        fclose( $f );
        if( !preg_match( '/^\/\*[\-a-f0-9]+:[\-a-f0-9]+\*\/$/', $line ) ) {
            continue;
        }
        if( $raw ) {
            $colorList[ basename( $fileName, '.css' ) ] = trim( $line );
        } else {
            $colorHash = explode( ':', trim( $line, "\n/*" ) );
            $colorList[ basename( $fileName, '.css' ) ][ 'icon' ] = explode( '-', $colorHash[ 0 ] );
            $colorList[ basename( $fileName, '.css' ) ][ 'vars' ] = array_combine( $colorVariableNameList, explode( '-', $colorHash[ 1 ] ) );
        }
    }
    if( $return ) {
        return $colorList;
    }
    if( empty( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ] ) ) {
        $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ] = $colorList;
    }
}

function wbrpt_get_design_color_variable_list( $designID )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $default = array( '$theme-color' => '#' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ] );
    $getDefault = true;
    ob_start();
    include( 'includes/color/style/' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorFile' ] );
    ob_end_clean();
    if( empty( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorVariableList' ] ) ) {
        $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorVariableList' ] = $default;
    }
}

/* Loading styles in admin panel for edit page */
function wbrpt_enqueue_admin_styles_scripts( $hook )
{
    $pages = array( 'post-new.php', 'post.php' );
    if( !in_array( $hook, $pages ) ) {
        return;
    }

    global $post;

    /* These files are required for all 'edit' pages to make the 'Insert pricing table' button work in TinyMCE */
    wp_register_style( 'wbr-main', plugins_url( 'assets/admin/css/main.css', __FILE__ ) );

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'jquery-ui-dialog' );
    wp_enqueue_style( 'wp-jquery-ui-dialog' );
    wp_enqueue_style( 'wbr-main' );

    if( $post->post_type != 'wbr-pricing-table' ) {
        return;
    }

    wp_enqueue_media( array( 'post' => $post->ID ) );

    wp_register_script( 'wbr-jquery-spectrum', plugins_url( 'assets/js/spectrum.js', __FILE__ ), array( 'jquery' ) );
    wp_register_script( 'wbr-jquery-scroll-to', plugins_url( 'assets/js/jquery.scrollTo.min.js', __FILE__ ), array( 'jquery' ) );
    wp_register_script( 'wbr-table-drag-drop', plugins_url( 'assets/admin/js/table-drag-drop.js', __FILE__ ), array( 'jquery' ) );
    wp_register_script( 'wbr-pricing-table-editor', plugins_url( 'assets/admin/js/pricing-table-editor.js', __FILE__ ), array( 'wbr-jquery-scroll-to', 'jquery-ui-position', 'wbr-table-drag-drop', 'wbr-jquery-spectrum' ) );
    wp_register_style( 'wbr-jquery-spectrum', plugins_url( 'assets/css/spectrum.css', __FILE__ ) );
    wp_register_style( 'wbr-font-awesome', plugins_url( 'assets/css/font-awesome.min.css', __FILE__ ) );
    wp_register_style( 'wbr-table-drag-drop', plugins_url( 'assets/admin/css/table-drag-drop.css', __FILE__ ) );
    wp_register_style( 'wbr-pricing-table-design', plugins_url( 'assets/admin/css/pricing-table-design.css', __FILE__ ) );
    wp_register_style( 'wbr-pricing-table-editor', plugins_url( 'assets/admin/css/pricing-table-editor.css', __FILE__ ), array( 'wbr-font-awesome', 'wbr-table-drag-drop', 'wbr-pricing-table-design', 'wbr-jquery-spectrum' ) );
    wp_localize_script( 'wbr-pricing-table-editor', 'wbrptLocalization', array(
        'pricingTableContentVersionsVersionD'    => _x('Version %d', 'Pricing Table Content Versions', 'wbrpt'),
        'cannotHaveLessThanOnePlan'              => _x('You cannot have less than one plan!', 'JavaScript alert', 'wbrpt'),
        'cannotHaveMoreThanFivePlans'            => _x('You cannot have more than five plans!', 'JavaScript alert', 'wbrpt'),
        'cannotRemoveAllFeatures'                => _x('You cannot remove all features, at least you need to have one!', 'JavaScript alert', 'wbrpt'),
        'cannotRemoveAllColors'                  => _x('You cannot remove all colors, there should be the one at least!', 'JavaScript alert', 'wbrpt'),
        'jsDTitleThemeColors'                    => _x('Theme colors', 'JavaScript dialog title', 'wbrpt'),
        'jsDTitleBuilder'                        => _x('Builder', 'JavaScript dialog title', 'wbrpt'),
        'jsDTitlePricingTablePreview'            => _x('Pricing table preview', 'JavaScript dialog title', 'wbrpt'),
        'jsDLMsgGeneratingColorTheme'            => _x('Generating color theme', 'JavaScript dialog loading message', 'wbrpt'),
        'jsDLMsgRemovingSelectedColor'           => _x('Removing selected color', 'JavaScript dialog loading message', 'wbrpt'),
        'jsDLMsgGeneratingAllDefaultColorThemes' => _x('Generating all default color themes', 'JavaScript dialog loading message', 'wbrpt'),
        'jsDLMsgAddingNewVersion'                => _x('Adding new version', 'JavaScript dialog loading message', 'wbrpt'),
        'jsDLMsgRemovingSelectedVersion'         => _x('Removing selected version', 'JavaScript dialog loading message', 'wbrpt'),
        'jsDLMsgChangingDesign'                  => _x('Changing design', 'JavaScript dialog loading message', 'wbrpt'),
        'allEnteredDataWillBeOverwritten'        => _x('All your entered data will be overwritten by demo ones!', 'JavaScript alert', 'wbrpt'),
        'selectThisDesign'                       => _x('Would you like to select this design?', 'JavaScript alert', 'wbrpt'),
        'jsDLMsgLoadingDemoContent'              => _x('Loading demo content', 'JavaScript dialog loading message', 'wbrpt')
    ));

    wp_enqueue_script( 'wbr-pricing-table-editor' );
    wp_enqueue_style( 'wbr-pricing-table-editor' ) ;
}

add_action( 'admin_enqueue_scripts', 'wbrpt_enqueue_admin_styles_scripts' );

function wbrpt_meta_box_post_data_processing()
{
    /* If there are no plans, then it's pointless to do something! */
    if( !isset( $_POST[ 'wbrptData' ] ) || empty( $_POST[ 'wbrptData' ] ) ) return;

    $plansFeaturesData = array();
    foreach( $_POST[ 'wbrptData' ] as $index => $instance ) {
        $plansFeaturesData[ $index ] = json_decode( stripcslashes( $instance ), true );
        $plansFeaturesData[ $index ][ 'PluginVersion' ] = wbrpt_get_plugin_data( 'Version' );
    }

    if( isset( $_POST[ 'wbrptActiveInstance' ] ) && isset( $plansFeaturesData[ $_POST[ 'wbrptActiveInstance' ] ] ) ) {
        $plansFeaturesData[ $_POST[ 'wbrptActiveInstance' ] ][ 'ActiveInstance' ] = true;
    }

    $designColorData = array();
    $designColorData[ 'design' ] = isset( $_POST[ 'wbrptDesign' ] ) ? $_POST[ 'wbrptDesign' ] : '';
    $designColorData[ 'color' ] = isset( $_POST[ 'wbrptColor' ] ) ? $_POST[ 'wbrptColor' ] : '';
    $designColorData[ 'animation' ] = isset( $_POST[ 'wbrptAnimation' ] ) ? $_POST[ 'wbrptAnimation' ] : 0;
    $designColorData[ 'smartResize' ][ 'LineUpContent' ] = isset( $_POST[ 'wbrptSmartResize' ][ 'LineUpContent' ] ) ? true : false;
    $designColorData[ 'styles' ] = isset( $_POST[ 'wbrptStyles' ] ) ? $_POST[ 'wbrptStyles' ] : '';

    return array( 'plansFeatures' => $plansFeaturesData, 'designColor' => $designColorData );
}

function wbrpt_clean_empty_plans_features_data( $value )
{
    $defaultList = array(
        'FeatureTooltipPosition'      => 't',
        'FeatureValueTooltipPosition' => 't',
        'FeatureRowHeight'            => '1',
        'FeatureValueTooltipType'     => '0',
        'FeatureTooltipType'          => '0'
    );

    foreach( $value as &$instance ) {
        foreach( $instance as $key => &$item ) {
            if( $key == 'PlanName' || $key == 'FeatureName' ) {
                continue;
            }
            if( is_array( $item ) ) {
                $item = wbrpt_array_filter( $item, ( isset( $defaultList[ $key ] ) ? $defaultList[ $key ] : null ) );
                if( $item === array() ) unset( $instance[ $key ] );
            }
        }
    }

    return $value;
}

function wbrpt_array_filter( $value, $default = null )
{
    foreach( $value as &$item ) {
        if( is_array( $item ) ) {
            $item = wbrpt_array_filter( $item, $default );
        }
    }
    return array_filter( $value, function( $var ) use( $default ) {
        return !( $var === '' || $var === false || $var === 0 || $var === array() || $var === $default );
    });
}

function wbrpt_get_template_result( $gtrTemplatePath, $gtrTemplateVars = array() )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    foreach( $gtrTemplateVars as $varName => $varValue ) {
        $$varName = $varValue;
    }
    ob_start();
    include $gtrTemplatePath;
    $result = ob_get_contents();
    ob_end_clean();
    return $result;
}

function wbrpt_plans_features_default_value( &$value, $designID )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $designControlList = array_fill_keys( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'controlList' ], true );
    $navigationControlList = array();
    if( isset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'navigationItemNumber' ] ) ) {
        for( $number = 1; $number <= $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'navigationItemNumber' ]; $number++ ) {
            $navigationControlList[ 'NavigationName' ][ $number ] = '';
            $navigationControlList[ 'NavigationLink' ][ $number ] = '';
        }
        $navigationControlList[ 'ActiveNavigation' ] = 0;
    }
    foreach( $value as $index => $instance ) {
        if( $navigationControlList !== array() ) {
            $value[ $index ] = array_replace_recursive( $navigationControlList, $value[ $index ] );
        }
        foreach( $wbrptGeneralSettings[ 'controlList' ][ 'base' ] as $controlID => $controlSettings ) {
            if( !isset( $designControlList[ $controlID ] ) ) continue;
            if( !isset( $value[ $index ][ $controlID ] ) ) {
                $value[ $index ][ $controlID ] = $controlSettings[ 2 ];
            }
        }
        foreach( array_merge( $wbrptGeneralSettings[ 'controlList' ][ 'column' ], array( 'PlanDesignColor' => array( 2 => '' ) ) ) as $controlID => $controlSettings ) {
            if( !isset( $designControlList[ $controlID ] ) ) continue;
            foreach( $value[ $index ][ 'PlanName' ] as $planID => $planName ) {
                if( !isset( $value[ $index ][ $controlID ][ $planID ] ) ) {
                    $value[ $index ][ $controlID ][ $planID ] = $controlSettings[ 2 ];
                }
            }
        }
        foreach( $wbrptGeneralSettings[ 'controlList' ][ 'row' ] as $controlID => $controlSettings ) {
            if( !isset( $designControlList[ $controlID ] ) ) continue;
            foreach( $value[ $index ][ 'FeatureName' ] as $featureID => $featureName ) {
                if( !isset( $value[ $index ][ $controlID ][ $featureID ] ) ) {
                    $value[ $index ][ $controlID ][ $featureID ] = $controlSettings[ 2 ];
                }
            }
        }
        foreach( $wbrptGeneralSettings[ 'controlList' ][ 'cell' ] as $controlID => $controlSettings ) {
            if( !isset( $designControlList[ $controlID ] ) ) continue;
            foreach( $value[ $index ][ 'PlanName' ] as $planID => $planName ) {
                foreach( $value[ $index ][ 'FeatureName' ] as $featureID => $featureName ) {
                    if( !isset( $value[ $index ][ $controlID ][ $planID ][ $featureID ] ) ) {
                        $value[ $index ][ $controlID ][ $planID ][ $featureID ] = $controlSettings[ 2 ];
                    }
                }
            }
        }
    }
}

/* Storing values in DB */
function wbrpt_save_meta_box_data( $post_id )
{
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    if( get_post_type() != 'wbr-pricing-table' ) return;

    $metaData = wbrpt_meta_box_post_data_processing();
    if( is_null( $metaData ) ) return;

    // START: save dummy data
    // $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    // $ddImportFilePath = plugin_dir_path( __FILE__ ) . 'imports/' . $wbrptGeneralSettings[ 'designList' ][ $metaData[ 'designColor' ][ 'design' ] ][ 'importFile' ];
    // $ddPreparedData = wbrpt_clean_empty_plans_features_data( $metaData[ 'plansFeatures' ] );
    // foreach( $ddPreparedData as $ddVersionIndex => $ddVersionInstance ) {
    //     if( isset( $ddPreparedData[ $ddVersionIndex ][ 'PluginVersion' ] ) ) {
    //         unset( $ddPreparedData[ $ddVersionIndex ][ 'PluginVersion' ] );
    //     }
    // }
    // file_put_contents( $ddImportFilePath, serialize( $ddPreparedData ) );
    // END: save dummy data

    update_post_meta( $post_id, 'wbrpt_design_color', $metaData[ 'designColor' ] );
    update_post_meta( $post_id, 'wbrpt_plans_features', wbrpt_clean_empty_plans_features_data( $metaData[ 'plansFeatures' ] ) );

    if( !wp_is_post_revision( $post_id ) ) {
        remove_action( 'save_post', 'wbrpt_save_meta_box_data' );
        $content = wbrpt_generate_pricing_table_content( $metaData[ 'designColor' ], $metaData[ 'plansFeatures' ], $post_id );
        wp_update_post( array(
            'ID'           => $post_id,
            'post_content' => $content
        ), true );
        add_action( 'save_post', 'wbrpt_save_meta_box_data' );
    }
}

add_action( 'save_post', 'wbrpt_save_meta_box_data' );

function wbrpt_enqueue_styles_scripts()
{
    wp_register_style( 'wbr-font-awesome', plugins_url( 'assets/css/font-awesome.min.css', __FILE__ ) );
    wp_register_style( 'wbr-pricing-tables', plugins_url( 'assets/css/pricing-tables.css', __FILE__ ) );
    wp_register_script( 'wbr-purchase-button-code', plugins_url( 'assets/js/purchase-button-code.js', __FILE__ ), array( 'jquery' ) );
    wp_register_script( 'wbr-repeated-pricing-table', plugins_url( 'assets/js/repeated-pricing-table.js', __FILE__ ), array( 'jquery' ) );
    wp_register_script( 'wbr-smart-resize', plugins_url( 'assets/js/smart-resize.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_style( 'wbr-pricing-tables' ) ;
}

add_action( 'wp_enqueue_scripts', 'wbrpt_enqueue_styles_scripts' );

function wbrpt_generate_pricing_table_content( $ptDesignColorMeta, $ptPlansFeaturesListMeta, $pricingTableID = 0 )
{
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $ptDesign = $ptDesignColorMeta[ 'design' ];
    $ptColor = $ptDesignColorMeta[ 'color' ];
    $template = $wbrptGeneralSettings[ 'designList' ][ $ptDesign ][ 'template' ];
    foreach( $ptPlansFeaturesListMeta as $index => $ptPlansFeaturesMeta ) {
        if( isset( $ptPlansFeaturesListMeta[ $index ][ 'PlanPurchaseButtonCode' ] ) ) {
            foreach( $ptPlansFeaturesListMeta[ $index ][ 'PlanPurchaseButtonCode' ] as $planID => $planPurchaseButtonCode ) {
                if( trim( $planPurchaseButtonCode ) != '' ) {
                    $ptPlansFeaturesListMeta[ $index ][ 'PlanPurchaseButtonLink' ][ $planID ] = '#wbrpt-purchase-button-' . $pricingTableID . '-' . $index . '-' . $planID;
                }
            }
        }
        if( isset( $ptPlansFeaturesListMeta[ $index ][ 'FeatureValuePurchaseButtonCode' ] ) ) {
            foreach( $ptPlansFeaturesListMeta[ $index ][ 'FeatureValuePurchaseButtonCode' ] as $planID => $featureValuePurchaseButtonCodeList ) {
                foreach( $featureValuePurchaseButtonCodeList as $featureID => $featureValuePurchaseButtonCode ) {
                    if( trim( $featureValuePurchaseButtonCode ) != '' ) {
                        $ptPlansFeaturesListMeta[ $index ][ 'FeatureValuePurchaseButtonLink' ][ $planID ][ $featureID ] = '#wbrpt-purchase-button-' . $pricingTableID . '-' . $index . '-' . $planID . '-' . $featureID;
                    }
                }
            }
        }
    }
    ob_start();
    include( 'templates/pricingTable.php' );
    $result = ob_get_contents();
    ob_end_clean();
    return $result;
}

/* Adding shortcode - tag for XML attributes */
function wbrpt_add_short_code( $attributes )
{
    static $purchaseButtonBlockIDList = array();
    static $repeatedInstancePricingTableList = array();

    if( !isset( $attributes[ 'id' ] ) ) {
        return __( 'Pricing table ID is undefined!', 'wbrpt' );
    }
    $pricingTableID = intval( $attributes[ 'id' ] );
    $pricingTablePost = get_post( $pricingTableID );
    if( $pricingTablePost === null ) {
        return __( 'Pricing table with current ID cannot be found!', 'wbrpt' );
    }
    if( $pricingTablePost->post_type != 'wbr-pricing-table' ) {
        return __( 'Post with current ID is not pricing table!', 'wbrpt' );
    }
    if( $pricingTablePost->post_status != 'publish' ) {
        return __( 'Current pricing table is not published!', 'wbrpt' );
    }

    $ptDesignColorMeta = get_post_meta( $pricingTableID, 'wbrpt_design_color', true );
    $ptPlansFeaturesMeta = get_post_meta( $pricingTableID, 'wbrpt_plans_features', true );

    /* Backward compatibility with older versions */
    if( isset( $ptPlansFeaturesMeta[ 'PlanName' ] ) || isset( $ptPlansFeaturesMeta[ 'FeatureName' ] ) ) {
        $ptPlansFeaturesMeta = array( 1 => $ptPlansFeaturesMeta );
    }

    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;

    if( isset( $wbrptGeneralSettings[ 'designList' ][ $ptDesignColorMeta[ 'design' ] ][ 'styleList' ] ) ) {
        foreach( $wbrptGeneralSettings[ 'designList' ][ $ptDesignColorMeta[ 'design' ] ][ 'styleList' ] as $additionalStyleIdentifier ) {
            if( isset( $wbrptGeneralSettings[ 'additionalStyleList' ][ $additionalStyleIdentifier ] ) ) {
                wp_enqueue_style( 'wbr-' . $additionalStyleIdentifier, $wbrptGeneralSettings[ 'additionalStyleList' ][ $additionalStyleIdentifier ] );
            }
        }
    }

    wp_enqueue_style( 'wbr-pricing-tables-' . $ptDesignColorMeta[ 'design' ], plugins_url( 'assets/css/pt-' . $ptDesignColorMeta[ 'design' ] . '.css', __FILE__ ), array( 'wbr-font-awesome' ) );
    wp_enqueue_style( 'wbr-pricing-tables-color-' . $ptDesignColorMeta[ 'design' ] . '-' . $ptDesignColorMeta[ 'color' ], plugins_url( 'assets/css/pt-color/' . $ptDesignColorMeta[ 'design' ] . '/' . $ptDesignColorMeta[ 'color' ] . '.css', __FILE__ ), array( 'wbr-pricing-tables-' . $ptDesignColorMeta[ 'design' ] ) );

    $pricingTableCode = '';
    foreach( $ptPlansFeaturesMeta as $index => $instance ) {
        if( isset( $ptPlansFeaturesMeta[ $index ][ 'PlanDesignColor' ] ) ) {
            foreach( $ptPlansFeaturesMeta[ $index ][ 'PlanDesignColor' ] as $planDesignColor ) {
                wp_enqueue_style( 'wbr-pricing-tables-color-' . $ptDesignColorMeta[ 'design' ] . '-' . $planDesignColor, plugins_url( 'assets/css/pt-color/' . $ptDesignColorMeta[ 'design' ] . '/' . $planDesignColor . '.css', __FILE__ ), array( 'wbr-pricing-tables-' . $ptDesignColorMeta[ 'design' ] ) );
            }
        }
        if( isset( $ptPlansFeaturesMeta[ $index ][ 'PlanPurchaseButtonCode' ] ) ) {
            foreach( $ptPlansFeaturesMeta[ $index ][ 'PlanPurchaseButtonCode' ] as $planID => $planPurchaseButtonCode ) {
                if( ( trim( $planPurchaseButtonCode ) != '' ) && !in_array( $pricingTableID . '-' . $index . '-' . $planID, $purchaseButtonBlockIDList ) ) {
                    wp_enqueue_script( 'wbr-purchase-button-code' );
                    $pricingTableCode .= '<div id="wbrpt-purchase-button-' . $pricingTableID . '-' . $index . '-' . $planID . '">' . wbrpt_do_shortcode( $planPurchaseButtonCode ) . '</div>';
                    $purchaseButtonBlockIDList[] = $pricingTableID . '-' . $index . '-' . $planID;
                }
            }
        }
        if( isset( $ptPlansFeaturesMeta[ $index ][ 'FeatureValuePurchaseButtonCode' ] ) ) {
            foreach( $ptPlansFeaturesMeta[ $index ][ 'FeatureValuePurchaseButtonCode' ] as $planID => $featureValuePurchaseButtonCodeList ) {
                foreach( $featureValuePurchaseButtonCodeList as $featureID => $featureValuePurchaseButtonCode ) {
                    if( ( trim( $featureValuePurchaseButtonCode ) != '' ) && !in_array( $pricingTableID . '-' . $index . '-' . $planID . '-' . $featureID, $purchaseButtonBlockIDList ) ) {
                        wp_enqueue_script( 'wbr-purchase-button-code' );
                        $pricingTableCode .= '<div id="wbrpt-purchase-button-' . $pricingTableID . '-' . $index . '-' . $planID . '-' . $featureID . '">' . wbrpt_do_shortcode( $featureValuePurchaseButtonCode ) . '</div>';
                        $purchaseButtonBlockIDList[] = $pricingTableID . '-' . $index . '-' . $planID . '-' . $featureID;
                    }
                }
            }
        }
    }
    if( !empty( $pricingTableCode ) ) {
        $pricingTableCode = '<div class="pt-hidden">' . $pricingTableCode . '</div>';
    }

    if( isset( $ptDesignColorMeta[ 'smartResize' ][ 'LineUpContent' ] ) && $ptDesignColorMeta[ 'smartResize' ][ 'LineUpContent' ] ) {
        wp_enqueue_script( 'wbr-smart-resize' );
    }

    if( isset( $repeatedInstancePricingTableList[ $pricingTableID ] ) ) {
        wp_enqueue_script( 'wbr-repeated-pricing-table' );
    }
    $repeatedInstancePricingTableList[ $pricingTableID ] = true;

    if( trim( $ptDesignColorMeta[ 'styles' ] ) != '' ) {
        wp_add_inline_style( 'wbr-pricing-tables-' . $ptDesignColorMeta[ 'design' ], $ptDesignColorMeta[ 'styles' ] );
    }

    if( empty( $pricingTablePost->post_content ) ) {
        wbrpt_plans_features_default_value( $ptPlansFeaturesMeta, $ptDesignColorMeta[ 'design' ] );
        return wbrpt_generate_pricing_table_content( $ptDesignColorMeta, $ptPlansFeaturesMeta, $pricingTableID ) . $pricingTableCode;
    }

    return $pricingTablePost->post_content . $pricingTableCode;
}

add_shortcode( 'wbr-pricing-table', 'wbrpt_add_short_code' );

function wbrpt_do_shortcode( $content )
{
    ob_start();
    $shortcodeReturn = do_shortcode( $content );
    $shortcodeOutput = ob_get_contents();
    ob_end_clean();
    return $shortcodeReturn . $shortcodeOutput;
}

function wbrpt_add_shortcode_column( $columns )
{
    $updatedColumns = array();
    foreach( $columns as $identifier => $value ) {
        $updatedColumns[ $identifier ] = $value;
        if( $identifier == 'title' ) {
            $updatedColumns[ 'shortcode' ] = __( 'Shortcode', 'wbrpt' );
        }
    }
    return $updatedColumns;
}

add_filter( 'manage_wbr-pricing-table_posts_columns' , 'wbrpt_add_shortcode_column' );

function wbrpt_add_shortcode_column_value( $column, $post_id )
{
    if( $column == 'shortcode' ) {
        echo "<code>[wbr-pricing-table id=$post_id]</code>";
    }
}

add_action( 'manage_wbr-pricing-table_posts_custom_column' , 'wbrpt_add_shortcode_column_value', 10, 2 );

/* Tinymce start */
function wbrpt_tinymce_button()
{
    if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
        add_filter( 'mce_buttons', 'wbrpt_register_tinymce_button' );
        add_filter( 'mce_external_plugins', 'wbrpt_add_tinymce_button' );
    }
}

add_action( 'admin_init', 'wbrpt_tinymce_button' );

function wbrpt_register_tinymce_button( $buttons )
{
    array_push( $buttons, 'insert_pricing_table' );
    return $buttons;
}

function wbrpt_add_tinymce_button( $plugin_array )
{
    $plugin_array[ 'wbrpt_button_script' ] = plugins_url( '/assets/admin/js/tinymce-buttons.js', __FILE__ );
    return $plugin_array;
}

add_action( 'wp_ajax_wbrpt-pricing-table-list', 'wbrpt_ajax_pricing_table_list' );

function wbrpt_ajax_pricing_table_list()
{
    include( 'templates/admin/ajaxPricingTableList.php' );
    die();
}

/* Duplicate start */
function wbrpt_change_row_actions( $actions, $post )
{
    if( $post->post_type == 'wbr-pricing-table' ) {
        $actions[ 'duplicate' ] = '<a href="' . admin_url( 'admin-post.php?action=wbrpt-duplicate-pt&ptid=' . $post->ID ) . '" title="" rel="permalink">' . _x( 'Duplicate', 'Post row actions', 'wbrpt' ) . '</a>';
        unset( $actions[ 'inline hide-if-no-js' ] );
    }
    return $actions;
}

add_filter( 'post_row_actions', 'wbrpt_change_row_actions', 10, 2 );

function wbrpt_duplicate_pricing_table()
{
    if( !isset( $_GET[ 'ptid' ] ) ) die();
    $ptID = $_GET[ 'ptid' ];
    $post = get_post( $ptID );
    if( is_null( $post ) ) die();
    if( $post->post_type != 'wbr-pricing-table' ) die();

    $currentUser = wp_get_current_user();
    $args = array(
        'comment_status' => $post->comment_status,
        'ping_status'    => $post->ping_status,
        'post_author'    => $currentUser->ID,
        'post_content'   => $post->post_content,
        'post_excerpt'   => $post->post_excerpt,
        'post_name'      => $post->post_name,
        'post_parent'    => $post->post_parent,
        'post_password'  => $post->post_password,
        'post_status'    => 'draft',
        'post_title'     => sprintf(__('Duplicate of %s', 'wbrpt'), $post->post_title),
        'post_type'      => $post->post_type,
        'to_ping'        => $post->to_ping,
        'menu_order'     => $post->menu_order
    );

    $newPostID = wp_insert_post( $args );

    $metaDataDesignColor =  get_post_meta( $ptID, 'wbrpt_design_color', true );
    $metaDataPlansFeatures = get_post_meta( $ptID, 'wbrpt_plans_features', true );

    update_post_meta( $newPostID, 'wbrpt_design_color', $metaDataDesignColor );
    update_post_meta( $newPostID, 'wbrpt_plans_features', $metaDataPlansFeatures );

    wp_redirect( admin_url( 'post.php?action=edit&post=' . $newPostID ) );
    die();
}

add_action( 'admin_post_wbrpt-duplicate-pt', 'wbrpt_duplicate_pricing_table' );

/* Preview pricing table */
function wbrpt_wp_ajax_pricing_table_preview()
{
    $metaData = wbrpt_meta_box_post_data_processing();
    if( is_null( $metaData ) ) die();
    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    array_walk_recursive( $metaData, function( &$value ){ $value = stripslashes( $value ); } );
    /* Two cases: preview one color or collection */
    if( !empty( $_POST[ 'wbrptColorVariableList' ] ) ) {
        $colorStyles = wbrpt_get_template_result(
            'includes/color/style/' . $wbrptGeneralSettings[ 'designList' ][ $metaData[ 'designColor' ][ 'design' ] ][ 'colorFile' ],
            array(
                'colorClass' => 'cpreview',
                'variables' => json_decode( stripcslashes( $_POST[ 'wbrptColorVariableList' ] ), true ),
                'calculateColors' => true,
                'generateStyles' => true
            )
        );
        $metaData[ 'designColor' ][ 'color' ] = 'preview';
    } else {
        wbrpt_get_available_design_color_list( $metaData[ 'designColor' ][ 'design' ] );
        $value = $metaData[ 'designColor' ];
    }
    $pricingTableContent = wbrpt_generate_pricing_table_content( $metaData[ 'designColor' ], $metaData[ 'plansFeatures' ], !empty( $_POST[ 'post_ID' ] ) ? $_POST[ 'post_ID' ] : 0 );
    $pluginFile = __FILE__;
    include( 'templates/admin/ajaxPricingTablePreview.php' );
    die();
}

add_action( 'wp_ajax_wbrpt-pricing-table-preview', 'wbrpt_wp_ajax_pricing_table_preview' );

function wbrpt_plugin_links( $links )
{
    $links[] = '<a href="https://swebdeveloper.com/apps/public/wordpress-builder-responsive-pricing-tables/documentation/" target="_blank">' . __( 'Documentation', 'wbrpt' ) . '</a>';
    return $links;
}

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'wbrpt_plugin_links' );

function wbrpt_generate_custom_color_style_file( $designID, $variables = array() )
{
    $designColorDirectory = plugin_dir_path( __FILE__ ) . 'assets/css/pt-color/' . $designID;
    if( !is_writable( $designColorDirectory ) ) {
        echo json_encode( array( 'error' => "WordPress: directory with design colors is not writable ($designColorDirectory), please set correct permissions for WordPress plugin directory" ) );
        exit;
    }

    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    if( empty( $variables[ '$theme-color' ] ) || !preg_match( '/^#[a-f0-9]{6}$/', $variables[ '$theme-color' ] ) ) {
        $variables[ '$theme-color' ] = '#' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ];
    }
    $colorName = ltrim( $variables[ '$theme-color' ], '#' );
    $default = array( '$theme-color' => '#' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ] );
    $getDefault = true;
    $calculateColors = true;

    include( 'includes/color/style/' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorFile' ] );

    $colorVariableList = array();
    foreach( $default as $variableName => $variableDefaultValue ) {
        $color = strtolower( ltrim( $variables[ $variableName ], '#' ) );
        if( strlen( $color ) == 3 ) {
            $color = $color[ 0 ] . $color[ 0 ] . $color[ 1 ] . $color[ 1 ] . $color[ 2 ] . $color[ 2 ];
        }
        $colorVariableList[ $variableName ] = $color;
    }

    if( isset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'iconPattern' ] ) ) {
        $colorIconHash = '';
        foreach( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'iconPattern' ] as $variableName ) {
            $colorIconHash .= $colorVariableList[ $variableName ] . '-';
        }
        $colorIconHash = rtrim( $colorIconHash, '-' );
    } else {
       $colorIconHash = $colorVariableList[ '$theme-color' ];
    }

    $hash = '/*' . $colorIconHash . ':' .  implode( '-', $colorVariableList ) . '*/';
    $currentDesignColorList = wbrpt_get_available_design_color_list( $designID, true, true );
    /* There cannot be more than 500 variatios of one color */
    $finalColorName = $colorName;
    for( $i = 2; $i <= 500; $i++ ) {
        if( !isset( $currentDesignColorList[ $finalColorName ] ) ) break;
        if( $currentDesignColorList[ $finalColorName ] == $hash ) break;
        $finalColorName = $colorName . 'n' . $i;
    }

    $colorClass = 'c' . $finalColorName;
    $getDefault = false;
    $calculateColors = false;
    $generateStyles = true;

    ob_start();
    include( 'includes/color/style/' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorFile' ] );
    $css = ob_get_contents();
    ob_end_clean();

    file_put_contents( $designColorDirectory . '/' . $finalColorName . '.css', $hash . "\n" . $css );

    return $finalColorName;
}

/* Getting custom color styles */
function wbrpt_wp_ajax_custom_color_load_all()
{
    header( 'Content-Type: application/json' );

    if( empty( $_POST[ 'design' ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design is required parameters' ) );
        exit;
    }

    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $designID = $_POST[ 'design' ];
    if( !isset( $wbrptGeneralSettings[ 'designList' ][ $designID ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design cannot be found' ) );
        exit;
    }

    include( plugin_dir_path( __FILE__ ) . 'includes/color/defaultColors.php' );

    $default = array( '$theme-color' => '#' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'defaultColor' ] );
    $getDefault = true;
    include( 'includes/color/style/' . $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorFile' ] );
    $colorVariableNameList = array_keys( $default );

    foreach( $wbrptGeneralSettings[ 'designDefaultColorHashList' ][ $designID ] as $colorHash ) {
        $colorVariableList = array_filter( array_combine( $colorVariableNameList, explode( '-', $colorHash ) ) );
        foreach( $colorVariableList as &$colorVariable ) {
            $colorVariable = '#' . $colorVariable;
        }
        wbrpt_generate_custom_color_style_file( $designID, $colorVariableList );
    }

    wbrpt_get_available_design_color_list( $designID );
    $color = ( empty( $_POST[ 'color' ] ) || !isset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ][ $_POST[ 'color' ] ] ) ) ? wbrpt_get_default_color( $designID ) : $_POST[ 'color' ];
    $result = array(
        'colorListHtml'       => wbrpt_get_template_result( 'templates/admin/parts/colorList.php', array( 'value' => array( 'design' => $designID, 'color' => $color ) ) ),
        'columnColorListHtml' => wbrpt_get_template_result( 'templates/admin/parts/columnColorList.php', array( 'designID' => $designID ) )
    );
    echo json_encode( $result );
    exit;
}

add_action( 'wp_ajax_wbrpt-custom-color-load-all', 'wbrpt_wp_ajax_custom_color_load_all' );

function wbrpt_wp_ajax_custom_color_generate()
{
    header( 'Content-Type: application/json' );

    if( empty( $_POST[ 'design' ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design is required parameters' ) );
        exit;
    }

    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $designID = $_POST[ 'design' ];
    if( !isset( $wbrptGeneralSettings[ 'designList' ][ $designID ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design cannot be found' ) );
        exit;
    }

    $color = wbrpt_generate_custom_color_style_file( $designID, ( ( isset( $_POST[ 'colorVars' ] ) && is_array( $_POST[ 'colorVars' ] ) ) ? $_POST[ 'colorVars' ] : array() ) );

    wbrpt_get_available_design_color_list( $designID );
    $result = array(
        'colorListHtml'       => wbrpt_get_template_result( 'templates/admin/parts/colorList.php', array( 'value' => array( 'design' => $designID, 'color' => $color ) ) ),
        'columnColorListHtml' => wbrpt_get_template_result( 'templates/admin/parts/columnColorList.php', array( 'designID' => $designID ) )
    );
    echo json_encode( $result );
    exit;
}

add_action( 'wp_ajax_wbrpt-custom-color-generate', 'wbrpt_wp_ajax_custom_color_generate' );

function wbrpt_wp_ajax_custom_color_remove()
{
    header( 'Content-Type: application/json' );

    if( empty( $_POST[ 'design' ] ) || empty( $_POST[ 'color' ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: color and design are required parameters' ) );
        exit;
    }

    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $designID = $_POST[ 'design' ];

    if( !isset( $wbrptGeneralSettings[ 'designList' ][ $designID ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design cannot be found' ) );
        exit;
    }

    $colorFile = plugin_dir_path( __FILE__ ) . 'assets/css/pt-color/' . $designID. '/' . $_POST[ 'color' ] . '.css';
    $result = array();

    if( !unlink( $colorFile ) ) {
        $result[ 'error' ] = 'WordPress: error while removing ' . $colorFile . ' file';
    } else {
        wbrpt_get_available_design_color_list( $designID );
        $result = array(
            'colorListHtml'       => wbrpt_get_template_result( 'templates/admin/parts/colorList.php', array( 'value' => array( 'design' => $designID, 'color' => wbrpt_get_default_color( $designID ) ) ) ),
            'columnColorListHtml' => wbrpt_get_template_result( 'templates/admin/parts/columnColorList.php', array( 'designID' => $designID ) )
        );
    }

    echo json_encode( $result );
    exit;
}

add_action( 'wp_ajax_wbrpt-custom-color-remove', 'wbrpt_wp_ajax_custom_color_remove' );

function wbrpt_wp_ajax_builder_generate()
{
    header( 'Content-Type: application/json' );

    if( empty( $_POST[ 'design' ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design is required parameter' ) );
        exit;
    }

    $wbrptGeneralSettings = &wbrptGeneralSettings::$data;
    $designID = $_POST[ 'design' ];

    if( !isset( $wbrptGeneralSettings[ 'designList' ][ $designID ] ) ) {
        echo json_encode( array( 'error' => 'WordPress: design cannot be found' ) );
        exit;
    }

    $result = array();
    if( !empty( $_POST[ 'colorData' ] ) ) {
        wbrpt_get_available_design_color_list( $designID );
        wbrpt_get_design_color_variable_list( $designID );
        $color = ( empty( $_POST[ 'color' ] ) || !isset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'colorList' ][ $_POST[ 'color' ] ] ) ) ? wbrpt_get_default_color( $designID ) : $_POST[ 'color' ];
        $result[ 'colorListHtml' ] = wbrpt_get_template_result( 'templates/admin/parts/colorList.php', array( 'value' => array( 'design' => $designID, 'color' => $color ) ) );
        $result[ 'columnColorListHtml' ] = wbrpt_get_template_result( 'templates/admin/parts/columnColorList.php', array( 'designID' => $designID ) );
        $result[ 'colorVariableListHtml' ] = wbrpt_get_template_result( 'templates/admin/parts/colorVariableList.php', array( 'value' => array( 'design' => $designID ) ) );
    }

    if( !empty( $_POST[ 'demoData' ] ) ) {
        $importFile = $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'importFile' ];
        $value = unserialize( file_get_contents( 'imports/' . $importFile, true ) );
        wbrpt_plans_features_default_value( $value, $designID );
    } else {
        $metaData = wbrpt_meta_box_post_data_processing();
        $value = $metaData[ 'plansFeatures' ];
        unset( $metaData );
    }

    if( !empty( $_POST[ 'addInstance' ] ) ) {
        reset( $value );
        $value[] = current( $value );
    } elseif( !empty( $_POST[ 'removeInstance' ] ) && isset( $value[ $_POST[ 'removeInstance' ] ] ) ) {
        unset( $value[ $_POST[ 'removeInstance' ] ] );
    }

    $result[ 'instanceAdding' ] = ( isset( $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'navigationItemNumber' ] ) && $wbrptGeneralSettings[ 'designList' ][ $designID ][ 'navigationItemNumber' ] > count( $value ) );

    wbrpt_plans_features_default_value( $value, $designID );

    $result[ 'builderHtml' ] = wbrpt_get_template_result( 'templates/admin/parts/plansFeaturesEditor.php', array( 'value' => $value, 'designID' => $designID ) );

    echo json_encode( $result );
    exit;
}

add_action( 'wp_ajax_wbrpt-builder-generate', 'wbrpt_wp_ajax_builder_generate' );

function wbrpt_media_view_strings( $strings, $post )
{
    if( ( $post instanceof WP_Post ) && $post->post_type == 'wbr-pricing-table' ) {
        $strings[ 'createGalleryTitle' ] = '';
        $strings[ 'setFeaturedImageTitle' ] = '';
        $strings[ 'createPlaylistTitle' ] = '';
        $strings[ 'createVideoPlaylistTitle' ] = '';
        $strings[ 'insertIntoPost' ] = __( 'Insert the image', 'wbrpt' );
    }

    return $strings;
}

add_filter( 'media_view_strings', 'wbrpt_media_view_strings', 10, 2 );

/* Storing plugin's settings */
function wbrpt_plugin_options_init()
{
    register_setting( 'wbrpt_plugin_options', 'wbrpt_plugin_options', 'wbrpt_plugin_options_validate' );
}

function wbrpt_plugin_options_add_page()
{
    add_options_page( 'Pricing Tables Options', __( 'Pricing Tables', 'wbrpt' ), 'manage_options', 'wbrpt_plugin_options', 'wbrpt_plugin_options_build_page' );
}

function wbrpt_plugin_options_build_page()
{
    include( 'templates/admin/pluginOptions.php' );
}

function wbrpt_plugin_options_validate( $input )
{
    $valid = array();
    if( isset( $input[ 'EnableSmartResize' ] ) ) {
        $valid[ 'EnableSmartResize' ] = true;
    }
    return $valid;
}

add_action( 'admin_init', 'wbrpt_plugin_options_init' );
add_action( 'admin_menu', 'wbrpt_plugin_options_add_page' );

function wbrpt_load_plugin_textdomain() {
    load_plugin_textdomain( 'wbrpt', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'plugins_loaded', 'wbrpt_load_plugin_textdomain' );
