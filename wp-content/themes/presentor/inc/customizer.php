<?php
/**
 * Theme Customizer.
 *
 * @package Presentor
 */

/**
 * Retrieve a holder for Customizer options.
 *
 * @since  1.0.0
 * @return array
 */
function presentor_get_customizer_options() {
	/**
	 * Filter a holder for Customizer options (for theme/plugin developer customization).
	 *
	 * @since 1.0.0
	 */
	return apply_filters( 'presentor_get_customizer_options' , array(
		'prefix'     => 'presentor',
		'capability' => 'edit_theme_options',
		'type'       => 'theme_mod',
		'options'    => array(

			/** `Site Identity` section */
			'show_tagline' => array(
				'title'    => esc_html__( 'Show tagline after logo', 'presentor' ),
				'section'  => 'title_tagline',
				'priority' => 60,
				'default'  => false,
				'field'    => 'checkbox',
				'type'     => 'control',
			),
			'totop_visibility' => array(
				'title'    => esc_html__( 'Show ToTop button', 'presentor' ),
				'section'  => 'title_tagline',
				'priority' => 61,
				'default'  => true,
				'field'    => 'checkbox',
				'type'     => 'control',
			),

			/** `General Site settings` panel */
			'general_settings' => array(
				'title'    => esc_html__( 'General Site settings', 'presentor' ),
				'priority' => 40,
				'type'     => 'panel',
			),

			/** `Logo & Favicon` section */
			'logo_favicon' => array(
				'title'    => esc_html__( 'Logo &amp; Favicon', 'presentor' ),
				'priority' => 25,
				'panel'    => 'general_settings',
				'type'     => 'section',
			),
			'header_logo_type' => array(
				'title'   => esc_html__( 'Logo Type', 'presentor' ),
				'section' => 'logo_favicon',
				'default' => 'image',
				'field'   => 'radio',
				'choices' => array(
					'image' => esc_html__( 'Image', 'presentor' ),
					'text'  => esc_html__( 'Text', 'presentor' ),
				),
				'type' => 'control',
			),
			'header_logo_url' => array(
				'title'           => esc_html__( 'Logo Upload', 'presentor' ),
				'description'     => esc_html__( 'Upload logo image', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => '%s/assets/images/logo.png',
				'field'           => 'image',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_image',
			),
			'invert_header_logo_url' => array(
				'title'           => esc_html__( 'Invert Logo Upload', 'presentor' ),
				'description'     => esc_html__( 'Upload logo image', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => '%s/assets/images/invert-logo.png',
				'field'           => 'image',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_image',
			),
			'retina_header_logo_url' => array(
				'title'           => esc_html__( 'Retina Logo Upload', 'presentor' ),
				'description'     => esc_html__( 'Upload logo for retina-ready devices', 'presentor' ),
				'section'         => 'logo_favicon',
				'field'           => 'image',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_image',
			),
			'invert_retina_header_logo_url' => array(
				'title'           => esc_html__( 'Invert Retina Logo Upload', 'presentor' ),
				'description'     => esc_html__( 'Upload logo for retina-ready devices', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => false,
				'field'           => 'image',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_image',
			),
			'header_logo_font_family' => array(
				'title'           => esc_html__( 'Font Family', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => 'Montserrat, sans-serif',
				'field'           => 'fonts',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_font_style' => array(
				'title'           => esc_html__( 'Font Style', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => 'normal',
				'field'           => 'select',
				'choices'         => presentor_get_font_styles(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_font_weight' => array(
				'title'           => esc_html__( 'Font Weight', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => '700',
				'field'           => 'select',
				'choices'         => presentor_get_font_weight(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_font_size' => array(
				'title'           => esc_html__( 'Font Size, px', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => '24',
				'field'           => 'number',
				'input_attrs'     => array(
					'min'  => 6,
					'max'  => 50,
					'step' => 1,
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'logo_favicon',
				'default'     => '1',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'logo_favicon',
				'default'     => '0.1',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_character_set' => array(
				'title'           => esc_html__( 'Character Set', 'presentor' ),
				'section'         => 'logo_favicon',
				'default'         => 'latin',
				'field'           => 'select',
				'choices'         => presentor_get_character_sets(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),
			'header_logo_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'logo_favicon',
				'default' => 'uppercase',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
				'active_callback' => 'presentor_is_header_logo_text',
			),

			/** `Breadcrumbs` section */
			'breadcrumbs' => array(
				'title'    => esc_html__( 'Breadcrumbs', 'presentor' ),
				'priority' => 30,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),
			'breadcrumbs_visibillity' => array(
				'title'   => esc_html__( 'Enable Breadcrumbs', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'breadcrumbs_front_visibillity' => array(
				'title'   => esc_html__( 'Enable Breadcrumbs on front page', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'breadcrumbs_page_title' => array(
				'title'   => esc_html__( 'Enable page title in breadcrumbs area', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'breadcrumbs_path_type' => array(
				'title'   => esc_html__( 'Show full/minified path', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => 'full',
				'field'   => 'select',
				'choices' => array(
					'full'     => esc_html__( 'Full', 'presentor' ),
					'minified' => esc_html__( 'Minified', 'presentor' ),
				),
				'type'    => 'control',
			),
			'breadcrumbs_bg_color' => array(
				'title'   => esc_html__( 'Background Color', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => '#42474C',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'breadcrumbs_bg_image' => array(
				'title'   => esc_html__( 'Background Image', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => '%s/assets/images/breadcrumbs_bg.jpg',
				'field'   => 'image',
				'type'    => 'control',
			),
			'breadcrumbs_bg_repeat' => array(
				'title'   => esc_html__( 'Background Repeat', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => 'no-repeat',
				'field'   => 'select',
				'choices' => presentor_get_bg_repeat(),
				'type'    => 'control',
			),
			'breadcrumbs_bg_position' => array(
				'title'   => esc_html__( 'Background Position', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => 'center',
				'field'   => 'select',
				'choices' => presentor_get_bg_position(),
				'type'    => 'control',
			),
			'breadcrumbs_bg_size' => array(
				'title'   => esc_html__( 'Background Size', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => 'cover',
				'field'   => 'select',
				'choices' => presentor_get_bg_size(),
				'type'    => 'control',
			),
			'breadcrumbs_bg_attachment' => array(
				'title'   => esc_html__( 'Background Attachment', 'presentor' ),
				'section' => 'breadcrumbs',
				'default' => 'scroll',
				'field'   => 'select',
				'choices' => presentor_get_bg_attachment(),
				'type'    => 'control',
			),
			'breadcrumbs_text_color' => array(
				'title'       => esc_html__( 'Text Color', 'presentor' ),
				'description' => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'presentor' ),
				'section'     => 'breadcrumbs',
				'default'     => 'light',
				'field'       => 'select',
				'choices'     => presentor_get_text_color(),
				'type'        => 'control',
			),
			'breadcrumbs_padding_y' => array(
				'title'       => esc_html__( 'Desktop Padding Top Bottom (px)', 'presentor' ),
				'section'     => 'breadcrumbs',
				'default'     => 70,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 5,
					'max'  => 120,
					'step' => 1,
				),
				'type' => 'control',
			),
			'breadcrumbs_padding_y_tablet' => array(
				'title'       => esc_html__( 'Tablet Padding Top Bottom (px)', 'presentor' ),
				'section'     => 'breadcrumbs',
				'default'     => 40,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 5,
					'max'  => 120,
					'step' => 1,
				),
				'type' => 'control',
			),
			'breadcrumbs_padding_y_mobile' => array(
				'title'       => esc_html__( 'Mobile Padding Top Bottom (px)', 'presentor' ),
				'section'     => 'breadcrumbs',
				'default'     => 20,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 5,
					'max'  => 120,
					'step' => 1,
				),
				'type' => 'control',
			),

			/** `Social links` section */
			'social_links' => array(
				'title'    => esc_html__( 'Social links', 'presentor' ),
				'priority' => 50,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),
			'header_social_links' => array(
				'title'   => esc_html__( 'Show social links in header', 'presentor' ),
				'section' => 'social_links',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'footer_social_links' => array(
				'title'   => esc_html__( 'Show social links in footer', 'presentor' ),
				'section' => 'social_links',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_share_buttons' => array(
				'title'   => esc_html__( 'Show social sharing to blog posts', 'presentor' ),
				'section' => 'social_links',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_share_buttons' => array(
				'title'   => esc_html__( 'Show social sharing to single blog post', 'presentor' ),
				'section' => 'social_links',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Page Layout` section */
			'page_layout' => array(
				'title'    => esc_html__( 'Page Layout', 'presentor' ),
				'priority' => 55,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),
			'page_container_type' => array(
				'title'   => esc_html__( 'Page type', 'presentor' ),
				'section' => 'page_layout',
				'default' => 'fullwidth',
				'field'   => 'select',
				'choices' => array(
					'boxed'     => esc_html__( 'Boxed', 'presentor' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'presentor' ),
				),
				'type' => 'control',
			),
			'page_boxed_width' => array(
				'title'       => esc_html__( 'Page boxed width (px)', 'presentor' ),
				'section'     => 'page_layout',
				'default'     => 1200,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 960,
					'max'  => 1920,
					'step' => 1,
				),
				'type' => 'control',
				'active_callback' => 'presentor_is_page_type_boxed',
			),
			'header_container_type' => array(
				'title'   => esc_html__( 'Header container type', 'presentor' ),
				'section' => 'page_layout',
				'default' => 'boxed',
				'field'   => 'select',
				'choices' => array(
					'boxed'     => esc_html__( 'Boxed', 'presentor' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'presentor' ),
				),
				'type' => 'control',
			),
			'breadcrumbs_container_type' => array(
				'title'   => esc_html__( 'Breadcrumbs container type', 'presentor' ),
				'section' => 'page_layout',
				'default' => 'boxed',
				'field'   => 'select',
				'choices' => array(
					'boxed'     => esc_html__( 'Boxed', 'presentor' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'presentor' ),
				),
				'type' => 'control',
			),
			'content_container_type' => array(
				'title'   => esc_html__( 'Content container type', 'presentor' ),
				'section' => 'page_layout',
				'default' => 'boxed',
				'field'   => 'select',
				'choices' => array(
					'boxed'     => esc_html__( 'Boxed', 'presentor' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'presentor' ),
				),
				'type' => 'control',
			),
			'footer_container_type' => array(
				'title'   => esc_html__( 'Footer container type', 'presentor' ),
				'section' => 'page_layout',
				'default' => 'boxed',
				'field'   => 'select',
				'choices' => array(
					'boxed'     => esc_html__( 'Boxed', 'presentor' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'presentor' ),
				),
				'type' => 'control',
			),
			'container_width' => array(
				'title'       => esc_html__( 'Container width (px)', 'presentor' ),
				'section'     => 'page_layout',
				'default'     => 1200,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 960,
					'max'  => 1920,
					'step' => 1,
				),
				'type' => 'control',
			),
			'sidebar_width' => array(
				'title'   => esc_html__( 'Sidebar width', 'presentor' ),
				'section' => 'page_layout',
				'default' => '1/3',
				'field'   => 'select',
				'choices' => array(
					'1/3' => '1/3',
					'1/4' => '1/4',
				),
				'sanitize_callback' => 'sanitize_text_field',
				'type'              => 'control',
			),

			/** `Page Preloader` section */
			'page_preloader_section' => array(
				'title'    => esc_html__( 'Page Preloader', 'presentor' ),
				'priority' => 60,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),

			'page_preloader' => array(
				'title'    => esc_html__( 'Show page preloader', 'presentor' ),
				'section'  => 'page_preloader_section',
				'priority' => 10,
				'default'  => true,
				'field'    => 'checkbox',
				'type'     => 'control',
			),

			'page_preloader_bg' => array(
				'title'           => esc_html__( 'Preloader Cover Background Color', 'presentor' ),
				'section'         => 'page_preloader_section',
				'default'         => '#fff',
				'field'           => 'hex_color',
				'type'            => 'control',
				'active_callback' => 'presentor_is_page_preloader_enable',
			),

			'page_preloader_url' => array(
				'title'           => esc_html__( 'Preloader Image Upload', 'presentor' ),
				'section'         => 'page_preloader_section',
				'field'           => 'image',
				'default'         => '%s/assets/images/preloader-image.png',
				'type'            => 'control',
				'active_callback' => 'presentor_is_page_preloader_enable',
			),

			/** `Color Scheme` panel */
			'color_scheme' => array(
				'title'       => esc_html__( 'Color Scheme', 'presentor' ),
				'description' => esc_html__( 'Configure Color Scheme', 'presentor' ),
				'priority'    => 40,
				'type'        => 'panel',
			),

			/** `Regular scheme` section */
			'regular_scheme' => array(
				'title'       => esc_html__( 'Regular scheme', 'presentor' ),
				'priority'    => 10,
				'panel'       => 'color_scheme',
				'type'        => 'section',
			),
			'regular_accent_color_1' => array(
				'title'   => esc_html__( 'Accent color (1)', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#da5c48',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_accent_color_2' => array(
				'title'   => esc_html__( 'Accent color (2)', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_text_color' => array(
				'title'   => esc_html__( 'Text color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_link_color' => array(
				'title'   => esc_html__( 'Link color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#899296',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_link_hover_color' => array(
				'title'   => esc_html__( 'Link hover color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#da5c48',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h1_color' => array(
				'title'   => esc_html__( 'H1 color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h2_color' => array(
				'title'   => esc_html__( 'H2 color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h3_color' => array(
				'title'   => esc_html__( 'H3 color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h4_color' => array(
				'title'   => esc_html__( 'H4 color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h5_color' => array(
				'title'   => esc_html__( 'H5 color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h6_color' => array(
				'title'   => esc_html__( 'H6 color', 'presentor' ),
				'section' => 'regular_scheme',
				'default' => '#42474c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Invert scheme` section */
			'invert_scheme' => array(
				'title'       => esc_html__( 'Invert scheme', 'presentor' ),
				'priority'    => 20,
				'panel'       => 'color_scheme',
				'type'        => 'section',
			),
			'invert_accent_color_1' => array(
				'title'   => esc_html__( 'Accent color (1)', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_text_color' => array(
				'title'   => esc_html__( 'Text color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_link_color' => array(
				'title'   => esc_html__( 'Link color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_link_hover_color' => array(
				'title'   => esc_html__( 'Link hover color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#da5c48',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_h1_color' => array(
				'title'   => esc_html__( 'H1 color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_h2_color' => array(
				'title'   => esc_html__( 'H2 color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_h3_color' => array(
				'title'   => esc_html__( 'H3 color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_h4_color' => array(
				'title'   => esc_html__( 'H4 color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_h5_color' => array(
				'title'   => esc_html__( 'H5 color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'invert_h6_color' => array(
				'title'   => esc_html__( 'H6 color', 'presentor' ),
				'section' => 'invert_scheme',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Greyscale colors` section */
			'grey_scheme' => array(
				'title'       => esc_html__( 'Greyscale colors', 'presentor' ),
				'priority'    => 30,
				'panel'       => 'color_scheme',
				'type'        => 'section',
			),

			'grey_color_1' => array(
				'title'   => esc_html__( 'Grey color (1)', 'presentor' ),
				'section' => 'grey_scheme',
				'default' => '#f7f7f7',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Colors` section */
			'page_bg_color' => array(
				'title'   => esc_html__( 'Page Background Color', 'presentor' ),
				'section' => 'colors',
				'default' => '#fff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Typography Settings` panel */
			'typography' => array(
				'title'       => esc_html__( 'Typography', 'presentor' ),
				'description' => esc_html__( 'Configure typography settings', 'presentor' ),
				'priority'    => 50,
				'type'        => 'panel',
			),

			/** `Body text` section */
			'body_typography' => array(
				'title'       => esc_html__( 'Body text', 'presentor' ),
				'priority'    => 5,
				'panel'       => 'typography',
				'type'        => 'section',
			),
			'body_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'body_typography',
				'default' => 'Montserrat, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'body_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'body_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'body_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'body_typography',
				'default' => '300',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'body_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'body_typography',
				'default'     => '18',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 6,
					'max'  => 50,
					'step' => 1,
				),
				'type' => 'control',
			),
			'body_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'body_typography',
				'default'     => '1.667',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'body_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'body_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'body_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'body_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'body_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'body_typography',
				'default' => 'left',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'body_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'body_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `H1 Heading` section */
			'h1_typography' => array(
				'title'    => esc_html__( 'H1 Heading', 'presentor' ),
				'priority' => 10,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h1_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'h1_typography',
				'default' => 'Oswald, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h1_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'h1_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'h1_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'h1_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'h1_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'h1_typography',
				'default'     => '48',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'h1_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'h1_typography',
				'default'     => '1.08',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'h1_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'h1_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'h1_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'h1_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'h1_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'h1_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'h1_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'h1_typography',
				'default' => 'uppercase',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `H2 Heading` section */
			'h2_typography' => array(
				'title'    => esc_html__( 'H2 Heading', 'presentor' ),
				'priority' => 15,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h2_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'h2_typography',
				'default' => 'Oswald, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h2_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'h2_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'h2_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'h2_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'h2_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'h2_typography',
				'default'     => '42',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'h2_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'h2_typography',
				'default'     => '1.43',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'h2_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'h2_typography',
				'default'     => '-0,025',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'h2_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'h2_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'h2_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'h2_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'h2_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'h2_typography',
				'default' => 'uppercase',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `H3 Heading` section */
			'h3_typography' => array(
				'title'    => esc_html__( 'H3 Heading', 'presentor' ),
				'priority' => 20,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h3_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'h3_typography',
				'default' => 'Oswald, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h3_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'h3_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'h3_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'h3_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'h3_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'h3_typography',
				'default'     => '30',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'h3_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'h3_typography',
				'default'     => '1.6',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'h3_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'h3_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'h3_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'h3_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'h3_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'h3_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'h3_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'h3_typography',
				'default' => 'uppercase',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `H4 Heading` section */
			'h4_typography' => array(
				'title'    => esc_html__( 'H4 Heading', 'presentor' ),
				'priority' => 25,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h4_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'h4_typography',
				'default' => 'Oswald, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h4_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'h4_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'h4_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'h4_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'h4_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'h4_typography',
				'default'     => '20',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'h4_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'h4_typography',
				'default'     => '1.8',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'h4_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'h4_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'h4_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'h4_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'h4_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'h4_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'h4_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'h4_typography',
				'default' => 'uppercase',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `H5 Heading` section */
			'h5_typography' => array(
				'title'    => esc_html__( 'H5 Heading', 'presentor' ),
				'priority' => 30,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h5_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'h5_typography',
				'default' => 'Montserrat, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h5_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'h5_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'h5_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'h5_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'h5_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'h5_typography',
				'default'     => '16',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'h5_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'h5_typography',
				'default'     => '1.33',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'h5_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'h5_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'h5_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'h5_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'h5_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'h5_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'h5_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'h5_typography',
				'default' => 'uppercase',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `H6 Heading` section */
			'h6_typography' => array(
				'title'    => esc_html__( 'H6 Heading', 'presentor' ),
				'priority' => 35,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h6_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'h6_typography',
				'default' => 'Montserrat, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h6_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'h6_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'h6_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'h6_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'h6_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'h6_typography',
				'default'     => '14',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'h6_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'h6_typography',
				'default'     => '1.2',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'h6_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'h6_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'h6_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'h6_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'h6_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'presentor' ),
				'section' => 'h6_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => presentor_get_text_aligns(),
				'type'    => 'control',
			),
			'h6_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'h6_typography',
				'default' => 'h6_text_transform',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `Accent Text` section */
			'accent_typography' => array(
				'title'    => esc_html__( 'Accent Text', 'presentor' ),
				'priority' => 45,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'accent_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'accent_typography',
				'default' => 'Libre Baskerville, serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'accent_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'accent_typography',
				'default' => 'italic',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'accent_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'accent_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'accent_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'accent_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'accent_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'accent_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'accent_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'accent_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `Breadcrumbs` section */
			'breadcrumbs_typography' => array(
				'title'    => esc_html__( 'Breadcrumbs', 'presentor' ),
				'priority' => 45,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'breadcrumbs_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'Montserrat, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'breadcrumbs_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'breadcrumbs_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'breadcrumbs_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'breadcrumbs_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'breadcrumbs_typography',
				'default'     => '14',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 6,
					'max'  => 50,
					'step' => 1,
				),
				'type' => 'control',
			),
			'breadcrumbs_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'breadcrumbs_typography',
				'default'     => '1.5',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'breadcrumbs_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'breadcrumbs_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'breadcrumbs_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'breadcrumbs_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `Meta` section */
			'meta_typography' => array(
				'title'       => esc_html__( 'Meta', 'presentor' ),
				'priority'    => 50,
				'panel'       => 'typography',
				'type'        => 'section',
			),
			'meta_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'presentor' ),
				'section' => 'meta_typography',
				'default' => 'Montserrat, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'meta_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'presentor' ),
				'section' => 'meta_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => presentor_get_font_styles(),
				'type'    => 'control',
			),
			'meta_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'presentor' ),
				'section' => 'meta_typography',
				'default' => '400',
				'field'   => 'select',
				'choices' => presentor_get_font_weight(),
				'type'    => 'control',
			),
			'meta_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'presentor' ),
				'section'     => 'meta_typography',
				'default'     => '12',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type' => 'control',
			),
			'meta_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'presentor' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'presentor' ),
				'section'     => 'meta_typography',
				'default'     => '1.75',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type' => 'control',
			),
			'meta_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'presentor' ),
				'section'     => 'meta_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type' => 'control',
			),
			'meta_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'presentor' ),
				'section' => 'meta_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => presentor_get_character_sets(),
				'type'    => 'control',
			),
			'meta_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'presentor' ),
				'section' => 'meta_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => presentor_get_text_transform(),
				'type'    => 'control',
			),

			/** `Typography misc` section */
			'misc_styles' => array(
				'title'    => esc_html__( 'Misc', 'presentor' ),
				'priority' => 60,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'word_wrap' => array(
				'title'   => esc_html__( 'Enable Word Wrap', 'presentor' ),
				'section' => 'misc_styles',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Header` panel */
			'header_options' => array(
				'title'    => esc_html__( 'Header', 'presentor' ),
				'priority' => 60,
				'type'     => 'panel',
			),

			/** `Header styles` section */
			'header_styles' => array(
				'title'    => esc_html__( 'Header Styles', 'presentor' ),
				'priority' => 5,
				'panel'    => 'header_options',
				'type'     => 'section',
			),
			'header_layout_type' => array(
				'title'   => esc_html__( 'Layout', 'presentor' ),
				'section' => 'header_styles',
				'default' => 'style-3',
				'field'   => 'select',
				'choices' => presentor_get_header_layout_options(),
				'type'    => 'control',
			),
			'header_transparent_layout' => array(
				'title'   => esc_html__( 'Header transparent overlay', 'presentor' ),
				'section' => 'header_styles',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
				'active_callback' => '__return_false',
			),
			'header_transparent_bg' => array(
				'title'   => esc_html__( 'Header Transparent Background', 'presentor' ),
				'section' => 'header_styles',
				'field'   => 'hex_color',
				'default' => '#42474c',
				'type'    => 'control',
				'active_callback' => '__return_false',
			),
			'header_transparent_bg_alpha' => array(
				'title'           => esc_html__( 'Header Transparent Background Alpha', 'presentor' ),
				'section'         => 'header_styles',
				'default'         => '20',
				'field'           => 'number',
				'input_attrs'     => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'type'            => 'control',
				'active_callback' => '__return_false',
			),
			'header_invert_color_scheme' => array(
				'title'   => esc_html__( 'Enable invert color scheme', 'presentor' ),
				'section' => 'header_styles',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'header_bg_color' => array(
				'title'   => esc_html__( 'Background Color', 'presentor' ),
				'section' => 'header_styles',
				'field'   => 'hex_color',
				'default' => '#ffffff',
				'type'    => 'control',
			),
			'header_bg_image' => array(
				'title'   => esc_html__( 'Background Image', 'presentor' ),
				'section' => 'header_styles',
				'field'   => 'image',
				'type'    => 'control',
			),
			'header_bg_repeat' => array(
				'title'   => esc_html__( 'Background Repeat', 'presentor' ),
				'section' => 'header_styles',
				'default' => 'no-repeat',
				'field'   => 'select',
				'choices' => presentor_get_bg_repeat(),
				'type'    => 'control',
			),
			'header_bg_position' => array(
				'title'   => esc_html__( 'Background Position', 'presentor' ),
				'section' => 'header_styles',
				'default' => 'center',
				'field'   => 'select',
				'choices' => presentor_get_bg_position(),
				'type'    => 'control',
			),
			'header_bg_size' => array(
				'title'   => esc_html__( 'Background Size', 'presentor' ),
				'section' => 'header_styles',
				'default' => 'cover',
				'field'   => 'select',
				'choices' => presentor_get_bg_size(),
				'type'    => 'control',
			),
			'header_bg_attachment' => array(
				'title'   => esc_html__( 'Background Attachment', 'presentor' ),
				'section' => 'header_styles',
				'default' => 'scroll',
				'field'   => 'select',
				'choices' => presentor_get_bg_attachment(),
				'type'    => 'control',
			),
			'header_search' => array(
				'title'   => esc_html__( 'Show search', 'presentor' ),
				'section' => 'header_styles',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'header_btn_visibility' => array(
				'title'   => esc_html__( 'Show header call to action button', 'presentor' ),
				'section' => 'header_styles',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'header_btn_text' => array(
				'title'           => esc_html__( 'Header call to action button', 'presentor' ),
				'description'     => esc_html__( 'Button text', 'presentor' ),
				'section'         => 'header_styles',
				'default'         => esc_html__( 'Get In Touch', 'presentor' ),
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_btn_enable',
			),
			'header_btn_url' => array(
				'title'           => '',
				'description'     => esc_html__( 'Button url', 'presentor' ),
				'section'         => 'header_styles',
				'default'         => '#',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_btn_enable',
			),
			'header_btn_target' => array(
				'title'           => esc_html__( 'Open Link in New Tab', 'presentor' ),
				'section'         => 'header_styles',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_btn_enable',
			),
			'header_btn_style_preset' => array(
				'title'   => esc_html__( 'Button Style Preset', 'presentor' ),
				'section' => 'header_styles',
				'default' => 'primary-transparent',
				'field'   => 'select',
				'choices' => presentor_get_btn_style_presets(),
				'type'    => 'control',
				'active_callback' => 'presentor_is_header_btn_enable',
			),

			/** `Top Panel` section */
			'header_top_panel' => array(
				'title'    => esc_html__( 'Top Panel', 'presentor' ),
				'priority' => 10,
				'panel'    => 'header_options',
				'type'     => 'section',
			),
			'top_panel_visibility' => array(
				'title'   => esc_html__( 'Enable top panel', 'presentor' ),
				'section' => 'header_top_panel',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'top_panel_text' => array(
				'title'           => esc_html__( 'Disclaimer Text', 'presentor' ),
				'description'     => esc_html__( 'HTML formatting support', 'presentor' ),
				'section'         => 'header_top_panel',
				'default'         => '',
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_top_panel_enable',
			),
			'top_panel_bg'        => array(
				'title'           => esc_html__( 'Background color', 'presentor' ),
				'section'         => 'header_top_panel',
				'default'         => '#1c2024',
				'field'           => 'hex_color',
				'type'            => 'control',
				'active_callback' => 'presentor_is_top_panel_enable',
			),
			'top_menu_visibility' => array(
				'title'           => esc_html__( 'Show top menu', 'presentor' ),
				'section'         => 'header_top_panel',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_top_panel_enable',
			),

			/** `Header contact block` section */
			'header_contact_block' => array(
				'title'       => esc_html__( 'Header Contact Block', 'presentor' ),
				'description' => esc_html__( 'This block shows only if Top Panel section is enabled!', 'presentor' ),
				'priority'    => 20,
				'panel'       => 'header_options',
				'type'        => 'section',
			),
			'header_contact_block_visibility' => array(
				'title'   => esc_html__( 'Show Header Contact Block', 'presentor' ),
				'section' => 'header_contact_block',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'header_contact_icon_1' => array(
				'title'           => esc_html__( 'Contact item 1', 'presentor' ),
				'description'     => esc_html__( 'Choose icon', 'presentor' ),
				'section'         => 'header_contact_block',
				'field'           => 'iconpicker',
				'default'         => 'fa-phone',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_label_1' => array(
				'title'           => '',
				'description'     => esc_html__( 'Label', 'presentor' ),
				'section'         => 'header_contact_block',
				'default'         => '',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_text_1' => array(
				'title'           => '',
				'description'     => esc_html__( 'Description', 'presentor' ),
				'section'         => 'header_contact_block',
				'default'         => presentor_get_default_contact_information( 'phones' ),
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_icon_2' => array(
				'title'           => esc_html__( 'Contact item 2', 'presentor' ),
				'description'     => esc_html__( 'Choose icon', 'presentor' ),
				'section'         => 'header_contact_block',
				'field'           => 'iconpicker',
				'default'         => 'fa-envelope',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_label_2' => array(
				'title'           => '',
				'description'     => esc_html__( 'Label', 'presentor' ),
				'section'         => 'header_contact_block',
				'default'         => '',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_text_2' => array(
				'title'           => '',
				'description'     => esc_html__( 'Description', 'presentor' ),
				'section'         => 'header_contact_block',
				'default'         => presentor_get_default_contact_information( 'email' ),
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_icon_3' => array(
				'title'           => esc_html__( 'Contact item 3', 'presentor' ),
				'description'     => esc_html__( 'Choose icon', 'presentor' ),
				'section'         => 'header_contact_block',
				'field'           => 'iconpicker',
				'default'         => 'fa-map-marker',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_label_3' => array(
				'title'           => '',
				'description'     => esc_html__( 'Label', 'presentor' ),
				'section'         => 'header_contact_block',
				'default'         => '',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),
			'header_contact_text_3' => array(
				'title'           => '',
				'description'     => esc_html__( 'Description', 'presentor' ),
				'section'         => 'header_contact_block',
				'default'         => presentor_get_default_contact_information( 'address-2' ),
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_header_contact_block_enable',
			),

			/** `Main Menu` section */
			'header_main_menu' => array(
				'title'    => esc_html__( 'Main Menu', 'presentor' ),
				'priority' => 20,
				'panel'    => 'header_options',
				'type'     => 'section',
			),
			'header_menu_sticky' => array(
				'title'   => esc_html__( 'Enable sticky menu', 'presentor' ),
				'section' => 'header_main_menu',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'header_menu_attributes' => array(
				'title'   => esc_html__( 'Enable description', 'presentor' ),
				'section' => 'header_main_menu',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Sidebar` section */
			'sidebar_settings' => array(
				'title'    => esc_html__( 'Sidebar', 'presentor' ),
				'priority' => 105,
				'type'     => 'section',
			),
			'sidebar_position' => array(
				'title'   => esc_html__( 'Sidebar Position', 'presentor' ),
				'section' => 'sidebar_settings',
				'default' => 'fullwidth',
				'field'   => 'select',
				'choices' => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'presentor' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'presentor' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'presentor' ),
				),
				'type' => 'control',
			),

			'sidebar_position_post_listing' => array(
				'title'   => esc_html__( 'Sidebar Position on Blog Listing', 'presentor' ),
				'section' => 'sidebar_settings',
				'default' => 'one-right-sidebar',
				'field'   => 'select',
				'choices' => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'presentor' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'presentor' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'presentor' ),
				),
				'type' => 'control',
			),

			'sidebar_position_post' => array(
				'title'   => esc_html__( 'Sidebar Position on Blog Post', 'presentor' ),
				'section' => 'sidebar_settings',
				'default' => 'one-right-sidebar',
				'field'   => 'select',
				'choices' => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'presentor' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'presentor' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'presentor' ),
				),
				'type' => 'control',
			),

			'sidebar_position_product' => array(
				'title'   => esc_html__( 'Sidebar Position on Single Product', 'presentor' ),
				'section' => 'sidebar_settings',
				'default' => 'one-left-sidebar',
				'field'   => 'select',
				'choices' => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'presentor' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'presentor' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'presentor' ),
				),
				'type' => 'control',
				'active_callback' => 'presentor_is_woocommerce_activated',
			),

			/** `MailChimp` section */
			'mailchimp' => array(
				'title'       => esc_html__( 'MailChimp', 'presentor' ),
				'description' => esc_html__( 'Setup MailChimp settings for subscribe widget', 'presentor' ),
				'priority'    => 109,
				'type'        => 'section',
			),
			'mailchimp_api_key' => array(
				'title'   => esc_html__( 'MailChimp API key', 'presentor' ),
				'section' => 'mailchimp',
				'field'   => 'text',
				'type'    => 'control',
			),
			'mailchimp_list_id' => array(
				'title'   => esc_html__( 'MailChimp list ID', 'presentor' ),
				'section' => 'mailchimp',
				'field'   => 'text',
				'type'    => 'control',
			),

			/** `Ads Management` panel */
			'ads_management' => array(
				'title'    => esc_html__( 'Ads Management', 'presentor' ),
				'priority' => 110,
				'type'     => 'section',
			),
			'ads_header' => array(
				'title'             => esc_html__( 'Header', 'presentor' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),
			'ads_home_before_loop' => array(
				'title'             => esc_html__( 'Front Page Before Loop', 'presentor' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),
			'ads_post_before_content' => array(
				'title'             => esc_html__( 'Post Before Content', 'presentor' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),
			'ads_post_before_comments' => array(
				'title'             => esc_html__( 'Post Before Comments', 'presentor' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),

			/** `Footer` panel */
			'footer_options' => array(
				'title'    => esc_html__( 'Footer', 'presentor' ),
				'priority' => 110,
				'type'     => 'panel',
			),

			/** `Footer styles` section */
			'footer_styles' => array(
				'title'    => esc_html__( 'Footer Styles', 'presentor' ),
				'priority' => 5,
				'panel'    => 'footer_options',
				'type'     => 'section',
			),
			'footer_logo_visibility' => array(
				'title'   => esc_html__( 'Show Footer Logo', 'presentor' ),
				'section' => 'footer_styles',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'footer_logo_url' => array(
				'title'           => esc_html__( 'Logo upload', 'presentor' ),
				'section'         => 'footer_styles',
				'field'           => 'image',
				'default'         => '%s/assets/images/footer-logo.png',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_logo_enable',
			),
			'footer_copyright' => array(
				'title'   => esc_html__( 'Copyright text', 'presentor' ),
				'section' => 'footer_styles',
				'default' => presentor_get_default_footer_copyright(),
				'field'   => 'textarea',
				'type'    => 'control',
			),
			'footer_layout_type' => array(
				'title'   => esc_html__( 'Layout', 'presentor' ),
				'section' => 'footer_styles',
				'default' => 'style-1',
				'field'   => 'select',
				'choices' => presentor_get_footer_layout_options(),
				'type' => 'control',
			),
			'footer_bg' => array(
				'title'   => esc_html__( 'Footer Background color', 'presentor' ),
				'section' => 'footer_styles',
				'default' => '#293138',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'footer_widget_area_visibility' => array(
				'title'   => esc_html__( 'Show Footer Widgets Area', 'presentor' ),
				'section' => 'footer_styles',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'footer_widget_columns' => array(
				'title'           => esc_html__( 'Widget Area Columns', 'presentor' ),
				'section'         => 'footer_styles',
				'default'         => '4',
				'field'           => 'select',
				'choices'         => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_area_enable',
			),
			'footer_widgets_bg' => array(
				'title'           => esc_html__( 'Footer Widgets Area Background color', 'presentor' ),
				'section'         => 'footer_styles',
				'default'         => '#303941',
				'field'           => 'hex_color',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_area_enable',
			),
			'footer_menu_visibility' => array(
				'title'   => esc_html__( 'Show Footer Menu', 'presentor' ),
				'section' => 'footer_styles',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Footer contact block` section */
			'footer_contact_block' => array(
				'title'    => esc_html__( 'Footer Contact Block', 'presentor' ),
				'priority' => 10,
				'panel'    => 'footer_options',
				'type'     => 'section',
			),
			'footer_contact_block_visibility' => array(
				'title'   => esc_html__( 'Show Footer Contact Block', 'presentor' ),
				'section' => 'footer_contact_block',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'footer_contact_icon_1' => array(
				'title'           => esc_html__( 'Contact item 1', 'presentor' ),
				'description'     => esc_html__( 'Choose icon', 'presentor' ),
				'section'         => 'footer_contact_block',
				'field'           => 'iconpicker',
				'default'         => '',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_label_1' => array(
				'title'           => '',
				'description'     => esc_html__( 'Label', 'presentor' ),
				'section'         => 'footer_contact_block',
				'default'         => esc_html__( 'Address:', 'presentor' ),
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_text_1' => array(
				'title'           => '',
				'description'     => esc_html__( 'Value (HTML formatting support)', 'presentor' ),
				'section'         => 'footer_contact_block',
				'default'         => presentor_get_default_contact_information( 'address' ),
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_icon_2' => array(
				'title'           => esc_html__( 'Contact item 2', 'presentor' ),
				'description'     => esc_html__( 'Choose icon', 'presentor' ),
				'section'         => 'footer_contact_block',
				'field'           => 'iconpicker',
				'default'         => '',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_label_2' => array(
				'title'           => '',
				'description'     => esc_html__( 'Label', 'presentor' ),
				'section'         => 'footer_contact_block',
				'default'         => esc_html__( 'E-mail:', 'presentor' ),
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_text_2' => array(
				'title'           => '',
				'description'     => esc_html__( 'Value (HTML formatting support)', 'presentor' ),
				'section'         => 'footer_contact_block',
				'default'         => presentor_get_default_contact_information( 'email' ),
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_icon_3' => array(
				'title'           => esc_html__( 'Contact item 3', 'presentor' ),
				'description'     => esc_html__( 'Choose icon', 'presentor' ),
				'section'         => 'footer_contact_block',
				'field'           => 'iconpicker',
				'default'         => '',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_label_3' => array(
				'title'           => '',
				'description'     => esc_html__( 'Label', 'presentor' ),
				'section'         => 'footer_contact_block',
				'default'         => esc_html__( 'Phone:', 'presentor' ),
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),
			'footer_contact_text_3' => array(
				'title'           => '',
				'description'     => esc_html__( 'Value (HTML formatting support)', 'presentor' ),
				'section'         => 'footer_contact_block',
				'default'         => presentor_get_default_contact_information( 'phones' ),
				'field'           => 'textarea',
				'type'            => 'control',
				'active_callback' => 'presentor_is_footer_contact_block_enable',
			),

			/** `Blog Settings` panel */
			'blog_settings' => array(
				'title'    => esc_html__( 'Blog Settings', 'presentor' ),
				'priority' => 115,
				'type'     => 'panel',
			),

			/** `Blog` section */
			'blog' => array(
				'title'           => esc_html__( 'Blog', 'presentor' ),
				'panel'           => 'blog_settings',
				'priority'        => 10,
				'type'            => 'section',
				'active_callback' => 'is_home',
			),
			'blog_layout_type' => array(
				'title'   => esc_html__( 'Layout', 'presentor' ),
				'section' => 'blog',
				'default' => 'default',
				'field'   => 'select',
				'choices' => array(
					'default'          => esc_html__( 'Listing', 'presentor' ),
					'grid'             => esc_html__( 'Grid', 'presentor' ),
					'masonry'          => esc_html__( 'Masonry', 'presentor' ),
					'vertical-justify' => esc_html__( 'Vertical Justify', 'presentor' ),
				),
				'type' => 'control',
			),
			'blog_layout_columns' => array(
				'title'           => esc_html__( 'Columns', 'presentor' ),
				'section'         => 'blog',
				'default'         => '2-cols',
				'field'           => 'select',
				'choices'         => array(
					'2-cols' => esc_html__( '2 columns', 'presentor' ),
					'3-cols' => esc_html__( '3 columns', 'presentor' ),
					'4-cols' => esc_html__( '4 columns', 'presentor' ),
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_blog_layout_type_grid_masonry',
			),
			'blog_sticky_type' => array(
				'title'   => esc_html__( 'Sticky label type', 'presentor' ),
				'section' => 'blog',
				'default' => 'icon',
				'field'   => 'select',
				'choices' => array(
					'label' => esc_html__( 'Text Label', 'presentor' ),
					'icon'  => esc_html__( 'Font Icon', 'presentor' ),
					'both'  => esc_html__( 'Text with Icon', 'presentor' ),
				),
				'type' => 'control',
			),
			'blog_sticky_icon' => array(
				'title'           => esc_html__( 'Icon for sticky post', 'presentor' ),
				'section'         => 'blog',
				'field'           => 'iconpicker',
				'default'         => 'fa-star-o',
				'icon_data'       => presentor_get_fa_icons_data(),
				'type'            => 'control',
				'transport'       => 'postMessage',
				'active_callback' => 'presentor_is_sticky_icon',
			),
			'blog_sticky_label' => array(
				'title'           => esc_html__( 'Featured Post Label', 'presentor' ),
				'description'     => esc_html__( 'Label for sticky post', 'presentor' ),
				'section'         => 'blog',
				'default'         => esc_html__( 'Featured', 'presentor' ),
				'field'           => 'text',
				'active_callback' => 'presentor_is_sticky_text',
				'type'            => 'control',
				'transport'       => 'postMessage',
			),
			'blog_featured_image' => array(
				'title'           => esc_html__( 'Featured image', 'presentor' ),
				'section'         => 'blog',
				'default'         => 'fullwidth',
				'field'           => 'select',
				'choices'         => array(
					'small'     => esc_html__( 'Small', 'presentor' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'presentor' ),
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_blog_featured_image',
			),
			'blog_posts_content' => array(
				'title'   => esc_html__( 'Post content', 'presentor' ),
				'section' => 'blog',
				'default' => 'excerpt',
				'field'   => 'select',
				'choices' => array(
					'excerpt' => esc_html__( 'Only excerpt', 'presentor' ),
					'full'    => esc_html__( 'Full content', 'presentor' ),
					'none'    => esc_html__( 'Hide', 'presentor' ),
				),
				'type' => 'control',
			),
			'blog_posts_content_length' => array(
				'title'           => esc_html__( 'Number of words in the excerpt', 'presentor' ),
				'section'         => 'blog',
				'default'         => '30',
				'field'           => 'number',
				'input_attrs'     => array(
					'min'  => 1,
					'max'  => 100,
					'step' => 1,
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_blog_posts_content_type_excerpt',
			),
			'blog_read_more_btn' => array(
				'title'   => esc_html__( 'Show Read More button', 'presentor' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_read_more_text' => array(
				'title'           => esc_html__( 'Read More button text', 'presentor' ),
				'section'         => 'blog',
				'default'         => esc_html__( 'Read more', 'presentor' ),
				'field'           => 'text',
				'type'            => 'control',
				'transport'       => 'postMessage',
				'active_callback' => 'presentor_is_blog_read_more_btn_enable',
			),
			'blog_post_author' => array(
				'title'   => esc_html__( 'Show post author', 'presentor' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_publish_date' => array(
				'title'   => esc_html__( 'Show publish date', 'presentor' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_categories' => array(
				'title'   => esc_html__( 'Show categories', 'presentor' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_tags' => array(
				'title'   => esc_html__( 'Show tags', 'presentor' ),
				'section' => 'blog',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_comments' => array(
				'title'   => esc_html__( 'Show comments', 'presentor' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_trend_views' => array(
				'title'   => esc_html__( 'Show views counter', 'presentor' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
				'active_callback' => 'presentor_is_cherry_trending_posts_activated',
			),

			/** `Post` section */
			'blog_post' => array(
				'title'           => esc_html__( 'Post', 'presentor' ),
				'panel'           => 'blog_settings',
				'priority'        => 20,
				'type'            => 'section',
				'active_callback' => 'callback_single',
			),
			'single_post_author' => array(
				'title'   => esc_html__( 'Show post author', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_publish_date' => array(
				'title'   => esc_html__( 'Show publish date', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_categories' => array(
				'title'   => esc_html__( 'Show categories', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_tags' => array(
				'title'   => esc_html__( 'Show tags', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_comments' => array(
				'title'   => esc_html__( 'Show comments', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_trend_views' => array(
				'title'   => esc_html__( 'Show views counter', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
				'active_callback' => 'presentor_is_cherry_trending_posts_activated',
			),
			'single_post_trend_rating' => array(
				'title'   => esc_html__( 'Show rating', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
				'active_callback' => 'presentor_is_cherry_trending_posts_activated',
			),
			'single_author_block' => array(
				'title'   => esc_html__( 'Enable the author block after each post', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_navigation' => array(
				'title'   => esc_html__( 'Enable post navigation', 'presentor' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Related Posts` section */
			'related_posts' => array(
				'title'           => esc_html__( 'Related posts block', 'presentor' ),
				'panel'           => 'blog_settings',
				'priority'        => 30,
				'type'            => 'section',
				'active_callback' => 'callback_single',
			),
			'related_posts_visible' => array(
				'title'   => esc_html__( 'Show related posts block', 'presentor' ),
				'section' => 'related_posts',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'related_posts_block_title' => array(
				'title'           => esc_html__( 'Related posts block title', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => esc_html__( 'Related posts', 'presentor' ),
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
				'transport'       => 'postMessage',
			),
			'related_posts_count' => array(
				'title'           => esc_html__( 'Number of post', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => '2',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_grid' => array(
				'title'           => esc_html__( 'Layout', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => '2',
				'field'           => 'select',
				'choices'         => array(
					'2' => esc_html__( '2 columns', 'presentor' ),
					'3' => esc_html__( '3 columns', 'presentor' ),
					'4' => esc_html__( '4 columns', 'presentor' ),
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_title' => array(
				'title'           => esc_html__( 'Show post title', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_title_length' => array(
				'title'           => esc_html__( 'Number of words in the title', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => '10',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_image' => array(
				'title'           => esc_html__( 'Show post image', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_content' => array(
				'title'           => esc_html__( 'Display content', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => 'hide',
				'field'           => 'select',
				'choices'         => array(
					'hide'         => esc_html__( 'Hide', 'presentor' ),
					'post_excerpt' => esc_html__( 'Excerpt', 'presentor' ),
					'post_content' => esc_html__( 'Content', 'presentor' ),
				),
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_content_length' => array(
				'title'           => esc_html__( 'Number of words in the content', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => '10',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_categories' => array(
				'title'           => esc_html__( 'Show post categories', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_tags' => array(
				'title'           => esc_html__( 'Show post tags', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_author' => array(
				'title'           => esc_html__( 'Show post author', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_publish_date' => array(
				'title'           => esc_html__( 'Show post publish date', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),
			'related_posts_comment_count' => array(
				'title'           => esc_html__( 'Show post comment count', 'presentor' ),
				'section'         => 'related_posts',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'presentor_is_related_posts_enable',
			),

			/** `Woocommerce Settings` panel */
			'woocommerce_settings' => array(
				'title'           => esc_html__( 'Woocommerce options', 'presentor' ),
				'priority'        => 120,
				'type'            => 'panel',
				'active_callback' => 'presentor_is_woocommerce_activated',
			),
			/** `Badge` section */
			'woo_badge_options' => array(
				'title'    => esc_html__( 'Woocommerce badge', 'presentor' ),
				'panel'    => 'woocommerce_settings',
				'priority' => 10,
				'type'     => 'section',
			),
			'onsale_badge_bg' => array(
				'title'   => esc_html__( 'Onsale badge background', 'presentor' ),
				'section' => 'woo_badge_options',
				'default' => '#ff3a4c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'featured_badge_bg' => array(
				'title'   => esc_html__( 'Featured badge background', 'presentor' ),
				'section' => 'woo_badge_options',
				'default' => '#fcfa06',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'new_badge_bg' => array(
				'title'   => esc_html__( 'New badge background', 'presentor' ),
				'section' => 'woo_badge_options',
				'default' => '#04e2f6',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `404` panel */
			'page_404_options' => array(
				'title'    => esc_html__( '404 Page Style', 'presentor' ),
				'priority' => 130,
				'type'     => 'section',
			),
			'page_404_bg_color' => array(
				'title'     => esc_html__( 'Background Color', 'presentor' ),
				'section'   => 'page_404_options',
				'field'     => 'hex_color',
				'default'   => '#000000',
				'type'      => 'control',
			),
			'page_404_bg_image' => array(
				'title'   => esc_html__( 'Background Image', 'presentor' ),
				'section' => 'page_404_options',
				'field'   => 'image',
				'default' => '%s/assets/images/bg_404.jpg',
				'type'    => 'control',
			),
			'page_404_bg_repeat' => array(
				'title'   => esc_html__( 'Background Repeat', 'presentor' ),
				'section' => 'page_404_options',
				'default' => 'no-repeat',
				'field'   => 'select',
				'choices' => presentor_get_bg_repeat(),
				'type' => 'control',
			),
			'page_404_bg_position' => array(
				'title'   => esc_html__( 'Background Position', 'presentor' ),
				'section' => 'page_404_options',
				'default' => 'center',
				'field'   => 'select',
				'choices' => presentor_get_bg_position(),
				'type' => 'control',
			),
			'page_404_bg_size' => array(
				'title'   => esc_html__( 'Background Size', 'presentor' ),
				'section' => 'page_404_options',
				'default' => 'cover',
				'field'   => 'select',
				'choices' => presentor_get_bg_size(),
				'type' => 'control',
			),
			'page_404_bg_attachment' => array(
				'title'   => esc_html__( 'Background Attachment', 'presentor' ),
				'section' => 'page_404_options',
				'default' => 'scroll',
				'field'   => 'select',
				'choices' => presentor_get_bg_attachment(),
				'type' => 'control',
			),
			'page_404_text_color' => array(
				'title'       => esc_html__( 'Text Color', 'presentor' ),
				'description' => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'presentor' ),
				'section'     => 'page_404_options',
				'default'     => 'light',
				'field'       => 'select',
				'choices'     => presentor_get_text_color(),
				'type'        => 'control',
			),
			'page_404_btn_style_preset' => array(
				'title'   => esc_html__( 'Button Style Preset', 'presentor' ),
				'section' => 'page_404_options',
				'default' => 'primary',
				'field'   => 'select',
				'choices' => presentor_get_btn_style_presets(),
				'type'    => 'control',
			),
		),
	) );
}

/**
 * Return true if setting is value. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @param  string $setting Setting name to check.
 * @param  string $value   Setting value to compare.
 * @return bool
 */
function presentor_is_setting( $control, $setting, $value ) {

	if ( $value == $control->manager->get_setting( $setting )->value() ) {
		return true;
	}

	return false;
}

/**
 * Return true if value of passed setting is not equal with passed value.
 *
 * @param  object $control Parent control.
 * @param  string $setting Setting name to check.
 * @param  string $value   Setting value to compare.
 * @return bool
 */
function presentor_is_not_setting( $control, $setting, $value ) {

	if ( $value !== $control->manager->get_setting( $setting )->value() ) {
		return true;
	}

	return false;
}

/**
 * Return true if logo in header has image type. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_header_logo_image( $control ) {
	return presentor_is_setting( $control, 'header_logo_type', 'image' );
}

/**
 * Return true if logo in header has text type. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_header_logo_text( $control ) {
	return presentor_is_setting( $control, 'header_logo_type', 'text' );
}

/**
 * Return blog-featured-image true if blog layout type is default. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_blog_featured_image( $control ) {
	return presentor_is_setting( $control, 'blog_layout_type', 'default' );
}

/**
 * Return true if sticky label type set to text or text with icon.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_sticky_text( $control ) {
	return presentor_is_not_setting( $control, 'blog_sticky_type', 'icon' );
}

/**
 * Return true if sticky label type set to icon or text with icon.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_sticky_icon( $control ) {
	return presentor_is_not_setting( $control, 'blog_sticky_type', 'label' );
}

/**
 * Return true if option Show Header Contact Block is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_header_contact_block_enable( $control ) {
	return presentor_is_setting( $control, 'header_contact_block_visibility', true );
}

/**
 * Return true if option Show Footer Contact Block is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_footer_contact_block_enable( $control ) {
	return presentor_is_setting( $control, 'footer_contact_block_visibility', true );
}

/**
 * Return true if option Show Related Posts Block is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_related_posts_enable( $control ) {
	return presentor_is_setting( $control, 'related_posts_visible', true );
}

/**
 * Return true if option Enable Top Panel is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_top_panel_enable( $control ) {
	return presentor_is_setting( $control, 'top_panel_visibility', true );
}

/**
 * Return true if option Show Footer Logo is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_footer_logo_enable( $control ) {
	return presentor_is_setting( $control, 'footer_logo_visibility', true );
}

/**
 * Return true if option Show Footer Widgets Area is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_footer_area_enable( $control ) {
	return presentor_is_setting( $control, 'footer_widget_area_visibility', true );
}

/**
 * Return true if option Blog posts content is excerpt. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_blog_posts_content_type_excerpt( $control ) {
	return presentor_is_setting( $control, 'blog_posts_content', 'excerpt' );
}

/**
 * Return true if option Show Read More button is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_blog_read_more_btn_enable( $control ) {
	return presentor_is_setting( $control, 'blog_read_more_btn', true );
}

/**
 * Return true if Blog layout selected Grid or Masonry. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_blog_layout_type_grid_masonry( $control ) {
	if ( in_array( $control->manager->get_setting( 'blog_layout_type' )->value(), array( 'grid', 'masonry' ) ) ) {
		return true;
	}

	return false;
}

/**
 * Return true if option Show header call to action button is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_header_btn_enable( $control ) {
	return presentor_is_setting( $control, 'header_btn_visibility', true );
}

/**
 * Return true if option Show Page Preloader is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_page_preloader_enable( $control ) {
	return presentor_is_setting( $control, 'page_preloader', true );
}

/**
 * Return true if option Page type is boxed. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function presentor_is_page_type_boxed( $control ) {
	return presentor_is_setting( $control, 'page_container_type', 'boxed' );
}

/**
 * Get default header layouts.
 *
 * @since  1.0.0
 * @return array
 */
function presentor_get_header_layout_options() {
	return apply_filters( 'presentor_header_layout_options', array(
		'style-1' => esc_html__( 'Style 1', 'presentor' ),
		'style-2' => esc_html__( 'Style 2', 'presentor' ),
		'style-3' => esc_html__( 'Style 3', 'presentor' ),
		'style-4' => esc_html__( 'Style 4', 'presentor' ),
	) );
}

/**
 * Get default footer layouts.
 *
 * @since  1.0.0
 * @return array
 */
function presentor_get_footer_layout_options() {
	return apply_filters( 'presentor_footer_layout_options', array(
		'style-1' => esc_html__( 'Style 1', 'presentor' ),
		'style-2' => esc_html__( 'Style 2', 'presentor' ),
	) );
}

/**
 * Get default header layouts options for Post Meta boxes
 *
 * @return array
 */
function presentor_get_header_layout_pm_options() {
	$inherit_option = array(
		'inherit' => esc_html__( 'Inherit', 'presentor' ),
	);

	$options = presentor_get_header_layout_options();

	return array_merge( $inherit_option, $options );
}

/**
 * Get default footer layouts options for Post Meta boxes
 *
 * @return array
 */
function presentor_get_footer_layout_pm_options() {
	$inherit_option = array(
		'inherit' => esc_html__( 'Inherit', 'presentor' ),
	);

	$options = presentor_get_footer_layout_options();

	return array_merge( $inherit_option, $options );
}

// Change native customizer control (based on WordPress core).
add_action( 'customize_register', 'presentor_customizer_change_core_controls', 20 );

// Bind JS handlers to instantly live-preview changes.
add_action( 'customize_preview_init', 'presentor_customize_preview_js' );

/**
 * Change native customize control (based on WordPress core).
 *
 * @since 1.0.0
 * @param  object $wp_customize Object wp_customize.
 * @return void
 */
function presentor_customizer_change_core_controls( $wp_customize ) {
	$wp_customize->get_control( 'site_icon' )->section         = 'presentor_logo_favicon';
	$wp_customize->get_section( 'background_image' )->priority = 45;
	$wp_customize->get_control( 'background_color' )->label    = esc_html__( 'Body Background Color', 'presentor' );

	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function presentor_customize_preview_js() {
	wp_enqueue_script( 'presentor-customize-preview', PRESENTOR_THEME_JS . '/customize-preview.js', array( 'customize-preview' ), '1.0', true );
}

// Set customize selective refresh.
add_action( 'customize_register', 'presentor_customizer_selective_refresh', 20 );

/**
 * Set customize selective refresh.
 *
 * @since 1.0.0
 * @param  object $wp_customize Object wp_customize.
 * @return void
 */
function presentor_customizer_selective_refresh( $wp_customize ) {}

// Typography utility function
/**
 * Get font styles
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_font_styles() {
	return apply_filters( 'presentor_get_font_styles', array(
		'normal'  => esc_html__( 'Normal', 'presentor' ),
		'italic'  => esc_html__( 'Italic', 'presentor' ),
		'oblique' => esc_html__( 'Oblique', 'presentor' ),
		'inherit' => esc_html__( 'Inherit', 'presentor' ),
	) );
}

/**
 * Get character sets
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_character_sets() {
	return apply_filters( 'presentor_get_character_sets', array(
		'latin'        => esc_html__( 'Latin', 'presentor' ),
		'greek'        => esc_html__( 'Greek', 'presentor' ),
		'greek-ext'    => esc_html__( 'Greek Extended', 'presentor' ),
		'vietnamese'   => esc_html__( 'Vietnamese', 'presentor' ),
		'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'presentor' ),
		'latin-ext'    => esc_html__( 'Latin Extended', 'presentor' ),
		'cyrillic'     => esc_html__( 'Cyrillic', 'presentor' ),
	) );
}

/**
 * Get text aligns
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_text_aligns() {
	return apply_filters( 'presentor_get_text_aligns', array(
		'inherit' => esc_html__( 'Inherit', 'presentor' ),
		'center'  => esc_html__( 'Center', 'presentor' ),
		'justify' => esc_html__( 'Justify', 'presentor' ),
		'left'    => esc_html__( 'Left', 'presentor' ),
		'right'   => esc_html__( 'Right', 'presentor' ),
	) );
}

/**
 * Get font weights
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_font_weight() {
	return apply_filters( 'presentor_get_font_weight', array(
		'100' => '100',
		'200' => '200',
		'300' => '300',
		'400' => '400',
		'500' => '500',
		'600' => '600',
		'700' => '700',
		'800' => '800',
		'900' => '900',
	) );
}

/**
 * Get tesx transform.
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_text_transform() {
	return apply_filters( 'presentor_get_text_transform', array(
		'none'       => esc_html__( 'None ', 'presentor' ),
		'uppercase'  => esc_html__( 'Uppercase ', 'presentor' ),
		'lowercase'  => esc_html__( 'Lowercase', 'presentor' ),
		'capitalize' => esc_html__( 'Capitalize', 'presentor' ),
	) );
}

// Background utility function
/**
 * Get background position
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_bg_position() {
	return apply_filters( 'presentor_get_bg_position', array(
		'top-left'      => esc_html__( 'Top Left', 'presentor' ),
		'top-center'    => esc_html__( 'Top Center', 'presentor' ),
		'top-right'     => esc_html__( 'Top Right', 'presentor' ),
		'center-left'   => esc_html__( 'Middle Left', 'presentor' ),
		'center'        => esc_html__( 'Middle Center', 'presentor' ),
		'center-right'  => esc_html__( 'Middle Right', 'presentor' ),
		'bottom-left'   => esc_html__( 'Bottom Left', 'presentor' ),
		'bottom-center' => esc_html__( 'Bottom Center', 'presentor' ),
		'bottom-right'  => esc_html__( 'Bottom Right', 'presentor' ),
	) );
}

/**
 * Get background size
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_bg_size() {
	return apply_filters( 'presentor_get_bg_size', array(
		'auto'    => esc_html__( 'Auto', 'presentor' ),
		'cover'   => esc_html__( 'Cover', 'presentor' ),
		'contain' => esc_html__( 'Contain', 'presentor' ),
	) );
}

/**
 * Get background repeat
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_bg_repeat() {
	return apply_filters( 'presentor_get_bg_repeat', array(
		'no-repeat' => esc_html__( 'No Repeat', 'presentor' ),
		'repeat'    => esc_html__( 'Tile', 'presentor' ),
		'repeat-x'  => esc_html__( 'Tile Horizontally', 'presentor' ),
		'repeat-y'  => esc_html__( 'Tile Vertically', 'presentor' ),
	) );
}

/**
 * Get background attachment
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_bg_attachment() {
	return apply_filters( 'presentor_get_bg_attachment', array(
		'scroll' => esc_html__( 'Scroll', 'presentor' ),
		'fixed'  => esc_html__( 'Fixed', 'presentor' ),
	) );
}

/**
 * Get text color
 *
 * @since 1.0.0
 * @return array
 */
function presentor_get_text_color() {
	return apply_filters( 'presentor_get_text_color', array(
		'light' => esc_html__( 'Light', 'presentor' ),
		'dark'  => esc_html__( 'Dark', 'presentor' ),
	) );
}

/**
 * Return array of arguments for dynamic CSS module
 *
 * @return array
 */
function presentor_get_dynamic_css_options() {
	return apply_filters( 'presentor_get_dynamic_css_options', array(
		'prefix'        => 'presentor',
		'type'          => 'theme_mod',
		'parent_handle' => 'presentor-theme-style',
		'single'        => true,
		'css_files'     => array(
			presentor_get_locate_template( 'assets/css/dynamic/dynamic.css' ),

			presentor_get_locate_template( 'assets/css/dynamic/site/elements.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/header.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/forms.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/social.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/menus.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/post.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/navigation.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/footer.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/misc.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/site/buttons.css' ),

			presentor_get_locate_template( 'assets/css/dynamic/widgets/widget-default.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/widgets/subscribe.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/widgets/custom-posts.css' ),
			presentor_get_locate_template( 'assets/css/dynamic/widgets/contact-information.css' ),
		),
		'options' => array(
			'header_logo_font_style',
			'header_logo_font_weight',
			'header_logo_font_size',
			'header_logo_line_height',
			'header_logo_font_family',
			'header_logo_letter_spacing',
			'header_logo_text_transform',

			'body_font_style',
			'body_font_weight',
			'body_font_size',
			'body_line_height',
			'body_font_family',
			'body_letter_spacing',
			'body_text_align',
			'body_text_transform',

			'h1_font_style',
			'h1_font_weight',
			'h1_font_size',
			'h1_line_height',
			'h1_font_family',
			'h1_letter_spacing',
			'h1_text_align',
			'h1_text_transform',

			'h2_font_style',
			'h2_font_weight',
			'h2_font_size',
			'h2_line_height',
			'h2_font_family',
			'h2_letter_spacing',
			'h2_text_align',
			'h2_text_transform',

			'h3_font_style',
			'h3_font_weight',
			'h3_font_size',
			'h3_line_height',
			'h3_font_family',
			'h3_letter_spacing',
			'h3_text_align',
			'h3_text_transform',

			'h4_font_style',
			'h4_font_weight',
			'h4_font_size',
			'h4_line_height',
			'h4_font_family',
			'h4_letter_spacing',
			'h4_text_align',
			'h4_text_transform',

			'h5_font_style',
			'h5_font_weight',
			'h5_font_size',
			'h5_line_height',
			'h5_font_family',
			'h5_letter_spacing',
			'h5_text_align',
			'h5_text_transform',

			'h6_font_style',
			'h6_font_weight',
			'h6_font_size',
			'h6_line_height',
			'h6_font_family',
			'h6_letter_spacing',
			'h6_text_align',
			'h6_text_transform',

			'breadcrumbs_font_style',
			'breadcrumbs_font_weight',
			'breadcrumbs_font_size',
			'breadcrumbs_line_height',
			'breadcrumbs_font_family',
			'breadcrumbs_letter_spacing',
			'breadcrumbs_text_transform',

			'breadcrumbs_bg_color',
			'breadcrumbs_bg_image',
			'breadcrumbs_bg_repeat',
			'breadcrumbs_bg_position',
			'breadcrumbs_bg_size',
			'breadcrumbs_bg_attachment',
			'breadcrumbs_padding_y',
			'breadcrumbs_padding_y_tablet',
			'breadcrumbs_padding_y_mobile',

			'meta_font_style',
			'meta_font_weight',
			'meta_font_size',
			'meta_line_height',
			'meta_font_family',
			'meta_letter_spacing',
			'meta_text_transform',

			'accent_font_style',
			'accent_font_weight',
			'accent_font_family',
			'accent_letter_spacing',
			'accent_text_transform',

			'regular_accent_color_1',
			'regular_accent_color_2',
			'regular_text_color',
			'regular_link_color',
			'regular_link_hover_color',
			'regular_h1_color',
			'regular_h2_color',
			'regular_h3_color',
			'regular_h4_color',
			'regular_h5_color',
			'regular_h6_color',

			'invert_accent_color_1',
			'invert_text_color',
			'invert_link_color',
			'invert_link_hover_color',
			'invert_h1_color',
			'invert_h2_color',
			'invert_h3_color',
			'invert_h4_color',
			'invert_h5_color',
			'invert_h6_color',

			'grey_color_1',

			'page_bg_color',

			'header_bg_color',
			'header_bg_image',
			'header_bg_repeat',
			'header_bg_position',
			'header_bg_attachment',
			'header_bg_size',

			'header_transparent_bg',
			'header_transparent_bg_alpha',

			'page_404_bg_color',
			'page_404_bg_image',
			'page_404_bg_repeat',
			'page_404_bg_position',
			'page_404_bg_attachment',
			'page_404_bg_size',

			'top_panel_bg',
			'page_preloader_bg',

			'page_boxed_width',
			'container_width',

			'footer_widgets_bg',
			'footer_bg',

			'onsale_badge_bg',
			'featured_badge_bg',
			'new_badge_bg',
		),
	) );
}

/**
 * Return array of arguments for Google Font loader module.
 *
 * @since  1.0.0
 * @return array
 */
function presentor_get_fonts_options() {
	return apply_filters( 'presentor_get_fonts_options', array(
		'prefix'  => 'presentor',
		'type'    => 'theme_mod',
		'single'  => true,
		'options' => array(
			'body' => array(
				'family'  => 'body_font_family',
				'style'   => 'body_font_style',
				'weight'  => 'body_font_weight',
				'charset' => 'body_character_set',
			),
			'h1' => array(
				'family'  => 'h1_font_family',
				'style'   => 'h1_font_style',
				'weight'  => 'h1_font_weight',
				'charset' => 'h1_character_set',
			),
			'h2' => array(
				'family'  => 'h2_font_family',
				'style'   => 'h2_font_style',
				'weight'  => 'h2_font_weight',
				'charset' => 'h2_character_set',
			),
			'h3' => array(
				'family'  => 'h3_font_family',
				'style'   => 'h3_font_style',
				'weight'  => 'h3_font_weight',
				'charset' => 'h3_character_set',
			),
			'h4' => array(
				'family'  => 'h4_font_family',
				'style'   => 'h4_font_style',
				'weight'  => 'h4_font_weight',
				'charset' => 'h4_character_set',
			),
			'h5' => array(
				'family'  => 'h5_font_family',
				'style'   => 'h5_font_style',
				'weight'  => 'h5_font_weight',
				'charset' => 'h5_character_set',
			),
			'h6' => array(
				'family'  => 'h6_font_family',
				'style'   => 'h6_font_style',
				'weight'  => 'h6_font_weight',
				'charset' => 'h6_character_set',
			),
			'meta' => array(
				'family'  => 'meta_font_family',
				'style'   => 'meta_font_style',
				'weight'  => 'meta_font_weight',
				'charset' => 'meta_character_set',
			),
			'header_logo' => array(
				'family'  => 'header_logo_font_family',
				'style'   => 'header_logo_font_style',
				'weight'  => 'header_logo_font_weight',
				'charset' => 'header_logo_character_set',
			),
			'accent' => array(
				'family'  => 'accent_font_family',
				'style'   => 'accent_font_style',
				'weight'  => 'accent_font_weight',
				'charset' => 'accent_character_set',
			),
			'breadcrumbs' => array(
				'family'  => 'breadcrumbs_font_family',
				'style'   => 'breadcrumbs_font_style',
				'weight'  => 'breadcrumbs_font_weight',
				'charset' => 'breadcrumbs_character_set',
			),
		),
	) );
}

/**
 * Get default footer copyright.
 *
 * @since  1.0.0
 * @return string
 */
function presentor_get_default_footer_copyright() {
	return esc_html__( '&copy; %%year%% %%site-name%%. All rights reserved.', 'presentor' );
}

/**
 * Get default contact information.
 *
 * @since  1.0.0
 * @return string
 */
function presentor_get_default_contact_information( $value ) {
	$contact_information = array(
		'address'   => esc_html__( '4096 N Highland St, Arlington VA 32101, USA', 'presentor' ),
		'address-2' => esc_html__( '121 WallStreet Street, NY York, USA', 'presentor' ),
		'phones'    => sprintf( '<a href="tel:#">%1$s</a>', esc_html__( '800 1234 56 78', 'presentor' ) ),
		'email'     => sprintf( '<a href="mailto:%1$s">%1$s</a>', esc_html__( 'info@company.com', 'presentor' ) ),
	);

	return $contact_information[ $value ];
}

/**
 * Get FontAwesome icons set
 *
 * @return array
 */
function presentor_get_icons_set() {

	static $font_icons;

	if ( ! $font_icons ) {

		ob_start();

		include PRESENTOR_THEME_DIR . '/assets/js/icons.json';
		$json = ob_get_clean();

		$font_icons = array();
		$icons      = json_decode( $json, true );

		foreach ( $icons['icons'] as $icon ) {
			$font_icons[] = $icon['id'];
		}
	}

	return $font_icons;
}

function presentor_get_fa_icons_data() {
	return apply_filters( 'presentor_get_fa_icons_data', array(
		'icon_set'    => 'presentorFontAwesome',
		'icon_css'    => PRESENTOR_THEME_URI . '/assets/css/font-awesome.min.css',
		'icon_base'   => 'fa',
		'icon_prefix' => 'fa-',
		'icons'       => presentor_get_icons_set(),
	) );
}

/**
 * Get iconsmind icons set.
 *
 * @return array
 */
function presentor_get_iconsmind_icons_set() {

	static $iconsmind_icons;

	if ( ! $iconsmind_icons ) {
		ob_start();

		include PRESENTOR_THEME_DIR . '/assets/css/iconsmind.min.css';

		$result = ob_get_clean();

		preg_match_all( '/\.([-_a-zA-Z0-9]+):before[, {]/', $result, $matches );

		if ( ! is_array( $matches ) || empty( $matches[1] ) ) {
			return;
		}

		$iconsmind_icons = $matches[1];
	}

	return $iconsmind_icons;
}

/**
 * Get iconsmind icons data for iconpicker control.
 *
 * @return array
 */
function presentor_get_iconsmind_icons_data() {
	return apply_filters( 'presentor_get_iconsmind_icons_data', array(
		'icon_set'    => 'presentorIconsmindIcons',
		'icon_css'    => PRESENTOR_THEME_URI . '/assets/css/iconsmind.min.css',
		'icon_base'   => 'iconsmind',
		'icon_prefix' => '',
		'icons'       => presentor_get_iconsmind_icons_set(),
	) );
}

/**
 * Get header button style presets.
 *
 * @return array
 */
function presentor_get_btn_style_presets() {
	return apply_filters( 'presentor_get_btn_style_presets', array(
		'primary'             => esc_html__( 'Primary', 'presentor' ),
		'secondary'           => esc_html__( 'Secondary', 'presentor' ),
		'primary-transparent' => esc_html__( 'Primary Transparent', 'presentor' ),
		'grey'                => esc_html__( 'Grey', 'presentor' ),
	) );
}
