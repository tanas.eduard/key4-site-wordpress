<?php
/**
 * Menu Template Functions.
 *
 * @package Presentor
 */

/**
 * Get main menu.
 *
 * @since  1.0.0
 * @return string
 */
function presentor_get_main_menu() {
	$args = apply_filters( 'presentor_main_menu_args', array(
		'theme_location'   => 'main',
		'container'        => '',
		'menu_id'          => 'main-menu',
		'echo'             => false,
		'fallback_cb'      => 'presentor_set_nav_menu',
		'fallback_message' => esc_html__( 'Set main menu', 'presentor' ),
	) );

	return wp_nav_menu( $args );
}

/**
 * Show main menu.
 * 
 * @since  1.0.0
 * @return void
 */
function presentor_main_menu() {

	$main_menu = presentor_get_main_menu();
	$menu_btn  = presentor_get_menu_toggle();

	printf( '<nav id="site-navigation" class="main-navigation" role="navigation">%2$s%1$s</nav><!-- #site-navigation -->', $main_menu, $menu_btn );
}

/**
 * Show footer menu.
 *
 * @since  1.0.0
 * @return void
 */
function presentor_footer_menu() {
	if ( ! get_theme_mod( 'footer_menu_visibility', presentor_theme()->customizer->get_default( 'footer_menu_visibility' ) ) ) {
		return;
	}

	$args = apply_filters( 'presentor_footer_menu_args', array(
		'theme_location'   => 'footer',
		'container'        => '',
		'menu_id'          => 'footer-menu-items',
		'menu_class'       => 'footer-menu__items',
		'depth'            => 1,
		'echo'             => false,
		'fallback_cb'      => 'presentor_set_nav_menu',
		'fallback_message' => esc_html__( 'Set footer menu', 'presentor' ),
	) );

	printf('<nav id="footer-navigation" class="footer-menu" role="navigation">%s</nav><!-- #footer-navigation -->', wp_nav_menu( $args ) );
}

/**
 * Show top page menu if active.
 *
 * @since  1.0.0
 * @return void
 */
function presentor_top_menu() {
	
	if ( ! presentor_is_top_menu_visible() ) {
		return;
	}

	wp_nav_menu( apply_filters( 'presentor_top_menu_args', array(
		'theme_location'  => 'top',
		'container'       => 'div',
		'container_class' => 'top-panel__menu',
		'menu_class'      => 'top-panel__menu-list inline-list',
		'depth'           => 1,
	) ) );
}

/**
 * Check visibility top menu.
 *
 * @return bool
 */
function presentor_is_top_menu_visible() {

	$is_visible = false;

	if ( has_nav_menu( 'top' ) && get_theme_mod( 'top_menu_visibility', presentor_theme()->customizer->get_default( 'top_menu_visibility' ) ) ) {
		$is_visible = true;
	}

	return $is_visible;
}

/**
 * Get social nav menu.
 *
 * @since  1.0.0
 * @since  1.0.0  Added new param - $item.
 * @since  1.0.1  Added arguments to the filter.
 * @param  string $context Current post context - 'single' or 'loop'.
 * @param  string $type    Content type - icon, text or both.
 * @return string
 */
function presentor_get_social_list( $context, $type = 'icon' ) {
	static $instance = 0;
	$instance++;

	$container_class = array( 'social-list' );

	if ( ! empty( $context ) ) {
		$container_class[] = sprintf( 'social-list--%s', sanitize_html_class( $context ) );
	}

	$container_class[] = sprintf( 'social-list--%s', sanitize_html_class( $type ) );

	$args = apply_filters( 'presentor_social_list_args', array(
		'theme_location'   => 'social',
		'container'        => 'div',
		'container_class'  => join( ' ', $container_class ),
		'menu_id'          => "social-list-{$instance}",
		'menu_class'       => 'social-list__items inline-list',
		'depth'            => 1,
		'link_before'      => ( 'icon' == $type ) ? '<span class="screen-reader-text">' : '',
		'link_after'       => ( 'icon' == $type ) ? '</span>' : '',
		'echo'             => false,
		'fallback_cb'      => 'presentor_set_nav_menu',
		'fallback_message' => esc_html__( 'Set social menu', 'presentor' ),
	), $context, $type );

	return wp_nav_menu( $args );
}

/**
 * Set fallback callback for nav menu.
 *
 * @param  array $args Nav menu arguments.
 * @return null|string
 */
function presentor_set_nav_menu( $args ) {

	if ( ! current_user_can( 'edit_theme_options' ) ) {
		return null;
	}

	$format = '<div class="set-menu %3$s"><a href="%2$s" target="_blank" class="set-menu_link">%1$s</a></div>';
	$label  = $args['fallback_message'];
	$url    = esc_url( admin_url( 'nav-menus.php' ) );

	return sprintf( $format, $label, $url, $args['container_class'] );
}

/**
 * Get menu button.
 *
 * @since  1.0.0
 *
 * @return string
 */
function presentor_get_menu_toggle() {
	$format = apply_filters(
		'presentor_menu_toggle_html',
		'<button class="menu-toggle" aria-expanded="false"><span class="menu-toggle-box"><span class="menu-toggle-inner"></span></span></button>'
	);

	return $format;
}
