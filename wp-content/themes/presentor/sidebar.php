<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Presentor
 */
$sidebar_position = get_theme_mod( 'sidebar_position', presentor_theme()->customizer->get_default( 'sidebar_position' ) );

if ( 'fullwidth' === $sidebar_position ) {
	return;
}

if ( is_active_sidebar( 'sidebar' ) && ! presentor_is_product_page() ) {
	do_action( 'presentor_render_widget_area', 'sidebar' );
}

if ( is_active_sidebar( 'shop-sidebar' ) && presentor_is_product_page() ) {
	do_action( 'presentor_render_widget_area', 'shop-sidebar' );
}
