<?php
/**
 * The base template.
 *
 * @package Presentor
 */
?>
<?php get_header( presentor_template_base() ); ?>

	<?php presentor_site_breadcrumbs(); ?>

	<?php do_action( 'presentor_render_widget_area', 'full-width-header-area' ); ?>

	<div <?php presentor_content_wrap_class(); ?>>

		<div class="row">

			<div id="primary" <?php presentor_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include presentor_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_sidebar(); // Loads the sidebar.php. ?>

		</div><!-- .row -->

	</div><!-- .site-content_wrap -->

	<?php do_action( 'presentor_render_widget_area', 'after-content-full-width-area' ); ?>

<?php get_footer( presentor_template_base() ); ?>
