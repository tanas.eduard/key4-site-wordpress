<?php
/**
 * Thumbnails configuration.
 *
 * @package Presentor
 */

add_action( 'after_setup_theme', 'presentor_register_image_sizes', 5 );
/**
 * Register image sizes.
 */
function presentor_register_image_sizes() {
	set_post_thumbnail_size( 370, 260, true );

	// Registers a new image sizes.
	add_image_size( 'presentor-thumb-l', 770, 460, true );   // default listing
	add_image_size( 'presentor-thumb-l-2', 770, 260, true ); // justify listing
	add_image_size( 'presentor-thumb-xl', 1170, 500, true ); // default listing + full-width

	add_image_size( 'presentor-thumb-masonry', 370, 9999 );      // masonry listing
	add_image_size( 'presentor-author-avatar', 512, 512, true ); // Widget Author bio

	add_image_size( 'presentor-thumb-72-62', 72, 62, true );     // Custom post widget
	add_image_size( 'presentor-thumb-170-125', 170, 125, true ); // Custom post widget
	add_image_size( 'presentor-thumb-250-222', 250, 222, true ); // Cherry Team
	add_image_size( 'presentor-thumb-266-250', 266, 250, true ); // Cherry Team
	add_image_size( 'presentor-thumb-270-200', 270, 200, true ); // Cherry Services List
	add_image_size( 'presentor-thumb-370-200', 370, 200, true ); // Cherry Services List
	add_image_size( 'presentor-thumb-370-300', 370, 300, true ); // Cherry Projects
	add_image_size( 'presentor-thumb-370-340', 370, 340, true ); // Cherry Team
	add_image_size( 'presentor-thumb-390-380', 390, 380, true ); // Cherry Team
	add_image_size( 'presentor-thumb-425-415', 425, 415, true ); // Cherry Projects
	add_image_size( 'presentor-thumb-770-750', 770, 750, true ); // Image widget

	add_image_size( 'presentor-thumb-wishlist', 60, 91, true ); // Wishlist
	add_image_size( 'presentor-thumb-listing-line-product', 420, 560, true ); // Woo
}
