<?php
/**
 * Menus configuration.
 *
 * @package Presentor
 */

add_action( 'after_setup_theme', 'presentor_register_menus', 5 );
/**
 * Register menus.
 */
function presentor_register_menus() {

	register_nav_menus( array(
		'top'    => esc_html__( 'Top', 'presentor' ),
		'main'   => esc_html__( 'Main', 'presentor' ),
		'footer' => esc_html__( 'Footer', 'presentor' ),
		'social' => esc_html__( 'Social', 'presentor' ),
	) );
}
