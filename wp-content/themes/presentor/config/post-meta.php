<?php
/**
 * Post meta configuration.
 *
 * @package Presentor
 */

/**
 * Get post meta settings.
 *
 * @return array
 */
function presentor_get_post_meta_settings() {

	$container_options = array(
		'inherit'   => array(
			'label'   => esc_html__( 'Inherit', 'presentor' ),
			'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/inherit.svg',
		),
		'boxed'     => array(
			'label'   => esc_html__( 'Boxed', 'presentor' ),
			'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/type-boxed.svg',
		),
		'fullwidth' => array(
			'label'   => esc_html__( 'Fullwidth', 'presentor' ),
			'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/type-fullwidth.svg',
		),
	);

	return apply_filters( 'presentor_get_post_meta_settings',  array(
		'id'            => 'page-settings',
		'title'         => esc_html__( 'Page settings', 'presentor' ),
		'page'          => array( 'post', 'page', 'team', 'projects', 'cherry-services' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'callback_args' => false,
		'fields'        => array(
			'tabs' => array(
				'element' => 'component',
				'type'    => 'component-tab-horizontal',
			),
			'layout_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Layout Options', 'presentor' ),
			),
			'header_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Header Style', 'presentor' ),
				'description' => esc_html__( 'Header style settings', 'presentor' ),
			),
			'header_elements_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Header Elements', 'presentor' ),
				'description' => esc_html__( 'Enable/Disable header elements', 'presentor' ),
			),
			'breadcrumbs_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Breadcrumbs', 'presentor' ),
				'description' => esc_html__( 'Breadcrumbs settings', 'presentor' ),
			),
			'footer_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Footer Settings', 'presentor' ),
				'description' => esc_html__( 'Footer settings', 'presentor' ),
			),
			'presentor_sidebar_position' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Sidebar layout', 'presentor' ),
				'description'   => esc_html__( 'Sidebar position global settings redefining. If you select inherit option, global setting will be applied for this layout', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => array(
					'inherit' => array(
						'label'   => esc_html__( 'Inherit', 'presentor' ),
						'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/inherit.svg',
					),
					'one-left-sidebar' => array(
						'label'   => esc_html__( 'Sidebar on left side', 'presentor' ),
						'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/page-layout-left-sidebar.svg',
					),
					'one-right-sidebar' => array(
						'label'   => esc_html__( 'Sidebar on right side', 'presentor' ),
						'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/page-layout-right-sidebar.svg',
					),
					'fullwidth' => array(
						'label'   => esc_html__( 'No sidebar', 'presentor' ),
						'img_src' => trailingslashit( PRESENTOR_THEME_URI ) . 'assets/images/admin/page-layout-fullwidth.svg',
					),
				),
			),
			'presentor_page_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Page type', 'presentor' ),
				'description'   => esc_html__( 'Page type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'presentor_header_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Header container type', 'presentor' ),
				'description'   => esc_html__( 'Header container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'presentor_breadcrumbs_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Breadcrumbs container type', 'presentor' ),
				'description'   => esc_html__( 'Breadcrumbs container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'presentor_content_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Content container type', 'presentor' ),
				'description'   => esc_html__( 'Content container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'presentor_footer_container_type'  => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Footer container type', 'presentor' ),
				'description'   => esc_html__( 'Footer container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'presentor_header_layout_type' => array(
				'type'    => 'select',
				'parent'  => 'header_tab',
				'title'   => esc_html__( 'Header Layout', 'presentor' ),
				'value'   => 'inherit',
				'options' => presentor_get_header_layout_pm_options(),
			),
			'presentor_header_transparent_layout' => array(
				'type'          => 'radio',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Header Transparent Overlay', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => array(
						'label' => esc_html__( 'Inherit', 'presentor' ),
					),
					'true'    => array(
						'label' => esc_html__( 'Enable', 'presentor' ),
						'slave' => 'header-transparent',
					),
					'false'   => array(
						'label' => esc_html__( 'Disable', 'presentor' ),
					),
				),
			),
			'presentor_header_transparent_bg' => array(
				'type'          => 'colorpicker',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Header Transparent Background', 'presentor' ),
				'value'         => '',
				'master'        => 'header-transparent',
				'display_input' => false,
			),
			'presentor_header_transparent_bg_alpha' => array(
				'type'          => 'slider',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Header Transparent Background Alpha', 'presentor' ),
				'max_value'     => 100,
				'min_value'     => -1,
				'step_value'    => 1,
				'master'        => 'header-transparent',
				'display_input' => false,
			),
			'presentor_header_invert_color_scheme' => array(
				'type'          => 'radio',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Invert Color Scheme', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => array(
						'label' => esc_html__( 'Inherit', 'presentor' ),
					),
					'true'    => array(
						'label' => esc_html__( 'Enable', 'presentor' ),
					),
					'false'   => array(
						'label' => esc_html__( 'Disable', 'presentor' ),
					),
				),
			),
			'presentor_top_panel_visibility' => array(
				'type'          => 'select',
				'parent'        => 'header_elements_tab',
				'title'         => esc_html__( 'Top panel', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'presentor' ),
					'true'    => esc_html__( 'Enable', 'presentor' ),
					'false'   => esc_html__( 'Disable', 'presentor' ),
				),
			),
			'presentor_header_contact_block_visibility' => array(
				'type'          => 'select',
				'parent'        => 'header_elements_tab',
				'title'         => esc_html__( 'Header Contact Block', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'presentor' ),
					'true'    => esc_html__( 'Enable', 'presentor' ),
					'false'   => esc_html__( 'Disable', 'presentor' ),
				),
			),
			'presentor_header_search' => array(
				'type'          => 'select',
				'parent'        => 'header_elements_tab',
				'title'         => esc_html__( 'Header Search', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'presentor' ),
					'true'    => esc_html__( 'Enable', 'presentor' ),
					'false'   => esc_html__( 'Disable', 'presentor' ),
				),
			),
			'presentor_breadcrumbs_visibillity' => array(
				'type'          => 'radio',
				'parent'        => 'breadcrumbs_tab',
				'title'         => esc_html__( 'Breadcrumbs visibillity', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => array(
						'label' => esc_html__( 'Inherit', 'presentor' ),
					),
					'true'    => array(
						'label' => esc_html__( 'Enable', 'presentor' ),
					),
					'false'   => array(
						'label' => esc_html__( 'Disable', 'presentor' ),
					),
				),
			),
			'presentor_footer_layout_type' => array(
				'type'    => 'select',
				'parent'  => 'footer_tab',
				'title'   => esc_html__( 'Footer Layout', 'presentor' ),
				'value'   => 'inherit',
				'options' => presentor_get_footer_layout_pm_options(),
			),
			'presentor_footer_widget_area_visibility' => array(
				'type'          => 'select',
				'parent'        => 'footer_tab',
				'title'         => esc_html__( 'Footer Widgets Area', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'presentor' ),
					'true'    => esc_html__( 'Enable', 'presentor' ),
					'false'   => esc_html__( 'Disable', 'presentor' ),
				),
			),
			'presentor_footer_contact_block_visibility' => array(
				'type'          => 'select',
				'parent'        => 'footer_tab',
				'title'         => esc_html__( 'Footer Contact Block', 'presentor' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'presentor' ),
					'true'    => esc_html__( 'Enable', 'presentor' ),
					'false'   => esc_html__( 'Disable', 'presentor' ),
				),
			),
		),
	) ) ;
}
