<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Presentor
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php presentor_ads_post_before_content() ?>

	<div class="post-featured-content"><?php
		do_action( 'cherry_post_format_link', array(
			'render' => true,
		) );
	?></div><!-- .post-featured-content -->

	<figure class="post-thumbnail"><?php
		presentor_get_template_part( 'template-parts/post/post-components/post-image' );
	?></figure><!-- .post-thumbnail -->

	<header class="entry-header">
		<div class="entry-meta"><?php
			presentor_get_template_part( 'template-parts/post/post-meta/content-meta-date' );
			presentor_get_template_part( 'template-parts/post/post-meta/content-meta-categories' );
			presentor_get_template_part( 'template-parts/post/post-meta/content-meta-comments' );
			presentor_get_template_part( 'template-parts/post/post-meta/content-meta-author' );
			presentor_get_template_part( 'template-parts/post/post-meta/content-meta-view' );
		?></div>
		<?php presentor_get_template_part( 'template-parts/post/post-components/post-title' ); ?>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links__title">' . esc_html__( 'Pages:', 'presentor' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span class="page-links__item">',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'presentor' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<div class="entry-footer-container"><?php
			presentor_get_template_part( 'template-parts/post/post-meta/content-meta-tags' );
			presentor_share_buttons( 'single' );
		?></div>
		<?php presentor_get_template_part( 'template-parts/post/post-meta/content-meta-rating' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
