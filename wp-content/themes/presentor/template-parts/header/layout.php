<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Presentor
 */

$search_visible = get_theme_mod( 'header_search', presentor_theme()->customizer->get_default( 'header_search' ) );
?>
<div <?php echo presentor_get_container_classes( array(), 'header' ) ?>>
	<div class="header-container_wrap">
		<div class="header-container__flex">
			<div class="site-branding">
				<?php presentor_header_logo() ?>
				<?php presentor_site_description(); ?>
			</div>

			<div class="header-nav-wrapper">
				<?php presentor_main_menu(); ?>

				<?php if ( $search_visible || has_action( 'presentor_header_woo_cart' ) ) : ?>
					<div class="header-components"><?php
						presentor_header_search_toggle();
						do_action( 'presentor_header_woo_cart' );
					?></div>
				<?php endif; ?>
				<?php presentor_header_search( '<div class="header-search">%s<button class="search-form__close"></button></div>' ); ?>
			</div>

			<?php presentor_header_btn(); ?>
		</div>
	</div>
</div>
