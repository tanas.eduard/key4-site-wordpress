<?php
/**
 * The template part for displaying page preloader.
 *
 * @package Presentor
 */
?>
<div class="page-preloader-cover">
	<?php presentor_preloader_logo(); ?>
	<div class="bar"></div>
</div>
