<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package Presentor
 */
?>

<div <?php presentor_footer_container_class(); ?>>
	<div <?php echo presentor_get_container_classes( array(), 'footer' ) ?>>
		<div class="site-info">
			<div class="site-info-wrap"><?php
				presentor_footer_logo();
				presentor_footer_copyright();
				presentor_footer_menu();
				presentor_contact_block( 'footer' );
				presentor_social_list( 'footer' );
			?></div>
		</div><!-- .site-info -->
	</div>
</div><!-- .footer-container -->
