<?php
/**
 * The template for displaying footer widget area.
 *
 * @package Presentor
 */
// Check footer-area visibility.
if ( ! get_theme_mod( 'footer_widget_area_visibility', presentor_theme()->customizer->get_default( 'footer_widget_area_visibility' ) ) || ! is_active_sidebar( 'footer-area' ) ) {
	return;
} ?>

<div <?php echo presentor_get_html_attr_class( array( 'footer-area-wrap' ), 'footer_widgets_bg' ); ?>>
	<div <?php echo presentor_get_container_classes( array(), 'footer' ) ?>>
		<?php do_action( 'presentor_render_widget_area', 'footer-area' ); ?>
	</div>
</div><!-- .footer-area-wrap -->
