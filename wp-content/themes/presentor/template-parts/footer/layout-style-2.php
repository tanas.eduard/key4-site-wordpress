<?php
/**
 * The template for displaying the style-2 footer layout.
 *
 * @package Presentor
 */
?>

<div <?php presentor_footer_container_class(); ?>>
	<div <?php echo presentor_get_container_classes( array(), 'footer' ) ?>>
		<div class="site-info"><?php
			presentor_footer_logo();
			presentor_footer_menu();
			presentor_contact_block( 'footer' );
			presentor_social_list( 'footer' );
			presentor_footer_copyright();
		?></div><!-- .site-info -->
	</div>
</div><!-- .footer-container -->
