<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Presentor
 */
?>


	</div><!-- #content -->

	<footer id="colophon" <?php presentor_footer_class() ?> role="contentinfo">
		<?php presentor_get_template_part( 'template-parts/footer/footer-area' ); ?>
		<?php presentor_get_template_part( 'template-parts/footer/layout', get_theme_mod( 'footer_layout_type', presentor_theme()->customizer->get_default( 'footer_layout_type' ) ) ); ?>
	</footer><!-- #colophon -->


</div><!-- #page -->

<style type="text/css">
.b24-widget-button-wrapper {
			     right: 30px;
			     bottom: 100px;
}

</style>

<?php wp_footer(); ?>
<!-- TODO -->
<script>
  var image = document.getElementsByTagName("img");
  var length = image.length;
  for(i = 0; i < length; i++)
{
    image[i].src = image[i].src.replace('.jpg', '.jpg.webp');
	console.log(image[i].src);
}
</script>
</body>
</html>
