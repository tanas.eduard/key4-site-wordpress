<?php
/**
 * The base template for displaying 404 pages (not found).
 *
 * @package Presentor
 */
?>
<?php get_header( presentor_template_base() ); ?>

	<div <?php presentor_content_wrap_class(); ?>>

		<div class="row">

			<div id="primary" <?php presentor_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include presentor_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_sidebar(); // Loads the sidebar.php. ?>

		</div><!-- .row -->

	</div><!-- .site-content_wrap -->

<?php get_footer( presentor_template_base() ); ?>
