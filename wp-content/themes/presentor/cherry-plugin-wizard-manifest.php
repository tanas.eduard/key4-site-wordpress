<?php
/**
 * TM Wizard configuration.
 *
 * @var array
 *
 * @package Presentor
 */
$plugins = array(
	'cherry-data-importer' => array(
		'name'   => esc_html__( 'Cherry Data Importer', 'presentor' ),
		'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
		'path'   => 'https://github.com/CherryFramework/cherry-data-importer/archive/master.zip',
		'access' => 'base',
	),
	'cherry-projects' => array(
		'name'   => esc_html__( 'Cherry Projects', 'presentor' ),
		'access' => 'skins',
	),
	'cherry-team-members' => array(
		'name'   => esc_html__( 'Cherry Team Members', 'presentor' ),
		'source' => 'local',
		'path'   => PRESENTOR_THEME_DIR . '/assets/includes/plugins/cherry-team-members.zip',
		'access' => 'skins',
	),
	'cherry-testi' => array(
		'name'   => esc_html__( 'Cherry Testimonials', 'presentor' ),
		'source' => 'local',
		'path'   => PRESENTOR_THEME_DIR . '/assets/includes/plugins/cherry-testi.zip',
		'access' => 'skins',
	),
	'cherry-services-list' => array(
		'name'   => esc_html__( 'Cherry Services List', 'presentor' ),
		'access' => 'skins',
	),
	'cherry-sidebars' => array(
		'name'   => esc_html__( 'Cherry Sidebars', 'presentor' ),
		'source' => 'local',
		'path'   => PRESENTOR_THEME_DIR . '/assets/includes/plugins/cherry-sidebars.zip',
		'access' => 'skins',
	),
	'cherry-socialize' => array(
		'name'   => esc_html__( 'Cherry Socialize', 'presentor' ),
		'access' => 'skins',
	),
	'cherry-search' => array(
		'name'   => esc_html__( 'Cherry Search', 'presentor' ),
		'access' => 'skins',
	),
	'cherry-popups' => array(
		'name'   => esc_html__( 'Cherry Popups', 'presentor' ),
		'access' => 'skins',
	),
	'cherry-trending-posts' => array(
		'name'   => esc_html__( 'Cherry Trending Posts', 'presentor' ),
		'access' => 'skins',
	),
	'wordpress-social-login' => array(
		'name'   => esc_html__( 'WordPress Social Login', 'presentor' ),
		'access' => 'skins',
	),
	'elementor' => array(
		'name'   => esc_html__( 'Elementor Page Builder', 'presentor' ),
		'access' => 'base',
	),
	'jet-menu' => array(
		'name'   => esc_html__( 'Jet Menu', 'presentor' ),
		'source' => 'local',
		'path'   => PRESENTOR_THEME_DIR . '/assets/includes/plugins/jet-menu.zip',
		'access' => 'base',
	),
	'jet-elements' => array(
		'name'   => esc_html__( 'Jet Elements', 'presentor' ),
		'source' => 'local',
		'path'   => PRESENTOR_THEME_DIR . '/assets/includes/plugins/jet-elements.zip',
		'access' => 'base',
	),
	'tm-photo-gallery' => array(
		'name'   => esc_html__( 'TM Photo Gallery', 'presentor' ),
		'access' => 'skins',
	),
	'tm-timeline' => array(
		'name'   => esc_html__( 'TM Timeline', 'presentor' ),
		'access' => 'skins',
	),
	'contact-form-7' => array(
		'name'   => esc_html__( 'Contact Form 7', 'presentor' ),
		'access' => 'skins',
	),
	'the-events-calendar' => array(
		'name'   => esc_html__( 'The Events Calendar', 'presentor' ),
		'access' => 'skins',
	),
	'woocommerce' => array(
		'name'   => esc_html__( 'Woocommerce', 'presentor' ),
		'access' => 'skins',
	),
	'tm-woocommerce-ajax-filters' => array(
		'name'   => esc_html__( 'TM Woocommerce Ajax Filters', 'presentor' ),
		'source' => 'local',
		'path'   => PRESENTOR_THEME_DIR . '/assets/includes/plugins/tm-woocommerce-ajax-filters.zip',
		'access' => 'skins',
	),
	'tm-woocommerce-quick-view' => array(
		'name'   => esc_html__( 'TM WooCommerce Quick View', 'presentor' ),
		'source' => 'remote',
		'path'   => 'https://github.com/CherryFramework/tm-woocommerce-quick-view/archive/master.zip',
		'access' => 'skins',
	),
	'tm-woocommerce-compare-wishlist' => array(
		'name'   => esc_html__( 'TM Woocommerce Compare Wishlist', 'presentor' ),
		'access' => 'skins',
	),
	'tm-woocommerce-package' => array(
		'name'   => esc_html__( 'TM Woocommerce Package', 'presentor' ),
		'access' => 'skins',
	),
	'woocommerce-social-media-share-buttons' => array(
		'name'   => esc_html__( 'Woocommerce Social Media Share Buttons', 'presentor' ),
		'access' => 'skins',
	),
	'tm-dashboard' => array(
		'name'   => esc_html__( 'Jetimpex Dashboard', 'presentor' ),
		'source' => 'remote',
		'path'   => 'http://cloud.cherryframework.com/downloads/free-plugins/tm-dashboard.zip',
		'access' => 'base',
	),
);

/**
 * Skins configuration example
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'elementor',
		'jet-elements',
		'jet-menu',
		'cherry-data-importer',
		'tm-dashboard',
	),
	'advanced' => array(
		'default' => array(
			'full'  => array(
				'cherry-projects',
				'cherry-team-members',
				'cherry-testi',
				'cherry-services-list',
				'cherry-sidebars',
				'cherry-socialize',
				'cherry-trending-posts',
				'cherry-search',
				'cherry-popups',
				'wordpress-social-login',
				'tm-photo-gallery',
				'tm-timeline',
				'contact-form-7',
				'the-events-calendar',
				'woocommerce',
				'tm-woocommerce-ajax-filters',
				'tm-woocommerce-quick-view',
				'tm-woocommerce-compare-wishlist',
				'tm-woocommerce-package',
				'woocommerce-social-media-share-buttons',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_presentor/presentor_v1/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/default/default.png',
			'name'  => esc_html__( 'Presentor', 'presentor' ),
		),
	),
);

$texts = array(
	'theme-name' => esc_html__( 'Presentor', 'presentor' ),
);
