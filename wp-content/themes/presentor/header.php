<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Presentor
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '777177499362110');
  fbq('track', 'PageView');  
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=777177499362110&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-55779856-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-55779856-1');
</script>
</head>

<body <?php body_class(); ?>>
<?php presentor_get_page_preloader(); ?>
<div id="page" <?php presentor_site_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'presentor' ); ?></a>
	<header id="masthead" <?php presentor_header_class(); ?> role="banner">
		<?php presentor_ads_header() ?>
		<?php presentor_get_template_part( 'template-parts/header/top-panel', get_theme_mod( 'header_layout_type', presentor_theme()->customizer->get_default( 'header_layout_type' ) ) ); ?>

		<div <?php presentor_header_container_class(); ?>>
			<?php presentor_get_template_part( 'template-parts/header/layout', get_theme_mod( 'header_layout_type', presentor_theme()->customizer->get_default( 'header_layout_type' ) ) ); ?>
		</div><!-- .header-container -->
	
	</header><!-- #masthead -->

	<div id="content" <?php presentor_content_class(); ?>>
